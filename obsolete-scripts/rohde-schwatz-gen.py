#!/usr/bin/env python3

from pprint import pprint
import socket
import time

# some constants of nature
c  = 299792458
fs = 100e6
n  = 2490000
common_alpha = -250302909023330.97

def to_freq(distance, alpha=None):
    '''rough conversion from distance values to freq values.

    Note that this takes a fixed value of the alpha sweep speed for
    granted. It might require correction if alpha varies too much.
    '''

    alpha = common_alpha if alpha is None else alpha
    idx_to_distance = c / 2 / (-alpha)
                                # around 24um / bin
    distance_to_freq = fs / (n * idx_to_distance)
                                # around 1.67 MHz/m

    return distance * distance_to_freq

def to_dist(freq, alpha=None):
    '''rough conversion from freq values to distance values.

    Note that this takes a fixed value of the alpha sweep speed for
    granted. It might require correction if alpha varies too much.
    '''

    distance_to_freq = to_freq(1)
    return freq / distance_to_freq

# list of test parameters
class RadTestParams:
    def __init__(self, freq, modulation, depth, amp, comment=None):

        self.freq            = freq
        self.modulation      = modulation
        self.depth           = depth
        self.amp             = amp
        self.comment         = comment

    def __repr__(self):
        return (f'<RadTestParams freq={self.freq/1e6:.3f}MHz, '
                f'mod={self.modulation}Hz, depth={self.depth}%, '
                f'amp={self.amp} dBm, ({self.comment})>')
    def params(self):
        return dict((f'rt_{k}', getattr(self, k, None)) for k in self.__dict__)

MHz = 1e6
rad_tests = [
    # tests as perpetrated in July 2023; probably useless
    # i            freq           modulation   depth  ampl   comment
    RadTestParams( 100   * MHz,      1000,     80,    -12,   '',                  ),
    RadTestParams( 100   * MHz,   1000000,     80,    -12,   '',                  ),
    RadTestParams( 100   * MHz,    134819,     80,    -12,   '(0.08m distance)',  ),
    RadTestParams( 100   * MHz,    134819,     80,    -12,   '(0.08m distance)',  ),
    RadTestParams(  50   * MHz,      1000,     80,    -12,   '',                  ),
    RadTestParams(  49   * MHz,    134819,     80,    -12,   '',                  ),
    RadTestParams( 101   * MHz,    134819,     80,    -12,   '',                  ),
    RadTestParams( 100.1 * MHz,    134819,     80,    -12,   '',                  ),
    RadTestParams(  99.9 * MHz,    134819,     80,    -12,   '',                  ),
    RadTestParams( 100   * MHz,    138192,     80,    -12,   '(HLS distance)',    ),
    RadTestParams( 100   * MHz,  24867911,     80,    -12,   '(fiber distance)',  ),
    RadTestParams( 100   * MHz,    197590,     80,    -12,   '(ball distance',    ),
    RadTestParams( 100   * MHz,     50562,     80,    -12,   '(0.03m dist)',      ),
    RadTestParams( 100   * MHz,     67429,     80,    -12,   '(0.04m distance)',  ),
    RadTestParams( 100   * MHz,     84286,     80,    -12,   '(0.05m distance)',  ),
    RadTestParams( 100   * MHz,    101144,     80,    -12,   '(0.06m distance)',  ),
    RadTestParams( 100   * MHz,    118000,     80,    -12,   '(0.07m distance)',  ),
    RadTestParams( 100   * MHz,    134859,     80,    -12,   '(0.08m distance)',  ),
    RadTestParams( 100   * MHz,    168572,     80,    -12,   '(0.10m distance)',  ),
    RadTestParams( 100   * MHz,    337145,     80,    -12,   '(0.20m distance)',  ),
    RadTestParams( 100   * MHz,    505718,     80,    -12,   '(0.30m distance)',  ),
    RadTestParams( 100   * MHz,    674291,     80,    -12,   '(0.40m distance)',  ),
    RadTestParams( 100   * MHz,    842864,     80,    -12,   '(0.50m distance)',  ),
]

def send_cmd(s, string):
    '''send a command for the R&S signal generator to socket s.'''
    cmd = bytes(f'{string}\r\n', 'ascii')
    s.send(cmd)
    time.sleep(0.1)

def read_answer(s):
    try:
        ret = s.recv(1024, socket.MSG_DONTWAIT)
    except BlockingIOError as e:
        ret = b''
    return ret

def setup_rad_test(freq, modulation, depth, amp,
                        host='cfb-864-allrfgen2', port=5025):
    '''send the parameters for a single rad test.

    freq        RF frequency in Hz
    modulation  LF/AM frequency in Hz
    depth       depth in percent units (e.g. 80)
    amp         signal amplitude/power in dBm (e.g. -12)
    '''

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port,))

        send_cmd(s, '*rst')
        send_cmd(s, f'freq {freq} Hz')
        
        send_cmd(s, 'am:source int')
        send_cmd(s, f'am:dept {depth} pct')
        send_cmd(s, 'am:state on')
        
        send_cmd(s, 'lfo:freq:mode fix')
        send_cmd(s, f'lfo:freq {modulation}')
        send_cmd(s, 'lfo on')

        send_cmd(s, f'pow {amp} dBm') 
        
        send_cmd(s, 'mod:stat on')
        send_cmd(s, 'outp on')

def get_rad_test_params(host='cfb-864-allrfgen2', port=5025):
    '''obtain the parameters for a single rad test.'''

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port,))

        send_cmd(s, 'freq?')
        send_cmd(s, 'am:source?')
        send_cmd(s, 'am:depth?')
        send_cmd(s, 'am:state?')
        
        send_cmd(s, 'lfo:freq:mode?')
        send_cmd(s, 'lfo:freq?')
        send_cmd(s, 'lfo?')

        send_cmd(s, f'pow?') 
        
        send_cmd(s, 'mod:stat?')
        send_cmd(s, 'outp?')

        ret = str(read_answer(s), 'ascii').split('\n')
        fmt = '\n'.join([
                f'freq:           {ret[0]} Hz',
                f'am source:      {ret[1]}',
                f'am depth:       {ret[2]}%',
                f'am state:       {ret[3]}',
                f'lfo freq mode:  {ret[4]}',
                f'lfo freq:       {ret[5]} Hz',
                f'lfo:            {ret[6]}',
                f'pow:            {ret[7]} dBm',
                f'mod state on:   {ret[8]}',
                f'output on:      {ret[9]}',
                f'' ])
        return fmt

def cmdloop(host='cfb-864-allrfgen3', port=5025):
    '''run a dumb command loop to test commands from the manual.'''

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port,))
        while True:
            scpi = input('cmd> ')
            if scpi == '':
                return
            print(f'executing {scpi}')
            s.send(bytes(f'{scpi}\r\n', 'ascii'))
            print(s.recv(100))

def emc_test_series(start=100e3, stop=10e6, ratio=1.01,
                        mod_freq=1e3, mod_depth=80, amp=-12,
                        output='list'):

    '''generate a series of test parameters sweeping freqs.
        start			- initial frequency of sweep range
        stop			- final   frequency of sweep range
        ratio			- ratio between next and prev freqs
        mod_freq		- AM modulation frequency
        mod_depth		- AM modulation depth
        amp				- amplitud/power of signal
        output			- default 'list' gives a list of RadTestParams;
                          if output='csv' return a string in csv format
    '''

    series = []
    freq = start
    while freq <= stop:
        series.append(RadTestParams(int(freq), mod_freq, mod_depth, amp,
                            comment=f'distance = {to_dist(freq):.3f}'))
        freq *= ratio
    if output == 'list':
        return series
    elif output == 'csv':
        import io
        import csv

        with io.StringIO('') as o:
            fieldnames = [ f'rt_{f}' for f in [
                        'freq', 'modulation', 'depth', 'amp', 'comment', ] ]
            c = csv.DictWriter(o, fieldnames=fieldnames)
            c.writeheader()
            for t in emc_test_series(start, stop, ratio, mod_freq, mod_depth, amp):
                c.writerow(t.params())
            content = o.getvalue()
        return content

def main():
    print(emc_test_series(start=100e3, stop=10e6, ratio=1.01, output='csv'))

if __name__ == '__main__':
    main()
