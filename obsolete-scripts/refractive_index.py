import ciddor
import easylog80cl

e = easylog80cl.EasyLog80CL('/dev/ttyUSB4')

wl = 1550 / 1e3 # wavelength in microns
t = e.temperature       # celsius temperature
p = e.pressure * 100    # pressure in Pa
h = e.humidity / 100    # fractional humidity
co2 = 450               # ppm, out of my hat
index = ciddor.n(wl, t, p, h, co2)

print(index)
