#!/usr/bin/env python3

import Pyro5.server
import fsi.laser.TLM8700 as tlm


# @Pyro5.server.expose
# class Laser(tlm.Tlm8700Wrapper):
#     def __init__(self):
#         tlm.Tlm8700Wrapper.__init__(self, '/dev/serial/by-id/laser')

LaserExposed = Pyro5.server.expose(tlm.Tlm8700Wrapper)
daemon = Pyro5.server.Daemon(host='cs-774-fsigpu', port=32000)
uri = daemon.register(LaserExposed('/dev/serial/by-id/laser'), objectId='gpu_laser')
print(uri)
daemon.requestLoop()
