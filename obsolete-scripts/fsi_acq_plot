#!/usr/bin/env python3 
# vim: ts=8 et sw=4 sts=4 ai

description=('obsolete plotting script, mostly useful'
    'to display old-style fsi_acq files.')

import sys
import os
import re
import pdb
import glob
import argparse

import numpy as np
import matplotlib
from matplotlib import pyplot as plt

# configure matplotlib style from horrendous defaults
matplotlib.use('tkagg')
matplotlib.rcParams['figure.figsize'] = [12.0, 8.0]
matplotlib.rcParams['figure.autolayout'] = True
matplotlib.rcParams['lines.linewidth'] = 0.2

# scan arguments
parser = argparse.ArgumentParser(description=description)
parser.add_argument('filelist', nargs='*',
                        help='file list to display, empty or "last" '
                             'will use the most recent acquisition set')
parser.add_argument('-c', '--channel', action='append', help='plot only these channel(s)')
parser.add_argument('-b', '--board', action='append', help='plot only these board(s)')
parser.add_argument('-s', '--spectrum', action='store_true', help='plot spectrum of data')
parser.add_argument('-l', '--log', action='store_true', help='plot spectrum of data, log scale')
args = parser.parse_args()

# determine list of files to plot: if not explicit,
# use the last timestamp. Then, if board(s) or channel(s)
# are given, keep the matching files only
regex = re.compile(
    r'(.*/)?fsi_acq_(?P<timestamp>[0-9]*T[0-9]*)-b(?P<board>[0-9]*)-ch(?P<channel>[0-9]*).dat')
files = args.filelist

if not files or files == ['last']:
    glb = 'fsi_acq_*-b*-ch*.dat'
    files = glob.glob(glb) 
    files = [regex.match(f) for f in files if regex.match(f)]
    if not files:
        print(f'no fsi_acq_* files in directory')
        sys.exit()
    timestamps = [ f.group('timestamp') for f in files]
    last = max(timestamps)
    files = [f for f in files if f.group('timestamp') == last]
else:
    files = [regex.match(f) for f in files if regex.match(f)]

chs = args.channel
if chs:
    files = [ f for f in files if f.group('channel') in chs ]
boards = args.board
if boards:
    files = [ f for f in files if f.group('board') in boards ]

if not files:
    print(f'no files to plot, exiting')
    sys.exit()

files = sorted([f.string for f in files])

# plot in nice array if more than 4 files
n = len(files)
rows = n
cols = 1
if n >= 4:
    rows = 4
    cols = (n - 1) // rows + 1
print(f'plotting in {rows} rows, {cols} columns')

fs = 100e6
fig, ax = plt.subplots(rows, cols, squeeze=False)
i = 0; j = 0
for f in files:
    with open(f) as o:
        [o.readline() for i in range(5)]
        v = np.fromstring(o.read(), dtype=np.int16, sep=' ')
        n = v.size
        if args.spectrum or args.log:
            v = np.abs(np.fft.rfft(v))
            v[0] = 0        # do not plot DC
            xax = np.fft.rfftfreq(n) * fs / 1e6
            ax[i, j].set_xlabel('Frequency [MHz]')
        else:
            xax = np.arange(0, n)
            ax[i, j].set_xlabel('Time [samples]')
    print(i, j)
    ax[i, j].set_title(f)
    if args.log:
        ax[i, j].set_yscale('log')
    ax[i, j].plot(xax, v, lw=0.2)

    i = i + 1
    if i >= rows:
        i = 0
        j = j + 1

plt.show()
