#!/usr/bin/env python3
# vim: ts=8 sw=4 sts=4 et ai

import re
import glob

import numpy as np
import h5py

def lipread(fname):
    '''read a numarray vector from fname.'''
    with open(fname) as o:
        lines = o.read().split('\n')
        start = 5 if lines[0][0] == '#' else 0
        stop = -1 if lines[-1] == '' else len(lines)
        v = np.array([int(i) for i in lines[start:stop]])
        return v

fpat = re.compile(r'((?P<dir>.*/))?fsi_acq_'
    r'(?P<ts>\d{8}T\d{6})-b(?P<board>\d+)-ch(?P<ch>[a-fA-F\d+]).dat')

def lip2h5(fnames, h5fname):
    '''archive files with fnames in h5 file h5fname.'''
    with h5py.File(h5fname, 'a') as f:
        for fname in fnames:
            v = lipread(fname)
            n = len(v)
            m = fpat.match(fname).groupdict()
            ts, board, ch = m['ts'], int(m['board'], 0), int(m['ch'], 0)
            dsname = f'/{ts}/{board:03d}/{ch:03d}'
            ds = f.require_dataset(dsname, chunks=True, compression='gzip',
                        dtype=np.uint16, shape=(len(v),), data = v)
            ds.attrs['n'] = n
            ds.attrs['timestamp'] = ts
            ds.attrs['board'] = board
            ds.attrs['channel'] = ch
            ds.attrs['fs'] = 100000000
            ds.attrs['filename'] = fname

def last(pattern='*'):
    '''find last timestamped file set in glob pattern.'''

    matches = [fpat.match(f) for f in glob.glob(pattern)]
    timestamps = [m.group('ts') for m in matches if m is not None]
    return max(timestamps)

def split(h5path):
    '''split an h5fname/pathname into (h5fname, pathname).'''
    p = h5path
    ds = ''
    while not os.path.isfile(p):
        p, b = os.path.split(p)
        ds = os.path.join(b, ds)

    return (p, ds)

def decode_url(url):
    '''
    Examples:
    h5:/home/dcobas/fsi/fsi-sw/hls-20220606T230043.hdf5/timestamp/20220614T191221/{g7.0,r7.3,m5.3}
    h5:/home/dcobas/fsi/fsi-sw/hls-20220606T230043.hdf5/timestamp/{t20220614T191221,g7.0,r7.3,m5.3}
    fsi_acq:./t6-20220614/t6-20220614T190433/fsi_acq_20220614T190909-{g7.0,r7.3,m5.3}.dat
    fsi_acq:./t6-20220614/t6-20220614T190433/{t20220614T190909,g7.0,r7.3,m5.3}.dat
    fsi_acq:./t6-20220614/{st6-20220614T190433,t*,g7.0,r7.3,m5.3}.dat ????
    '''
    pass

if __name__ == '__main__':

    last_ts = last()
    files = glob.glob(f'fsi_acq_{last_ts}*.dat')
    out = f'cap-{last_ts}.hdf5'
    lip2h5(files, out)
    print('saved ')
    print('\n'.join(files))
    print(f'as HDF5 file {out}')
