#!/usr/bin/env python3
# vim: ts=8 sw=4 et sts=4 ai
# -*- coding: utf-8 -*-

import socket
import os, os.path
import time
from collections import deque    

socket_fname = "/tmp/.fsi_socket.s"

server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
server.connect(socket_fname)
while True:
    txt = input('rollo: ')
    if txt == 'quit':
        break
    server.send(bytes(txt, encoding='utf8'))
server.close()

