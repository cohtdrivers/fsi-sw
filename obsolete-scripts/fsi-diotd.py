#!/usr/bin/env python3

import os
from ctypes import *
import Pyro5.server

# FIXME: all this is configuration-dependent
fsidir = '/home/root/fsi'
default_libpath = os.path.join(fsidir, 'libfsidiot.so')
base_addr = 0xb0000000      # FIXME: config
diot_hostname = 'diot-sb-fsi-isr.dyndns.cern.ch'
diot_service_port = 4510

class FsiDiot:

    def __init__(self, libfsipath=None):
        self.libfsipath = default_libpath if libfsipath is None else libfsipath
        self.lib = CDLL(self.libfsipath)
        self.lib.axi_map.restype = c_void_p
        self._vaddr = self.lib.axi_map(base_addr)

    def acdc(self, board, dc):
        self.lib.fsi_set_acdc(board, dc)

    def gain(self, board, mask):
        self.lib.fsi_set_afe(board, mask)

    def resync(self):
        self.lib.fsi_resync()

    def __del__(self):
        self.lib.axi_unmap(self._vaddr)

if __name__ == '__main__':

    FsiDiotExposed = Pyro5.server.expose(FsiDiot)
    daemon = Pyro5.server.Daemon(host=diot_hostname,
    port=diot_service_port)
    uri = daemon.register(FsiDiotExposed(), objectId='fsi_diot')
    print(uri)
    daemon.requestLoop()
