#!/usr/bin/env python3
# vim: ts=8 sw=4 et sts=4 ai
# -*- coding: utf-8 -*-

import socket
import os, os.path
import time

socket_fname = "/tmp/.fsi_socket.s"
if os.path.exists(socket_fname):
	os.remove(socket_fname)    

server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
server.bind(socket_fname)
print('bound')
server.listen()
print('now listening')
while True:
    conn, addr = server.accept()
    print('now connected')
    while True:
        datagram = conn.recv(1024)
        if datagram:
            tokens = datagram.strip().split()
            print(tokens)
        else:
            break
conn.close()

