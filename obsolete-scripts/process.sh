RES_DIR=results
PYTHON_INTERP=python3.7		# Mamta's default
FILE_PATH='.'
SHOW_PLOT=

[ ! -d "$RES_DIR" ] && mkdir -p "$RES_DIR"

while getopts i:p:t:r:g:m:sh flag
do
    case "${flag}" in
        p) FILE_PATH=${OPTARG};;
        t) TIMESTAMP=${OPTARG};;
        r) REF_CH=${OPTARG};;
        g) GAS_CH=${OPTARG};;
	m) MEAS_CH=${OPTARG};;
	s) SHOW_PLOT=-s ;;
	i) PYTHON_INTERP=${OPTARG};;
        \?) echo Usage: $0 -p {File Path} -t {timestamp} -r {Reference Channel} -g {Gas Channel} -m {Meas Channel} [-s];
           echo -h Prints this help message;
           exit 1;;
	h) echo Usage: $0 -p {File Path} -t {timestamp} -r {Reference Channel} -g {Gas Channel} -m {Meas  Channel} [-s];
	   exit 1;;
    esac
done

CUDA_VISIBLE_DEVICE=0, ${PYTHON_INTERP} process.py \
	-p $FILE_PATH  $SHOW_PLOT \
	-t $TIMESTAMP -r $REF_CH -g $GAS_CH -m $MEAS_CH
