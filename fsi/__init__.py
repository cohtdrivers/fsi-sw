#!/usr/bin/env python3
# vim: ts=8 sw=4 sts=4 et ai

import sys
import os
import logging
import logging.handlers

from ctypes import *

# configure logging here, before anything
# else happens. Settle for DEBUG, but only
# for ifm messages, not imported modules
logging.basicConfig(level=logging.DEBUG,
    format='%(asctime)s %(message)s',
    handlers=[
        logging.StreamHandler(),
	logging.handlers.RotatingFileHandler(f'fsi-{os.getpid()}.log'), ])
                                        # FIXME: log file path should be
                                        # ini- or argparse-configurable
                                        # or by environ
for module in ( 'matplotlib', 'fabric', 'PIL', 'paramiko', 'invoke', ):
    logging.getLogger(module).setLevel(logging.WARNING)
logging.getLogger('fsi').setLevel(logging.DEBUG)

git_version_path = os.path.join(os.path.dirname(__file__), '.version')
git_version = open(git_version_path, 'r').read().strip()

# paths to solibs used by fsi package
fsi_dir = os.path.dirname(__file__)
default_memolib_path = os.path.join(fsi_dir, 'memo.so')
libfsi_solib = os.path.join(fsi_dir, 'libfsi.so')

# very basic global functions
def library_path():
    return fsi_dir

def to_channel_id(oblock, board, channel):
    return (oblock<<6) | ((board-1)<<3) | channel

def to_obc(channel_id):
    oblock  =  (channel_id>>6) & 0x3
    board   = ((channel_id>>3) & 0x7) + 1
    channel =   channel_id     & 0x7
    return (oblock, board, channel)


try:
    librt = CDLL('librt.so', mode=RTLD_GLOBAL)
    libfsi = CDLL(libfsi_solib, mode=RTLD_GLOBAL)
except OSError as e:
    logging.error(f'fatal: cannot load solib {libfsi_solib}')
    sys.exit()

