#!/usr/bin/env python3
# vim: ts=8 sw=4 et sts=4 ai

import configparser
from ctypes import *
import sqlite3
import sys
import string
import os
import time
import socket
import logging
import itertools
from multiprocessing.shared_memory import SharedMemory
from inspect import currentframe

import numpy as np
import scipy.signal
import scipy.io
import fabric
import jinja2
import lmfit

import fsi
from fsi import to_channel_id, to_obc
import fsi.laser.TLM8700 as TLM8700
from fsi.laser.edfa import KeopsysEDFA, FailedPowerConfig
import fsi.easylog80cl
import fsi.ciddor
import fsi.linearize as linearize
import fsi.alphafit as alphafit
import fsi.models as models
import fsi.fsitypes as fsitypes
from fsi.channel import Channel, BadChannelId

class DiotDown(Exception):
    '''signals a DI/OT crate unresponsive to ssh.'''
    pass
class LostPackets(Exception):
    pass
class InconsistentChannelMetadata(Exception):
    pass
class ConnectionLost(Exception):
    '''signals a lost connection in comm socket.'''
    pass
class OutOfSync(Exception):
    '''signals a loss of synchronism between libfsi and fsi_server.'''
    pass
class BadStderr(Exception):
    '''signals a None value for fit stderr.'''
    pass


# FIXME: from memo.h, DRY!
FSI_ERR_BASE		        = fsitypes.FSI_ERR_BASE
FSI_SOCKET_ERROR		= fsitypes.FSI_SOCKET_ERROR
FSI_SOCKET_BUF_ERROR		= fsitypes.FSI_SOCKET_BUF_ERROR
FSI_SOCKET_RECVFROM_TIMEOUT	= fsitypes.FSI_SOCKET_RECVFROM_TIMEOUT
FSI_SOCKET_READV_TIMEOUT	= fsitypes.FSI_SOCKET_READV_TIMEOUT
FSI_SOCKET_CFGTIMEO_ERROR	= fsitypes.FSI_SOCKET_CFGTIMEO_ERROR
FSI_SOCKET_LISTEN_ERROR		= fsitypes.FSI_SOCKET_LISTEN_ERROR
FSI_SOCKET_ACCEPT_ERROR		= fsitypes.FSI_SOCKET_ACCEPT_ERROR
FSI_SOCKET_NOCONN		= fsitypes.FSI_SOCKET_NOCONN

# FIXME: from fsi.h
FSI_CH_DISTANCE     = fsitypes.FSI_CH_DISTANCE
FSI_UNIT_M          = fsitypes.FSI_UNIT_M
FSI_UNIT_NONE       = fsitypes.FSI_UNIT_NONE

# FIXME: configurables
fsi_shm = 'fsi'
fsi_shm_size = 16384 * 2**20        # 16GB
diot_hostname = 'cdc-947-gfsisct'
server_socket = str(fsitypes.FSI_SERVER_SOCKET, 'ascii')

def mini_snr(y):
    '''estimate SNR (in dB) of window around peak.

    This mostly a heuristic computation that gives back some substantial
    value when a peak is well defined, and gives a low, close-to-zero dB
    value when given noise. Not to be taken as an extremely accurate estimator.

    It is assumed that x and y have a meaningful length, around 200.
    '''

    summit = max(y)
    perc = np.percentile(y, 20)
    lowz = y[y < perc]
    if len(lowz) <= 0.05 * len(y):  # stupid check for empty lowz...
        return 0
    noise = np.linalg.norm(lowz) / np.sqrt(len(lowz))
    return 10 * np.log10(summit / noise)

class Diot:

    C_UDP = 0
    C_TCP = 1

    def __init__(self, hostname=None,
                memolib_path=fsi.default_memolib_path):
        try:
            self.memo = CDLL(memolib_path)
        except OSError as e:
            logging.error(f'cannot load solib {memolib_path}, error [{e}]')
            sys.exit()
        if hostname is None:
            self.hostname = diot_hostname
        else:
            self.hostname = hostname
        self.proto = self.C_TCP
        self.sockfd = c_int(0)
        # this horrendous kludge is needed thanks to the absurd way
        # memo.fsi_connect is conceived. We should do better than this,
        # i.e., ensure a full-duplex TCP stream in a more robust way

        self.connect()
        return

    def connect(self):
        'open data link with diot crate.'

        logging.info('starting diot fsi_tx program')
        try:
            self.ssh = fabric.connection.Connection(self.hostname)
            res = self.ssh.open()
            self.ssh.run('sleep 2; ./restart_fsi_tx', disown=True)
        except Exception as e:
            logging.error(f'cannot ssh into diot host {self.hostname} with error [{e}], leaving')
            sys.exit()
        self.fd = self.memo.fsi_connect(byref(self.sockfd), self.proto)
        if self.fd < 0:
            logging.error(f'cannot create data link with diot host {self.hostname}, error {self.fd}, leaving')
            sys.exit()
        # FIXME: open control link for diot if necessary
        # FIXME: launch control app in diot if necessary

    def __del__(self):
        'disconnect from socket when dying.'
        if hasattr(self, 'fd'):
            self.memo.fsi_disconnect(self.fd)

    def acquire(self):
        # call the memo.c program to acquire
        record = (64*FsiChanAcqHeader)()
        perf = (20*c_long)()
        nchans = self.memo.fsi_receive_acquisition(0xf0, 0xff, byref(record),
                    self.proto, self.fd, byref(perf))
        if nchans < 0:
            logging.error(f'timeout: error {-nchans}')
            return nchans, record
        elif nchans == 0:
            # FIXME: check: this is never reached
            logging.error(f'no connection!')
            return nchans, record
        else:
            self.channels = record[:nchans]
        return nchans, record

    def close(self):
        self.memo.fsi_disconnect(self.sockfd)

    def reconnect(self):
        '''close connection and connect again.'''
        self.close()
        time.sleep(2.0)
        self.connect()

    def fancy_show(self):
        ret = []
        ret.append(f'nchans = {len(a.channels)}')
        for ch in a.channels:
            ret.append(f'    0.{ch.board}.{ch.channel}:  {ch.data[:10]}')
        ret.append('\n')
        return '\n'.join(ret)

    def probe(self, port=22, diot_link_probe_timeout=0.400):
        '''probe diot host and return True if it is provably up.'''
        host = self.hostname
        try:
            with socket.socket(socket.AF_INET, type=socket.SOCK_STREAM) as s:
                s.settimeout(diot_link_probe_timeout)
                s.connect((host, port))
                s.close()
                return True
        except TimeoutError as e:
            return False
        except IOError as e:
            return False

# configuration values

class FsiConfig:

    def __init__(self, config_file='fsi.ini'):

        # parse config file, force case-sensitive options
        cp = self.cp = configparser.ConfigParser()
        cp.optionxform = lambda option : option
        cp.read_file(open(config_file))

        ports = cp['ports']
        self.laser_serial = ports['laser_serial']
        self.edfa_serial =  ports['edfa_serial']
        self.weather_port = ports['weather_port']

        # obtain optional diot hostname
        self.diot_hostname = cp.get('diot', 'diot_hostname', fallback=None)

        laser = cp['laser']
        self.setpoint = laser.getfloat('edfa_setpoint') # dBm

        acquisition = cp['acquisition']
        self.batch = acquisition.get('batch', None)
        self.fs = acquisition.getfloat('fs')
        self.n = acquisition.getint('n')
        self.gas_channel_label = acquisition['gas_channel']
        self.gas_channel_backup_label = acquisition['gas_channel_backup']
        self.reference_channel_label = acquisition['reference_channel']
        self.reference_channel_labels = acquisition['reference_channels'].split()

        channel_map = dict(cp['channel map'])
        self.channel_map = channel_map
        self.gas_channel = channel_map[self.gas_channel_label]
        self.gas_channel_backup = channel_map[self.gas_channel_backup_label]
        self.reference_channel = channel_map[self.reference_channel_label]
        # FIXME: create self.reference_channel_labels array for 4 diots

        hints = cp['hints']
        self.hints = dict((chlabel, list(enumerate(map(float, values.split()))))
                                for chlabel, values in hints.items())

        process = cp['process']
        zpf = 1
        zpf = process.getint('zpf') if 'zpf' in process else zpf
        zpf = ( process.getint('zero_padding_factor')
                             if 'zero_padding_factor' in process
                             else zpf )
        self.zpf = zpf

        store = cp['store']
        self.save_raw_matlab = store.getboolean('save_raw_matlab')
        self.save_spectra_matlab = store.getboolean('save_spectra_matlab')
        self.save_directory = store.get('save_directory')

    def cdecls(self):
        '''generate C declarations for channel configs.'''

        env = jinja2.Environment(loader=jinja2.FileSystemLoader('/'),
            trim_blocks=True, lstrip_blocks=True)
        template_file = os.path.join(os.path.dirname(fsi.__file__), 'cdecls.jinja')
        template = env.get_template(template_file)

        return template.render({
            'channel_map' :   self.channel_map,
            'hints' :         self.hints,
            'to_channel_id' : to_channel_id,
            'gas' :           self.gas_channel,
            'gas_backup' :    self.gas_channel_backup,
            'ref' :           self.reference_channel,
            })

    def listing(self):
        '''produce human-readable form of configuration.'''

        ret = [
            'laser: {}'.format(self.laser_serial),
            'edfa:  {}'.format(self.edfa_serial),
            'diot:  {}'.format(self.diot_hostname),
            '',
            'batch: {}'.format(self.batch),
            'n:     {}'.format(self.n),
            'fs:    {:.0f} Hz'.format(self.fs),
            '',
        ]

        for label in self.channel_map:
            addr = self.channel_map[label]
            if addr == self.gas_channel:
                typ = 'gc1'
            elif addr == self.gas_channel_backup:
                typ = 'gc2'
            elif addr == self.reference_channel:
                typ = 'ref'
            else:
                typ = 'dis'
            board, channel = addr.split('.')
            board, channel = int(board), int(channel)
            diot = 0        # FIXME: this will be 0..3
            channel_id = to_channel_id(diot, board, channel)
            if label in self.hints:
                hint_display = ''.join('p{:d}:{:6.3f}'.format(peak_id, distance)
                        for peak_id, distance in self.hints[label])
            else:
                hint_display = '     ----    '
            ret.append(
                '{:10}: {:3s} {:4d} = {:1d}.{:1d}.{:1d}     {}'.format(
                        label, typ, channel_id, diot, board, channel, hint_display))
        ret.append('\n')
        return '\n'.join(ret)

def channel_common_metadata(channels):
    '''produces the common (n, timestamp) of all channels.'''
    s = set((ch.n, ch.timestamp,) for ch in channels)
    bc = set((ch.board, ch.channel) for ch in channels)
    if len(bc) != len(channels):
        raise InconsistentChannelMetadata(f'non-unique (board, ch) metadata: {bc}')
    if len(s) != 1:
        raise InconsistentChannelMetadata(f'non-unique (n, fs) metadata: {s}')
    n, timestamp = s.pop()
    return n, timestamp

class FsiChanAcqHeader(Structure):
    _fields_ = [
        ("bom",            c_uint32),
        ("board",          c_uint32),
        ("channel",        c_uint32),
        ("timestamp",      16*c_char),
        ("n",              c_uint32),
        ("data",           POINTER(c_uint16)),
        ("reserved",       8*c_uint32),
    ]

    def __repr__(self):
        ts = str(self.timestamp, 'ascii')
        block = 0        # we have only one oblock now
        return f'{ts}:{block}:{self.board}:{self.channel}[{self.n}]'

class Peak:
    def __init__(self, hint, hwidth1, hwidth2=None):
        self.hint = hint
        if hwidth2 is None:
            hwidth2 = hwidth1
        self.hwidth1 = hwidth1
        self.hwidth2 = hwidth2

    def repr(self):
        return ( f'{self.mu:12.6f}m +/- {1e6*self.sigma:5.1f}μm'
                        f'(from max:{self.max:12.7f}m)')

class Server:
    """representation of an FSI interferometer apparatus/server.

    An interferometer controls:
        * a laser unit
        * nblocks oblocks (optical blocks), in which we have
            - an EDFA
            - a 2ch power meter
            - a chassis ambient thermometer
            - 64 optical channels
        * nblocks DIOT chassis corresponding 1:1 with the oblocks,
          containing each
            - 64 channels enumerated d:b:ch (diot/board/channel) in the
              ranges 0-3:1-8:0-7; this translates into a byte channel_id
              whose value is the bit pattern ddbbbccc, where bbb is the
              board# - 1
            - a data link to transfer the acquired data
            - a control link (socket) to configure and master the DIOT
              behaviour from the server
        * up to nblocks * 64 channels as per the above
        * a configuration that defines mostly static parameters of the
          interferometer (mostly defaulted to the fsi.ini config
          file from which a Server object gets initialized)
        * some constants of nature that define the ifm behaviour like
            - n (sample size)
            - fs (sampling frequency)
            - laser params / edfa params
            - two channel positions reserved for gas cell channels,
              and nblocks channel positions, one for each oblock,
              reserved for the reference channel of the oblock
        * a software interlock that switches power off under a software
          condition to be specified
    """

    def __init__(self, config='fsi.ini', batch=None, synchronized=None,
                    diot=None, noscan=None):
        '''fsi engine initialization.

        A starting server must ensure that a number of
        entities/conditions are in place to consider it set up

            * configuration (mainly constants of nature, e.g. serial ports)
            * a connected link to the DIOT for data transfer -> start fsi_tx
            * [notimp] ditto for control/config -> start diot_ctrl
            * the large shm are to communicate with libfsi
            * the libfsi ctrl link (socket) itself
            * a channel config/list/map from the ini file
        '''

        self.synchronized = synchronized
        self.noscan = noscan

        try:
            cfg = FsiConfig(config)
        except FileNotFoundError as e:
            logging.error(f'cannot find configuration file: {e}')
            sys.exit(1)
        logging.info(f'configuration read from {config}')

        self.cfg = cfg
        self.fs  = cfg.fs
        self.n   = cfg.n
        self.zpf = cfg.zpf
        self.batch = batch

        self.cfg.diot_hostname = diot or self.cfg.diot_hostname
        if not self.cfg.diot_hostname.endswith('.mat'):
            self.diot = [ Diot(hostname=self.cfg.diot_hostname) ]
            logging.info(f'connected to diot {self.cfg.diot_hostname}, '
                            f'fd = {self.diot[0].fd}')

        # the default name "fsi" for the shmem area can be overriden
        # via the environment variable FSI_SERVER_SHMEM
        default_shm_name = str(fsitypes.FSI_SERVER_DEFAULT_SHM, 'ascii')
        shm_env = str(fsitypes.FSI_SERVER_SHM_ENVIRON, 'ascii')
        shm_name = os.environ.get(shm_env, default_shm_name)
        shmpath = os.path.join('/dev/shm', shm_name)
        self.init_shmem(shm_name)

        if self.synchronized:
            self.init_socket(server_socket)
        self.init_channels()

    def init_channels(self):
        '''prepare interferometer channel table from configs.'''

        # all channels will be FSI_CH_NONE, i.e., ignorable and unconfigured,
        # until .ini or libfsi decree otherwise
        self.ch = [ Channel(i, typ=fsitypes.FSI_CH_NONE)
                        for i in range(fsitypes.FSI_NCHANNELS) ]

        # configure channels labeled in .ini
        self.ch_by_label = {}
        for label, bc in self.cfg.channel_map.items():
            *pfx, board, channel = bc.split('.')
            diot    = 0 if not pfx else int(pfx[0])
            board   = int(board)
            channel = int(channel)
            chid = to_channel_id(diot, board, channel)
            self.ch_by_label[label] = self.ch[chid]
            self.ch[chid].label = label
            self.ch[chid].type = fsitypes.FSI_CH_DISTANCE
            hints = self.cfg.hints.get(label)
            if hints:
                self.ch[chid].hints = hints
            # record address of C shmem for config
            self.ch[chid].config = self.FSI_state.channels[chid]

        # register gas cells and reference channels as configured
        self.gas1 = self.ch_by_label[self.cfg.gas_channel_label]
        self.gas2 = self.ch_by_label[self.cfg.gas_channel_backup_label]
        self.ref_ = self.ch_by_label[self.cfg.reference_channel_label]
        self.refs = [ self.ch_by_label[r] for r in
                    self.cfg.reference_channel_labels ]

        # configure channel types accordingly
        self.gas1.type = fsitypes.FSI_CH_GAS_CELL
        self.gas2.type = fsitypes.FSI_CH_GAS_CELL
        self.ref_.type  = fsitypes.FSI_CH_INTERF
        for ch in self.refs:
            ch.type = fsitypes.FSI_CH_INTERF

    def __del__(self):
        self.cleanup()

    def cleanup(self):
        self.cleanup_shmem()
        self.cleanup_sync()

    def cleanup_shmem(self):
        '''free resources of shmem area.'''
        try:
            # this is required for close() to succeed
            # without the 'exported pointers exist' error, see
            # why in # https://stackoverflow.com/questions/53339931/
            if hasattr(self, 'FSI_state'):
                self.FSI_state._objects['ffffffff'].release()
            if hasattr(self, 'expert_vectors'):
                self.expert_vectors._objects['ffffffff'].release()
            if hasattr(self, 'mempool'):
                self.mempool.close()
        except BufferError as e:
            logging.error(f'cannot close mempool, ignoring')
        if hasattr(self, 'mempool'):
            self.mempool.unlink()

    def cleanup_sync(self):
        ''' free resources related to sync mode.'''
        if hasattr(self, 'ctrlconn') and self.ctrlconn is not None:
            self.ctrlconn.close()
        if hasattr(self, 'ctrlsocket') and self.ctrlsocket is not None:
            self.ctrlsocket.close()
        if self.synchronized:
            try:
                os.unlink(server_socket)
            except FileNotFoundError as e:
                pass
            except PermissionError as e:
                logging.error(f'cannot remove socket {server_socket}, not owner')

    def init_shmem(self, shm_name):
        '''initialize shared memory area of server.'''

        try:
            shmpath = os.path.join('/dev/shm', shm_name)
            os.unlink(shmpath)
        except FileNotFoundError as e:
            pass
        self.mempool = SharedMemory(shm_name, create=True, size=fsi_shm_size)
        FSI_state_offset = 0
        expert_vectors_offset = sizeof(fsitypes.FSI_state)
        self.FSI_state = fsitypes.FSI_state.from_buffer(
                        self.mempool.buf[:expert_vectors_offset])
        self.expert_vectors = fsitypes.fsi_expert_vectors_array.from_buffer(
                        self.mempool.buf[expert_vectors_offset:])
        logging.info(f'initialized shared mem area {shmpath}')
        logging.info(f'FSI_state at offset:      {FSI_state_offset}')
        logging.info(f'expert_vectors at offset: {expert_vectors_offset}')

    def init_socket(self, sockfile):
        '''initialize synchronization channel with library.'''

        self.ctrlsocket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.socket_path = sockfile
        if os.path.exists(sockfile):
            # this 'leave' trick is necessary to cleanly exit from IPython,
            # see issue #13796 at https://github.com/ipython/ipython/issues/13796
            leave = False
            try:
                os.unlink(sockfile)
            except PermissionError as e:
                logging.error(f'cannot delete existing control socket {sockfile}, aborting')
                logging.error(f'please delete {sockfile} and restart!')
                leave = True
            if leave: sys.exit(1)
        self.ctrlsocket.bind(sockfile)
        self.ctrlsocket.listen()
        logging.info(f'server control socket {sockfile} created')
        self.wait_for_connect()

    def wait_for_connect(self):
        self.ctrlconn = None
        if self.synchronized:
            logging.info('waiting for ctrl socket... ')
            self.ctrlconn, _ = self.ctrlsocket.accept()
            logging.info(f'connected with {self.ctrlconn}')
            greet = self.ctrlconn.recv(256)
            logging.debug(f'received greet = {greet}')
            if greet == b'hello fsi server':
                self.ctrlconn.send(b'hello libfsi')

    def wait_on_lib(self, label=''):
        '''wait until library signals change of state.'''

        msg = self.ctrlconn.recv(256)
        if msg == b'':
            logging.error('connection lost, exiting')
            raise ConnectionLost(f'waiting on {label}')
            # this exception should lead to a re-calling
            # of wait_for_connect(), client is dead
        msg = str(msg, 'ascii')
        logging.debug(f'**** {msg} ****')
        # FIXME: this block is probably superfluous, just as the optional
        # 'label' param
        if False:
            words = msg.split()
            if words[0] != label:
                logging.error(f'out of sync! [{msg}] while waiting on [{label}]')
        return msg

    def wake_lib(self, msg):
        '''wake library waiting on server finished op.'''
        try:
            self.ctrlconn.send(bytes(msg, 'ascii'))
        except BrokenPipeError as e:
            raise ConnectionLost(f'lost connection sending {msg}')
            # this exception should lead to a re-calling
            # of wait_for_connect(), client is dead

    def scan(self, check_edfa=None):
        'perform acquisition as configured.'

        # connect and configure laser
        laser = TLM8700.Tlm8700Wrapper(self.cfg.laser_serial)
        laser.configure_laser()

        # connect and configure EDFA
        if check_edfa:
            edfa = KeopsysEDFA(self.cfg.edfa_serial)
            edfa.configure_edfa(setpoint=self.cfg.setpoint)

        # # FIXME: just laser and EDFA stuff
        # print(time.strftime('%Y%m%dT%H%M%S: laser & edfa'))
        # time.sleep(3)
        # return

        # no refraction index calculations foreseen after acceptance
        # tests, hence n_air is 1.0 for all purposes
        self.n_air = 1.0

        # connect to meteo and get T,p,h to compute air refraction
        # index by Ciddor equation
        # e = easylog80cl.EasyLog80CL(self.cfg.weather_port)
        # self.temperature = e.temperature
        # self.pressure = e.pressure
        # self.humidity = e.humidity
        # # obtain refraction index of air assuming CO2
        # # concentration as 450ppm par faute de mieux
        # self.n_air = ciddor.n(1.550, self.temperature,
        #     self.pressure * 100, self.humidity / 100.0, 450)

        # record laser statuses
        if check_edfa:
            self.laser_configs = '\n'.join([
                f'EDFA: {edfa.description}',
                f'LASER: {laser.device_id}',
                f'{laser.display_config()}',
                f'{edfa.display_config()}'])
        else:
            self.laser_configs = '\n'.join([
                f'EDFA: **same**',
                f'LASER: {laser.device_id}',
                f'{laser.display_config()}',
                f'**edfa same**'])

        # do scan
        laser.scan()

    def display_ch_info(self):
        '''show representation of configured channels.'''
        for corech in self.FSI_state.channels:
            if corech.type_ != 0:
                o, b, c = to_obc(corech.channel_id)
                print(f'channel {corech.channel_id}:{o}:{b}:{c} '
                      f'type {corech.type_}, disabled {corech.disabled}, '
                      f'peaks {corech.rpeaks}, peak[0].freq '
                      f'{corech.requested_peaks[0].frequency}')

    def acquire(self):
        '''do the actual data transfer from DIOT.'''

        nchans, record = self.diot[0].acquire()
        if nchans < 0 and not self.diot[0].probe():
            raise DiotDown(f'DI/OT host#{0}:{diot[0].hostname} is down, please restart!')
        elif nchans < 0:
            self.diot[0].reconnect()
            raise LostPackets(f'{-nchans}')
        self.channels = record[:nchans]

        # sanity check what we received, and record
        # sample length n and acquisition timestamp
        common_n, timestamp = channel_common_metadata(self.channels)
        if self.n != common_n:
            raise InconsistentChannelMetadata(
                f'nsamples configured {self.n} != acquired {common_n}')
        self.timestamp = str(timestamp.strip(b'\0'), 'ascii')
        if self.batch is None:
            self.batch = self.cfg.batch
        if not self.batch:
            self.batch = self.timestamp

        # store raw data into its interferometer channel object
        for ch in self.channels:
            # FIXME: diot is not somehow in the FsiChanAcqHeader
            chid = to_channel_id(0, ch.board, ch.channel)
            self.ch[chid].v = np.ctypeslib.as_array(ch.data, (ch.n,))

        # pointer to special channels
        self.gas = self.gas1
        self.ref = self.ref_

        # only if master, copy configs to C shmem area
        if not self.synchronized:
            for i, ch in enumerate(self.ch):
                if not hasattr(ch, 'config'):
                    continue
                ch.config.channel_id = ch.channel_id
                ch.config.disabled = ch.disabled
                ch.config.type_ = ch.type
                ch.config.rpeaks = 0
                if hasattr(ch, 'hints'):
                    ch.config.rpeaks = len(ch.hints)
                    for i, hint in enumerate(ch.hints):
                        ch.config.requested_peaks[i].position = hint[1]
                        ch.config.requested_peaks[i].peak_id  = hint[0]
                        ch.config.requested_peaks[i].range1 = 0.001
                        ch.config.requested_peaks[i].range2 = 0.001
                        ch.config.requested_peaks[i].type_  = fsitypes.FSI_PEAK_GAUSS
                        # always GAUSS
        # swap now to rid ourselves of any trace of FsiChanAcqHeader
        self.acq_buffer, self.channels = self.channels, self.ch

    def display(self):
        'display acquisition in human-readable form.'
        one = []
        one.append('\n'.join([
            f'---------------------------',
            f'ts: {self.timestamp}',
            f'fs: {self.fs}   n: {self.n}  zpf = {self.zpf}',
            f'---------------------------']))
        for ch in self.channels:
            if not hasattr(ch, 'label'): continue
            one.append(f'{ch.board}:{ch.channel}: {ch.label:10s}   '
                f'     {ch.v[:8]} ... hints: {ch.hints}''')
        one.append('')
        return '\n'.join(one)

    def linearize(self):
        'linearize time for ref and gas cel channel.'
 
        # linearize reference and gas cell
        self.t, self.tlin =  linearize.linearize(self.fs, self.ref.v)
        self.refl = self.ref.l = np.interp(self.t, self.tlin, self.ref.v)
        self.gasl = self.gas.l = np.interp(self.t, self.tlin, self.gas.v)

    def linearize_channel(self, channel):
        'linearize channel with computed linearized time.'
        channel.l = np.interp(self.t, self.tlin, channel.v)

    def compute_alpha(self):
        'compute alpha from gas cell data and record freq/length scales.'
       
        try:
            self.gfit = alphafit.compute_alpha(self.gasl, fs=self.fs, n=self.n)
        except alphafit.MalformedGasCell as e:
            self.gfit = alphafit.GasCellFit()
            self.gfit.alpha =  -252e12
            logging.warning(f'WARNING: malformed gas cell data: {self.timestamp}')
            logging.warning(f'WARNING: using -252THz as default alpha')
            raise

        self.alpha             = self.gfit.alpha
        self.alpha_rms_error   = self.gfit.alpha_rms_error
        self.alpha_true_peaks  = self.gfit.alpha_true_peaks
        self.freq_to_dist      = self.gfit.freq_to_dist
        self.idx_to_distance   = self.gfit.idx_to_distance

        # FIXME: these do not belong here, they should go
        # together with the bulk spectral computation
        self.nspec = self.zpf * self.n      # zero-padded FFT
                                            # spectrum (magnitude)
        self.f = np.fft.rfftfreq(self.nspec, d=1/self.fs)
                                            # Frequency axis calculation
        self.distance = self.freq_to_dist * self.f
                                            # distance axis

    def compute_distances(self, channel):

        # linearize channel and compute spectrum
        m = channel.l = np.interp(self.t, self.tlin, channel.v)
        m = m - np.mean(m)
        channel.spec = np.abs(np.fft.rfft(m, self.nspec))
        channel.parent = self

        offset = 300 * self.zpf     # used only if auto-detecting peaks
        if not hasattr(channel, 'hints'):
            hint_idxs, _ = scipy.signal.find_peaks(channel.spec[offset:], prominence=200000,
                    distance=self.zpf*500, width=[1,self.zpf*50], wlen=self.zpf*1000)
            hint_idxs += offset
            hints = self.distance[hint_idxs]
        else:
            hint_idxs = np.array(channel.hints / self.freq_to_dist * self.nspec /self.fs,
                dtype=np.uint32)
            hints = channel.hints

        channel.peaks = []
        width = 0.002   # 2mm
        xdelta = self.idx_to_distance
        rpeaks = channel.config.rpeaks
        for req in channel.config.requested_peaks[:rpeaks]:
            # the basis for this computation are the
            # self.distance X-axis and the channel.spec Y-axis,
            # the hint/hint_idx pair and the window
            # (hint-range1, hint+range2) in X units.
            # self.idx_to_distance is the conversion factor from
            # index to X units in meters
            range1 = req.range1
            range2 = req.range2
            hint = req.position
            pk = Peak(hint, range1, range2)
            pk.peak_id = req.peak_id
            fro, to = hint - range1, hint + range2
            fro_idx, to_idx = int(fro / xdelta), int(to / xdelta)
            hint_idx = int(hint / xdelta)

            pk.valid = 0 < fro_idx < to_idx < len(channel.spec)
            if not pk.valid:
                continue

            x = self.distance[fro_idx:to_idx]
            y = channel.spec[fro_idx:to_idx]
            fit = models.gauss_fit(x, y)
            if not fit.success:
                pk.valid = 0
                continue

            pk.mu    = fit.params['mu'].value
            pk.sigma = fit.params['sigma'].value
            pk.stderr = fit.params['mu'].stderr
            pk.fit   = fit

            # good fits should be below 1e-6 stderr
            # fit.success is extremely lenient!
            if pk.stderr > 1e-3:
                pk.valid = 0
                continue

            # copy FSI_PEAK_LENGTH samples to display; not anymore
            # the simple "pk.x, pk.y = x, y", which is hint- and width-dependent
            muidx    = int(pk.mu / xdelta)
            leftidx  = muidx   - fsitypes.FSI_PEAK_LENGTH//2
            rightidx = leftidx + fsitypes.FSI_PEAK_LENGTH
            pk.x = self.distance[leftidx:rightidx]
            pk.y = channel.spec [leftidx:rightidx]

            # this condition happens if the fit is so bad that
            # the slice [leftidx:rightidx] is not within the sample
            # range boundaries. Maybe 0 < leftidx < rightidx < len(channel.spec)
            # is a necessary addition
            pk.valid = int(
                len(pk.x) == fsitypes.FSI_PEAK_LENGTH and
                leftidx > 0 and rightidx < len(channel.spec) and
                abs(fit.params['mu'].value - fit.init_values['mu']) < 0.005)
            if not pk.valid:
                continue

            pk.fro, pk.to, pk.fro_idx, pk.to_idx = fro, to, fro_idx, to_idx
            pk.leftidx, pk.rightidx = leftidx, rightidx
            pk.xdelta = xdelta
            pk.hint_idx = hint_idx
            pk.max = self.distance[hint_idx]    # FIXME: is this of any use?
            channel.peaks.append(pk)

        return channel.peaks

    def labeled_channels(self):
        '''iterate over channels of interest.'''
        for ch in self.channels:
            if getattr(ch, 'label', None) is None: continue
            yield ch

    def measurement_channels(self):
        '''iterator over measurement channels only.'''
        for ch in self.channels:
            if not hasattr(ch, 'config'):
                continue
            if ch.config.type_ == FSI_CH_DISTANCE:
                yield ch

    def report(self):
        res = []
        for ch in self.measurement_channels():
            for i, peak in enumerate(ch.peaks):
                peak.i = i
                res.append(
                    '{batch:14s} {timestamp:14s} '
                    '{temperature:4.1f} {pressure:>6.1f} '
                    '{humidity:4.1f} {n_air:12.9f} '
                    'α:{alpha:>13.8f} '
                    '({board:d}:{channel:d}) {label:10s} '
                    '{i:1d}: {mu:>11.7f} +/- {sigma:6.3f}um '
                    '{hint:7.4f}+/-{half_width:6.4f}'
                    '{max_:12.7f}'.format(
                        batch = self.batch,
                        timestamp = self.timestamp,
                        temperature = self.temperature,
                        pressure = self.pressure,
                        humidity = self.humidity,
                        alpha = self.alpha / 1.0e12,
                        n_air = self.n_air,
                        label = ch.label,
                        board = ch.board,
                        channel = ch.channel,
                        i = peak.i,
                        hint = peak.hint,
                        half_width = peak.width / 2,
                        mu = peak.mu,
                        sigma = 1.0e6 * peak.sigma,
                        max_ = peak.max))
        return '\n'.join(res)

    def matlab_raw_store(self):
        '''save acquisition in MATLAB file.'''
        record = { ch.label : ch.v for ch in self.labeled_channels()}
        attrs = 'n fs timestamp'.split()
        for attr in attrs:
            record[attr] = getattr(self, attr)
        filename = os.path.join(self.cfg.save_directory, f'{self.timestamp}.mat')
        scipy.io.savemat(filename, record, do_compression=True)

    def matlab_spectra_store(self):
        '''save spectra in MATLAB file.'''
        record = { f'FFT_{ch.label}' : ch.spec for ch in self.labeled_channels()
                                    if getattr(ch, 'spec', None) is not None}
        attrs = '''n fs alpha idx_to_distance'''.split()
        for attr in attrs:
            record[attr] = getattr(self, attr)
        filename = os.path.join(self.cfg.save_directory, f'{self.timestamp}_fft.mat')
        scipy.io.savemat(filename, record, do_compression=True)

    def log_results(self, fname=None):
        '''save human-readable acquisition data in file.'''

        if fname is None:
            fname = f'{self.timestamp}-results.txt'

        res = []
        res.append(f'{self.batch}:{self.timestamp} at  n:{self.n}, fs:{self.fs}')
        for ch in self.measurement_channels():
            chid = to_channel_id(0, ch.board, ch.channel)
            label = getattr(ch, 'label', '<none>')
            res.append(f'    ch#{chid}:{ch.board}.{ch.channel}:{label}:    {len(ch.peaks)} peaks')
            for i, peak in enumerate(ch.peaks):
                res.append(f'        pk#{i}: {peak.mu:12.6f}+-{1e6*peak.sigma:>12.3f}um  max={peak.max:12.3f}     hint:{peak.hint}+-{peak.hwidth1}')
        res.append('')
        with open(fname, 'w') as o:
            o.write('\n'.join(res))

    def db_store(self, dbname=None):

        if dbname is None:
            dbname = 'fsi-acq-data.db'
        c = sqlite3.connect(dbname, isolation_level='IMMEDIATE')
        cur = c.cursor()
        cur.execute('''begin transaction''')
        cur.execute('''
            insert into acquisition(timestamp, batch, n, fs,
                temperature, pressure, humidity, n_air, alpha)
            values (:timestamp, :batch, :n, :fs,
                :temperature, :pressure, :humidity, :n_air, :alpha)''', dict(
                timestamp = self.timestamp,
                batch = self.batch,
                n = self.n,
                fs = self.fs,
                temperature = self.temperature,
                pressure = self.pressure,
                humidity = self.humidity,
                n_air = self.n_air,
                alpha = self.alpha))
        for ch in self.measurement_channels():
            cur.execute('''
                    insert into channel(board, channel, diot, timestamp, label)
                    values (:board, :channel, :diot, :timestamp, :label)''', dict(
                        timestamp = self.timestamp,
                        board = ch.board,
                        channel = ch.channel,
                        diot = 0,   # FIXME: there will be more chassis
                        label = ch.label))
            channel_id = cur.lastrowid

            for i, peak in enumerate(ch.peaks):
                peak.i = i
                cur.execute('''
                    insert into hints(hint, width, channel_id, index_)
                    values (:hint, :width, :channel_id, :index)''', dict(
                        hint = peak.hint,
                        width = peak.width,
                        index = peak.i,
                        channel_id = channel_id))
                hint_id = cur.lastrowid

                cur.execute('''
                    insert into peaks(hint_id, mu, sigma, max_)
                    values(:hint_id, :mu, :sigma, :maximum)''', dict(
                        hint_id = hint_id,
                        mu = peak.mu,
                        sigma = peak.sigma,
                        maximum = peak.max))
        cur.execute('''commit''')

    def put_msmt_into_mempool(self, ch):
        '''record measurement results in the shared mem area.'''
        channel_id = ch.config.channel_id
        lib_channel = self.FSI_state.channels[channel_id]
        assert lib_channel.channel_id == channel_id
        t = time.strptime(self.timestamp, '%Y%m%dT%H%M%S')
        t = int(time.strftime('%s', t))
        lib_channel.timestamp[0] = t
        lib_channel.timestamp[1] = 0        # no us fraction yet
        mpeaks = len(ch.peaks)
        lib_channel.mpeaks = mpeaks
        for p in range(mpeaks):
            mp = lib_channel.measured_peaks[p]
            peak = ch.peaks[p]
            mp.type_ = ch.config.requested_peaks[p].type_
            if not peak.valid:
                mp.valid = 0
                continue
            mp.valid = 1
            mp.mu = peak.mu
            mp.sigma = peak.sigma
            mp.snr   = mini_snr(peak.y)

            # FIXME: specific for gauss, be careful with others
            mp.fit_params.amplitude = peak.fit.params['A'].value
            mp.fit_params.mu        = peak.mu      # peak.fit.params['mu'].value
            mp.fit_params.sigma     = peak.sigma   # peak.fit.params['sigma'].value
            # undocumented lmfit feature: lmfit reports a stderr None value
            # when the fit result is bad enough; we prevent the ensuing exception by
            # giving a dreafully high stderr if it so happens.
            stderr                  = peak.fit.params['mu'].stderr
            mp.fit_params.stderr = stderr if stderr is not None else 1e99

            nsamples = len(peak.y)
            mp.nsamples = nsamples
            mp.samples[:nsamples] = (nsamples*c_double).from_buffer(peak.y.data)
            mp.start = peak.x[0]
            mp.end   = peak.x[-1]
            mp.delta = self.idx_to_distance
            mp.xunit = FSI_UNIT_M
            mp.yunit = FSI_UNIT_NONE

    def put_raw_into_mempool(self):
        'copy channel designated for full msmt  into expert_vectors mempool area.'

        linear_modes = [ fsitypes.FSI_EXPERT_LIN_DATA,
                         fsitypes.FSI_EXPERT_GAS_CELL,
                         fsitypes.FSI_EXPERT_INTERF, ]
        raw_modes = [ fsitypes.FSI_EXPERT_OSCILLOSCOPE,
                      fsitypes.FSI_EXPERT_RAW_GAS_CELL,
                      fsitypes.FSI_EXPERT_RAW_INTERF, ]

        chid = self.FSI_state.full_chid
        mode = self.FSI_state.full_mode
        if mode == fsitypes.FSI_EXPERT_NONE:
            return
        ch = self.ch[chid]
        expert_vectors = self.expert_vectors

        # FIXME: optimize usage of space
        l = len(ch.v)
        vtype = c_double * l
        if mode in raw_modes:
            expert_vectors[chid].raw[:l] = vtype.from_buffer(ch.v.astype(np.double).data)
        if mode in linear_modes:
            expert_vectors[chid].lin[:l] = vtype.from_buffer(ch.l.astype(np.double).data)
        if mode in (fsitypes.FSI_EXPERT_LIN_FFT_M, fsitypes.FSI_EXPERT_LIN_FFT_HZ):
            sl = len(ch.spec)
            stype = c_double * sl
            expert_vectors[chid].fft[:sl] = stype.from_buffer_copy(ch.spec.astype(np.double).data)

    def put_globals_into_mempool(self):
        '''copy general acquisition data into mempool data structure.'''

        fsi_state = self.FSI_state
        fsi_state.n  = self.n
        fsi_state.fs = int(self.fs)
        fsi_state.idx_to_distance = self.idx_to_distance

    def put_qa_into_mempool(self):
        '''copy qa data into mempool data structure.'''

        qa = self.FSI_state.qa_params
        qa.nref = 1     # FIXME: number of oblocks, will be 4
        qa.ngc  = 1     # FIXME: number of gcs, will be 2
        qa_ref = qa.ref[0]
        qa_gas = qa.gc[0]

        qa_ref.ref_id            = self.ref.channel_id
        qa_ref.ref_center        = self.ref_center
        qa_ref.ref_deviation     = self.ref_deviation
        qa_ref.ref_deviation95   = self.ref_deviation95

        qa_gas.gc_id             = self.gas.channel_id
        qa_gas.gc_alpha          = self.alpha
        qa_gas.gc_zero_value     = 0
        qa_gas.gc_fitting_err    = self.alpha_rms_error

        rlen = len(self.gfit.r_lobe)
        plen = len(self.gfit.p_lobe)
        qa_gas.gc_npeaks_fit_R = rlen
        qa_gas.gc_npeaks_fit_P = plen

        #FIXME: kludge: we are presuming a constant of nature in the
        # sweep parameters here. Sorry.
        #FIXME: this scale change would better be moved into alphafit.py
        rlobe = self.gfit.r_lobe / self.n * (1570 - 1520) + 1520
        plobe = self.gfit.p_lobe / self.n * (1570 - 1520) + 1520
        qa_gas.gc_peaks_R[:rlen] = (c_double*rlen).from_buffer_copy(rlobe.data)
        qa_gas.gc_peaks_P[:plen] = (c_double*plen).from_buffer_copy(plobe.data)

    def display_channel_results(self, ch):
        '''show measurements of a channel.'''
        '''FIXME: this method must go, just scaffolding'''
        channel_id = ch.config.channel_id
        peaks = len(ch.peaks)
        print(f'''
        ch#{channel_id}: t:{ch.config.type_} peaks:{peaks}''', end='')
        for i, peak in enumerate(ch.peaks):
            fit_type = ch.config.requested_peaks[i].type_
            x, y = peak.x, peak.y
            print(f'''
            #{i}: type:{fit_type} center:{peak.mu} +- {peak.sigma}
                snr:FIXME   fit_params:FIXME
                samples[{len(y)}] = {y[:5]} ...  {y[-5:]}
                xunit:m FIXME   yunit:FIXME
                x = {x[0]}:{x[1]-x[0]}:{x[-1]}
                idx_to_distance = {self.idx_to_distance}
            ''', end='')

    def mainloop(self, iterations=None):
        o = sys.stderr
        if not iterations:
            seq = itertools.count(0)
        else:
            seq = range(iterations)
        for acqno in seq:
            ts = time.asctime()
            try:
                if self.synchronized:
                    # lib<->server sync point A
                    # wait for scan done
                    msg = self.wait_on_lib('scan')
                    if msg == 'scan ok':
                        self.wake_lib('ack')
                    elif msg == 'scan err':
                        self.wake_lib('ack')
                        continue
                    elif msg == 'meas':
                        self.wake_lib('calc err: acq not ready')
                        continue
                    else:
                        self.wake_lib('''nack at 'scan' wait''')
                        raise OutOfSync(f'expected scan, got {msg}')
                elif self.noscan:
                    pass    # do not scan, raspberry pi will do for you
                    logging.info('ML: independent laser control...')
                else:
                    logging.info('launching scan')
                    self.scan(check_edfa=1)

                logging.info('acquiring...')
                self.acquire()
                if self.cfg.save_raw_matlab:
                    logging.info('saving raw data...')
                    self.matlab_raw_store()
                # self.display_ch_info()

                logging.info('linearizing...')
                self.linearize()
                tmp = linearize.lin_quality(self.ref, self.t, self.tlin, self.n)
                self.ref_center, self.ref_deviation, self.ref_deviation95 = tmp

                logging.info('alpha...')
                self.compute_alpha()

                if self.synchronized:
                    # lib<->server sync point B
                    # wait for lib measurement request
                    # FIXME: there is little reason to sync at this
                    # point, when it might be more efficient and logical
                    # to do it at the next FIXME comment
                    cmd = self.wait_on_lib('meas')
                    if cmd != 'meas':
                        self.wake_lib('''nack at 'meas' wait''')
                        logging.error(f'{ts}: {cmd} received waiting for "meas"')
                        logging.error(f'resyncing')
                        raise OutOfSync(f'{ts}:meas')

                logging.info('computing distances...')
                for ch in self.measurement_channels():
                    peaks = self.compute_distances(ch)
                    self.put_msmt_into_mempool(ch)
                self.put_globals_into_mempool()
                self.put_qa_into_mempool()
                self.put_raw_into_mempool()

                if self.synchronized:
                    # lib<->server sync point C
                    # signal results ready
                    # FIXME: why not sync B: wait_on_lib('meas') here?
                    self.wake_lib('calc')
                else:
                    self.log_results()

                # self.db_store()
                logging.info(f'{ts}: {self.timestamp}: OK')
                # wide_csv_db_report()
                if self.cfg.save_spectra_matlab:
                    logging.info('saving spectral data...')
                    self.matlab_spectra_store()

            except alphafit.MalformedGasCell as e:
                logging.error(f'{ts}: {self.timestamp}: Malformed gas cell {e}')
                self.wake_lib('calc err: malformed gas cell')
            except DiotDown as e:
                logging.error(f'{ts}: DI/OT host down: {e}')
                self.wake_lib('calc err: diot down')
            except LostPackets as e:
                logging.error(f'{ts}: Botched acquisition, {e} packets lost')
                self.wake_lib('calc err: packets lost')
            except InconsistentChannelMetadata as e:
                logging.error(f'{ts}: Inconsistent channel metadata, possibly failed acq')
                self.wake_lib('calc err: failed acq')
            except FailedPowerConfig as e:
                logging.error(f'{ts}: Power at EDFA output not stable, acq aborted')
                self.wake_lib('calc err: unstable output power at EDFA')
            except OutOfSync as e:
                logging.error(f'{ts}: out-of-sync {e} message received')
                continue
            except BadStderr as e:
                logging.error(f'{ts}: {e}')
                self.wake_lib('calc err: bad fit stderr')
                continue
            except (ConnectionLost, ConnectionResetError) as e:
                logging.error(f'{ts}: Ctrl Socket connection lost, restarting server')
                self.wait_for_connect()
                continue

    def acquire_from_matlab(self, mlab_file):
        '''read an acquisition from a MATLAB file.'''

        from scipy.io import loadmat

        # use as acquired channels all labels made up from [A-Z0-9_]
        valid_chars = string.ascii_uppercase + string.digits + '_'
        m = loadmat(mlab_file)
        labels = [l for l in list(m) if set(l).issubset(valid_chars)]
        for l in labels:
            ch = self.ch_by_label[l]
            ch.v = m[l][0]
        nchans = len(labels)

        # sanity check what we received
        # common_n, timestamp = channel_common_metadata(self.channels)
        # if self.n != common_n:
        #     raise InconsistentChannelMetadata(
        #         f'nsamples configured {self.n} != acquired {common_n}')
        self.timestamp, _ = os.path.splitext(os.path.basename(mlab_file))
        if self.batch is None:
            self.batch = self.cfg.batch
        if not self.batch:
            self.batch = self.timestamp

        # pointer to special channels
        self.gas = self.gas1
        self.ref = self.ref_

        # only if master, copy configs to C shmem area
        if not self.synchronized:
            for i, ch in enumerate(self.ch):
                if not hasattr(ch, 'config'):
                    continue
                ch.config.channel_id = ch.channel_id
                ch.config.disabled = ch.disabled
                ch.config.type_ = ch.type
                ch.config.rpeaks = 0
                if hasattr(ch, 'hints'):
                    ch.config.rpeaks = len(ch.hints)
                    for i, hint in enumerate(ch.hints):
                        ch.config.requested_peaks[i].position = hint[1]
                        ch.config.requested_peaks[i].peak_id  = hint[0]
                        ch.config.requested_peaks[i].range1 = 0.001
                        ch.config.requested_peaks[i].range2 = 0.001
                        ch.config.requested_peaks[i].type_  = fsitypes.FSI_PEAK_GAUSS
                        # always GAUSS
        self.channels = self.ch

    def emc_process(self, emc_file):
        '''process a single acq of EMC data.'''
        o = sys.stderr
        ts = time.asctime()
        try:
            logging.info(f'acquiring from matlab file {emc_file}...')
            self.acquire_from_matlab(emc_file)

            logging.info('linearizing...')
            self.linearize()
            logging.info('alpha...')
            self.compute_alpha()
            logging.info('computing distances...')
            for ch in self.measurement_channels():
                peaks = self.compute_distances(ch)
                self.put_msmt_into_mempool(ch)
            self.put_raw_into_mempool()

            # self.db_store()
            logging.info(f'{ts}: {self.timestamp}: OK')
            # wide_csv_db_report()
            if self.cfg.save_spectra_matlab:
                logging.info('saving spectral data...')
                self.matlab_spectra_store()

        except alphafit.MalformedGasCell as e:
            logging.error(f'{ts}: {self.timestamp}: Malformed gas cell {e}')
        except ValueError as e:
        #     we reach this point when fixing (incompletely) the zero-length
        #     array condition that broke the server more often than not
             raise e
        except alphafit.MalformedGasCell as e:
            logging.error(f'{ts}: {self.timestamp}: Malformed gas cell')
        except LostPackets as e:
            logging.error(f'{ts}: Botched acquisition, {e} packets lost')
        except InconsistentChannelMetadata as e:
            logging.error(f'{ts}: Inconsistent channel metadata, possibly failed acq')
        except FailedPowerConfig as e:
            logging.error(f'{ts}: Power at EDFA output not stable, acq aborted')
        except (ConnectionLost, ConnectionResetError) as e:
            logging.error(f'{ts}: Ctrl Socket connection lost, restarting server')
            self.wait_for_connect()
            return

    def piscan(self, pihost='cobas-raspi'):
        '''trigger a scan into raspberry pi laser control.'''
        
        try:
           self.piname = pihost
           self.pi = fabric.connection.Connection(self.piname)
           self.pi.run('./laser-config-and-scan')
        except Exception as e:
          logging.error(f'cannot ssh into host {self.piname}')

    def emc_mainloop(self, iterations=None, pihost='cobas-raspi',
                    rs_host='cfb-864-allrfgen2', rs_port=5025):
        '''mainloop for EMC special application.'''

        import fsi.rs as rs
        o = sys.stderr
        if not iterations:
            seq = itertools.count(0)
        else:
            seq = range(iterations)
        for acqno in seq:
            ts = time.asctime()
            try:

                logging.info('config sig gen')
                rs.setup_rad_test(100e3, 1e3, 80, -12, host=rs_host, port=rs_port)
                test_params = rs.get_rad_test_params(host=rs_host, port=rs_port)
                print(test_params)

                logging.info('launching scan')
                self.piscan(pihost)

                logging.info('acquiring...')
                self.acquire()
                if self.cfg.save_raw_matlab:
                    logging.info('saving raw data...')
                    self.matlab_raw_store()
                # self.display_ch_info()

                logging.info('linearizing...')
                self.linearize()
                tmp = linearize.lin_quality(self.ref, self.t, self.tlin, self.n)
                self.ref_center, self.ref_deviation, self.ref_deviation95 = tmp

                logging.info('alpha...')
                self.compute_alpha()

                if self.synchronized:
                    # lib<->server sync point B
                    # wait for lib measurement request
                    # FIXME: there is little reason to sync at this
                    # point, when it might be more efficient and logical
                    # to do it at the next FIXME comment
                    cmd = self.wait_on_lib('meas')
                    if cmd != 'meas':
                        self.wake_lib('''nack at 'meas' wait''')
                        logging.error(f'{ts}: {cmd} received waiting for "meas"')
                        logging.error(f'resyncing')
                        raise OutOfSync(f'{ts}:meas')

                logging.info('computing distances...')
                for ch in self.measurement_channels():
                    peaks = self.compute_distances(ch)
                    self.put_msmt_into_mempool(ch)
                self.put_globals_into_mempool()
                self.put_qa_into_mempool()
                self.put_raw_into_mempool()

                if self.synchronized:
                    # lib<->server sync point C
                    # signal results ready
                    # FIXME: why not sync B: wait_on_lib('meas') here?
                    self.wake_lib('calc')
                else:
                    self.log_results()

                # self.db_store()
                logging.info(f'{ts}: {self.timestamp}: OK')
                # wide_csv_db_report()
                if self.cfg.save_spectra_matlab:
                    logging.info('saving spectral data...')
                    self.matlab_spectra_store()

            except alphafit.MalformedGasCell as e:
                logging.error(f'{ts}: {self.timestamp}: Malformed gas cell {e}')
                self.wake_lib('calc err: malformed gas cell')
            except LostPackets as e:
                logging.error(f'{ts}: Botched acquisition, {e} packets lost')
                self.wake_lib('calc err: packets lost')
            except InconsistentChannelMetadata as e:
                logging.error(f'{ts}: Inconsistent channel metadata, possibly failed acq')
                self.wake_lib('calc err: failed acq')
            except FailedPowerConfig as e:
                logging.error(f'{ts}: Power at EDFA output not stable, acq aborted')
                self.wake_lib('calc err: unstable output power at EDFA')
            except OutOfSync as e:
                logging.error(f'{ts}: out-of-sync {e} message received')
                continue
            except BadStderr as e:
                logging.error(f'{ts}: {e}')
                self.wake_lib('calc err: bad fit stderr')
                continue
            except (ConnectionLost, ConnectionResetError) as e:
                logging.error(f'{ts}: Ctrl Socket connection lost, restarting server')
                self.wait_for_connect()
                continue


def wide_csv_db_report(dbname=None, outfile=None):
    '''transform acquisition database into wide CSV.'''

    if dbname is None:
        dbname = 'fsi-acq-data.db'
    if outfile is None:
        outfile = 'fsi-acq-data.csv'

    cur = sqlite3.connect(dbname).cursor()
    cur.execute('''select timestamp from acquisition''')
    timestamps = [t[0] for t in cur]

    header = True
    with open(outfile, 'w') as o:
        for ts in timestamps:
            cur.execute(
                '''select
                acq.batch, acq.timestamp,
                acq.n, acq.fs, acq.zpf,
                acq.temperature, acq.pressure, acq.humidity, acq.n_air,
                acq.alpha / 1.0e12 as alpha,
                ch.board, ch.channel, ch.label,
                h.index_, h.hint, h.width,
                pk.mu, pk.sigma, pk.max_
                from acquisition acq join channel ch
                on ch.timestamp = acq.timestamp
                join hints h
                on h.channel_id = ch.id
                join peaks pk
                on pk.hint_id = h.id
                where ch.timestamp = :timestamp''',
                { 'timestamp' : ts })
            t = cur.fetchall()
            if header:
                unoh = '{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}'.format(
                                *[x[0] for x in cur.description])
                dosh = ','.join(['{2}_{3} mu, {2}_{3} sigma'.format(*ch[10:]) for ch in t])
                o.write(unoh + ',' + dosh + '\n')
                header = False
            uno = '{0},{1},{2},{3},{4},{5},{6},{7}'.format(*t[0])
            n_air, alpha = t[0][8], t[0][9]
            cell1 = f'{n_air:14.12f}' if n_air is not None else ''
            cell2 = f'{alpha:13.8f}' if alpha is not None else ''
            uno = f'{uno},{cell1},{cell2}'
            dos = ','.join(['{:.7f},{:.7f}'.format(ch[16], ch[17]) for ch in t])
            o.write(uno + ',' + dos + '\n')

def diot_main():
    logging.info('waiting for connections')
    a = Diot()
    print('diot connected')
    while True:
        print('waiting for acq... ', end='')
        if a.acquire() == FSI_SOCKET_NOCONN:
            print('connection lost!')
            break
        print('acq finished')
        for i in range(len(a.channels)):
            print(repr(a.channels[i]))
        print(a.fancy_show())
        ans = input('press ENTER to continue, q to close: ')
        if ans == 'q':
            break
    a.close()

def generate_cdecls(ini_file='sct.ini'):
    '''generate a C header file with INI channel configs.'''

    cfg = FsiConfig(ini_file)
    decls = cfg.cdecls()
    with open('generated-channel-configs.h', 'w') as o:
        o.write(decls)
