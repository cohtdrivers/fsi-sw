#!/usr/bin/env python
# coding: utf-8
# vim: ts=8 sw=4 et sts=4 ai

import sys
import logging

import numpy as np
from scipy.signal import savgol_coeffs, convolve, find_peaks

# these can also be obtained by reading the file
# nist_file = os.path.join(os.path.dirname(fsi.__file__), 'SRM2519a.csv')
nist_wavelengths = np.array((
    1527.63342, 1528.05474, 1528.48574, 1528.92643, 1529.37681, 1529.83688,
    1530.30666, 1530.78615, 1531.27537, 1531.7743 , 1532.28298, 1532.80139,
    1533.32954, 1533.86745, 1534.41514, 1534.97258, 1535.53981, 1536.11683,
    1536.70364, 1537.30029, 1537.90675, 1538.52305, 1539.14921, 1539.78523,
    1540.4312 , 1541.08703, 1541.7528 , 1543.11423, 1543.80967, 1544.51503,
    1545.23033, 1545.95549, 1546.69055, 1547.43558, 1548.19057, 1548.95555,
    1549.73051, 1550.51546, 1551.31045, 1552.11546, 1552.93051, 1553.75562,
    1554.59079, 1555.43605, 1556.29141, 1557.15686, 1558.0324 , 1558.91808,
))

class GasCellFit:
    '''data concerning a GC measure -> NIST HCN spectral data fit.

        rms     - the rms error in the fit
        [opt]   - a dict with poly, rms and peaks (acutal peaks used in fit) entries.

        alpha              alpha (scan speed in Hz/s), around 252.6 THz/s
        alpha_rms_error    lsq fitting error, around 50 MHz
        alpha_true_peaks   number of HCN peak lines used in the computation
        freq_to_dist       conversion scale freq -> dist in m/Hz
        idx_to_distance    conversion scale bin units -> dist in m/bin',
        poly               linear polynomial fit (of which alpha = p[0])
        p_lobe, r_lobe     vectors of x-coordinates of P- and R-peaks
        attemp             number of attemps to fit (used in loose_compute_alpha)
        rigour             rigour level achieved    (used in loose_compute_alpha)
    '''

    def __init__(self, *args, **kwds):

        self.alpha            = None
        self.alpha_rms_error  = None
        self.alpha_true_peaks = None
        self.freq_to_dist     = None
        self.idx_to_distance  = None
        self.poly             = None
        self.p_lobe           = None
        self.r_lobe           = None
        self.attempt          = None
        self.rigour           = None

    def __str__(self):
        return  '\n'.join([
           f'alpha        {self.alpha/1e12:>12.6f} THz/s',
           f'rms_error    {self.alpha_rms_error/1e6:>12.2f} MHz',
           f'npeaks used  {self.alpha_true_peaks:>12d}',
           f'm/MHz        {self.freq_to_dist*1e6:>12.8f} m/MHz',
           f'um/bin       {self.idx_to_distance*1e6:>12.8f} um/bin',
           f'',
        ])

class GasCellParams:
    '''params that mediate gas cell peak computations.'''

    # moving average length: used to compute the low frequency trend
    # in the noise floor. The trend is subtracted from the gas cell
    # sample to flatten the noise floor, which is usually "wavy". This
    # length must span a handful of peaks of the HCN spectrum; as a
    # rule, n / 25 is a good choice
    mavg_length = 50000         # took n / 50 long ago

    # prominence: one minus the fraction of max peak height that serves
    # as threshold for peak detection
    percent_threshold = 0.7                # 70% of peak dynamic range

    # distance: number of samples that separate peaks with complete
    # certainty. It must be set to the minimal distance (in sample
    # units) between peaks in the HCN spectrum. As a rule of thumb,
    # distance = n / 50 / 2 / 2 is a safe choice: there are less than 50
    # peaks, the widest gap is less than 2x the narrowest, and the
    # spectrum occupies more than 1/2 of the swept range.
    distance = 3000         # took n / 800 long ago; very conservative

    # sg_window: an odd integer, the size of the Savitzky-Golay
    # smoothing filter window. The window must be wide enough to iron
    # out noise, but never comparable to the width of a peak. As a rule
    # of thumb, choose (far) smaller than the peak width at half maximum
    # of the sampled peaks.
    sg_window = 101
    sg_tails  = (sg_window + 1) // 2
    sg_coeffs = savgol_coeffs(sg_window, 4)

def sg_smooth(gc, length=None, degree=None):
    '''smooth the sample reading with a long Savitzky-Golay filter.'''

    # use non-default parameters (this is costly)
    if length is None and degree is None:
        coeffs = GasCellParams.sg_coeffs
        tails  = GasCellParams.sg_tails
    else:
        if degree is None: degree = 4
        if length is None: length = GasCellParams.sg_window
        tails  = (length + 1) // 2
        coeffs = savgol_coeffs(length, degree)

    # do the smoothing
    smoothed_gc = convolve(gc, coeffs, mode='same')

    # flatten both ends of the vector with a constant
    smoothed_gc[:tails]  = smoothed_gc[tails]
    smoothed_gc[-tails:] = smoothed_gc[-tails]

    return smoothed_gc

class MalformedGasCell(Exception):
    pass

def p1_idx(arr):
    '''find the index of p1 peak in arr data.'''

    arr_p1 = np.abs(np.diff(np.diff(arr)))
    if len(arr_p1) <= 0:
        raise MalformedGasCell(f'gas cell reading has no peaks!')
    maxes = np.argwhere(arr_p1 > 0.9 * np.max(arr_p1))
    if len(maxes) != 2:
        # FIXME: here, a safety check of DC coupling would help a lot
        raise MalformedGasCell(f'more than two potential P1 idxs: {maxes}')
    return maxes[0][0] + 2

def alpha_fit(gc, wnist=nist_wavelengths,
                rigour=GasCellParams.percent_threshold,
                smooth=False,
                fs=None, n=None):
    '''compute alpha from gas cell data and record freq/length scales.

    Inputs:
        gc      - gas cell channel reading
        wnist   - vector of NIST SRM2519a HCN peaks in nm
	rigour  - fussiness in admitting peaks -> noise sensitivity
		  0 < rigour < 1: higher rigour favours rejection of noisy gas cell data
		   with the dreaded "malformed gas cell data" exception
        smooth  - perform a Savitzky-Golay smooth stage before flattening
        fs      - sampling frequency, set to 2490000 if fs is None/unused
        n       - length of gc sample, set to len(gc) if n is None/unused

    Returns: a GasCellFit object with the parameters of the achieved fit

              * alpha              alpha (scan speed in Hz/s), around 252.6 THz/s
              * alpha_rms_error    lsq fitting error, around 50 MHz
              * alpha_true_peaks   number of HCN peak lines used in the computation
              * freq_to_dist       conversion scale freq -> dist in m/Hz
              * idx_to_distance    conversion scale bin units -> dist in m/bin',

    If the gas cell is unfit for the task, throws a MalformedGasCell expcetion.
    '''

    # flatten the signal noise floor
    thick = GasCellParams.mavg_length
    cs = np.cumsum(gc)
    mavg = (cs[thick:] - cs[:-thick])/thick
    flat = gc[thick:] - mavg
    flat = np.hstack([np.ones(thick) * flat[-1], flat])

    # do a S-G smoothing step if so requested
    if smooth:
        flat = sg_smooth(flat)

    # detect peaks by thresholding at percentage '100 * (1-rigour)' below noise floor
    gcmax, gcmin = np.max(flat), np.min(flat)
    threshold = gcmin + rigour * (gcmax - gcmin)
    x, props = find_peaks(-flat, height=-threshold, distance=GasCellParams.distance)

    c = 299792458
    fnist = c / (wnist * 1e-9)      # NIST freqs in Hz
    
    fp1 = p1_idx(fnist)     # index of P1 peak in fnist: always 28
    xp1 = p1_idx(x)         # index of P1 peak in gas cell peaks
    l = min(fp1, xp1)
    r = min(len(fnist) - fp1, len(x) - xp1)
    p1 = l                  # index of P1 in chopped x_, fnist_ vectors
    x_ = x[xp1-l:xp1+r]
    fnist_ = fnist[fp1-l:fp1+r]
    p_lobe, r_lobe = x_[p1:], x_[:p1]
    p, residuals, *_ = np.polyfit(x_, fnist_, 1, full=True)
    rms = np.sqrt(residuals[0] / len(x_))

    ret = GasCellFit()
    ret.poly             = p
    ret.alpha_rms_error  = rms
    ret.p_lobe           = p_lobe
    ret.r_lobe           = r_lobe
    ret.alpha_true_peaks = len(p_lobe) + len(r_lobe)

    # constants of nature
    c, nAir = 299792458, 1
    n = len(gc) if n is None else n
    fs = 2490000 if fs is None else fs

    # dependents on fs, n et al.
    ret.alpha = fs * ret.poly[-2]
    ret.freq_to_dist = c / (2*abs(ret.alpha)*nAir)
    ret.idx_to_distance = ret.freq_to_dist * fs / n

    return ret

# after refactoring, two functions are not necessary
compute_alpha = alpha_fit

def loose_compute_alpha(gasl, initial_rigour=0.7, tolerable_rms=1000e6):
    '''compute alpha, with the necessary lenience.

    initial_rigour - number in (0.0, 1.0), setting the threshold for peak
                     height; the closer to 1.0, the pickier we get as to
                     what a peak should be
    tolerable_rms  - maximum fit deviation to accept for an alpha fit. Good
                     values in ideal conditions are in the ballpark
                     of 60-100 MHz

    This performs a computation of the alpha coefficient applying
    a default threshold for peak height at 30% (=0.7 rigour) of the gas cell
    dynamic range. If this proves to fail, because the number/structure of
    the peak array is non-conformant to the HCN spectrum, or the fit
    has an rms error bigger than the tolerable_rms specified, the computation
    is repeated with rigour reduced by 10%, until the expected, correct fit
    with tolerable rms error is attained.

    In noisy (e.g. EMC test) conditions, rigour must sometimes be reduced
    beyond reasonable measures to make a decent fit, because spurious peaks
    are hard to weed out automatically
    '''

    rig = initial_rigour
    attempt = 1
    while rig > 0.1:
        try:
            fit = compute_alpha(gasl, rigour=rig)
            if fit.alpha_rms_error <= tolerable_rms:
                # fantastik suksesss!!!
                fit.attempt = attempt
                fit.rigour  = rig
                return fit
            else:
                condition = f'alphafit poor {fit.alpha_rms_error/1e6:.0f} MHz'
        except MalformedGasCell as e:
            condition = f'alphafit failed by malformed data'
        rig = rig / 1.1
        attempt += 1
        condition = condition + f': reducing rigour to {rig:5.3f}'
        logging.info(condition)
    else:
        # utter failure at alpha computation, bail out
        raise MalformedGasCell(f'fit impossible after {attempt} attempts, '
                                        f'rigour down to {rig}')
