#!/usr/bin/env python3
# vim: ts=8 sw=4 et sts=4 ai

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pickle

# configure matplotlib style from horrendous defaults
matplotlib.rcParams['figure.autolayout'] = True
matplotlib.rcParams['lines.linewidth'] = 0.3

plot_types = ('raw', 'spectra', 'peaks')

def plot_raw(run):
    '''plot the raw acquisition data.'''

    fig_raw = plt.figure()
    plt.subplot(3, 1, 1)
    plt.plot(run.gas, 'b-')
    plt.title('Raw Gas Cell Data')
    plt.subplot(3, 1, 2)
    plt.plot(run.ref, 'r-')
    plt.title('Raw Reference Cell Data')
    plt.subplot(3, 1, 3)
    plt.plot(run.meas, 'g-')
    plt.title('Raw Measurement Channel Data')

    return fig_raw

def plot_spectrum(run, chop=100, peaks=None):
    '''plot spectrum of the measured channel.'''

    if peaks is None:
        dist_to_idx = run.nspec / run.fs / run.freq_to_dist
        peaks = [round(p['distance'] * dist_to_idx)
                            for p in run.peaks]

    fig_fft = plt.figure()
    plt.subplot(2,1,1)
    plt.plot(run.distance[chop:], run.spec[chop:], 'b-')
    plt.plot(run.distance[peaks], run.spec[peaks], 'rx', ms=2.0)
    plt.title('Meas Channel FFT')
    plt.xlabel('distance [m]')

    plt.subplot(2,1,2)
    plt.semilogy(run.distance[chop:], run.spec[chop:], 'g-')
    plt.semilogy(run.distance[peaks], run.spec[peaks], 'rx', ms=2.0)
    plt.title('Meas Channel FFT (semilogy)')
    plt.xlabel('distance [m]')

    return fig_fft

def plot_peaks(run, peaks=None):
    '''plot peaks viewport of the measured channel.'''

    if peaks is None:       # FIXME: this must be true all the time
        dist_to_idx = run.nspec / run.fs / run.freq_to_dist
        peaks = [round(p['distance'] * dist_to_idx)
                            for p in run.peaks]

    margin = 100
    viewport_a = min(peaks) - margin
    viewport_b =  max(peaks) + margin

    chop = 0        # FIXME: suppress when proven this works
    fig_fft = plt.figure()
    plt.subplot(2,1,1)
    plt.plot(run.distance[viewport_a:viewport_b],
             run.spec[viewport_a:viewport_b], 'b-')
    plt.plot(run.distance[peaks], run.spec[peaks], 'rx', ms=2.0)
    plt.title('Meas Channel FFT')
    plt.xlabel('distance [m]')

    plt.subplot(2,1,2)
    plt.semilogy(run.distance[viewport_a:viewport_b],
                run.spec[viewport_a:viewport_b], 'g-')
    plt.semilogy(run.distance[peaks], run.spec[peaks], 'rx', ms=2.0)
    plt.title('Meas Channel FFT (semilogy)')
    plt.xlabel('distance [m]')

    return fig_fft

def show_plots(types, run):
    for typ in types:
        if typ == 'raw':
            fig = plot_raw(run)
            # fig.show()
        elif typ == 'spectra':
            fig = plot_spectrum(run)
            # fig.show()
        elif typ == 'peaks':
            fig = plot_peaks(run)
    plt.show(block=True)

if __name__ == '__main__':
    run = pickle.load(open('tmp.pickle', 'rb'))
    show_plots(plot_types, run)
