#!/usr/bin/env python3

from pprint import pprint
import itertools as it
import socket
import time

# some constants of nature
c  = 299792458
fs = 100e6
n  = 2490000
common_alpha = -250302909023330.97

def to_freq(distance, alpha=None):
    '''rough conversion from distance values to freq values.

    Note that this takes a fixed value of the alpha sweep speed for
    granted. It might require correction if alpha varies too much.
    '''

    alpha = common_alpha if alpha is None else alpha
    idx_to_distance = c / 2 / (-alpha)
                                # around 24um / bin
    distance_to_freq = fs / (n * idx_to_distance)
                                # around 1.67 MHz/m

    return distance * distance_to_freq

def to_dist(freq, alpha=None):
    '''rough conversion from freq values to distance values.

    Note that this takes a fixed value of the alpha sweep speed for
    granted. It might require correction if alpha varies too much.
    '''

    distance_to_freq = to_freq(1)
    return freq / distance_to_freq

# list of test parameters
class RadTestParams:
    def __init__(self, freq, modulation, depth, amp, comment=None):

        self.freq            = freq
        self.modulation      = modulation
        self.depth           = depth
        self.amp             = amp
        self.comment         = comment

        self._off            = False    # used to switch pert off

    def __bool__(self):
        return not self._off

    def off(self):
        self._off = True

    def __repr__(self):
        return (f'<RadTestParams freq={self.freq/1e6:.3f}MHz, '
                f'mod={self.modulation}Hz, depth={self.depth}%, '
                f'amp={self.amp} dBm, ({self.comment})>'
                if not self._off else '<RadTestParams OFF>' )

    def params(self):
        return dict((f'rt_{k}', getattr(self, k, None)) for k in self.__dict__)

MHz = 1e6
rad_tests = [
    # tests as perpetrated in July 2023; probably useless
    # i            freq           modulation   depth  ampl   comment
    RadTestParams( 100   * MHz,      1000,     80,    -12,   '',                  ),
    RadTestParams( 100   * MHz,   1000000,     80,    -12,   '',                  ),
    RadTestParams( 100   * MHz,    134819,     80,    -12,   '(0.08m distance)',  ),
    RadTestParams( 100   * MHz,    134819,     80,    -12,   '(0.08m distance)',  ),
    RadTestParams(  50   * MHz,      1000,     80,    -12,   '',                  ),
    RadTestParams(  49   * MHz,    134819,     80,    -12,   '',                  ),
    RadTestParams( 101   * MHz,    134819,     80,    -12,   '',                  ),
    RadTestParams( 100.1 * MHz,    134819,     80,    -12,   '',                  ),
    RadTestParams(  99.9 * MHz,    134819,     80,    -12,   '',                  ),
    RadTestParams( 100   * MHz,    138192,     80,    -12,   '(HLS distance)',    ),
    RadTestParams( 100   * MHz,  24867911,     80,    -12,   '(fiber distance)',  ),
    RadTestParams( 100   * MHz,    197590,     80,    -12,   '(ball distance',    ),
    RadTestParams( 100   * MHz,     50562,     80,    -12,   '(0.03m dist)',      ),
    RadTestParams( 100   * MHz,     67429,     80,    -12,   '(0.04m distance)',  ),
    RadTestParams( 100   * MHz,     84286,     80,    -12,   '(0.05m distance)',  ),
    RadTestParams( 100   * MHz,    101144,     80,    -12,   '(0.06m distance)',  ),
    RadTestParams( 100   * MHz,    118000,     80,    -12,   '(0.07m distance)',  ),
    RadTestParams( 100   * MHz,    134859,     80,    -12,   '(0.08m distance)',  ),
    RadTestParams( 100   * MHz,    168572,     80,    -12,   '(0.10m distance)',  ),
    RadTestParams( 100   * MHz,    337145,     80,    -12,   '(0.20m distance)',  ),
    RadTestParams( 100   * MHz,    505718,     80,    -12,   '(0.30m distance)',  ),
    RadTestParams( 100   * MHz,    674291,     80,    -12,   '(0.40m distance)',  ),
    RadTestParams( 100   * MHz,    842864,     80,    -12,   '(0.50m distance)',  ),
]

class RSSigGen:

    def __init__(self, host='cfb-864-allrfgen2', port=5025):
        '''set up the R&S signal generator.'''
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((host, port,))

    def __del__(self):
        self.cleanup()

    def cleanup(self):
        self.sock.close()
        time.sleep(1)

    def send_cmd(self, string):
        '''send a command to the R&S signal generator.'''
        cmd = bytes(f'{string}\r\n', 'ascii')
        self.sock.send(cmd)
        time.sleep(0.1)

    def read_answer(self):
        try:
            ret = self.sock.recv(1024, socket.MSG_DONTWAIT)
        except BlockingIOError as e:
            ret = b''
        return ret

    def off(self):
        '''switch siggen output off for unperturbed test.'''

        self.send_cmd('*rst')
        self.send_cmd('outp off')

    def setup_rad_test(self, freq, modulation, depth, amp,
                            host='cfb-864-allrfgen2', port=5025):
        '''send the parameters for a single rad test.

        freq        RF frequency in Hz
        modulation  LF/AM frequency in Hz
        depth       depth in percent units (e.g. 80)
        amp         signal amplitude/power in dBm (e.g. -12)
        '''

        self.send_cmd('*rst')
        self.send_cmd(f'freq {freq} Hz')
        
        self.send_cmd('am:source int')
        self.send_cmd(f'am:dept {depth} pct')
        self.send_cmd('am:state on')
        
        self.send_cmd('lfo:freq:mode fix')
        self.send_cmd(f'lfo:freq {modulation}')
        self.send_cmd('lfo on')

        self.send_cmd(f'pow {amp} dBm')
        
        self.send_cmd('mod:stat on')
        self.send_cmd('outp on')

    def get_rad_test_params(self):
        '''obtain the parameters for a single rad test.'''

        self.send_cmd('freq?')
        self.send_cmd('am:source?')
        self.send_cmd('am:depth?')
        self.send_cmd('am:state?')
        
        self.send_cmd('lfo:freq:mode?')
        self.send_cmd('lfo:freq?')
        self.send_cmd('lfo?')

        self.send_cmd(f'pow?')

        self.send_cmd('mod:stat?')
        self.send_cmd('outp?')

        ret = str(self.read_answer(), 'ascii').split('\n')
        fmt = '\n'.join([
                f'freq:           {ret[0]} Hz',
                f'am source:      {ret[1]}',
                f'am depth:       {ret[2]}%',
                f'am state:       {ret[3]}',
                f'lfo freq mode:  {ret[4]}',
                f'lfo freq:       {ret[5]} Hz',
                f'lfo:            {ret[6]}',
                f'pow:            {ret[7]} dBm',
                f'mod state on:   {ret[8]}',
                f'output on:      {ret[9]}',
                f'' ])
        return fmt

    def cmdloop(self):
        '''run a dumb command loop to test commands from the manual.'''

        while True:
            scpi = input('cmd> ')
            if scpi == '':
                return
            print(f'executing {scpi}')
            self.sock.send(bytes(f'{scpi}\r\n', 'ascii'))
            print(self.sock.recv(100))

def emc_test_series(start=100e3, stop=10e6, ratio=1.01,
                        mod_freq=1e3, mod_depth=80, amp=-12,
                        output='list'):

    '''generate a series of test parameters sweeping freqs.
        start			- initial frequency of sweep range
        stop			- final   frequency of sweep range
        ratio			- ratio between next and prev freqs
        mod_freq		- AM modulation frequency
        mod_depth		- AM modulation depth
        amp				- amplitud/power of signal
        output			- default 'list' gives a list of RadTestParams;
                          if output='csv' return a string in csv format
    '''

    series = []
    freq = start
    while freq <= stop:
        series.append(RadTestParams(int(freq), mod_freq, mod_depth, amp,
                            comment=f'distance = {to_dist(freq):.3f}'))
        # for simplicity, use integer frequencies
        freq = int(ratio * freq)
    if output == 'list':
        return series
    elif output == 'csv':
        import io
        import csv

        with io.StringIO('') as o:
            fieldnames = [ f'rt_{f}' for f in [
                        'freq', 'modulation', 'depth', 'amp', 'comment', ] ]
            c = csv.DictWriter(o, fieldnames=fieldnames)
            c.writeheader()
            for t in emc_test_series(start, stop, ratio, mod_freq, mod_depth, amp):
                c.writerow(t.params())
            content = o.getvalue()
        return content

def emc_full_test_run(unpert=5, pert=20,
                    start=100e3, stop=10e6, ratio=1.01,
                    mod_freq=1e3, mod_depth=80, amp=-12):
    '''create a full series of radiated tests.

    Returns an iterator with pairs (<RadTestParams>, index); it yields a
    sequence of pairs of the form
        (<RadTestParams OFF>, 0),
        (<RadTestParams OFF>, 1),
        ...
        (<RadTestParams OFF>, unpert-1),
        (<RadTestParams freq=... >, 0),
        (<RadTestParams freq=... >, 1),
        ...
        (<RadTestParams freq=... >, pert-1),
        (<RadTestParams freq=... >, 0),
        ...
        (<RadTestParams freq=... >, pert-1),
    to describe 'unpert' numbered repetitions of an unperturbed acq
    cycle, and then, 'pert' repetitions of each of the perturbed
    acquisitions sweeping the range
        start, start * ratio, start * ratio^2, ..... , stop

    Inputs:
        unpert          - numer of initial acqs without perturbation
        pert            - numer of acqs repeated for each perturbation type

        start			- initial frequency of sweep range
        stop			- final   frequency of sweep range
        ratio			- ratio between next and prev freqs
        mod_freq		- AM modulation frequency
        mod_depth		- AM modulation depth
        amp				- amplitud/power of signal
    '''

    # set up R&S to NOT produce perturbation
    no_perturbation = RadTestParams(start, mod_freq, mod_depth, amp, comment='reference')
    no_perturbation.off()
    unpert_run = zip(it.repeat(no_perturbation), range(unpert))

    # now produce the perturbation list * number of repetitions
    pert_run = it.product(
        iter(emc_test_series(
            start=start, stop=stop, ratio=ratio,
            mod_freq=mod_freq, mod_depth=mod_depth, amp=amp)),
        range(pert))
    return it.chain(unpert_run, pert_run)

def main():
    print(emc_test_series(start=100e3, stop=10e6, ratio=1.01, output='csv'))

def mmain():
    rs = RSSigGen()
    try:
        for t, iteration in emc_full_test_run(5, 20,
                        start=100e3, stop=10e6, ratio=1.01):
            if not t:
                print(f'iteration {iteration} - OFF -----------\n')
                rs.off()
                time.sleep(1)
            else:
                rs.setup_rad_test(t.freq, t.modulation, t.depth, t.amp, t.comment)
                print(f'iteration {iteration} ------------\n', rs.get_rad_test_params())
    except KeyboardInterrupt as e:
        print('closing stream...', end='')
        rs.cleanup()
        raise

if __name__ == '__main__':
    mmain()
