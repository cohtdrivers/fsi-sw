#!/usr/bin/env python3
# vim: ts=8 sw=4 et sts=4 ai

import argparse
import re
import math

import numpy as np
from scipy.io import loadmat
from matplotlib import pyplot as plt

def squarify(n):
    '''compute a decent row x cols size for matrix of plots.'''

    k = math.isqrt(n)
    if n == k*k:
        return (k,k)
    elif n > k*(k+1):
        return (k+1, k+1)
    else:
        return (k, k+1)

def display_matlab_file(args):

    pat = re.compile(args.labels)
    dc_off = args.dc_offset
    from_ = max(dc_off, args.from_)

    m = loadmat(args.fname, squeeze_me=True)
    xscale = m.get('idx_to_distance', 24.0e-6)
        
    print(f'''x scale: {xscale*1e6:7.4f} um/div''')
    if args.to_ is None:
        args.to_ = max(len(m[x])
            for x in m if isinstance(m[x], np.ndarray))
    rang = range(from_, args.to_)
    if not args.integer_xaxis:
        x = xscale * np.array(rang)

    keys = [ k for k in m if pat.match(k) ]
    nkeys = len(keys)

    rows, cols = squarify(nkeys)
    fig, axs = plt.subplots(rows, cols, figsize=(16, 9), squeeze=False)
    axit = list(reversed(axs.flatten()))

    for key in keys:
        ax = axit.pop()
        ax.plot(x, m[key][rang], lw=0.3)
        ax.set_title(key)
        ax.set_xlabel('m')
    plt.tight_layout()
    plt.show()

def matlab_plot_main():
    '''main entry point of the module.'''

    parser = argparse.ArgumentParser()
    parser.add_argument('fname')
    parser.add_argument('--from', dest='from_', type=int, default=0)
    parser.add_argument('--to', dest='to_', type=int, default=None)
    parser.add_argument('-l', '--labels', '--label-regex', default=r'[A-Z].*')
    parser.add_argument('--dc-offset', type=int, default=10)
    parser.add_argument('--integer-xaxis', '-i', action='store_true')

    args = parser.parse_args()
    display_matlab_file(args)

if __name__ == '__main__':
    matlab_plot_main()
