#!/usr/bin/env python3
# vim: ai ts=8 sw=4 et sts=4

# Keopsys CEFA EDFA M201 interface functions
# Author: Juan David Gonzalez Cobas
#
# (c) CERN 2023
# All rights reserved

"""
Support for the control of EDFA Keopsys model
CEFA-C-PB-LP-SM-18-MSA0-M201-FA-FA

described in Keopsys User Manual UM no. 70.
Products CEFA-C-HG CEFA-C-PB-LP CEFA-L-PB-LP - Platform M201
"""

import serial
import serial.tools.list_ports as lp

class IdentificationError(Exception):
    pass
class MismatchedAnswer(Exception):
    pass
class CommandUnknown(Exception):
    pass
class NotAuthorized(Exception):
    pass
class CommandNotValid(Exception):
    pass
class FailedReset(Exception):
    pass
class FailedPowerConfig(Exception):
    pass


class KeopsysEDFA:

    @classmethod
    def list_ports(cls):
            return '\n'.join(
                f'{p.device:30s} {p.manufacturer!s:20s} {p.serial_number!s:20s}'
                          for p in lp.comports())

    def __init__(self, port):
        '''initialize a KeopsysEDFA object.'''
        self._serial = serial.Serial(port, baudrate=19200,
            bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE, xonxoff=False,
            timeout=3)
        if not self.is_edfa():
            raise IdentificationError(f'could not identify EDFA at port {port}')
        self.port = self._serial.port
        self.err_code = None

    def disconnect(self):
        '''close associated serial port.'''
        self._serial.close()

    def raw_command(self, cmdline):
        '''executes a raw command without interpretation.'''
        self._serial.write(bytes(cmdline, 'ascii') + b'\r')
        ans = self._serial.read_until(b'\r')
        return str(ans, 'ascii').strip()

    def is_edfa(self):
        snu = self.raw_command('SNU?')
        return snu.startswith('SNU=')
    
    def get_variable(self, name):
        ans = self.raw_command(f'{name}?'.upper())
        if not ans.startswith(name.upper()):
            raise MismatchedAnswer(f'command {name} gave answer {ans}')
        n, value = ans.split('=')
        if ans.endswith('*'):
            return 'Command unknown'
        return value

    def set_variable(self, name, value):
        nameu = name.upper()
        cmd = f'{nameu}={value}'
        ans = self.raw_command(cmd)
        errstring = f'[{cmd}] => [{ans}]'
        if ans.startswith(f'{name}/'):
            raise CommandNotValid(errstring)
        elif ans.startswith(f'{nameu}!'):
            self.err_code = 'OK'
        elif ans.startswith(f'{nameu}*'):
            raise CommandUnknown(errstring)
        elif ans.startswith(f'{nameu}#'):
            raise NotAuthorized(errstring)
        elif ans.startswith(f'{nameu}$'):
            raise CommandNotValid(errstring)
        elif not ans.startswith(f'{nameu}'):
            raise MismatchedAnswer(errstring)

    def recover_factory_settings(self):
        '''recover all settings to factory defaults.'''
        ans = self.set_variable('RKP', 1)
        if ans != 'RKP!':
            raise FailedReset(f'answer = {ans}')

    @property
    def serial_number(self):
        return self.get_variable('snu')
    @property
    def version(self):
        return self.get_variable('ver')
    @property
    def case_temperature(self):
        return self.get_variable('tcr')

    @property
    def output_power(self):
        '''output power in dBm.'''
        return self.get_variable('pus')

    @property
    def control_mode(self):
        '''control mode (one of 0 = OFF, 1 = ACC, 2 = APC).'''
        breakpoint()
        return self.get_variable('ass')

    @control_mode.setter
    def control_mode(self, mode):
        breakpoint()
        ret = self.set_variable('ASS', f'{mode}')
        self.err_code = ret

    @property
    def setpoint_output_power(self):
        '''set point for output power in APC mode (ASS=2).'''
        return self.get_variable('cpu')

    @setpoint_output_power.setter
    def setpoint_output_power(self, value):
        ret = self.set_variable('cpu', value)
        self.err_code = ret

    @property
    def input_power(self):
        '''measured input power.'''
        return self.get_variable('pue')

    @property
    def output_power(self):
        '''measured output power.'''
        return self.get_variable('pus')

    @property
    def nominal_power(self):
        '''maximum set point output power.'''
        return self.get_variable('PON')

    @property
    def minimum_power(self):
        '''minimum set point output power.'''
        return self.get_variable('POM')

    def set_check_output_power(self, setpoint, threshold=0.15):
        '''Sets the output power and checks up to  ten times the measured value.'''

        self.setpoint_output_power = setpoint
        for i in range(10):
            err = abs(float(self.output_power) - setpoint)
            if (err < threshold):
                return True
        return False

    def configure_edfa(self, setpoint=14):
        '''set sensible default EDFA configuration.'''

        self.control_mode = 2               # APC
        self.setpoint_output_power = setpoint
                                # dBm, about 25 dBm giving ~1mW after /16
        if not self.set_check_output_power(setpoint):
            raise FailedPowerConfig('EDFA not stable around required power'
                f'{self.setpoint_output_power}')

    def display_config(self):
        '''display human-readable configuration of EDFA.'''
        return '\n'.join([
            f'mode = {self.control_mode}',
            f'requested output power = {self.setpoint_output_power}',
            f'measured output power = {self.setpoint_output_power} dBm',
            f'measured input power = {self.input_power} dBm',
            f'',])

'''
command list (manual, p. 20):

SNU?			Read Serial Number
DES?			Read the description of the device
SNC?			Read the customer serial number
SNC=			Write the customer serial number
PAR1?			User parameter 1
PAR1=
PAR2?			User parameter 2
PAR2=
VER?			Read Firmware Version
FWI?			Read Firmware Information
ASS?			Read the control mode
ASS=			Write the control mode
AST?			Read the autostart status
AST=			Write the autostart mode
CAT?			Read the case temperature
RKP			Recover factory settings

IC1?			Read the preamp diode current set point
IC1=			Write the preamp diode current set point
ID1?			Read the actual preamp diode current
TD1?			Read preamp diode temperature
IC2?			Read the prebooster diode current set point
IC2=			Write the prebooster diode current set point
ID2?			Read the actual prebooster diode current

IPW?			Read the current input power
OPW?			Read the current output power

PON?			Nominal power of the product
POM?			Minimum power setpoint
SOP?			APC setpoint
SOP=			Set the APC setpoint

GAMIN?			Red the minimum settable gain
GAMAX?			Read the maximum settable gain
CGA?			AGC setpoint
CGA=			Set the AGC setpoint

IPT1?			Read the input power alarm threshold
IPT1=			Set the input power alarm threshold
ALA?			Read Alarms
ALA=			Reset alarms
WAR?			Read warnings
LCM1?			Read the threshold for the automatic shutdown in case of high current in preamp
LCM2?			Read the threshold for the automatic shutdown in case of high current in preboost
PWM?			Read the output power mute
'''

'''
alarm bit meanings (p. 21):
  31			not used
  30                    not used
  29                    not used
  28                    not used
  27                    not used
  26                    not used
  25                    not used
  24                    not used
  23                    not used
  22                    not used
  21                    Board temperature error		CAT<TCLor CAT>TCH
  20                    Power supply error              power supply too low or too high
  19                    GPIO Key OFF activated          Key pin on the interface is low
  18                    not used                        
  17                    not used                        
  16                    not used                        
  15                    Low input power 1               IPW < IPT1
  14                    Low output power 1              OPW < OPT1 for ID1 > I1T (depend of the config)
  13                    Low input power 2               IPW2 < IPT2
  12                    Low output power 2              OPW2 < OPT2 for ID2 > I2T (depend of the config)
  11                    Preamp current error            
  10                    Booster current error           Alarm if IDx>LCMx
   9                    not used                        Persistant alarm (reset only if KeyOFF or ALA=0)
   8                    not used
   7                    not used
   6                    not used
   5                    not used
   4                    not used
   3                    preamp temp error
   2                    booster temp error
   1                    not used
   0                    not used
'''
