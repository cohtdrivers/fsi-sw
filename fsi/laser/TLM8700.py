#! /usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

'''
TLM-8700 laser module RS-232/USB interface functions, based on the "legacy" protocol.

Derived from original TLM8700.py module by Jaroslaw Rutkowski
(c) CERN 2022
'''

import os.path
import configparser
import time

import serial

from . laser_error_codes import error_codes
from . import laser_get_config

class UnitIsNotTLM(Exception):
    pass
class TimeOut(Exception):
    pass
class CommandError(Exception):
    pass
class ArgumentError(Exception):
    pass
class ExecutionError(Exception):
    pass

class Tlm8700Wrapper:
    ACK = '*'
    CMD_ERR = '!'
    ARG_ERR = '#'
    EXE_ERR = '&'

    CMD_XLAT = {
        ACK : 'ACK',
        CMD_ERR : 'CMD_ERR',
        ARG_ERR : 'ARG_ERR',
        EXE_ERR : 'EXE_ERR',
    }

    HW_LOCKED_SW_UNLOCKED   = 2
    BOTH_LOCKED             = 3

    def __init__(self, comPort, timeout=3):
        """open serial connection with the laser module.

        Keyword arguments:
        ------------------
        comPort -- laser COM port (string)
        timeout -- max response time in seconds
        """

        # as per sec 5.1 of the manual, 115200 8N1
        self.ser = serial.Serial(comPort, baudrate=115200, timeout=timeout,
            bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE)
        if not self.is_tlm8700():
            raise UnitIsNotTLM(f'port {comPort} failed to identify as a TLM8699')
        self.serial = comPort

    def disconnect(self):
        """close the serial connection with the laser module."""
        self.ser.close()

    def raw_cmd(self, cmdline):
        '''execute a single command cycle.'''
        line = bytes(cmdline, 'ascii') + b'\n'
        self.ser.write(line)
        self.last_answer = self.ser.read_until(b'\n\r')
        if self.last_answer[-2:] != b'\n\r':
            raise TimeOut(f'timed out at: [{cmdline}]')
        self.last_answer = str(self.last_answer.rstrip(), 'ascii')
        if self.last_answer == self.CMD_ERR:
            raise CommandError(f'cmd error executing [{cmdline}]')
        elif self.last_answer == self.ARG_ERR:
            raise ArgumentError(f'bad argument executing [{cmdline}]')
        elif self.last_answer == self.EXE_ERR:
            raise ExecutionError(f'error executing [{cmdline}]')
        return self.last_answer

    def query(self, cmdline):
        '''execute a query (command ending in ?).'''
        return self.raw_cmd(cmdline)

    def command(self, cmdline):
        '''execute a command (command with side effects).'''
        self.err = self.raw_cmd(cmdline)
        return self.err == Tlm8700Wrapper.ACK

    def is_tlm8700(self):
        """check whether the device is actually the TLM8700 laser module.
        """
        ver = self.device_id
        model = ';' in ver and ver.split(';')[1]
        return model == 'TLM8700'

    @property
    def device_id(self):
        """laser device ID string."""
        return self.query('id?')

    @property
    def state(self):
        'laser ON/OFF state, ON=1, OFF=0'
        return self.query('laz?')

    @state.setter
    def state(self, state):
        state = str(state)
        arg = 1 if state.lower() in ['on', '1'] else 0
        command = f'laz {arg}'
        try:
            ret = self.command(command)
            return ret
        except ExecutionError as e:
            print(f'cannot execute [{command}], check laser lock status')

    @property
    def lock(self):
        """lock status of the laser."""
        return self.query('int?')

    @lock.setter
    def lock(self, value):
        if self.command(f'int {value}'):
            return 
        state = self.query('int?')
        if int(state) in [self.HW_LOCKED_SW_UNLOCKED, self.BOTH_LOCKED]:
            raise ValueError('Laser module hardware interlock is locked, please unlock with dedicated key!')

    @property
    def pmax(self):
        '''laser max power.'''
        return self.query('pmax?')
    @property
    def pmin(self):
        '''laser min power.'''
        return self.query('pmin?')

    @property
    def power(self):
        '''laser output power setting.'''
        return self.query('pwr?')

    @power.setter
    def power(self, value):
        return self.command(f'pwr {value}')

    @property
    def power_unit(self):
        '''laser output power unit.'''
        return self.query('pwru?')

    @power_unit.setter
    def power_unit(self, unit):
        return self.command(f'pwru {unit}')

    @property
    def domain(self):
        '''laser tuning domain, 0 = wavelength (nm), 1 = frequency (GHz).'''
        return self.query('unit?')

    @domain.setter
    def domain(self, dom):
        self.command(f'unit {dom}')

    @property
    def wmax(self):
        'maximum laser tuning setpoint.'
        return self.command('wmax?')
    @property
    def wmin(self):
        'minimum laser tuning setpoint.'
        return self.command('wmin?')

    @property
    def modulation_source(self):
        'laser modulation source: 0 no mod; 1 coherence ctrl; 3: ext.  analog.'
        return self.query('sms?')

    @modulation_source.setter
    def modulation_source(self, source):
        self.command(f'sms {source}')

    @property
    def start(self):
        'laser scan start setpoint.'
        return self.query('str?')
    @start.setter
    def start(self, f0):
        self.command(f'str {f0}')

    @property
    def stop(self):
        'laser scan stop setpoint.'
        return self.query('stop?')
    @stop.setter
    def stop(self, f1):
        self.command(f'stop {f1}')

    @property
    def speed(self):
        return self.query('spd?')
    @speed.setter
    def speed(self, sp):
        return self.command(f'spd {sp}')

    @property
    def mode(self):
        return self.query(f'mode?')
    @mode.setter
    def mode(self, args):
        return self.command(f'mode {args}')

    def operation_complete(self):
        opc_xlat = {
           '-2' : 'unrecoverable error',
           '-1' : 'recoverable error',
            '0' : 'operation pending',
            '1' : 'operation complete',
            '2' : 'module initializing',
        }
        st = self.query('opc?')
        return f'{st} ({opc_xlat[st]})'

    def error(self):
        # FIXME: this can crash for several reasons,
        # mainl KeyError exceptions
        m = self.query('err?')
        errors = [ f'{err:4}: {error_codes[err]}'
                    for err in m.split(';')]
        return '\n'.join(errors)

    @property
    def dwell_time(self):
        '''laser dwell time between scans.'''
        return self.query('dwl?')
    @dwell_time.setter
    def dwell_time(self, args):
        self.command(f'dwl {args}')

    @property
    def cycles(self):
        '''number of scans to perform.'''
        return self.query('num?')
    @cycles.setter
    def cycles(self, args):
        self.command(f'num {args}')

    def scan(self):
        '''do laser scan cycle.'''
        self.command(f'scan')
    def abort(self):
        '''abort scan cycle.'''
        self.command(f'abort')

    @property
    def ext_temp(self):
        '''environmental temperature.'''
        return self.query('tmpe?')

    @property
    def uptime(self):
        '''operating hours of the laser diode.'''
        return self.query('ophours?')

    @property
    def cnt(self):
       '''number of scan iterations to go.'''
       return self.query('cnt?')

    @property
    def trigger_pol(self):
        return self.query('trpol?')
    @trigger_pol.setter
    def trigger_pol(self, pol):
        self.command(f'trpol {pol}')

    def configure_laser(self, on=1, power=3.0, cycles=1):
        '''set a bare default startup laser conf.'''

        self.lock       = 0
        self.state      = on
        self.power_unit = 1         # mW
        self.power      = power
        self.domain     = 0
        self.modulation_source = 0
        self.start      = 1520      # nm
        self.stop       = 1570      # nm
        self.mode       = 2         # uni_forward
        self.speed      = 2000      # nm/s
        self.cycles     = cycles
        self.trigger_pol = 1        # active_high

    def display_config(self):
        '''human-readable version of current config.'''
        return '\n'.join([
            f'serial      = {self.serial}',
            f'interlock   = {self.lock}',
            f'state       = {self.state}',
            f'unit        = {self.power_unit}',
            f'power       = {self.power}',
            f'domain      = {self.domain}',
            f'mod_source  = {self.modulation_source}',
            f'start       = {self.start}',
            f'stop        = {self.stop}',
            f'scan_mode   = {self.mode}',
            f'scan_speed  = {self.speed}',
            f'cycles      = {self.cycles}',
            f'trigger_pol = {self.trigger_pol}',
            f''])

