#!/usr/bin/env python3

import configparser

class LaserConfig:
    '''laser configuration parameters.

    Possible values (numerics are correlative to literals):

        serial     = serial_port
        interlock  = lock|unlock|1|0
        state      = on|off|1|0
        unit       = mW|dBm|1|0
        power      = number (power unit)
        domain     = nm|GHz|wavelength|frequency|0|1
        mod_source = no_mod|coherence|ext_analog|0|1|3
        start      = number (domain units)
        stop       = number (domain units)
        scan_mode  = auto_step|uni_forward|bidirectional|uni_reverse|1|2|3|4
        scan_speed = number (domain units/s)
        cycles     = number
        trigger_pol = active_low|active_high|0|1
    '''

    def __init__(self):
        '''default values if no configuration explicitly given.'''

        self.serial     = '/dev/ttyUSB0'
        self.interlock  = 0
        self.state      = 1
        self.unit       = 1         # mW
        self.power      = 1.0
        self.domain     = 0
        self.mod_source = 0
        self.start      = 1520      # nm
        self.stop       = 1570      # nm
        self.scan_mode  = 2         # uni_forward
        self.scan_speed = 2000      # nm/s
        self.cycles     = 1
        self.trigger_pol = 1        # active_high

    def __str__(self):
        return '\n'.join([
            f'serial      = {self.serial}',
            f'interlock   = {self.interlock}',
            f'state       = {self.state}',
            f'unit        = {self.unit}',
            f'power       = {self.power}',
            f'domain      = {self.domain}',
            f'mod_source  = {self.mod_source}',
            f'start       = {self.start}',
            f'stop        = {self.stop}',
            f'scan_mode   = {self.scan_mode}',
            f'scan_speed  = {self.scan_speed}',
            f'cycles      = {self.cycles}',
            f'trigger_pol = {self.trigger_pol}',
            f''])

    def load(self, fname):
        '''read configuration values from file named fname.'''

        # fetch laser configuration section from fname
        self.cfg = configparser.ConfigParser(
                                inline_comment_prefixes=('#', ';',))
        self.cfg.read(fname)
        if 'laser' not in self.cfg:
            return

        lc = self.cfg['laser']

        self.serial     = lc['port']
        self.interlock  = lc['interlock']
        self.state      = lc['state']
        self.unit       = lc['unit']
        self.power      = lc['power']
        self.domain     = lc['domain']
        self.mod_source = lc['mod_source']
        self.start      = lc['start']
        self.stop       = lc['stop']
        self.scan_mode  = lc['scan_mode']
        self.scan_speed = lc['scan_speed']
        self.cycles     = lc['cycles']
        self.trigger_pol = lc['trigger_pol']
