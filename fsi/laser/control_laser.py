#!/usr/bin/env python3
# vim: ai ts=8 sw=4 et sts=4 

import sys
import time
import argparse

import cmd2 as cmd
import serial
import serial.tools.list_ports as lp

from . import TLM8700

class Cli(cmd.Cmd):

    prompt = 'TLM8700> '

    def do_list_serials():
        for port in lp.comports():
            print('{:48} {:16s} {:32s}'.format(
                port.device, port.description, port.name))

    def __init__(self, laser):
        cmd.Cmd.__init__(self,
            persistent_history_file='~/.tlm8700_history',
            allow_cli_args=False)
        self.laser = laser
        self.register_postcmd_hook(self.print_command_status)

    def do_last_answer(self, args):
        '''show last reply from laser cmdline.'''
        print(f'last answer: [{self.laser.last_answer}]')
        print(f'last_cmd_code: {self.laser.err}')

    def do_temperature(self, args):
        """measure external temperature."""
        temp = self.laser.ext_temp
        print(f'{temp} C')

    def do_scan(self, args):
        """start scan with configured parameters."""
        if self.laser.lock != '0':
            print('WARNING: will not scan, laser locked!')
            return
        if self.laser.state != '1':
            print('WARNING: will not scan, laser is OFF!')
            return
        self.laser.scan()
        if self.laser.err != self.laser.ACK:
            print(f'scan command failed with err {self.laser.err}')
            return

    def do_abort(self, args):
        '''abort current series of scans.'''
        self.laser.abort()

    def do_lock(self, args):
        """show interlock status."""
        lock_xlat = {
            '3' : 'both interlocks active',
            '2' : 'hardware interlock active',
            '1' : 'software interlock active',
            '0' : 'both interlock deactivated',
        }
        if not args:
            state = self.laser.lock
            print(f'{state} ({lock_xlat[state]})')
        elif args in ['on', '1']:
            self.laser.lock = 1
        elif args in ['off', '0']:
            self.laser.lock = 0
        else:
            print('usage: lock {on|off|0|1}')


    def do_unlock(self,args):
        self.do_lock('off')

    def do_cycle_scan(self, args):
        """start series of scans at regular intervals."""
        try:
            args = args.split()
            period = float(args[0])
            n = int(args[1]) if (len(args) > 1) else -1 
        except IndexError as e:
            print('usage: cycle_scan period [ntimes]')
            print('    * repetition period in seconds (e.g. 1.2)')
            print('    * without ntimes, repeat infinitely')
            return
        except Exception as e:
            raise

        scans = 0
        total = n if n >= 0 else 'inf'
        while n != 0:
            self.do_scan('')
            time.sleep(period)
            n -= 1
            scans += 1
            print(f'scan: {scans}/{total} scans to go')

    def do_id(self, args):
        """print identity of laser."""
        ID = self.laser.device_id
        print(f'ID string: {ID}')
        brand, model, cust_ser_num, nf_ser_num, firmware_version, \
                *fpga_version = ID.split(';')
        descr = '\n'.join([
            f'Manufacturer: {brand}',
            f'Model: {model}',
            f'Cust. Ser. #: {cust_ser_num}',
            f'NF. Ser. #:   {nf_ser_num}',
            f'Firmware version: {firmware_version}',
            f'FPGA version: {fpga_version}',
            f''])
        print(descr)

    def do_up_time(self, args):
        """show up time. (NB: not implemented in current specimen)"""
        print(f'{self.laser.uptime} hours')

    def do_configure(self, args):
        """configure laser with defaults."""
        self.laser.configure_laser()

    def do_sleep(self, args):
        '''sleep a number of seconds.'''
        try:
            secs = float(args)
        except ValueError as e:
            print('usage: sleep <seconds (can be float)>')
            return
        time.sleep(secs)

    def do_state(self, args):
        """display laser ON/OFF state."""
        xlat_state = { '1' : 'on', '0' : 'off' }
        if not args:
            st = self.laser.state
            print(f'laser {xlat_state[st]} ({st})')
        else:
            args = args.lower()
            if args in ['on', '1']:
                args = 1
            elif args in ['off', '0']:
                args = 0
            else:
                print('usage: state [on|off|1|0]')
                return
            self.laser.state = args

    def do_operation_complete(self, args):
        """display status of operation completion."""
        print(self.laser.operation_complete())

    def do_power(self, args):
        """display/set laser power."""
        if not args:
            unit = 'dBm' if self.laser.power_unit == '0' else 'mW'
            print(f'{self.laser.power} {unit}')
        else:
            try:
                self.laser.power = float(args)
            except Exception as e:
                print('usage: set_power <power in mW>')
                return

    def do_domain(self, args):
        xlat_domain = { '1' : 'frequency (GHz)', '0' : 'wavelength (nm)' }
        if not args:
            dom = self.laser.domain
            print(f'{xlat_domain[dom]} ({dom})')
        else:
            if args in ['1', 'GHz', 'frequency']:
                args = '1'
            elif args in ['0', 'nm', 'wavelength']:
                args = '0'
            else:
                print('usage: domain [1|0|nm|GHz|wavelength|frequency]')
                return
            self.laser.domain = args

    def do_modulation(self, args):
        xlat_modulation = {
            '0' : 'no modulation',
            '1' : 'coherence control',
            '3' : 'external analog',
        }
        if not args:
            txt = xlat_modulation[self.laser.modulation_source]
            print(f'{txt} ({self.laser.modulation_source})')
        elif args.lower() in ['none', '0']:
            self.laser.modulation_source = 0
        elif args.lower() in ['coherence', '1']:
            self.laser.modulation_source = 1
        elif args.lower() in ['ext', '3']:
            self.laser.modulation_source = 3
        else:
            print('usage: modulation [none|coherence|ext|0|1|3]')

    def do_start(self, args):
        """get/set scan start."""
        if not args:
            print(self.laser.start)
        else:
            self.laser.start = args

    def do_stop(self, args):
        """get/set scan stop."""
        if not args:
            print(self.laser.stop)
        else:
            self.laser.stop = args

    def do_speed(self, args):
        """get/set scan speed."""
        if not args:
            print(self.laser.speed)
        else:
            self.laser.speed = args

    def do_mode(self, args):
        """get/set scan mode."""
        args = str(args) if args else None
        xlat_mode = {
           '0' : 'none',        # FIXME: undocumented, unknown mode number
           '1' : 'auto_step',
           '2' : 'uni_forward',
           '3' : 'bidirectional',
           '4' : 'uni_reverse',
        }
        if not args:
            md = self.laser.mode
            txt = xlat_mode[md]
            print(f'{txt} ({md})')
        elif args in xlat_mode:
            self.laser.mode = args
        elif args in xlat_mode.values():
            key = [k for k in xlat_mode if xlat_mode[k] == args][0]
            self.laser.mode = key
        else:
            print('usage: mode [1|2|3|4|auto_step|uni_forward|bidirectional|uni_reverse]')

    def do_dwell(self, args):
        """display laser dwell time."""
        if not args:
            print(self.laser.dwell_time)
        else:
            self.laser.dwell_time = args
    def do_cycles(self, args):
        """display scan cycle count."""
        if not args:
            print(self.laser.cycles)
        else:
            self.laser.cycles = args

    def do_cnt(self, args):
        '''display number of scan cycles to go.'''
        print(self.laser.cnt)

    def do_trigger_polarity(self, args):
        '''display trigger polarity.'''
        xlat_tp = {
            '0' : 'negative = active LOW',
            '1' : 'positive = active HIGH',
        }
        if not args:
            tp = self.laser.trigger_pol
            print(f'{tp}: {xlat_tp[tp]}')
        elif args.lower() in ['0', 'low']:
            self.laser.trigger_pol = '0'
        elif args.lower() in ['1', 'high']:
            self.laser.trigger_pol = '1'
        else:
            print('usage: trigger_polarity r[{0|1|low|high]')

    def do_err(self, args):
        """display complete list of current error conditions."""
        print(self.laser.error())

    def do_status(self, args):
        """display full, human-readable laser status."""
        l = self.laser
        ID = l.device_id
        state = l.state
        lock = l.lock
        start, stop, speed = l.start, l.stop, l.speed
        mode = l.mode
        power = l.power
        opc = l.operation_complete()
        domain = l.domain

        status_fmt = f"""
    ID: {ID}
    State: {state}       Power: {power}
    Lock:  {lock}

    Domain: {domain}
    Scan:  start: {start}
           stop:  {stop}
           speed: {speed}
           mode:  {mode}

    Operation: {opc}
        """
        print(status_fmt)

    def do_raw(self, args):
        '''pass a raw command line to the laser.'''
        print(self.laser.raw_cmd(args))

    def do_serials(self, args):
        list_serials()

    def print_command_status(self, data: cmd.plugin.PostcommandData) -> cmd.plugin.PostcommandData:
        commands = '''state lock power power_unit domain
                modulation_source start stop speed mode
                dwell cycles trigger_pol'''.split()
        noarg_commands = '''scan abort next rst reboot'''.split()
        if data.statement.command in noarg_commands or (
           data.statement.command in commands and data.statement.args):
            err_code = self.laser.CMD_XLAT.get(self.laser.last_answer, self.laser.last_answer)
            print(err_code)
        return data

    def do_defaults(self, args):
        '''leave laser in its most frequently used state.'''
        self.laser.configure_laser(self)

    def do_EOF(self, args):
        return True

    def do_h(self, args):
        self.do_help(args)
    def help_h(self):
        self.do_help('help')

    # useful aliases
    do_q = do_EOF
    do_st = do_status


default_serial = '/dev/serial/by-id/laser'

def main():
    '''entry point for control_laser.'''

    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--list-serials', action='store_true',
                                help='list serial ports in system and exit')
    parser.add_argument('--port', default=default_serial,
                                help='TLM8700 serial control port')
    parser.add_argument('--no-config', action='store_true',
                                help='do not configure laser at startup')

    args = parser.parse_args()
    port = args.port
    if args.list_serials:
        Cli.do_list_serials()
        sys.exit()

    try:
        laser = TLM8700.Tlm8700Wrapper(port)
    except serial.serialutil.SerialException as e:
        print(f'cannot open {port}, exiting')
        sys.exit()
    except TLM8700.TimeOut as e:
        print(f'talking to {port}: {e!s} exiting')
        sys.exit()
    except TLM8700.UnitIsNotTLM as e:
        print(f'port {port} could not be identified as TLM-8700')
        sys.exit()

    if not args.no_config:
        laser.configure_laser()
    print(laser.display_config())
    cli = Cli(laser)
    cli.cmdloop()
