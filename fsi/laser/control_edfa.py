#!/usr/bin/env python3
# vim: ai ts=8 sw=4 et sts=4 

import math
import cmd2 as cmd
import serial
import serial.tools.list_ports as lp

from . import edfa

def mW(dBm):
    return math.pow(10, float(dBm)/10.0)
def dBm(mW):
    return 10.0 * math.log10(mW)

class Cli(cmd.Cmd):

    prompt = 'EDFA> '


    def __init__(self, port='/dev/serial/by-id/edfa'):
        cmd.Cmd.__init__(self, allow_cli_args=False,
                persistent_history_file='~/.edfa_history')
        self.edfa = None
        if port is not None:
            self.do_connect(port)

    def do_serial_devices(self, args):
        '''list serial devices in the system.'''
        print(edfa.KeopsysEDFA.list_ports())

    def do_connect(self, port):
        '''connect to specified serial port.'''
        self.edfa = edfa.KeopsysEDFA(port)
        prompt = f'EDFA({port})> '

    def do_port(self, args):
        '''port to which we are connected.'''
        print(self.edfa.port)

    def do_raw(self, args):
        '''pass a raw command line to the laser.'''
        print(self.edfa.raw_command(args))

    def do_serial_number(self, args):
        '''EDFA serial number.'''
        print(self.edfa.serial_number)

    def do_identification(self, args):
        '''display identification data.'''
        e = self.edfa
        print(f'''
        Port: {e.port:>21s}
        Serial number: {e.serial_number:>12s}        Description: {e.description}
        Version: {e.version:>12s}        Firmware information: {e.firmware_information}
        ''')

    def do_factory_reset(self, args):
        '''reset setting to factory defaults.'''
        self.edfa.recover_factory_settings()

    def do_control_mode(self, args):
        '''control mode (one of 0 = OFF, 1 = ACC, 2 = APC).'''
        xlat = { 0 : 'OFF', 1 : 'ACC', 2 : 'APC', }
        rxlat = { 'OFF' : 0, 'ACC' : 1, 'APC' : 2,
                    '0' : 0, '1'   : 1, '2'   : 2, }
        if not args:
            mode = int(self.edfa.control_mode)
            print(f'mode {mode} = {xlat[mode].upper()}')
            return
        mode = rxlat.get(args.args.upper())
        if mode is not None:
            self.edfa.control_mode = mode
        else:
            print(f'invalid mode "{args}"')

    def do_autostart(self, args):
        '''autostart mode (one of 0/1 = OFF/ON).'''
        xlat = { 0 : 'OFF', 1 : 'ON', }
        rxlat = { 'OFF' : 0, 'ON' : 1, '0' : 0, '1' : 1, }
        if not args:
            mode = int(self.edfa.autostart)
            print(f'mode {mode} = {xlat[mode].upper()}')
            return
        mode = rxlat.get(args.args.upper())
        if mode is not None:
            self.edfa.autostart = mode
        else:
            print(f'invalid mode "{args}"')

    def do_case_temperature(self, args):
        '''show case temperature.'''
        print(float(self.edfa.case_temperature)/100, 'C')

    def do_measured_power(self, args):
        '''show measured input and output power.'''
        ipw, opw = self.edfa.input_power, self.edfa.output_power
        print(f'Input  power: {ipw} dBm  ({mW(ipw):.1f} mW)')
        print(f'Output power: {opw} dBm  ({mW(opw):.1f} mW)')

    def do_nominal_limits(self, args):
        '''nominal max and min output power setpoints in APC mode.'''
        pon, pom = self.edfa.nominal_power, self.edfa.minimum_power
        print(f'Max nominal power: {pon:>2s} dBm  ({mW(pon):5.1f} mW)')
        print(f'Min nominal power: {pom:>2s} dBm  ({mW(pom):5.1f} mW)')

    def do_power(self, args):
        '''APC mode power set point.'''
        if not args:
            po = self.edfa.setpoint_output_power
            print(f'APC setpoint power: {po} dBm ({mW(po):4.1f} mW)')
            return
        args = args.strip()
        if args.endswith('mW'):
            sop = dBm(float(args[:-2]))
        elif args.endswith('dBm'):
            sop = float(args[:-3])
        else:
            sop = float(args)
        min_, max_ = float(self.edfa.minimum_power), float(self.edfa.nominal_power)
        if sop < min_ or sop > max_:
            print(f'setpoint {sop} not within range ({min_}, {max_}) dBm')
            return
        self.edfa.setpoint_output_power = sop

    def do_EOF(self, args):
        return True
    def do_h(self, args):
        self.do_help(args)
    def help_h(self):
        self.do_help('help')

    # useful aliases
    do_q = do_EOF

def main():
    '''edfa module entry point for console script.'''

    import argparse

    default_serial = '/dev/serial/by-id/edfa'

    parser = argparse.ArgumentParser()
    parser.add_argument('--port', default=default_serial,
                                help='KeopsysEDFA serial control port')
    args = parser.parse_args()
    try:
        cli = Cli(port=args.port)
        cli.cmdloop()
    except Exception as e:
        print(e)
