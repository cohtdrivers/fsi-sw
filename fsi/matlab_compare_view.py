#!/usr/bin/env python3
# vim: ts=8 sw=4 et sts=4 ai

import math
import argparse
import numpy as np
import scipy.io
from matplotlib import pyplot as plt

def squarify(n):
    '''compute a decent row x cols size for matrix of plots.'''

    k = math.isqrt(n)
    if n == k*k:
        return (k,k)
    elif n > k*(k+1):
        return (k+1, k+1)
    else:
        return (k, k+1)

class MatlabFile:

    def __init__(self, fname, fro=None, to=None):
        '''initialize data on a MATLAB file.'''

        m = scipy.io.loadmat(fname)
        self.fname = fname
        self.m = m
        # dirtiest way to weed out non-channels
        labels = [k for k in m if isinstance(m[k], np.ndarray)
                        and len(m[k][0]) > 100]
        self.labels = list(enumerate(labels))
        self.nchannels = len(labels)
        self.spectral = 'idx_to_distance' in m
        self.fro, self.to = fro, to

class MatlabFilesCompare:
    # same class but shows the files for comparison,
    # by showing the each identical named channel on the same plot, but
    # in different colors.
    def __init__(self, fnames, fro=None, to=None, blind_fft=False):
        # load all files
        self.m = []
        self.fnames = fnames
        self.blind_fft = blind_fft
        for fname in fnames:
            tmp = MatlabFile(fname, fro=fro, to=to)
            if blind_fft:
                # find all channels with "BLIND" in the name
                blind_channels = [k for k in tmp.m if 'BLIND' in k]
                print(f"blind channels: {blind_channels}")
                for k in blind_channels:
                    print(f"before: {tmp.m[k]}")
                    #tmp.m[k][0] = compute_fft(tmp.m[k][0], 2490000)
                    fft = compute_fft(tmp.m[k][0], 2490000)
                    tmp.m[k] = np.array([fft])
                    print(f"after: {tmp.m[k]}")
            self.m.append(tmp.m)

        # assert that all files have the same channels
        # and put the channels in the same order
        labels = [k for k in self.m[0] if isinstance(self.m[0][k], np.ndarray) and len(self.m[0][k][0]) > 100]
        for m in self.m:
            assert [k for k in m if isinstance(m[k], np.ndarray) and len(m[k][0]) > 100] == labels, 'files have different channels'
        self.labels = list(enumerate(labels))
        self.nchannels = len(labels)
        self.fro, self.to = fro, to
        self.spectral = 'idx_to_distance' in self.m[0]

    def namelist(self):
        lines = [ f'{self.nchannels:d} channels, spectral: {self.spectral}' ]
        lines.extend([ f'{i:3d}: {label}' for i, label in self.labels])
        lines.extend([''])
        return '\n'.join(lines)
    
    def plot_spectrum(self, label, axes=None):
        if not self.spectral:
            print(f'{label} not a spectral channel')
            return
        v = self.m[0][label][0]
        x = self.m[0]['idx_to_distance'][0] * np.arange(len(v))
        fro, to = self.fro, self.to
        for i in range(len(self.fnames)):
            axes.plot(x[fro:to], self.m[i][label][0][fro:to], lw=0.5)
        axes.set_xlabel('distance [m]')
        axes.set_title(f'{label}')
        
    def plot_raw(self, label, axes=None):
        if self.spectral:
            print(f'{label} not a raw channel')
            return
        v = self.m[0][label][0]
        fro, to = self.fro, self.to
        for i in range(len(self.fnames)):
            axes.plot(self.m[i][label][0][fro:to], lw=0.5)
        axes.set_xlabel('sample [10ns/sample]')
        axes.set_title(f'{label}')
    
    def plot_blind_fft(self, label, axes=None):
        v = self.m[0][label][0]
        fro, to = self.fro, self.to
        for i in range(len(self.fnames)):
            axes.plot(self.m[i][label][0][fro:to], lw=0.5)
        axes.set_xlabel('non-lin fft')
        axes.set_title(f'FFT_{label}')

    def plot_all(self, labels=None):
        if labels is None:
            labels = [l for (i, l) in self.labels]
        n = len(labels)
        cols, rows = squarify(n)
        plt.subplots(rows, cols, squeeze=False, figsize=(24,18),
                                    layout='constrained')
        for i, label in enumerate(labels):
            ax = plt.subplot(rows, cols, i+1)
            if self.spectral:
                self.plot_spectrum(label, ax)
            else:
                if 'BLIND' in label and self.blind_fft:
                    print(f'plotting fft of {label}')
                    self.plot_blind_fft(label, ax)
                else: 
                    self.plot_raw(label, ax)
        plt.show()

def main():
    '''main program, parse arguments.'''
    parser = argparse.ArgumentParser()

    # add n -i options arguments for input files
    parser.add_argument('-i', '--input', action='append', dest='fnames', default=[],
                                help='input file')
    # add n -l options arguments for labels
    parser.add_argument('-l', '--label', action='append', dest='labels', default=[],
                                help='label to plot')
    parser.add_argument('--fro', type=int, default=None,
                                help='plot from  given index')
    parser.add_argument('--to',  type=int, default=None,
                                help='plot up to given index')
    # add --blind_fft boolean option (present: True, absent: False)
    parser.add_argument('--blind-fft', action='store_true', default=False, help='compute fft of BLIND channels')

    options = parser.parse_args()

    m = MatlabFilesCompare(options.fnames, fro=options.fro, to=options.to, blind_fft=options.blind_fft)
    if not options.labels:
        print(m.namelist())
        m.plot_all()
        return
    elif 'all' in options.labels:
        m.plot_all()
        return
    else:
        m.plot_all(options.labels)
        return
        
if __name__ == '__main__':
    main()
