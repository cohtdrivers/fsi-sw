#!/usr/bin/env python3
# vim: ts=8 sw=4 sts=4 et ai

import numpy as np
from numpy import pi
from lmfit import Parameters, minimize

def gaussian(A, mu, sigma, x, offset=0):
    p = (x - mu) / sigma
    ret = offset + A * np.exp(-np.square(p/2))
    return ret

def lorentzian(A, mu, sigma, x, offset=0):
    p = (x - mu) / sigma
    ret = offset + A / (1 + np.square(p))
    return ret

def gauss_residual(params, x, y):
    parvals = params.valuesdict()
    A     = parvals['A']
    mu    = parvals['mu']
    sigma = parvals['sigma']

    return y - gaussian(A, mu, sigma, x)

def lorentz_residual(params, x, y):
    parvals = params.valuesdict()
    A     = parvals['A']
    mu    = parvals['mu']
    sigma = parvals['sigma']

    return y - lorentzian(A, mu, sigma, x)

def gauss_fit(x, y):
  
    # estimate gaussian params from sample geometry
    #     A is the maximum, mu its position
    #     sigma is fwhm / 2 sqrt(2 log2)
    A = np.max(y)
    mu = np.mean(x[np.argwhere(y > 0.95*A)])
    r = x[np.argwhere(y > 0.5*A)]
    fwhm = (r[-1] - r[0])[0]

    # sigma = fwhm / 2 sqrt(2 log(2)) for gaussian
    # or peak too narrow
    sigma = fwhm / 2.3548 if fwhm > 0  else x[1]-x[0]

    params = Parameters()
    params.add('A', value=A)
    params.add('mu', value=mu)
    params.add('sigma', value=sigma)
    fit = minimize(gauss_residual, params, args=(x, y))

    return fit

def lorentz_fit(x, y):
  
    # estimate lorentz params from sample geometry
    #     A is the maximum, mu its position
    #     sigma is fwhm / 2
    A = np.max(y)
    mu = np.mean(x[np.argwhere(y > 0.95*A)])
    r = x[np.argwhere(y > 0.5*A)]
    fwhm = (r[-1] - r[0])[0]
    sigma = fwhm / 2

    params = Parameters()
    params.add('A', value=A)
    params.add('mu', value=mu)
    params.add('sigma', value=sigma)
    fit = minimize(lorentz_residual, params, args=(x, y))

    return fit

if __name__ == '__main__':

    x = np.arange(1000) / 100   # interval [0, 10]

    y = gaussian(2.71, 3.14, 0.577, x) + 0.00005 * np.random.randn(x.size)
    fit = gauss_fit(x, y)
    print(f'\ngaussian: reduced chisq = {fit.redchi:5g}')
    for p in fit.params.values():
        print(f'{p.name:10s}: {p.value:12.10f}  +/- {p.stderr:3g}')

    y = lorentzian(2.71, 3.14, 0.577, x) + 0.00005 * np.random.randn(x.size)
    fit = lorentz_fit(x, y)
    print(f'\nlorentzian: reduced chisq = {fit.redchi:5g}')
    for p in fit.params.values():
        print(f'{p.name:10s}: {p.value:12.10f}  +/- {p.stderr:3g}')
        
        
