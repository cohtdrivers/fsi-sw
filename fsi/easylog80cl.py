#!/usr/bin/env python3

'''
interface to EasyLog 80CL climate logger

(c) CERN 2022
author: Juan David Gonzalez Cobas

based on Jaroslaw Rutkowski code derived from Robert Gieseke's pyEasybus
and cross-checked with
"Der EASYBUS – ein kleiner Einblick", Greisinger Electronic GmbH, 2007
'''

import serial


class EasyLog80CLError(Exception):

    error_codes = {
        16352 : 'error  1: Measurement above range',
        16353 : 'error  2: Measurement below range',
        16362 : 'error 11: Calculation not possible',
        16363 : 'error  7: System error',
        16364 : 'error  8: Battery low',
        16365 : 'error  9: Faulty sensor',
    }


class EasyLog80CL:

    vars = {
            'humidity'      : bytes.fromhex(' fe 00 3d '),
            'temperature'   : bytes.fromhex(' fd 00 02 '),
            'pressure'      : bytes.fromhex(' fc 00 17 '),
            'var4'          : bytes.fromhex(' fb 00 7c '),
            'var5'          : bytes.fromhex(' fa 00 69 '),
            'var6'          : bytes.fromhex(' f9 00 56 '),
    }

    def __init__(self, port= '/dev/tty.usbserial-1310'):
        '''connect to serial port of EasyBus device.'''
        self.connect(port)

    def connect(self, port):
        '''connect to serial interface of EasyBus device.'''

        # according to manual: 4800 8N1,
        # contrary to manual: NO flow control (it's usb...)
        # manual claims that answers come in up to  1 second
        self._serial = serial.Serial(
            port = port,
            baudrate = 4800,
            bytesize = serial.EIGHTBITS,
            parity = serial.PARITY_NONE,
            stopbits = serial.STOPBITS_ONE,
            rtscts = False,
            dsrdtr = False,
            timeout = 2)

    def raw_command(self, cmd):
        '''send a raw 3-byte command, return 6-byte raw answer.'''
        self._serial.write(cmd)
        ans = self._serial.read(6)
        return ans

    def decode_answer(self, ans):
        '''decode the 6-byte answer to a variable request.'''
        
        integer_value = 0x3fff & (((0xff^ans[3]) << 8) | ans[4]) - 2048
        decimal_point = (ans[3]^0xff) >> 6
        divisor = [1.0, 10.0, 100.0, 1000.0][decimal_point]
        float_value = integer_value / divisor

        if integer_value >= 16352:
            raise EasyLog80CLError(EasyLog80CLError.error_codes[integer_value])

        return float_value

    def get_variable(self, varname):
        '''get one of the variables = 'temperature', 'humidity', 'pressure'.'''
        tmp = self.raw_command(EasyLog80CL.vars[varname])
        return self.decode_answer(tmp)

    @property
    def temperature(self):
        '''measured temperature.'''
        return self.get_variable('temperature')

    @property
    def pressure(self):
        '''measured pressure.'''
        return self.get_variable('pressure')

    @property
    def humidity(self):
        '''measured humidity.'''
        return self.get_variable('humidity')

if __name__ == '__main__':

    import time
    import sqlite3

    serial_by_id = ( '/dev/serial/by-id/'
        'usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_G9ue03d0915s001568-if00-port0' )

    e = EasyLog80CL(serial_by_id)
    db = sqlite3.connect('climate.db')
    c = db.cursor()

    while True:
        
        temp, press, humi, var4 = e.temperature, e.pressure, e.humidity, e.get_variable('var4')
        secs = time.time()
        tm = time.asctime()
        c.execute('''insert into meteo_log_data (temperature, pressure, humidity,
                                                var4, textual_date, second)
                        values(?, ?, ?, ?, ?, ?);
                ''', (temp, press, humi, var4, tm, secs))
        db.commit()
        print(temp, press, humi, var4, tm, secs)
        time.sleep(10)
