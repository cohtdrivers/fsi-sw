#!/usr/bin/env python3
# vim: ts=8 sw=4 et sts=4 ai

import argparse
import fsi.ifm

def main():
    options = parse_args()
    if options.library_path:
        print(fsi.library_path())
        return
    try:
        if options.matlab:
            server = fsi.ifm.Server(options.config,
                        synchronized=False,
                        diot=options.matlab)
            server.emc_process(options.matlab)
        elif options.emc:
            server = fsi.ifm.Server(options.config,
                                synchronized=False)
            server.emc_mainloop(iterations=options.iterations)
        else:
            server = fsi.ifm.Server(options.config,
                        synchronized=not options.master,
                        diot=options.diot,
                        noscan=options.noscan)
            server.mainloop(iterations=options.iterations)
        del server
    except KeyboardInterrupt as e:
        print('interrupted, exiting...')

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', default='sct.ini',
                            help='configuration (.ini) file')
    parser.add_argument('-m', '--master',  action='store_true',
                            help='run as master, not as libfsi slave')
    parser.add_argument('-M', '--matlab', action='store',
                            help='process data from raw MATLAB file')
    parser.add_argument('-n', '--iterations', type=int, default=0,
                            help='number of acquisitions to run, or forever if zero')
    parser.add_argument('-d', '--diot', default=None,
                            help='name of DI/OT crate to link to')
    parser.add_argument('--noscan', action='store_true', default=None,
                            help='leave laser scan to external device')
    parser.add_argument('-l', '--library-path', action='store_true',
                            help='print the value of LD_LIBRARY_PATH and exit')
    parser.add_argument('--emc', action='store_true',
                            help='perform an EMC test mainloop')
    return parser.parse_args()

if __name__ == '__main__':
    main()
