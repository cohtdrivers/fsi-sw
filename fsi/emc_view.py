#!/usr/bin/env python3
# vim: ts=8 sw=4 et sts=4 ai

import argparse
import numpy as np
import scipy.io
from matplotlib import pyplot as plt
import regex as re
import os

def squarify(n):
    # compute closest rectangle larger or equal to n
    # with a pleasant aspect ratio near 16:9
    # with preference for more rows than columns:
    tmp = np.sqrt(n)
    rows = int(np.ceil(tmp))
    cols = int(np.ceil(n / rows))
    return cols, rows

def compute_fft(samples, nspec):
    fft = np.fft.rfft(samples, nspec)
    return np.abs(fft)

class MatlabFile:

    def __init__(self, fname, fro=None, to=None):
        '''initialize data on a MATLAB file.'''

        m = scipy.io.loadmat(fname)
        self.fname = fname
        self.m = m
        # dirtiest way to weed out non-channels
        labels = [k for k in m if isinstance(m[k], np.ndarray)
                        and len(m[k][0]) > 100]
        self.labels = list(enumerate(labels))
        self.nchannels = len(labels)
        self.spectral = 'idx_to_distance' in m
        # rename the channels: if the channel is FFT_<label> then, rename it to <label>_FFT
        # otherwise, rename to <label>_RAW
        for label in labels:
            if label.startswith('FFT_'):
                m[label[4:] + '_FFT'] = m[label]
                del m[label]
            else:
                m['RAW_' +label] = m[label]
                del m[label]
        self.fro, self.to = fro, to

    def list(self):
        lines = [ f'{self.nchannels:d} channels, spectral: {self.spectral}' ]
        lines.extend([ f'{i:3d}: {label}' for i, label in self.labels])
        lines.extend([''])
        return '\n'.join(lines)

    def plot_spectrum(self, label, axes=None):
        if not self.spectral:
            print(f'{label} not a spectral channel')
            return
        v = self.m[label][0]
        x = self.m['idx_to_distance'][0] * np.arange(len(v))
        fro, to = self.fro, self.to
        # div fro, to by idx_to_distance
        fro = int(fro / self.m['idx_to_distance'][0])
        to = int(to / self.m['idx_to_distance'][0])
        axes.plot(x[fro:to], v[fro:to], lw=0.5)
        axes.set_xlabel('distance [m]')
        axes.set_title(f'{label}')

    def plot_raw(self, label, axes=None):
        if self.spectral:
            print(f'{label} not a raw channel')
            return
        v = self.m[label][0]
        fro, to = None, None
        axes.plot(v[fro:to], lw=0.5)
        axes.set_xlabel('sample [10ns/sample]')
        axes.set_title(f'{label}')

    def plot_all(self, labels=None):
        if labels is None:
            labels = [l for (i, l) in self.labels]
        n = len(labels)
        cols, rows = squarify(n)
        plt.subplots(rows, cols, squeeze=False, figsize=(24,18),
                                    layout='constrained')
        for i, label in enumerate(labels):
            ax = plt.subplot(rows, cols, i+1)
            if self.spectral:
                self.plot_spectrum(label, ax)
            else:
                self.plot_raw(label, ax)
        #plt.get_current_fig_manager().full_screen_toggle() # toggle fullscreen mode
        plt.show()

    def __str__(self):
        return self.list()
    
def channel_result_to_str(res):
    (dist, peak) = res
    # res is (peak distance in m, peak accuracy in um).
    # format it in e3 decimal and with the correct unit. 
    # ie (0.082836, 12.380) -> '82.8mm±12.4um'
    # ie (7.500046, 14.464) -> '7.5m±14.5um'
    distance_unit = 'm'
    accuracy_unit = 'um'
    if dist < 1:
        dist *= 1000
        distance_unit = 'mm'
    if peak < 1:
        peak *= 1000
        accuracy_unit = 'mm'
    return f'{dist:.1f}{distance_unit}±{peak:.1f}{accuracy_unit}'


class EmcFilesCompare:
    # same class but shows the files for comparison,
    # by showing the each identical named channel on the same plot, but
    # in different colors.
    def __init__(self,ts, ref_fnames, fnames, result_fname, fro=None, to=None):
        self.ts = ts
        # load all files
        self.ref_m = []
        self.m = []
        # if result file is given in fnames, open it and extract the data:
        self.result_fname = result_fname
        self.fd_ref = open(self.result_fname, 'r')
        self.fd_ref.readline() # skip header
        #example content of file
        # ch#34:5.2:HLS:    1 peaks
        # pk#0:     0.082836+-      12.380um  max=       0.082     hint:0.082+-0.001
        # ch#35:5.3:FIBER:    1 peaks
        # pk#0:     7.500046+-      14.464um  max=       7.500     hint:7.5+-0.001
        # ch#36:5.4:BALL4_1:    1 peaks
        # pk#0:     0.117225+-      19.091um  max=       0.117     hint:0.117+-0.001
        
        # for each channel, extract the peak position and the accuracy and store in a dict with the channel name as key
        self.result_dict = {}
        while True:
            line = self.fd_ref.readline()
            # strip heading and trailing spaces
            line = line.strip()
            if line == '':
                break
            if line.startswith('ch#'):
                # parse the channel number and name
                # information is:
                # ch#<number>:<channel ID in the form of x.y>:<channel name>: <peak count> peaks
                m = re.match(r'ch#(\d+):(\d+\.\d+):(.+):\s+(\d+)\speaks', line)
                if m:
                    ch_num = int(m.group(1))
                    ch_name = m.group(3)+"_FFT"
                    ch_peaks_num = int(m.group(4))
                    if ch_peaks_num > 0:
                        # print result of the regex
                        # read the next line
                        line = self.fd_ref.readline()
                        # strip heading and trailing spaces
                        line = line.strip()
                        # parse the peak position and accuracy (numbers can be negative as well)
                        m = re.match(r'pk#0:\s+(-?\d+\.\d+)\+\-\s+(\d+\.\d+)um', line)
                        if m:
                            peak_pos = float(m.group(1))
                            peak_acc = float(m.group(2))
                            # print result of the regex
                            # store the result in the dict
                            self.result_dict[ch_name] = (peak_pos, peak_acc)
        

        
        
        self.ref_fnames = ref_fnames
        self.fnames = fnames
        for ref_fname in ref_fnames:
            tmp = MatlabFile(ref_fname, fro=fro, to=to)
            self.ref_m.append(tmp.m)

        
        for fname in fnames:
            tmp = MatlabFile(fname, fro=fro, to=to)
            self.m.append(tmp.m)

        # drop all channels named "BLIND<>_FFT" from both fileset
        for m in self.ref_m:
            if 'idx_to_distance' in self.m[0]:
                # match with regex
                blind_channels = [k for k in m if re.match(r'BLIND\d+_FFT', k)]
                for k in blind_channels:
                    del m[k]
        for m in self.m:
            if 'idx_to_distance' in self.m[0]:
                # match with regex
                blind_channels = [k for k in m if re.match(r'BLIND\d+_FFT', k)]
                for k in blind_channels:
                    del m[k]        

        # assert that all files have the same channels
        # and put the channels in the same order
        #labels = [k for k in self.m[0] if isinstance(self.m[0][k], np.ndarray) and len(self.m[0][k][0]) > 100]
        #ref_labels = [k for k in self.ref_m[0] if isinstance(self.ref_m[0][k], np.ndarray) and len(self.ref_m[0][k][0]) > 100]

        self.ref_data = {}
        for m in self.ref_m:
            for k in m:
                # find all channels with "BLIND" in the name
                if "RAW_BLIND" in k:
                    print(f"saw {k}")
                    fft = compute_fft(m[k][0], 2490000)
                    m[k] = np.array([fft])
                    k_update = k.replace("RAW_BLIND", "RAW_FFT_BLIND")
                else:
                    k_update = k
                self.ref_data[k_update] = m[k]

        self.data = {}
        for m in self.m:
            for k in m:
                if "RAW_BLIND" in k:
                    print(f"saw {k}")
                    fft = compute_fft(m[k][0], 2490000)
                    m[k] = np.array([fft])
                    k_update = k.replace("RAW_BLIND", "RAW_FFT_BLIND")
                else:
                    k_update = k
                self.data[k_update] = m[k]

        #for m in self.m:
        #    assert [k for k in m if isinstance(m[k], np.ndarray) and len(m[k][0]) > 100] == labels, 'files have different channels'
        self.fro, self.to = fro, to

    def list(self):
        real_ref_channels = [k for k in self.ref_data if isinstance(self.ref_data[k], np.ndarray) and len(self.ref_data[k][0]) > 100]
        real_channels = [k for k in self.data if isinstance(self.data[k], np.ndarray) and len(self.data[k][0]) > 100]
        intersection = set(real_ref_channels).intersection(set(real_channels))
        # sort by name
        intersection = sorted(intersection)
        print(f'present in both reference and data:')
        for k in intersection:
            print(f'  {k}')
        print(f'present in reference only:')
        for k in real_ref_channels:
            if k not in intersection:
                print(f'  {k}')
        print(f'present in data only:')
        for k in real_channels:
            if k not in intersection:
                print(f'  {k}')
        


    
    def plot_spectrum(self, label, axes=None):
        if "FFT" not in label:
            print(f'{label} not a spectral channel')
            return
        if label not in self.ref_data:
            print(f'{label} not in reference data')
            return
        v = self.ref_data[label][0]
        x = self.ref_data['idx_to_distance'][0] * np.arange(len(v))
        # crush the first mm to 0
        start, end = int(0.000* self.ref_data['idx_to_distance'][0][0]),  int(0.001 / self.ref_data['idx_to_distance'][0][0])
        mean_ref = np.mean(self.ref_data[label][0])
        mean_data = np.mean(self.data[label][0])
        for i in range(start, end):
            self.ref_data[label][0][i] = mean_ref
            self.data[label][0][i] = mean_data

        fro, to = self.fro, self.to
        # div fro, to by idx_to_distance
        if fro is not None:
            fro = int(fro / self.ref_data['idx_to_distance'][0][0])
        if to is not None:
            to = int(to / self.ref_data['idx_to_distance'][0][0])
        # plot ref
        #axes.semilogy(x[fro:to], self.ref_data[label][0][fro:to], lw=0.5)
        axes.semilogy(x[fro:to], self.data[label][0][fro:to], lw=0.5)
        axes.semilogy(x[fro:to], self.ref_data[label][0][fro:to], lw=0.5)
        axes.set_xlabel('distance [m]')
        if label in self.result_dict:
            infos = channel_result_to_str(self.result_dict[label])
        else:
            infos = 'none'
        axes.set_title(f'{label}:{infos}')
        
    def plot_raw(self, label, axes=None):
        if "FFT" in label:
            print(f'{label} not a raw channel')
            return
        v = self.ref_data[label][0]
        fro, to = None, None
        # plot ref
        #axes.plot(self.ref_data[label][0][fro:to], lw=0.5)
        axes.plot(self.data[label][0][fro:to], lw=0.5)
        axes.plot(self.ref_data[label][0][fro:to], lw=0.5)
        #for i in range(len(self.fnames)):
        #    axes.plot(self.m[i][label][0][fro:to], lw=0.5)
        axes.set_xlabel('sample [10ns/sample]')
        axes.set_title(f'{label}')
    
    def plot_blind_fft(self, label, axes=None):
        v = self.ref_data[label][0]
        # get frequency for data to display as x axis
        sample_rate = 100000000
        x = np.linspace(0, sample_rate/2, len(v))
        fro, to = self.fro, self.to
        # set index 0 of plots to 0 to remove DC component
        self.ref_data[label][0][0] = 0
        self.data[label][0][0] = 0
        # plot ref
        #axes.semilogy(x[fro:to],self.ref_data[label][0][fro:to], lw=0.5)
        axes.semilogy(x[fro:to],self.data[label][0][fro:to], lw=0.5)
        axes.semilogy(x[fro:to],self.ref_data[label][0][fro:to], lw=0.5)
        axes.set_xlabel('non-lin fft [Hz]')
        axes.set_title(f'{label}')

    def plot_emc(self, labels=None):
        # if we want to draw:
        # GAS_RAW FIBER_RAW HLS_RAW [BALL_RAW]
        # REF_RAW FIBER_FFT HLS_FFT [BALL_FFT]
        # [BLIND_FFT]
        # to compute the geometry of the plot, we need to know the max x dimension
        if (labels is None):
            ordered_labels = [
                'HLS_FFT', 'FIBER_FFT', 'BALL4_1_FFT', 'BALL4_2_FFT', "BALL16_1_FFT", "BALL16_2_FFT",
                "BALL16_3_FFT", "BALL16_4_FFT", "BALL16_5_FFT", "BALL16_6_FFT", "BALL16_7_FFT", "BALL16_8_FFT",
                "RAW_HLS", "RAW_FIBER", "RAW_BALL4_1", "RAW_BALL4_2", "RAW_BALL16_1", "RAW_BALL16_2",
                "RAW_BALL16_3", "RAW_BALL16_4", "RAW_BALL16_5", "RAW_BALL16_6", "RAW_BALL16_7", "RAW_BALL16_8",
                "RAW_FFT_BLIND0", "RAW_FFT_BLIND1", "RAW_FFT_BLIND2", "RAW_FFT_BLIND3", "RAW_FFT_BLIND4", "RAW_FFT_BLIND5",
                "RAW_FFT_BLIND6", "RAW_FFT_BLIND7", "RAW_FFT_BLIND8", "RAW_FFT_BLIND9", "RAW_REF", "RAW_GAS"
            ]
        else:
            ordered_labels = labels
        rows, cols = squarify(len(ordered_labels))
        plt.subplots(rows, cols, squeeze=True, figsize=(20,13),
                                    layout='constrained')
        for (i,label) in enumerate(ordered_labels):
            ax = plt.subplot(rows, cols, i+1)
            if label not in self.data:
                print(f'{label} not in data')
                # plot empty data but show the label
                ax.set_title(label)
                continue
            if "RAW_FFT_BLIND" in label:
                print(f'plotting  blind fft {label}')
                self.plot_blind_fft(label, ax)
            elif "FFT" in label:
                print(f'plotting  linearized {label}')
                self.plot_spectrum(label, ax)
            else:
                print(f'plotting  raw {label}')
                self.plot_raw(label, ax)
        #find fname without fft in it and remove trailing .mat to get the timestamp
        print(f"saving as {self.ts}.png")
        plt.suptitle(self.ts)
        plt.savefig(f'{self.ts}.png', bbox_inches='tight')
        plt.show()

    def __str__(self):
        return self.list()



def main():
    '''main program, parse arguments.'''
    parser = argparse.ArgumentParser()

    # workdir path
    parser.add_argument('-w', '--workdir', type=str, default='.')

    # add n -i options arguments for input files
    parser.add_argument('-r', '--ref', dest='ref_ts', default=None,
                                help='reference files timestamp')
    # add n -i options arguments for input files
    parser.add_argument('-i', '--input', dest='ts', default=None,
                                help='input files timestamp')
    # add n -l options arguments for labels
    parser.add_argument('-l', '--label', action='append', dest='labels', default=None,
                                help='label to plot')
    parser.add_argument('--fro', type=int, default=None,
                                help='plot from  given index')
    parser.add_argument('--to',  type=int, default=None,
                                help='plot up to given index')
    options = parser.parse_args()

    # sanity check:
    # ref_fnames == for each ref_ts, [workdir]/[ref_ts].mat + [workdir]/[ref_ts]_fft.mat
    ref_fnames = [os.path.join(options.workdir, f'{options.ref_ts}.mat'), os.path.join(options.workdir, f'{options.ref_ts}_fft.mat')]
    # check that files exists
    for f in ref_fnames:
        if not os.path.isfile(f):
            print(f'reference file {f} does not exist')
            exit(-1)
    # fnames == for each ts, [workdir]/[ts].mat + [workdir]/[ts]_fft.mat
    fnames = [os.path.join(options.workdir, f'{options.ts}.mat'), os.path.join(options.workdir, f'{options.ts}_fft.mat')]
    # check that files exists
    for f in fnames:
        if not os.path.isfile(f):
            print(f'file {f} does not exist')
            exit(-1)
    # result_fname == [workdir]/[ts]-results.txt
    result_fname = os.path.join(options.workdir, f'{options.ts}-results.txt')
    # check that file exist
    if not os.path.isfile(result_fname):
        print(f'result file {result_fname} does not exist')
        exit(-1)



    m = EmcFilesCompare(options.ts, ref_fnames, fnames, result_fname, fro=options.fro, to=options.to)
    if options.labels == None:
        m.list()
    m.plot_emc(options.labels)
        

if __name__ == '__main__':
    main()
