#!/usr/bin/env python3
# vim: ts=8 sw=4 et sts=4 ai

import sys
import logging
from ctypes import *

from fsi import libfsi

# bootstrap number of oblocks to declare fsi_constants
FSI_NEDFAS = c_int.in_dll(libfsi, '_FSI_NBLOCKS').value

class fsi_constants(Structure):
    _fields_ = [
    ('n',             c_int),
    ('fs',            c_double),
    ('noblocks',      c_int),
    ('zpf',           c_int),
    ('port_laser',    c_char_p),
    ('port_edfa',     FSI_NEDFAS * c_char_p),
    ('port_meteo',    c_char_p),
    ('port_console',  c_char_p),

    ('_FSI_MAX_MODEL_PARAMS', c_int),
    ('_FSI_PEAK_LENGTH',      c_int),
    ('_FSI_HW_DIAG_SIZE',     c_int),
    ('_FSI_MAX_PEAKS',        c_int),
    ('_FSI_SAMPLE_SIZE',      c_int),

    ('_FSI_NBLOCKS',          c_int),
    ('_FSI_NEDFAS',           c_int),
    ('_FSI_NCHANNELS',        c_int),
    ('_FSI_NGCS',             c_int),
    ('_FSI_NREFS',            c_int),

    ('_FSI_ERR_BASE',                c_int),
    ('_FSI_SOCKET_ERROR',            c_int),
    ('_FSI_SOCKET_BUF_ERROR',        c_int),
    ('_FSI_SOCKET_RECVFROM_TIMEOUT', c_int),
    ('_FSI_SOCKET_READV_TIMEOUT',    c_int),
    ('_FSI_SOCKET_CFGTIMEO_ERROR',   c_int),
    ('_FSI_SOCKET_LISTEN_ERROR',     c_int),
    ('_FSI_SOCKET_ACCEPT_ERROR',     c_int),
    ('_FSI_SOCKET_NOCONN',           c_int),

    ('_FSI_CH_NONE',      c_int),
    ('_FSI_CH_DISTANCE',  c_int),
    ('_FSI_CH_BRAGG',     c_int),
    ('_FSI_CH_GAS_CELL',  c_int),
    ('_FSI_CH_INTERF',    c_int),

    ('_FSI_PEAK_NONE',    c_int),
    ('_FSI_PEAK_LORENTZ', c_int),
    ('_FSI_PEAK_GAUSS',   c_int),
    ('_FSI_PEAK_SINC',    c_int),
    ('_FSI_PEAK_MOFFAT',  c_int),

    ('_FSI_UNIT_NONE',    c_int),
    ('_FSI_UNIT_M',       c_int),
    ('_FSI_UNIT_NM',      c_int),
    ('_FSI_UNIT_HZ',      c_int),
    ('_FSI_UNIT_SAMPLE',  c_int),
    ('_FSI_UNIT_DB',      c_int),
    ('_FSI_UNIT_S',       c_int),

    ('_FSI_EXPERT_NONE',          c_int),
    ('_FSI_EXPERT_LIN_DATA',      c_int),
    ('_FSI_EXPERT_OSCILLOSCOPE',  c_int),
    ('_FSI_EXPERT_GAS_CELL',      c_int),
    ('_FSI_EXPERT_INTERF',        c_int),
    ('_FSI_EXPERT_RAW_GAS_CELL',  c_int),
    ('_FSI_EXPERT_RAW_INTERF',    c_int),
    ('_FSI_EXPERT_LIN_FFT_M',     c_int),
    ('_FSI_EXPERT_LIN_FFT_HZ',    c_int),

    ('_FSI_SERVER_DEFAULT_SHM',   c_char_p),
    ('_FSI_SERVER_SHM_ENVIRON',   c_char_p),

    ('_FSI_SERVER_SOCKET', c_char_p),
    ('_FSI_SERVER_GREET',  c_char_p),
    ('_FSI_SERVER_ANS',    c_char_p),

    ('_FSI_REF_CH_MAX',    c_int),
    ('_FSI_GC_CH_MAX',     c_int),
    ]


Constants = POINTER(fsi_constants)
constants = Constants.in_dll(libfsi, 'fsi_constants')

FSI_NBLOCKS                             = constants[0]._FSI_NBLOCKS
FSI_MAX_MODEL_PARAMS    = constants[0]._FSI_MAX_MODEL_PARAMS
FSI_PEAK_LENGTH                 = constants[0]._FSI_PEAK_LENGTH
FSI_HW_DIAG_SIZE                = constants[0]._FSI_HW_DIAG_SIZE
FSI_MAX_PEAKS                   = constants[0]._FSI_MAX_PEAKS
FSI_SAMPLE_SIZE                 = constants[0]._FSI_SAMPLE_SIZE
FSI_NCHANNELS                   = constants[0]._FSI_NCHANNELS
FSI_NGCS                                = constants[0]._FSI_NGCS
FSI_NREFS                               = constants[0]._FSI_NREFS

FSI_ERR_BASE                 = constants[0]._FSI_ERR_BASE
FSI_SOCKET_ERROR             = constants[0]._FSI_SOCKET_ERROR
FSI_SOCKET_BUF_ERROR         = constants[0]._FSI_SOCKET_BUF_ERROR
FSI_SOCKET_RECVFROM_TIMEOUT  = constants[0]._FSI_SOCKET_RECVFROM_TIMEOUT
FSI_SOCKET_READV_TIMEOUT     = constants[0]._FSI_SOCKET_READV_TIMEOUT
FSI_SOCKET_CFGTIMEO_ERROR    = constants[0]._FSI_SOCKET_CFGTIMEO_ERROR
FSI_SOCKET_LISTEN_ERROR      = constants[0]._FSI_SOCKET_LISTEN_ERROR
FSI_SOCKET_ACCEPT_ERROR      = constants[0]._FSI_SOCKET_ACCEPT_ERROR
FSI_SOCKET_NOCONN            = constants[0]._FSI_SOCKET_NOCONN

FSI_CH_NONE                  = constants[0]._FSI_CH_NONE
FSI_CH_DISTANCE              = constants[0]._FSI_CH_DISTANCE
FSI_CH_BRAGG                 = constants[0]._FSI_CH_BRAGG
FSI_CH_GAS_CELL              = constants[0]._FSI_CH_GAS_CELL
FSI_CH_INTERF                = constants[0]._FSI_CH_INTERF

FSI_PEAK_NONE                = constants[0]._FSI_PEAK_NONE
FSI_PEAK_LORENTZ             = constants[0]._FSI_PEAK_LORENTZ
FSI_PEAK_GAUSS               = constants[0]._FSI_PEAK_GAUSS
FSI_PEAK_SINC                = constants[0]._FSI_PEAK_SINC
FSI_PEAK_MOFFAT              = constants[0]._FSI_PEAK_MOFFAT

FSI_UNIT_NONE                = constants[0]._FSI_UNIT_NONE
FSI_UNIT_M                   = constants[0]._FSI_UNIT_M
FSI_UNIT_NM                  = constants[0]._FSI_UNIT_NM
FSI_UNIT_HZ                  = constants[0]._FSI_UNIT_HZ
FSI_UNIT_SAMPLE              = constants[0]._FSI_UNIT_SAMPLE
FSI_UNIT_DB                  = constants[0]._FSI_UNIT_DB
FSI_UNIT_S                   = constants[0]._FSI_UNIT_S

FSI_EXPERT_NONE              = constants[0]._FSI_EXPERT_NONE
FSI_EXPERT_LIN_DATA          = constants[0]._FSI_EXPERT_LIN_DATA
FSI_EXPERT_OSCILLOSCOPE      = constants[0]._FSI_EXPERT_OSCILLOSCOPE
FSI_EXPERT_GAS_CELL          = constants[0]._FSI_EXPERT_GAS_CELL
FSI_EXPERT_INTERF            = constants[0]._FSI_EXPERT_INTERF
FSI_EXPERT_RAW_GAS_CELL      = constants[0]._FSI_EXPERT_RAW_GAS_CELL
FSI_EXPERT_RAW_INTERF        = constants[0]._FSI_EXPERT_RAW_INTERF
FSI_EXPERT_LIN_FFT_M         = constants[0]._FSI_EXPERT_LIN_FFT_M
FSI_EXPERT_LIN_FFT_HZ        = constants[0]._FSI_EXPERT_LIN_FFT_HZ

FSI_SERVER_DEFAULT_SHM       = constants[0]._FSI_SERVER_DEFAULT_SHM
FSI_SERVER_SHM_ENVIRON       = constants[0]._FSI_SERVER_SHM_ENVIRON

FSI_SERVER_SOCKET            = constants[0]._FSI_SERVER_SOCKET
FSI_SERVER_GREET             = constants[0]._FSI_SERVER_GREET
FSI_SERVER_ANS               = constants[0]._FSI_SERVER_ANS

FSI_REF_CH_MAX               = constants[0]._FSI_REF_CH_MAX
FSI_GC_CH_MAX                = constants[0]._FSI_GC_CH_MAX

class fsi_peak_config(Structure):
    _fields_ = [
        ( 'peak_id',    c_int),
        ( 'position',   c_double),
        ( 'range1',     c_double),
        ( 'range2',     c_double),
        ( 'type_',      c_int),
        ( 'xunit',      c_int),
    ]

class fsi_fit_params(Structure):
    _fields_ = [

        ( 'amplitude',  c_double),
        ( 'mu',         c_double),
        ( 'sigma',      c_double),
        ( 'stderr',     c_double),
        ( 'reserved',   (FSI_MAX_MODEL_PARAMS-4) * c_double),
    ]
class fsi_peak_result(Structure):
    _fields_ = [
        ( 'peak_id',     c_int),
        ( 'valid',       c_int),
        ( 'type_',       c_int),
        ( 'mu',          c_double),
        ( 'sigma',       c_double),
        ( 'snr',         c_double),
        ( 'fit_params',  fsi_fit_params),
        ( 'samples',     FSI_PEAK_LENGTH * c_double),
        ( 'xunit',       c_int),
        ( 'yunit',       c_int),
        ( 'nsamples',    c_int),
        ( 'start',       c_double),
        ( 'end',         c_double),
        ( 'delta',       c_double),
    ]

class fsi_channel_config(Structure):
    _fields_ = [
        ( 'channel_id',   c_uint),
        ( 'type_',        c_int),
        ( 'npeaks',       c_int),
        ( 'channel_gain', c_int),
        ( 'peaks',        FSI_MAX_PEAKS * fsi_peak_config),
        ( 'disabled',     c_uint),
    ]

class fsi_hw_status(Structure):
    _fields_ = [
        ( 'diag',  FSI_HW_DIAG_SIZE * c_ubyte),
    ]

class fsi_channel_measurement(Structure):
    _fields_ = [
        ( 'channel_id',    c_uint),
        ( 'type_',         c_int),
        ( 'npeaks',        c_int),
        ( 'n_valid_peaks', c_int),
        ( 'hw_status',     fsi_hw_status),
        ( 'peaks',         FSI_MAX_PEAKS * fsi_peak_result),
    ]

class fsi_full_measurement(Structure):
    _fields_ = [
        ( 'channel_id', c_uint),
        ( 'type_',      c_int),
        ( 'mode',       c_int),
        ( 'data',       FSI_SAMPLE_SIZE * c_uint16),
        ( 'xunit',      c_int),
        ( 'nsamples',   c_int),
        ( 'start',      c_double),
        ( 'delta',      c_double),
    ]

class fsi_channel(Structure):
    _fields_ = [
        ( 'channel_id',        c_uint),
        ( 'type_',             c_int),
        ( 'channel_gain',      c_int),
        ( 'channel_coupling',  c_int),
        ( 'disabled',          c_uint),
        ( 'rpeaks',            c_int),
        ( 'requested_peaks',   FSI_MAX_PEAKS * fsi_peak_config),
        ( 'timestamp',         2 * c_ulong),
        ( 'mpeaks',            c_int),
        ( 'measured_peaks',    FSI_MAX_PEAKS * fsi_peak_result),
        ( 'hw_status',         fsi_hw_status),
        ( 'full',              POINTER(fsi_full_measurement)),
    ]

class fsi_qa_ref_params(Structure):
    _fields_ = [
        ( 'ref_id',           c_int),
        ( 'ref_center',       c_double),
        ( 'ref_deviation',    c_double),
        ( 'ref_deviation95',  c_double),
    ]
class fsi_qa_gc_params(Structure):
    _fields_ = [
        ( 'gc_id',            c_int),
        ( 'gc_alpha',         c_double),
        ( 'gc_zero_value',    c_double),
        ( 'gc_fitting_err',   c_double),
        ( 'gc_npeaks_fit_R',  c_int),
        ( 'gc_npeaks_fit_P',  c_int),
        ( 'gc_peaks_R',       27 * c_double),
        ( 'gc_peaks_P',       26 * c_double),
        ( 'gc_reserved',       4 * c_double),
    ]
class fsi_qa_params(Structure):
    _fields_ = [
        ( 'nref',         c_int),
        ( 'ngc',          c_int),
        ( 'ref',          FSI_REF_CH_MAX * fsi_qa_ref_params),
        ( 'gc',           FSI_GC_CH_MAX * fsi_qa_gc_params),
        ( 'qa_reserved',  256 * c_uint32),
    ]

class fsi_diot(Structure):
    _fields_ = [
    ]

class fsi_interferometer(Structure):
    _fields_ = [
        ( 'nblocks',     c_int), 
        ( 'nedfas',      c_int), 
        ( 'nrefs',       c_int), 
        ( 'ngas',        c_int), 
        ( 'nchannels',   c_int), 
        ( 'laser',       c_void_p), 
        ( 'edfa',        FSI_NBLOCKS * c_void_p),
        ( 'diot',        FSI_NBLOCKS * c_void_p),
        ( 'channel',     POINTER(fsi_channel)),
        ( 'channels',    FSI_NCHANNELS * fsi_channel),
    ]

class FSI_state(Structure):
    _fields_ = [
        ( 'nblocks',     c_int),
        ( 'nedfas',      c_int),
        ( 'nrefs',       c_int),
        ( 'ngas',        c_int),
        ( 'nchannels',   c_int),
        ( 'laser',       c_void_p),
        ( 'edfa',        FSI_NBLOCKS * c_void_p),
        ( 'diot',        FSI_NBLOCKS * c_void_p),
        ( 'channel',     POINTER(fsi_channel)),
        ( 'channels',    FSI_NCHANNELS * fsi_channel),

        ( 'fs',               c_int),
        ( 'n',                c_int),
        ( 'zpf',              c_int),
        ( 'batch',            c_char * 256),
        ( 'laser_configs',    c_char * 1024),
        ( 'gas_cell_ids',     c_int * FSI_NGCS),
        ( 'ref_ids',          c_int * FSI_NREFS),

        ( 'timestamp',        c_ulong * 2),
        ( 'alpha',            c_double),
        ( 'idx_to_distance',  c_double),
        ( 'n_air',            c_double),

        ( 'full_chid',        c_int),
        ( 'full_mode',        c_int),

        ( 'qa_params',        fsi_qa_params),

        ( 'proto',            c_int),
        ( 'sockfd',           c_int),
        ( 'fd',               c_int),
        ( 'config',           c_void_p),
    ]

class fsi_expert_vectors(Structure):
    _fields_ = [
        ( 'raw',        FSI_SAMPLE_SIZE * c_double),
        ( 'lin',        FSI_SAMPLE_SIZE * c_double),
        ( 'fft',        FSI_SAMPLE_SIZE * c_double),
    ]
fsi_expert_vectors_array = FSI_NCHANNELS * fsi_expert_vectors

if __name__ == '__main__':

    bits = open('libfsi/peak.bin', 'rb').read()
    bits = bytearray(bits)
    c = fsi_peak_result.from_buffer(bits)
    for (attr, _) in c._fields_:
        v = getattr(c, attr)
        if attr in ('fit_params', 'samples'):
            v = v[:5]
        print('{:10s}: {}'.format(attr, v))

    ifm = fsi_interferometer()

    print('fsi_peak_config',               sizeof(fsi_peak_config))
    print('fsi_peak_result',               sizeof(fsi_peak_result))
    print('fsi_channel_config',            sizeof(fsi_channel_config))
    print('fsi_hw_status',                 sizeof(fsi_hw_status))
    print('fsi_channel_measurement',       sizeof(fsi_channel_measurement))
    print('fsi_full_measurement',          sizeof(fsi_full_measurement))
    print('fsi_channel',                   sizeof(fsi_channel))
    print('fsi_diot',                      sizeof(fsi_diot))
    print('fsi_interferometer',            sizeof(fsi_interferometer))
