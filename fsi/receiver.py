#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt

from ctypes import *
import struct
import sys
import os.path

# parameters of the test
BIG = threemega = 3000000           # crazy sample number
NCHANS = 32                         # max no of channels to play with
few = 10                            # few to display

# use lib path relative to root script
arg0 = sys.argv[0]
path0 = os.path.dirname(arg0)
libcruncher = os.path.join(
        path0, 'solib', 'libreceiver.so')
                                    # the data source lib

class ChannelAcq(Structure):        # defined in cruncher.c
    _fields_ = [
        ('done',     c_int),
        ('board',    c_int),
        ('channel',  c_int),
        ('nsamples', c_int),
        ('samples',  BIG * c_uint16),
    ]

def basic_cruncher():
    "be a beast: request up to 3 Msamples of 16 bits, 32 ch."
    libc = CDLL('libc.so.6')
    l = CDLL(libcruncher)
    huge_buf_t = NCHANS * ChannelAcq
    huge_buf = huge_buf_t()
    libc.memset(byref(huge_buf), 0, sizeof(huge_buf))
    n = l.gimme_data(byref(huge_buf), NCHANS)

    print(f'received {n} channels, first {few} of which are:')
    for i in range(few):
        dn, ch, bd, ns = (huge_buf[i].done, huge_buf[i].channel,
                          huge_buf[i].board, huge_buf[i].nsamples)
        print(f'{i:02}: ch:{ch} bd:{bd} ns:{ns} | ', end='')
        eight_samps = [str(huge_buf[i].samples[j]) for j in range(few)]
        print(' '.join(eight_samps))

    # converting into numpy arrays: let's take four ch
    ch0 = np.frombuffer(huge_buf[0].samples, dtype=np.uint16, count=2500000)
    ch0 = ch0.astype(np.float64)    # FIXME: warning: costly cast
    print(len(ch0), ' ', ch0[:10], '..', ch0[-10:])

def fsi_cruncher(argv):
    "request up to 3 Msamples of 16 bits, 32 ch from the DIOTS."
    libc = CDLL('libc.so.6')
    l = CDLL(libcruncher)
    huge_buf_t = NCHANS * ChannelAcq
    huge_buf = huge_buf_t()
    libc.memset(byref(huge_buf), 0, sizeof(huge_buf))

    argc = len(argv)
    argv_t = c_char_p * (argc+1)
    argv = [c_char_p(bytes(arg, 'utf8')) for arg in argv] + [c_char_p(0)]
    argv = argv_t(*argv)
    n = l.fsi_main(argc, byref(argv), byref(huge_buf))
    # pdb.set_trace()

    # converting into numpy arrays: let's take four ch
    ret = []
    for ch in [0, 1, 2, 3]:
        b, c, s = huge_buf[ch].board, huge_buf[ch].channel, huge_buf[ch].nsamples
        chn = np.frombuffer(huge_buf[ch].samples, dtype=np.uint16, count=s)
        chn = chn.astype(np.float64)    # FIXME: warning: costly cast
        ret.append(chn)
        print(f'{b}:{c}({ch}): {s}({len(chn)}, ', chn[:10], '..', chn[-10:])
    return ret

if __name__ == '__main__':
    fsi_cruncher(sys.argv)
