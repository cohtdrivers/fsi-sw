#!/usr/bin/env python3
# vim: ts=8 sw=4 et sts=4 ai

import fsi.ifm

def main():
    try:
        server = fsi.ifm.Server('sct.ini', synchronized=True)
        server.mainloop()
        del server
    except KeyboardInterrupt as e:
        print('interrupted, exiting...')

if __name__ == '__main__':
    main()
