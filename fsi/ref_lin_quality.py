def ref_lin_quality(refl, nspec, fs, lin_spec_chop=10, plot=None):
    '''quality assessment of linearization.

    The linearized reference signal spectrum must have a single, very
    sharp peak corresponding to a single, pure tone (of frequency around
    12MHz for our typical application). This peak is located, and its
    center and degree of purity is computed.

    Returns a triple (c, fwhm, width), all the values in frequency units:
    with c being the frequency of the peak center, fwhm the full width
    at half-maximum, and r the width of the peak at 0.01 of its maximum.

    refl            - linearized reference interferometer signal
    nspec           - spectrum length (usually N * zpf)
    fs              - sampling frequency
    lin_spec_chop   - number of DC bins to ignore (default 10)
    plot            - if True, display a plot (default None)
    '''

    N = len(ref)
    spectrum = np.abs(np.fft.rfft(refl, nspec))

    # find max peak in spectrum and bracket it at 40dB
    # relevance. Ignore the low frequencies given by the lin_spec_chop
    # first bins
    dc = spectrum[:lin_spec_chop]       # drop the first bins
    spectrum[:lin_spec_chop] = 0
    peak = max(spectrum)
    idxh = np.argwhere(spectrum > 0.5 * peak)
    idx  = np.argwhere(spectrum > 0.01 * peak)
    idx0 = np.argwhere(spectrum == peak)
    a, c, b = idx[0][0], idx0[0][0], idx[-1][0]
    aa, bb  = idxh[0][0], idxh[-1][0]
    spectrum[:lin_spec_chop] = dc       # restore the discarded first bins

    # compute size of bracket
    c     = c * fs / N
    fwhm  = (bb - aa) * fs / N
    width = (b - a) * fs / N
    if plot:
        fig_refl = plt.figure()
        plt.semilogy(np.arange(lin_spec_chop, (nspec//2)) / N * fs, spectrum)
        plt.title('linearized reference spectrum')
    return c, fwhm, width

