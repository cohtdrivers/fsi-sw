#!/usr/bin/env python3
# vim: ts=8 sw=4 et sts=4 ai

from fsi import to_channel_id, to_obc

class BadChannelId(Exception):
    pass
    
class Channel:
    '''representation of an interferometer channel.'''

    def __init__(self, channel_id, label=None, typ=None):
        '''create a channel instance with an id in it.'''

        if isinstance(channel_id, int):
            self.channel_id = channel_id
            self.diot, self.board, self.channel = to_obc(channel_id)
        elif isinstance(channel_id, tuple):
            if len(channel_id) == 2:
                diot = 0
            elif len(channel_id) == 3:
                diot = int(channel_id[0])
            else:
                raise BadChannelId('channel_id tuple must be of the form'
                    ' (board, channel) or (diot, board, channel)')
            self.diot    = diot
            self.board   = int(channel_id[-2])
            self.channel = int(channel_id[-1])
            self.channel_id = to_channel_id(self.diot, self.board, self.channel)
        else:
            raise BadChannelId('channel_id must be int, pair or triple')

        # usually, this will be FSI_CH_NONE to mean 'unconfigured' hence 'ignorable'
        self.type       = typ

        self.label      = label
        self.v          = None
        self.l          = None
        self.spec       = None

        self.disabled   = False
        self.dc_coupled = False
        self.amplified  = False

    def __str__(self):
        return '{}:{:03d}:{:1d}.{:1d}.{:1d}'.format(
            self.label, self.channel_id, self.diot, self.board, self.channel)

def main():
    ch = Channel((0,6,1), 'REF')
    print(repr(ch))
    print(ch)
    ch = Channel((1,5,0), 'GAS')
    print(repr(ch))
    print(ch)

if __name__ == '__main__':
    main()
