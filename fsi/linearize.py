#!/usr/bin/env python3
# vim: ts=8 sw=4 et sts=4 ai

import numpy as np
import scipy.signal as sig

def linearize(fs, ref):
    """produce a linearized time vector from reference channel."

    arguments:
        fs      -- sampling frequency
        ref     -- reference interferometer raw data vector
       
    returns:
        t       -- time vector
        tins    -- linearized measurement interferometer data

    tinst, ref = linearize(fs, ref) uses a reference signal ref and a
    sampling frequency fs to produce the time values tinst that refer to
    the exact moment in time of correlative samples in ref. ref_lin is
    returned with corrected values for a uniform time scale. Any
    additional vector of samples added as argument is linearized
    like ref and returned and appended to the result.
    """

    n = len(ref)
    tlength = n / fs

    # compute hilbert transform in freq domain; suppress DC
    # along the way
    f = np.fft.fft(ref)
    f[0] = 0                        # suppress DC
                                    # leave f[n//2] intact
    f[:n//2-1] = 2 * f[:n//2-1]     # fold w < 0 half over w > 0
    f[n//2+1:] = 0                  #    half, giving DFT of hilbert(ref)

    hilbert = np.fft.ifft(f)

    # find actual time abscissae of freq sweep samples
    inst_phase = np.unwrap(np.angle(hilbert))
    t = np.linspace(0.0, tlength, num=n)
    tinst = (inst_phase - inst_phase[0])/(inst_phase[-1] - inst_phase[0]) * tlength;

    return t, tinst

def lin_quality(ref, t, tinst, n=None, fs=None):
    '''compute quality of linearization.

    As a side effect, ref gets its linearized version and FFT computed.

    ref     - reference Channel object
    t       - uniform sample times
    tinst   - instantaneous phase-derived times of samples

    Returns a tuple (ref_center, ref_deviation, ref_deviation95)
    with the peak center, FWHM and FW at 5% of maximum, all in units of
    frequency.
    '''

    if n is None: n = len(ref)
    if fs is None: fs = 100e6

    ref.l    = np.interp(t, tinst, ref.v)
    ref.spec = np.abs(np.fft.rfft(ref.l, n))

    ref.spec[0], tmp = 0, ref.spec[0]
    ref_center      = np.where(ref.spec == np.max(ref.spec))[0][0]
    ref_deviation   = np.where(ref.spec > .50 * np.max(ref.spec))[0]
    ref_deviation95 = np.where(ref.spec > .05 * np.max(ref.spec))[0]
    ref.spec[0] = tmp

    hz_per_bin = fs/n
    ref_center      = hz_per_bin * ref_center
    ref_deviation   = hz_per_bin * len(ref_deviation)
    ref_deviation95 = hz_per_bin * len(ref_deviation95)

    return ref_center, ref_deviation, ref_deviation95
