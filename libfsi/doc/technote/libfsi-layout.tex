\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{palatino}
\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{parskip}
\usepackage{paralist}

\newcommand\libfsi{\texttt{libfsi}}

\title{FSI interferometer: a guide for the perplexed.\\
	An informal layout description as seen from \texttt{libfsi}.}

\author{Juan David Gonz\'alez Cobas}
\date{February 2023}

\begin{document}

\maketitle

\section{Abstract}

The intent of this document is to informally explain the layout and mode
of operation of the FRAS FSI interferometer, as it might be seen from a
software perspective, i.e., as an opaque device solely manipulable via a
C~library named~\libfsi.

As such, only those elements that the software developer \emph{needs to
know}, and nothing else, is described here, in terms excluding any
technicality not relevant to such an interaction.

As an added value, conventions on the naming, enumeration and labeling
of the different \emph{things} living in the interferometer are
proposed.

\section{Structure of the apparatus}

A detailed schematics of the FSI interferometer optical path layout can
be perused in Figure~\ref{optical-path}.
\begin{figure}
\includegraphics[width=0.95\textwidth]{sct-schematics.pdf}
\caption{FSI interferometer optical path(s)}
\label{optical-path}
\end{figure}

From a control software point of view, we need not bother too much with
the topology of the system, but only with its cardinalities. The
interferometer contains
\begin{compactitem}
\item one sweeping laser
\item four optical distribution paths , each one delivering 64 optical
    channels into a DIOT crate. In total, up to 256 optical signals will
    be measured.
\item four EDFAs (laser power amplifiers), exactly one EDFA per optical
    distribution path; and eight power meters (two power meters per
    EDFA) to monitor the power delivered.
\item four identical DIOT crates (not shown in
    Figure~\ref{optical-path}), each one receiving 64 optical signals,
    totaling up to 256 optical channels. Each crate has 8 so-called
    \emph{photodetector modules}, each one having 8 channels.
\item a GPU-powered server that controls all the above and orchestrates
    the operation.
\end{compactitem}

From all of the above components, we care about the following:
\begin{compactdesc}
\item[laser] requires control and monitoring of its operational
parameters, which are a handful like output power and scanning
range/speed, and, critically, needs its sweeping action to be invoked
periodically from software.
\item[EDFA] requires control and monitoring of its operational
parameters, which essentially are switching on/off and setting a power
set point.
\item[power meters] require periodic reading.
\item[DIOT] this is \emph{transparent} to \libfsi\ library users; they
\emph{never} see anything of its inner workings, hence
we need not care about it%
\footnote{Well, almost. Some esoteric operations, like setting
photodetector gains or recovering the photodetector status, may be
required, but this can be safely ignored. In this document, 97.5\% of
statements will be true; there will be a 2.5\% of white lies or harmless
oversimplifications for the sake of better understanding}. 
Data acquired by its photodetector boards are
transferred back to the server, processed by the server GPU and \libfsi,
and returned in a suitable form to the server
\item[server] it runs a FESA process that takes care of periodically
read monitoring parameters of all the peripherals above,
invoke the laser sweep start, wait a predefined period for laser sweep,
data acquisition and data processing to finish, and finally retrieve
the values computed as a result; all of which is facilitated by calls to
\libfsi.
\end{compactdesc}

\section{Conventions on naming and enumeration of items}

Settling on some nomenclature and enumeration conventions from now on
will \emph{greatly avoid confusion} in our dealing with the FSI interferometer
components.

First of all, \emph{the server} will run \emph{the FESA code} mastering
all the operation of \emph{the interferometer}, aka \emph{the
apparatus}; all the operation of \emph{the apparatus} hinges on
\emph{the laser} feeding with optical power the measurement setup.
There is one and only one instance of each of the emphasized
terms in the last sentence; therefore, we are justified in using the
definite article~\emph{the} in our reference to them. So, remember:
there is one and only one \emph{interferometer}, with its \emph{server}
controlling via \emph{FESA code} its \emph{sweeping laser}.

The interferometer will have up to four EDFAs, numbered by and index or
identifier going from 0 to 3. In other words, the laser will feed up to
four EDFAs, numbered 0..3; each EDFA feeds one of the four separate
optical distribution blocks.

Each EDFA output will split in two branches numbered 0..1;
each of which will in its turn split into 32 branches. In total, 64
optical outputs or \emph{channels} will come out of each EDFA; we will
number them from 0 to 63. Channels 0..31 will come out from branch 0 of
the EDFA output, and channels 32..63 will come out from the branch 1 of
the EDFA output. There is nothing special making channels 0..31 and
32..63 different; the only thing the user must keep in mind is that
power delivered to channels 32..63 might be manually \emph{attenuated};
hence the split between branches 0 and 1 at the EDFA output.
Software-wise, we do not care at all.

Finally, we must put together all these items in a coherent, flat
schema of channel, EDFA, power meter, etc. labeling/enumeration. Let's
see how. This will be better understood if we go now backwards from
optical channels/outputs, upstream towards the laser.

The interferometer will have up to 256 channels; some of them may be
unused, of course. Channels will be numbered from 0 to 255, in order of
\begin{itemize}
\item optical distribution path; channels 0..63 will belong to optical
distribution path 0; channels 64..127 to optical distribution path 1,
and so forth. In other words, the first block of 64 channels will be in
the first optical distribution path; the second block of 64 channels
will be in the second optical distribution path, etc.
\item non-attenuated/attenuated split: within an optical distribution
path, the first block of 32 channels will belong to branch 0 of the
EDFA output, and the second block of 32 channels will belong to branch 1 of the
EDFA output.
\item DIOT crate: each optical distribution path will correspond with
the DIOT crate of the same number, i.e., DIOT crate 2 will correspond to
o.d.p. 2 and hence receive channels 128..191, of which 128..159 will be
non-attenuated and 160..191 might be attenuated.
\item photodetector board: each DIOT has eight photodetector boards
numbered 0..7, each one having eight channels numbered 0..7. Channels
will fill the DIOT crate in order, 
\end{itemize}

\end{document}
