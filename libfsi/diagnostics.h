/*
 * Copyright CERN 2021-2023
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

/*
 * This file contains a detailed explanation of the structs relevant to
 * diagnostic information for the FSI interferometer.
 *
 * The explanation is verbose as it hinges on a large set of nested
 * structures. To guide navigation, the file is split in sections. It is
 * convenient to know that the roots of the data structure tree are the
 * following:
 *
 *	struct fsi_qa_params
 *	struct fsi_edfa_status
 *	struct fsi_edfa_expert_status
 *	struct fsi_laser_status
 *	struct fsi_laser_expert_status
 *	struct fsi_hw_diagnostics
 */

/**
 * ---------------------------- REF/GC diagnostics ----------------------------
 */

/* Quality assurance diagnostics for linearization and gas cell reading
 * are obtained by calling \ref fsi_get_qa_params. Grosso modo, the
 * diagnostics compute how narrow is the reference interferometer
 * spectrum, and how good is the LSQ fit of the GC peak frequencies
 * found with respect to the NIST reference data of the HCN spectrum
 */

/** retrieve quality parameters of linearized and gas cell fit */
int fsi_get_qa_params(FSI *h, struct fsi_qa_params *qa);


/** 
 * \brief linearization quality figures of merit for a reference channel
 *
 * The meaning of each field is the following:
 *
 * \var fsi_qa_ref_params::ref_id
 *     Channel ID of the reference channel affected (up to \ref FSI_NBLOCKS ref
 *     channels can live in an interferometer)
 * \var fsi_qa_ref_params::ref_center
 *	Center of the single peak found in the ref channel spectrum
 *	after linearization
 * \var fsi_qa_ref_params::ref_deviation
 *	Full width at half maximum of said single peak, giving a measure
 *	of its spread
 * \var fsi_qa_ref_params::ref_deviation95
 *	Full width at 95% (i.e., at 5% height of the peak), giving a
 *	measure of its spread at the base, near the noise floor
 */
struct fsi_qa_ref_params {
	int	ref_id;			/**< reference channel id, -1 if unknown */
	double	ref_center;		/**< center of peak in lin. ref. spectrum */
	double	ref_deviation;		/**< FWHM of peak in lin. ref. spectrum */
	double	ref_deviation95;	/**< FW at 95% of peak in lin. ref. spectrum */
};

/** 
 * \brief linearization quality figures of merit for a reference channel
 *
 * The meaning of each field is the following:
 *
 * \var	fsi_qa_gc_params::gc_id
 *	Channel ID of the analyzed gas cell
 * \var	fsi_qa_gc_params::gc_alpha
 *	Computed alpha for the gas cell
 * \var	fsi_qa_gc_params::gc_zero_value
 *	Ordinate at the origin of the fit. The lsq linear fit gives
 *	freq = A t + B, where the speed (alpha) is A and B is the
 *	zero-time freq.
 * \var	fsi_qa_gc_params::gc_fitting_err
 *	This is the RMS (quadratic norm) of the deviations from the
 *	ideal NIST peaks computed over the found peaks in the alpha
 *	computation process
 * \var	fsi_qa_gc_params::gc_npeaks_fit_R
 *	Number of peaks used from the R lobe. The fitting algorithm is
 *	fed with a contiguous set of peaks spread from the center gap
 *	between R and P lobes, i.e., it may omit some of the initial or
 *	final peaks of the standard NIST HCN spectrum, which are most
 *	uncertain because of their smaller amplitude
 * \var	fsi_qa_gc_params::gc_npeaks_fit_P
 *	Number of peaks used from the P lobe
 * \var	fsi_qa_gc_params::gc_peaks_R[27]
 *	Actual R lobe peaks used to fit
 * \var	fsi_qa_gc_params::gc_peaks_P[26]
 *	Actual P lobe peaks used to fit
 */
struct fsi_qa_gc_params {
	int	gc_id;			/**< gas cell channel id, -1 if unknown */
	double	gc_alpha;		/**< alpha value computed through GC fit */
	double	gc_zero_value;		/**< value of linear fit at zero freq */
	double	gc_fitting_err;		/**< rms of deviation from NIST HCN ref */
	int	gc_npeaks_fit_R;	/**< number of R peaks used in GC fit */
	int	gc_npeaks_fit_P;	/**< number of P peaks used in GC fit */
	double	gc_peaks_R[27];		/**< wavelengths of R peaks going into fit */
	double	gc_peaks_P[26];		/**< wavelengths of P peaks going into fit */
	double	gc_reserved[4];		/**< reserved for future extensions */
};

/** quality parameters of linearization and gas cell fit
 *
 * \var	nref
 *	Number of entries in the ref array below (usually 1 or 4)
 * \var	ngc
 *	Number of entries in the gc array below (one, up to two)
 * \var	ref[FSI_REF_CH_MAX]
 *	Array of \ref fsi_qa_ref_params results
 * \var	gc[FSI_REF_CH_MAX]
 *	Array of \ref fsi_qa_gc_params results
 *  \note
 *  The linearization can be assessed by the spectral shape of the
 *  reference interferometer signal; it should ideally be a single,
 *  very narrow peak around a frequency dependent only on the geometry
 *  of the reference channel.
 *
 *  \note
 *  As to the gas cell fit, the linear fit of the GC signal peaks to
 *  the NIST SRM2519a data has the RMS deviation as its figure of
 *  merit. For the sake of post-mortem analysis, the peaks actually
 *  found *and* used in the fit are also produced. Note that not all R
 *  and P peaks may intervene in the fit computation; there might be
 *  some undetected/unused peaks at the extremes of the swept range
 *  (i.e., the highest-numbered peak IDs).
 */
struct fsi_qa_params {
	int				nref;			/**< number of valid ref entries */
	int				ngc;			/**< number of valid gc  entries */
	struct fsi_qa_ref_params	ref[FSI_REF_CH_MAX];	/**< ref qa values cf. \ref fsi_qa_ref_params */
	struct fsi_qa_gc_params		gc[FSI_GC_CH_MAX];	/**< gc  qa values cf. \ref fsi_qa_gc_params */
	uint32_t			qa_reserved[256];	/**< reserved for future extensions */
};


/**
 * ---------------------------- EDFA diagnostics/statuses ----------------------------
 */
/**
 * Both laser and EDFA can have their status reported at different
 * levels of detail.
 *
 * The *_get_status functions are meant to be called periodically to
 *      report states of permanent interest to the application
 * The *_expert_status functions are meant to be called very rarely,
 *      to report states of sporadic or one-off interest to the application
 * The *alarm* functions retrieve the alarm states of the device; they
 *	would typically be called often. See at the end the list of EDFA
 *	alarms and their meanings, which can be extracted with the
 *	fsi_decode_alarms convenience function
 */
int fsi_edfa_get_status(struct fsi_edfa *h, struct fsi_edfa_status *st);
int fsi_edfa_get_expert_status(struct fsi_edfa *h, struct fsi_edfa_expert_status *st);
int fsi_edfa_get_alarms(struct fsi_edfa *h, uint32_t *alarms);
void fsi_decode_alarms(uint32_t alarm_mask, char *dst);

/**
 * set of parameters of interest for us in EDFA monitoring. We 
 */
struct fsi_edfa_status {
	double	input_power;			/**< measured power at EDFA input (mW) */
	union {	/* alias */
		double	power;			/**< power setpoint requested (mW) */
		double	power_setpoint;		/**< power setpoint requested (mW) */
	};
	union {	/* alias */
		double	output_power;		/**< measured power at EDFA output (mW) */
		double	measured_power;		/**< measured power at EDFA output (mW) */
	};
	int	edfa_on;			/**< is power (=amplification) activated? */
	int	key_locked;			/**< is EDFA key-locked? */
	int	reserved[8];
};

/**
 * this set of parameters is only of interest to the expert monitoring
 * the correct EDFA functioning; it should not be retrieved unless
 * explicitly requested to do so by an expert command. Most of them are
 * constants of the appartus and never change (e.g. serial number or
 * operational limits)
 */
struct fsi_edfa_expert_status {
	char	serial_number[20];			/* SNU?		 */
	char	description[128];			/* DES?		 */
	char	version[20];				/* VER?		 */
	char	firmware_information[128];		/* FWI?		 */

	int	control_mode;				/* ASS?/ASS=	 */

	int	autostart;				/* AST?/AST=	 */
	double	case_temperature;			/* CAT?		 */

	int	preamp_diode_current_setpoint;		/* IC1?/IC1=	 */
	int	measured_preamp_diode_current_setpoint;	/* ID1?		 */
	double	preamp_diode_temperature;		/* TD1?		 */

	int	prebooster_diode_current_setpoint;	/* IC2?/IC2=	 */
	int	measured_prebooster_diode_current_setpoint;	/* ID2?  */
	double	nominal_power;				/* PON?		 */
	double	minimum_power_setpoint;			/* POM?		 */
	double	setpoint_output_power;			/* SOP?/SOP=	 */

	double	input_power_alarm_threshold;		/* IPT1?	 */

	double	limit_current_preamp;			/* LCM1?	 */
	double	limit_current_preboost;			/* LCM2?	 */
	double	output_power_mute;			/* PWM?		 */
	int	reserved[16];
};

/**
 *  edfa_alarms - table to decode the alarm mask bits
 */
static struct edfa_alarms {
	int	bit;
	char	*message;
} edfa_alarms[] = {
	{ .bit = 21, .message = "Board temperature error", },
	{ .bit = 20, .message = "Power supply error", },
	{ .bit = 19, .message = "GPIO Key OFF activated", },
	{ .bit = 15, .message = "Low input power 1", },
	{ .bit = 14, .message = "Low output power 1", },
	{ .bit = 13, .message = "Low input power 2", },
	{ .bit = 12, .message = "Low output power 2", },
	{ .bit = 11, .message = "Preamp current error", },
	{ .bit = 10, .message = "Booster current error", },
	{ .bit = 3 , .message = "preamp temp error", },
	{ .bit = 2 , .message = "booster temp error", },
};


/**
 * ---------------------------- laser diagnostics/statuses ----------------------------
 */
/**
 * Both laser and EDFA can have their status reported at different
 * levels of detail.
 *
 * The *_get_status functions are meant to be called periodically to
 *      report states of permanent interest to the application
 * The *_expert_status functions are meant to be called very rarely,
 *      to report states of sporadic or one-off interest to the application
 * The fsi_laser_get_diagnostic_info reports specifically the laser
 *	errors accumulated in the device error buffer. There is a large
 *	number of conditions reported (see table below,
 *	fsi_laser_error_codes), but it is likely that all of them should
 *	be coalesced to at most three categories (OK, error, warning).
 */
int fsi_laser_get_status(struct fsi_laser *h, struct fsi_laser_status *st);
int fsi_laser_get_expert_status(struct fsi_laser *h, struct fsi_laser_expert_status *st);
int fsi_laser_get_diagnostic_info(struct fsi_laser *h, struct fsi_laser_errs *errs);
char* fsi_laser_decode_error(int code);

/** laser status properties for routine laser operation
 *
 *  we normally want to have all of them retrieved/refreshed at every
 *  cycle/scan event
 */
struct fsi_laser_status {
	int	key_locked;		/**< is physical key locked? */
	int	soft_locked;		/**< is software interlock on? */
	int	power_on;		/**< is laser on? */
	double	power;			/**< laser power */
	double	sweep_start;		/**< wavelength in nm for sweep start */
	double	sweep_stop;		/**< wavelength in nm for sweep end */
	double	sweep_speed;		/**< sweep speed in nm/s */
	double	temperature;		/**< diode temperature in C */

	struct fsi_laser_errs
		diagnostic_info;	/**< last error messages */

	struct timeval
		timestamp;		/**< timestamp of status request */
	long	reserved[4];
};

/** laser expert status properties, never required in normal operation
 *  and useful only for expert manipulation of laser
 *
 *  it would be advisable to retrieve these only when explicitly
 *  required, for debugging purposes, as they are seldom changed or even
 *  used
 */
struct fsi_laser_expert_status {
	char	serial_device[PATH_MAX];	/**< serial line for comm */
	char	device_id[FSI_LASER_MAX_LENGTH];
						/**< laser identification string */
	double	power_max;			/**< nominal max power */
	double	power_min;			/**< nominal min power */
	double	wavelength_max;			/**< nominal maximum sweep wavelength */
	double	wavelength_min;			/**< nominal minimum sweep wavelength */
	double	operation_hours;		/**< unit total operation time */
	int	mode;				/**< sweep mode (always 2) */
	int	modulation_source;		/**< never used **/
	int	trigger_polarity;		/**< 0 active low, 1 active high */
	int	cycles;				/**< number of sweep iterations per scan */
	int	remaining_cycle_cnt;		/**< remaining number of sweeps in current scan */
	int	dwell_time;			/**< time in ms between sweeps */
	int	operation_complete;		/**< see fsi_operation_complete_codes */

	struct timeval
		timestamp;			/**< timestamp of status request */
	long	reserved[4];
};


static struct fsi_laser_error_codes {
    int     code;
    char    *message;
} fsi_laser_error_codes[] = {

	{ -104,	"data received different than expected", },
	{ -108,	"more parameters were received than expected", },
	{ -109,	"fewer parameters were received than required", },
	{ -110,	"unknown command received", },
	{ -113,	"header syntactically correct, but undefined for device", },
	{ -124,	"argument length exceeds the limit", },
	{ -140,	"parser could not find any known data type", },
	{ -141,	"data contains invalid characters", },
	{ -148,	"character found in argument not permitted", },
	{ -151,	"string consists too many chars or missing 2nd double quote", },

	{ -200,	"command can not be executed due to an illegal device condition", },
	{ -221,	"command can not be executed as settings conflict", },
	{ -222,	"command can not be executed as the argument was outside the range", },
	{ -310,	"error occurred while performing system preset", },
	{ -320,	"fault detected while writing to EEPROM", },

  	{    0, "no error has occurred", },
  	{    1, "laser over power occurred", },
  	{    2, "laser over current occurred", },
 	{   20, "temperature regulation not achieved before time-out", },
 	{   21, "temperature regulation not achieved after dropping out of regulation", },
	{ 200,	"FPGA initialization failure", },
	{ 201,	"power on self test (POST) failure", },
	{ 204,	"RTOS task initialization failure", },
	{ 220,	"POST: module operating temperature outside of limits", },
	{ 222,	"POST: voice coil motor (VCM) power test failure", },
	{ 223,	"POST: VCM offset calibration failure", },
	{ 224,	"POST: encoder initialization failure", },
	{ 225,	"POST: VCM polarity fault", },
	{ 226,	"POST: VCM travel limits not successfully found", },
	{ 227,	"POST: thermo-electric cooler (TEC) driver initialization failure", },
	{ 228,	"POST: analog circuit initialization failure", },
	{ 229,	"POST: over power test failure", },
	{ 230,	"POST: I2C communication failure", },
	{ 231,	"POST: Fan tachometer outside limits", },

	{ 232,	"POST: VCM Power, enabling -12V FPGA detects overvoltage", },
	{ 233,	"POST: VCM Power, enabling -12V FPGA detects duty limit", },
	{ 234,	"POST: VCM Power, VCMP greater than 50mV off ground", },
	{ 235,	"POST: VCM Power, VCMI offset exceeds limits", },
	{ 236,	"POST: VCM Power, VCM boost converter out-of-range", },
	{ 237,	"POST: High Speed configuration with only one operating fan", },
	{ 240,	"POST: VCM Encoder Misalignment Fault", },
	{ 241,	"POST: encoder index reset failure", },
	{ 242,	"POST: encoder index search failure", },
	{ 245,	"POST: measured TEC current suggests High Speed Assembly on Standard electronics configuration", },
	{ 247,	"POST: measured limits imply 4096x interpolation for High Speed Laser Assembly", },
	{ 248,	"POST: measured limits imply 416x interpolation for Standard Laser Assembly", },
	{ 250,	"POST: incorrect TEC drive polarity", },
	{ 251,	"POST: low TEC current reading while heating", },
	{ 252,	"POST: low TEC current reading while cooling", },
	{ 253,	"POST: external +5V excess deltaV under load", },
	{ 254,	"POST: +VLASER outside expected limits", },
	{ 255,	"POST: FPGA OVERI protection reset error", },
	{ 256,	"POST: laser current regulation exceeds limits", },
	{ 257,	"POST: laser diode short circuit protect error", },
	{ 258,	"POST: OVERI comparator tripped unexpectedly", },
	{ 259,	"POST: OVERI comparator did not trip as expected", },
	{ 260,	"VCM torque bias first move failure", },
	{ 261,	"VCM torque bias forward move failure", },
	{ 262,	"VCM torque bias reverse move failure", },
	{ 263,	"VCM homing failure", },
	{ 264,	"POST: VCM driver diagnostic failed during positive excursion", },
	{ 265,	"POST: VCM driver diagnostic failed during negative excursion", },
	{ 266,	"POST: VCM driver diagnostic, external +12V excess deltaV under load", },
	{ 267,	"POST: VCM driver diagnostic, VCM current lower than expected for installed laser configuration", },
	{ 268,	"POST: VCM driver diagnostic, -12V supply FPGA shutdown duty cycle limit timeout", },
	{ 269,	"POST: VCM driver diagnostic, -12V supply FPGA shutdown over voltage detected", },
	{ 270,	"laser tuning initialization failure", },
	{ 271,	"laser scanning data structure initialization failure", },
	{ 272,	"laser power initialization failure", },
	{ 273,	"laser current initialization failure", },
	{ 300,	"sweep motion failure", },
	{ 301,	"internal motion failure", },
	{ 302,	"flash memory erase or write failure", },
	{ 305,	"laser temperature thermal runaway error", },

	{ 306,	"move requested when motor controller is not initialized", },
	{ 307,	"move requested when motor controller is already busy", },
	{ 312,	"Calibration Data Save Failure", },
	{ 313,	"FPGA Update Failure", },
	{ 314,	"VCM Encoder Misalignment Run-Time Fault", },
	{ 320,	"power fault: external +5V supply less than 20 percent low", },
	{ 321,	"power fault: external +12V supply less than 20 percent low", },
	{ 322,	"power fault: internal -12V supply shutdown due to output over-voltage", },
	{ 323,	"power fault: internal -12V supply shutdown due to duty cycle limit", },
	{ 324,	"power fault: internal -12V supply less than 20 percent low", },
	{ 325,	"Fan1 tachometer reading outside expected limits", },
	{ 326,	"Fan2 tachometer reading outside expected limits", },
};

/**
 * ---------------------------- DIOT/HW/ACQ diagnostics ----------------------------
 */

/*
 * Diagnostics of conditions appearing at the level of
 *    - DI/OT hardware (essentially, board presence)
 *    - data acquisition (ADC signal conversion and DMA)
 *    - data transmission (server<->DI/OT data link losses)
 *    - photodetector hardware (severe health problems thereof)
 * Most of the contents of these data structures are irrelevant/overkill
 * or unsuitable as FESA properties; only the "status" fields should be
 * reported in a tri-state (OK/ERROR/WARNING) value; all else being
 * furnished (if at all) in a form HW experts might peruse when
 * diagnosing severe errors.
 *
 * In other words: ignore everything here except the OK/WARNING/ERROR
 * statuses, and export the entire fsi_hw_diagnostics structure as a
 * binary uninterpreted buffer if further analysis is required.
 */

/** amount of reserved bytes for hardware diagnostics */
#define	FSI_HW_DIAG_SIZE	16
#define FSI_CUSTOM		10		/**< some bytes for dev data */
#define FSI_RESERVED		10		/**< reserved for future use */

/**
 * \brief
 *	hardware diagnostics - main structure retrieved in one shot via
 *	the API call \ref fsi_get_hw_diagnostics. It encompasses DI/OT
 *	hw diagnostics, transmission diagnostics and acquisition
 *	diagnostics; each of the related structures contains a 'status'
 *	field, the only one of interest for the upper layers to report
 *	in tri-state form (OK/WARNING/ERROR). If further analysis is
 *	required, the entire struct should be exported as a binary
 *	buffer to be interpreted by an expert (tool)
 */
struct fsi_hw_diagnostics {
	struct fsi_diag_hw       hw;		/**< see fsi_diag_hw    in fsi_txrx.h */
	struct fsi_diag_trans    trans;		/**< see fsi_diag_trans in fsi_txrx.h */
	struct fsi_diag_acq      acq;		/**< see fsi_diag_acq   in fsi_txrx.h */
	uint32_t custom[FSI_CUSTOM];
	uint32_t reserved[FSI_RESERVED];
};
const int fsi_hw_diagnostics_size = sizeof(struct fsi_hw_diagnostics);

/**
 * \note
 *	TODO: a macro/translator function must be given along these lines:
 *
 * #define FSI_STATUS_OK	0
 * #define FSI_STATUS_WARNING	-1
 * #define FSI_STATUS_ERROR	1
 * int fsi_status_classify(int status);		/**< status field translation * /
 * int fsi_is_ok(int status);			/**< status field is OK * /
 * int fsi_is_warning(int status);		/**< status field is WARNING * /
 * int fsi_is_error(int status);		/**< status field is ERROR * /
 *
 * or, better, just leave 'status' fields with the FSI_STATUS_* values
 */

/**
 * ---------------------------- DI/OT diagnostics/statuses ----------------------------
 */
/* DI/OT HW statuses */
#define FSI_HSTATUS_OK       0			/**< no HW errors,  */
#define FSI_HSTATUS_ERR      1			/**< HW error -> go read DIOT-sepecific diags */
/** 
 * \brief
	DI/OT hw diagnostics
 * \warning
 *	only tri-state 'status' field of interest here for FESA
 */
struct fsi_diag_hw {
	uint32_t status;			/**< generic OK or not OK status */
	uint32_t board_pres_mask;		/**< mask of present boards as detected by DIOT-SB */
	uint32_t reserved[FSI_RESERVED];
};

/**
 * ---------------------------- transmission (server<->DIOT) diagnostics ----------------------------
 */
/* transmission statuses */
#define FSI_TSTATUS_OK       0			/**< no transmssion errors,  */
#define FSI_TSTATUS_ERR      1			/**< an unidentified transmission error */
#define FSI_TSTATUS_LOST_PKT 2			/**< an unidentified transmission error */
/** 
 * \brief
 *	transmission diagnostics
 * \warning
 *	only tri-state 'status' field of interest here for FESA
 */
struct fsi_diag_trans {
	uint32_t status;				/**< TCP/UDP: status of transmission */
	uint32_t pkts_txed;				/**< TCP=1, UDP: number of successfully txed pkts */
	uint32_t pkts_lost;				/**< TCP=0, UDP: number of lost pkts */
	uint32_t bytes_txed;				/**< TCP/UPD: sccessfully transmitted bytes */
	uint32_t reserved[FSI_RESERVED];
};

/**
 * ---------------------------- acquisition (photodetector/DMA) diagnostics ----------------------------
 */

/** 
 * \brief
 *	acquisition diagnostics
 * \warning
 *	only tri-state 'status' field of interest here for FESA
 */
struct fsi_diag_acq {
	uint32_t			status;		/**< acquisition status (general OK or not) */
	struct fsi_dma_status		dma_s[2];	/**< DMA status for the two banks */
	struct fsi_dma_config   	dma_c[2];	/**< DMA config for the two banks */
	struct fsi_periph_status	per_s[8];	/**< Photodet peripheral boards status */
	struct fsi_periph_config	per_c[8];	/**< Photodet peripheral boards config */
	uint32_t			acq_trig_type;	/**< Enum: 0: disabled; 1: external; 2: software */
	uint32_t reserved[FSI_RESERVED];
};

/**
 * \brief
 *	DMA status (aligned with sys_top_sub_regs.cheby)
 * \warning
 *	summarized as OK/ERR/WARNING in the 'status' field of
 *	fsi_diag_acq
 * \warning
 *	If at all, exported by FESA within the fsi_hw_diagnostics
 *	uninterpreted binary blob; feel free to ignore
 */
struct fsi_dma_status{
	uint8_t	axi_err;				/**< AXI bus errors */
	uint8_t	fifo_max;				/**< Max the FIFO was filled */
	uint8_t	fifo_err;				/**< FIFO errors */
	uint8_t	reserved[FSI_RESERVED];
};

/**
 * \brief
 *	DMA configuration of last acquisition
 * \warning
 *	of interest only in post-mortem debugging of a failed
 *	acquisition.
 * \warning
 *	If at all, exported by FESA within the fsi_hw_diagnostics
 *	uninterpreted binary blob; feel free to ignore
 */
struct fsi_dma_config {
	uint32_t dma_length;				/**< depth of dma buffer */
	uint32_t dma_addr;				/**< start address of memory */
	uint32_t reserved[FSI_RESERVED];
};

/** 
 * \brief
 *	photodetector board status (aligned with sys_top_sub_regs.cheby)
 * \warning
 *	summarized as OK/ERR/WARNING in the 'status' field of
 *	fsi_diag_acq
 * \warning
 *	these conditions have several levels of severity. In general:
 *	- spi_busy should NEVER occur (it is a transient condition only
 *	  during configuration of the ADC, not in acquisition)
 *	- the *_locked bits MUST be true; it is a serious problem if not
 *	- in short, a boolean function of these bits must be translated
 *	  into an OK/ERROR value of the fsi_diag_acq status field
 */
struct fsi_periph_status {
	union {
	uint32_t bitmask;	/**< status bitmask, see below struct */
	struct periph_status {
		unsigned spi_busy    :1;	/**< 0: SPI busy bit */
		unsigned triggers    :1;    	/**< 1: Trigger bit */
		unsigned servmod_in  :1;  	/**< 2: Servmod inputs (1: not present, 0: present) */
		unsigned fsi_ready   :1;   	/**< 3: Ready status */
		unsigned sys_locked  :1;  	/**< 4: sys->periph synchronized */
		unsigned adc_locked  :1;  	/**< 5: periph adc synchronized */
		unsigned rx_locked   :1;   	/**< 6: peryph->sys rx synchronized */
		unsigned reserved    :25;	/**< 7-31: reserved bits, 25 to make the mask 32 bits.. */
	};
	/**< WARNING: check layout of these bitfields: NONPORTABLE!! */
	};
};

/* some values for the configuration record of a photodetector */
# define FSI_ADC_CONFIG_MEAS   0		/**< acquire data */
# define FSI_ADC_CONFIG_RAMP   1		/**< generate ramp */
/**
 * \brief
 *	photodetector board config relevant for the acquisition
 * \warning
 *	of interest only in post-mortem debugging of a failed
 *	ADC reading operation
 * \warning
 *	If at all, exported by FESA within the fsi_hw_diagnostics
 *	uninterpreted binary blob; feel free to ignore
 */
struct fsi_periph_config {
	uint32_t periph_enabled;			/**< Enable/disable periph board
								0: disabled (even if present)
								1: enabled (it might not be present) */
	uint32_t afe_gain;				/**< Configured gain */
	uint32_t adc_config;				/**< Measure or generate ramp */
	uint32_t reserved[FSI_RESERVED];
};
