/*
 * Copyright CERN 2021
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "fsi-edfa.h"
#include "fsi-edfa-private.h"

/* type-unchecked generic property manipulation interface
 *
 * A property of the EDFA can be read or set by means of the narrow
 * interface below. The argument value is a structure that contains
 * the property number to get/set (from enum fsi_edfa_property_idx),
 * the type of the property (from enum fsi_edfa_property_type), and
 * a type-unchecked area for the actual value of the property, which
 * might be
 *  - a command (e.g. fsi_ep_reset_alarms): no actual value, write-only action
 *  - an integer or boolean
 *  - a float
 *  - a string less than FSI_EDFA_MAX_LENGTH (global static limit)
 *
 *  While properties are sanity-checked (e.g, you cannot get() a command
 *  property nor set() a read-only property; the type is double-checked
 *  internally for consistency, etc), it is up to the user to
 *  ensure the correctness of the untyped value of the property.
 *  It is probably far more convenient and safer, in general, to
 *  call type-checked convenience functions of the wide API.
 */

struct fsi_edfa_property;
int fsi_edfa_set(struct fsi_edfa *h, struct fsi_edfa_property *value);
int fsi_edfa_get(struct fsi_edfa *h, struct fsi_edfa_property *value);

enum fsi_edfa_property_idx {
	fsi_ep_serial_number                                  = 0,
	fsi_ep_description                                    = 1,
	fsi_ep_customer_serial_number                         = 2,
	fsi_ep_user_parameter_1                               = 3,
	fsi_ep_user_parameter_2                               = 4,
	fsi_ep_version                                        = 5,
	fsi_ep_firmware_information                           = 6,
	fsi_ep_control_mode                                   = 7,
	fsi_ep_autostart                                      = 8,
	fsi_ep_case_temperature                               = 9,
	fsi_ep_recover_factory_settings                       = 10,
	fsi_ep_preamp_diode_current_setpoint                  = 11,
	fsi_ep_measured_preamp_diode_current_setpoint         = 12,
	fsi_ep_preamp_diode_temperature                       = 13,
	fsi_ep_prebooster_diode_current_setpoint              = 14,
	fsi_ep_measured_prebooster_diode_current_setpoint     = 15,
	fsi_ep_input_power                                    = 16,
	fsi_ep_output_power                                   = 17,
	fsi_ep_nominal_power                                  = 18,
	fsi_ep_minimum_power_setpoint                         = 19,
	fsi_ep_setpoint_output_power                          = 20,
	fsi_ep_minimum_settable_gain                          = 21,
	fsi_ep_maximum_settable_gain                          = 22,
	fsi_ep_agc_setpoint                                   = 23,
	fsi_ep_input_power_alarm_threshold                    = 24,
	fsi_ep_alarms                                         = 25,
	fsi_ep_reset_alarms                                   = 26,
	fsi_ep_warnings                                       = 27,
	fsi_ep_limit_current_preamp                           = 28,
	fsi_ep_limit_current_preboost                         = 29,
	fsi_ep_output_power_mute                              = 30,
};

enum fsi_edfa_property_type {
	fsi_ept_command		= 0,
	fsi_ept_boolean		= 1,
	fsi_ept_integer		= 2,
	fsi_ept_floating_point	= 3,
	fsi_ept_string		= 4,
};

struct fsi_edfa_property {
	enum fsi_edfa_property_idx	id;
	enum fsi_edfa_property_type	type;
	int	reserved[4];
	union {
		int	integer;
		double	floating_point;
		char	string[FSI_EDFA_MAX_LENGTH];
	};
};


struct fsi_edfa_property_ops {
	int (*setter)(struct fsi_edfa *h, struct fsi_edfa_property *prop);
	int (*getter)(struct fsi_edfa *h, struct fsi_edfa_property *prop);
	int (*validate)(struct fsi_edfa *h, struct fsi_edfa_property *prop);
};

static struct fsi_property_descriptor {
	enum fsi_edfa_property_idx	id;
	enum fsi_edfa_property_type	type;
	struct fsi_edfa_property_ops *ops;
	int (*(reserved[5]))(struct fsi_edfa *h, struct fsi_edfa_property *prop);
} edfa_properties[] = {
	[fsi_ep_serial_number] 			= { .id = fsi_ep_serial_number,			.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_description] 			= { .id = fsi_ep_description,			.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_customer_serial_number]		= { .id = fsi_ep_customer_serial_number,	.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_user_parameter_1] 		= { .id = fsi_ep_user_parameter_1,		.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_user_parameter_2] 		= { .id = fsi_ep_user_parameter_2,		.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_version] 			= { .id = fsi_ep_version,			.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_firmware_information] 		= { .id = fsi_ep_firmware_information,		.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_control_mode] 			= { .id = fsi_ep_control_mode,			.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_autostart] 			= { .id = fsi_ep_autostart,			.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_case_temperature] 		= { .id = fsi_ep_case_temperature,		.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_recover_factory_settings] 	= { .id = fsi_ep_recover_factory_settings,	.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_preamp_diode_current_setpoint] 	= { .id = fsi_ep_preamp_diode_current_setpoint,	.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_measured_preamp_diode_current_setpoint]		= { .id = fsi_ep_measured_preamp_diode_current_setpoint,	.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_preamp_diode_temperature]			= { .id = fsi_ep_preamp_diode_temperature,			.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_prebooster_diode_current_setpoint]		= { .id = fsi_ep_prebooster_diode_current_setpoint,		.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_measured_prebooster_diode_current_setpoint]	= { .id = fsi_ep_measured_prebooster_diode_current_setpoint,	.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_input_power] 			= { .id = fsi_ep_input_power,			.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_output_power] 			= { .id = fsi_ep_output_power,			.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_nominal_power] 			= { .id = fsi_ep_nominal_power,			.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_minimum_power_setpoint] 	= { .id = fsi_ep_minimum_power_setpoint,	.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_setpoint_output_power] 		= { .id = fsi_ep_setpoint_output_power,		.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_minimum_settable_gain] 		= { .id = fsi_ep_minimum_settable_gain,		.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_maximum_settable_gain]	 	= { .id = fsi_ep_maximum_settable_gain,		.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_agc_setpoint] 			= { .id = fsi_ep_agc_setpoint,			.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_input_power_alarm_threshold] 	= { .id = fsi_ep_input_power_alarm_threshold,	.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_alarms] 			= { .id = fsi_ep_alarms,			.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_reset_alarms] 			= { .id = fsi_ep_reset_alarms,			.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_warnings] 			= { .id = fsi_ep_warnings,			.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_limit_current_preamp] 		= { .id = fsi_ep_limit_current_preamp,		.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_limit_current_preboost] 	= { .id = fsi_ep_limit_current_preboost,	.type = fsi_ept_command, .ops = NULL },
	[fsi_ep_output_power_mute] 		= { .id = fsi_ep_output_power_mute,		.type = fsi_ept_command, .ops = NULL },
};

__attribute__ ((__unused__))
static int edfa_properties_no = sizeof(edfa_properties) / sizeof(edfa_properties[0]);

int fsi_edfa_set(struct fsi_edfa *h, struct fsi_edfa_property *value)
{
	return 0;
}

int fsi_edfa_get(struct fsi_edfa *h, struct fsi_edfa_property *value)
{
	return 0;
}

/*
SNU?	serial_number
DES?	description
SNC?	customer_serial_number
SNC=

PAR1?	user_parameter_1
PAR1=
PAR2?	user_parameter_2
PAR?=

VER?	version
FWI?	firmware_information
ASS?	control_mode
ASS=

AST?	autostart
AST=
CAT?	case_temperature
RKP	recover_factory_settings

IC1?	preamp_diode_current_setpoint
IC1=
ID1?	measured_preamp_diode_current_setpoint

TD1?	preamp_diode_temperature

IC2?	prebooster_diode_current_setpoint
IC2=
ID2?    measured_prebooster_diode_current_setpoint

IPW?	input_power
OPW?	output_power
PON?	nominal_power
POM?	minimum_power_setpoint
SOP?	setpoint_output_power
SOP=	

GAMIN?	minimum_settable_gain
GAMAX	maximum_settable_gain?
CGA?	agc_setpoint
CGA=

IPT1?	input_power_alarm_threshold
IPT1=
ALA?	alarms
ALA=	reset_alarms
WAR?	warnings

LCM1?	limit_current_preamp
LCM2?	limit_current_preboost
PWM?	output_power_mute
*/
