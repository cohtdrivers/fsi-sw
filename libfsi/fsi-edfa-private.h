/*
 * Copyright CERN 2023
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef FSI_LASER_PRIVATE_H_
#define FSI_LASER_PRIVATE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <linux/limits.h>
#include <stdio.h>

/* GPIO pin for the interlock key of the EDFA */
#define	FSI_EDFA_ALARM_KEYOFF_BIT	19

struct fsi_edfa {
	char				serial_port[PATH_MAX];
	int				fd;
	int				index;
	struct fsi_edfa_status		status;
	struct fsi_edfa_expert_status	expert_status;
	double                          dbm_scale;      /* 10 for 500mW EDFA, 1 for old model */
	char				*num_format;	/* .0f for 500mW EDFA, .1f for old model */
};

#ifdef __cplusplus
}
#endif

#endif /* FSI_LASER_PRIVATE_H_ */
