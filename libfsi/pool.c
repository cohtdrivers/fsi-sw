#include <sys/mman.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

/* FIXME: share these values with python side */
char pool_name[] = "fsi";
unsigned long pool_size = 16384 * (1L<<20);

static struct fsi_mem_pool {
	int		fd;
	void		*addr;
	unsigned long	size;
} mem_pool;

struct fsi_mem_pool *fsi_get_mem_pool(void)
{
	struct fsi_mem_pool *mp = &mem_pool;
	int trace = 1;

	mp->size = pool_size;
	mp->fd = shm_open(pool_name, O_CREAT|O_RDWR, 0600);
	if (mp->fd < 0)
		goto errout;
	mp->addr = mmap(NULL, mp->size, PROT_READ|PROT_WRITE, MAP_SHARED, mp->fd, 0);
	if (mp->addr == NULL)
		goto errout;

	if (trace) {
		printf("fd %d\n", mp->fd);
		printf("shmem [%s] size [%ld] mapped at %p for fd %d\n",
			pool_name, mp->size, mp->addr, mp->fd);
	}

	return mp;
errout:
	return NULL;
}

int fsi_release_mem_pool(struct fsi_mem_pool *pool)
{
	int err;

        if ((err = munmap(pool->addr, pool->size)) < 0)
		return err;
        if ((err = close(pool->fd)) < 0)
		return err;
	return 0;
}
