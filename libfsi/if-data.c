/*
 * Copyright CERN 2023
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "fsi.h"
#include "fsi-private.h"

static double gauss(double x, double mu, double sigma)
{
	const double m_1_sqrt_2pi = .39894228040143267794;
	double t = (x-mu)/sigma;

	return m_1_sqrt_2pi / sigma * exp(-t*t/2.0);
}

static double lorentz(double x, double mu, double fwhm)
{
	double t = 2.0*(x-mu)/fwhm;

	return M_1_PI /(1 + t*t);
}

static double rand1(void)
{
	static int first = 1;

	if (first) {
		first = !first;
		srand(time(NULL));
	}
	return (double)rand() / RAND_MAX;
}

int simulate_peak(struct fsi_peak_config *cfg,
			 struct fsi_peak_result *res)
{
	const double relative_width = 10.0 + rand1();	/**< FIXME: randomize */
	double freq;
	int i, n = sizeof(res->samples) / sizeof(res->samples[0]);
	double (*profile)(double x, double mu, double sigma);

	res->center = cfg->position * (0.95 + rand1() * 0.1);
	res->width = (cfg->range1 + cfg->range2) / relative_width;
	res->type = cfg->type;

	switch (cfg->type) {
	case FSI_PEAK_LORENTZ:
		profile = lorentz;
		break;
	case FSI_PEAK_GAUSS:
		profile = gauss;
		break;
	default:
		return FSI_BAD_PEAK_TYPE;
		break;
	}
	for (i = 0; i < n; i++) {
		freq = res->center + res->width * (double)i / n / 2.0;
		res->samples[i] = profile(freq, res->center, res->width);
	}
	res->xunit = FSI_UNIT_HZ;
	res->snr = 70;

	return 0;	
}

int fulfil_peak(struct fsi_peak_config *cfg,
			 struct fsi_peak_result *res)
{
	return 0;
}

int if_data_main()
{
	int ret, i;
	FILE *f;

	struct fsi_peak_config cfg = {
		.position	= 5000000,
		.range1	= 4000,
		.range2 = 4000,
		.type = FSI_PEAK_GAUSS,
	};
	struct fsi_peak_result r, *res = &r;

	srand(time(NULL));
	ret = simulate_peak(&cfg, res);
	f = fopen("peak.bin", "wb");
	fwrite(res, sizeof(*res), 1, f);
	fclose(f);
	printf("sizeof %zd\n", sizeof(r));
	printf("ret: %d\n", ret);
	printf("mu = %.2f Hz, s = +-%.2f Hz, snr = %.1f dB, type = %d\n",
		res->center, res->width, res->snr, res->type);

	f = fopen("samples", "w");
	for (i = 0; i < FSI_PEAK_LENGTH; i++)
		fprintf(f, "%.2f\n", res->samples[i]);
	fclose(f);

	return 0;
}
