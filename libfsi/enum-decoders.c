/*
 * Copyright CERN 2021-2023
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdlib.h>

#include "fsi.h"
#include "fsi-laser.h"
#include "fsi-edfa.h"

/* auxiliary translation functions --- decode enums, constants,
 * errcodes, etc. into human-readable text
 */

static const char *decode_num(int num, const char *const xlat[], const int nconsts)
{
	if (num < 0 || num >= nconsts)
		return NULL;
	else
		return xlat[num];
}

/** produce a stringified name of an integer channel type */
const char *fsi_decode_channel_type(enum fsi_channel_type type)
{
	static const char *const xlat[] = {
		[FSI_CH_NONE] = "none",
		[FSI_CH_DISTANCE] = "distance",
		[FSI_CH_BRAGG] = "bragg",
		[FSI_CH_GAS_CELL] = "gas cell",
		[FSI_CH_INTERF] = "interf",
	};
	static int n = sizeof(xlat)/sizeof(xlat[0]);

	return decode_num(type, xlat, n);
}

/** produce a stringified name of an integer peak type */
const char *fsi_decode_peak_type(enum fsi_peak_type type)
{
	static const char *const xlat[] = {
		[FSI_PEAK_NONE] = "none",
		[FSI_PEAK_LORENTZ] = "lorentz",
		[FSI_PEAK_GAUSS] = "gauss",
		[FSI_PEAK_SINC] = "sinc",
		[FSI_PEAK_MOFFAT] = "moffat",
	};
	static int n = sizeof(xlat)/sizeof(xlat[0]);
	return decode_num(type, xlat, n);
}

/** produce a stringified name of an integer expert mode */
const char *fsi_decode_expert_mode(enum fsi_expert_mode type)
{
	static const char *const xlat[] = {
		[FSI_EXPERT_NONE] = 		"none",
		[FSI_EXPERT_LIN_DATA] = 	"lin_data",
		[FSI_EXPERT_OSCILLOSCOPE] = 	"oscilloscope",
		[FSI_EXPERT_GAS_CELL] = 	"gas_cell",
		[FSI_EXPERT_INTERF] = 		"interf",
		[FSI_EXPERT_RAW_GAS_CELL] = 	"raw_gas_cell",
		[FSI_EXPERT_RAW_INTERF] = 	"raw_interf",
		[FSI_EXPERT_LIN_FFT_M] = 	"lin_fft(m)",
		[FSI_EXPERT_LIN_FFT_HZ] =	"lin_fft(Hz)",
	};
	static int n = sizeof(xlat)/sizeof(xlat[0]);
	return decode_num(type, xlat, n);
}

struct err_xlat {
	int	errcode;
	char	*translation;
};

static const char *find_num(int num, struct err_xlat xlat[], const int nconsts)
{
	int i;

	for (i = 0; i < nconsts; i++)
		if (xlat[i].errcode == num)
			return xlat[i].translation;
	return NULL;
}

/** produce a stringified name of a libfsi error return code */
const char *fsi_decode_error_number(int err)
{
	static struct err_xlat xlat[] = {
		/* library error codes */
		{ FSI_CFG_BAD_NPEAKS,		"bad number of peaks", },
		{ FSI_BAD_PEAK_TYPE,		"bad peak type", },
		{ FSI_NOT_IMPLEMENTED,		"not implemented", },
		{ FSI_ABORTED_COMPUTATION,	"computation failed, no results", },
		{ FSI_SCAN_SYNC_ERR,		"server did not ack scan message", },
		{ FSI_SERVER_OUT_OF_SYNC,	"lib out of sync with server", },
		{ FSI_BAD_FULL_MODE,		"ello", },
		{ FSI_DIOT_DOWN,		"diot host not reachable", },

		/* error codes for laser API bad */
		{ FSI_LASER_OK,			"success", },
		{ FSI_LASER_CMD_TOO_LONG,	"raw cmd too long", },
		{ FSI_LASER_WRITE_ERR,		"laser serial write error", },
		{ FSI_LASER_WRITE_TRUNC,	"laser serial write truncated", },
		{ FSI_LASER_READ_ERR,		"laser answer read error", },
		{ FSI_LASER_ANSWER_ERR,		"unterminated laser cli answer", },
		{ FSI_LASER_INTERNAL_ERR,	"unterminated laser cli answer", },
		{ FSI_LASER_BAD_PROPERTY,	"unterminated laser cli answer", },
		{ FSI_LASER_CMD_ERR,		"laser cli cmd unknown", },
		{ FSI_LASER_ARG_ERR,		"laser cli invalid argument", },
		{ FSI_LASER_EXE_ERR,		"laser cli cmd execution error", },
		{ FSI_LASER_ACK,		"laser cli cmd acknowledged", },
		{ FSI_LASER_BAD_FLOAT,		"failed strtod on cli out", },
		{ FSI_LASER_BAD_INT,		"failed strtol on cli out", },
		{ FSI_LASER_BAD_BOOL,		"failed bool conversion on cli out", },
		{ FSI_LASER_UNKN_FAIL,		"failed conversion on cli out", },
		{ FSI_LASER_BAD_CONV,		"failed conversion into cli input", },

		/* EDFA API status codes */
		{ FSI_EDFA_OK,			"! answer from serial port", },
		{ FSI_EDFA_COMMAND_UNKNOWN,	"# answer from serial port", },
		{ FSI_EDFA_NOT_AUTHORIZED,	"* answer from serial port", },
		{ FSI_EDFA_COMMAND_NOT_VALID,	"$ answer from serial port", },
		{ FSI_EDFA_WRITE_ERR,		"command too long", },
		{ FSI_EDFA_WRITE_TRUNC,		"could not write to serial", },
		{ FSI_EDFA_READ_ERR,		"write incomplete", },
		{ FSI_EDFA_ANSWER_ERR,		"bad answer", },
	};
	static int n = sizeof(xlat)/sizeof(xlat[0]);
	return find_num(err, xlat, n);
}
