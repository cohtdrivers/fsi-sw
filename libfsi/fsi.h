/*
 * Copyright CERN 2021
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef FSI_LIB_H_
#define FSI_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "../solib/fsi_txrx.h"

/** types of channel inputs: distance meas. or Bragg */
enum fsi_channel_type {
	FSI_CH_NONE = 0,
	FSI_CH_DISTANCE = 1,	/**< configure as distance meas. channel */
	FSI_CH_BRAGG,		/**< configure as Bragg channel */
	FSI_CH_GAS_CELL,	/**< gas cell channel */
	FSI_CH_INTERF,		/**< reference interferometer channel */
};

/** types of functional forms to fit the peaks found */
enum fsi_peak_type {
	FSI_PEAK_NONE = 0,
	FSI_PEAK_LORENTZ = 1,	/**< Cauchy-Lorentz 1/(1+xˆ2) distribution */
	FSI_PEAK_GAUSS,		/**< Gaussian (normal) distribution */
	FSI_PEAK_SINC,		/**< Sinc (sin(x)/x)) distribution */
	FSI_PEAK_MOFFAT,	/**< Moffat (power of Cauchy pdf) distribution */
};

/** types of raw buffer requested by expert diagnostic functions */
enum fsi_expert_mode {
    FSI_EXPERT_NONE = 0,
    FSI_EXPERT_LIN_DATA,	/**< linearized time-domain data */
    FSI_EXPERT_OSCILLOSCOPE,	/**< oscilloscope mode - non-linearized raw signal */
    FSI_EXPERT_GAS_CELL,	/**< linearized input data buffer from gas cell */
    FSI_EXPERT_INTERF,		/**< linearized input data buffer from reference interferometer */
    FSI_EXPERT_RAW_GAS_CELL,	/**< non-linearized, raw input data buffer from gas cell */
    FSI_EXPERT_RAW_INTERF,	/**< non-linearized, raw input data buffer from reference interferometer */
    FSI_EXPERT_LIN_FFT_M,	/**< buffer of linearized FFT data (x axis in meters)  */
    FSI_EXPERT_LIN_FFT_HZ,	/**< buffer of linearized FFT data (x axis in Hz) */
};

/** natural units for different channel types*/
enum fsi_unit {
    FSI_UNIT_NONE = 0,
    FSI_UNIT_M,		/**< meters (distance channels) */
    FSI_UNIT_NM,	/**< nanometers (wavelength in Bragg channels) */
    FSI_UNIT_HZ,	/**< Hertz (for spectra; unused?) */
    FSI_UNIT_SAMPLE,	/**< dimensionless samples (unused?) */
    FSI_UNIT_DB,	/**< decibels for SNR (unused?) */
    FSI_UNIT_S,		/**< seconds for timescales (unused?) */
};

/** biggest number of model parameters among different peak types */
#define FSI_MAX_MODEL_PARAMS	10
/** max number of peaks to find in a channel sample */
#define FSI_MAX_PEAKS		6
/** length of return peak data */
#define	FSI_PEAK_LENGTH		200
/** size of sample vectors - this should not be hardcoded - tbd */
#define	FSI_SAMPLE_SIZE		2500000
#define FSI_BYTES_PER_SAMPLE	2
#define	FSI_SAMPLE_SIZE_BYTES	(FSI_SAMPLE_SIZE*FSI_SAMPLE_SIZE_BYTES)

/** amount of reserved bytes for hardware diagnostics */
#define	FSI_HW_DIAG_SIZE	16

/* library error codes */
#define	__FSI_ERR_START	-1024
#define	FSI_CFG_BAD_NPEAKS	__FSI_ERR_START-1
#define	FSI_BAD_PEAK_TYPE	__FSI_ERR_START-2
#define	FSI_NOT_IMPLEMENTED	__FSI_ERR_START-3
#define	FSI_ABORTED_COMPUTATION	__FSI_ERR_START-4
#define	FSI_SCAN_SYNC_ERR	__FSI_ERR_START-5
#define	FSI_SERVER_OUT_OF_SYNC	__FSI_ERR_START-6
#define	FSI_BAD_FULL_MODE	__FSI_ERR_START-7
#define	FSI_DIOT_DOWN		__FSI_ERR_START-8

/** peak finder/fitting configuration for an individual peak
 *
 * A channel will be configured with up to FSI_MAX_PEAKS of these to
 * find in its spectrum
 */
struct fsi_peak_config {
	int			peak_id;	/**< peak identifier -- unique within channel */
	double			position;	/**< estimated position where peak should be found (aka hint) */
	double			range1, range2;	/**< estimated interval of peak range */
	enum fsi_peak_type	type;		/**< shape of peak to fit (Gauss, Lorentz...) */
	enum fsi_unit		xunit;		/**< unit of the x axis (m, Hz if dist/Bragg) */
};

/** peak finder/fitting result for an individual peak
 *
 * A channel measurement result will be constituted by up to
 * FSI_MAX_PEAKS of these
 */
struct fsi_peak_result {
	int			peak_id;		/**< peak identifier -- unique within channel */
	int			valid;			/**< true only if peak fit was successful */
	enum fsi_peak_type	type;			/**< shape of peak fit to data */
	double			center;			/**< center of peak fitted to data */
	double			width;			/**< peak width derived from parameters */
	double			snr;			/**< peak signal-to-noise ratio (? tbd) */
	struct fit_params	{
		double	amplitude;
		double	mu;
		double	sigma;
		double	stderr;
		double	reserved[FSI_MAX_MODEL_PARAMS-4];
	} fit_params;				 	/**< goodness-of-fit params, model-dependent */
	double			samples[FSI_PEAK_LENGTH];	/**< actual peak samples */
	enum fsi_unit		xunit, yunit;
	int			nsamples;		/**< number of samples in samples[]
						             WARNING: nsamples == FSI_PEAK_LENGTH */
	double			start;			/**< value of x[0] in x units */
	double			end;			/**< reserved: unused */
	double			delta;			/**< value of x[1]-x[0] in x units
							     x(i) = begin + delta * i */
};

/**
 * channel configuration - this will set up a channel's data processing
 * pipeline for the next acq cycle. Up to FSI_MAX_PEAKS will be
 * requested
 */
struct fsi_channel_config {
	unsigned int	channel_id;		/**< channel to be configured */
	enum fsi_channel_type	type;		/**< type of channel (distance/Bragg) */
	int		npeaks;			/**< number of valid peaks to be requested in peaks array */
	int		channel_gain;		/**< bool: *do* make use of channel gain */
	int		dc_coupling;		/**< bool: true only if channel is DC coupled */
	struct fsi_peak_config
			peaks[FSI_MAX_PEAKS];	/**< peak requests (each peak has separate params */
	/** a disabled channel, though fully configured, is ignored by
	 * the measurement logic and does not return measurements.
	 * Useful to reserve a channel that might be swapped with
	 * another during commissioning */
	unsigned int	disabled;
};

/**
 * ADC/photodetector module HW diagnostics - tbd
 */
struct fsi_hw_status {
    union {
	uint8_t	diag[FSI_HW_DIAG_SIZE];
    };
};

/**
 * channel measurement results - the result of acq data processing for
 * an individual channel is returned here, with npeaks (up to 
 * FSI_MAX_PEAKS) peaks given in the case by their fit results (type of
 * function and its params, and an array of FSI_PEAK_LENGTH samples)
 *
 * WARNING: after d62dffe, peaks is a "sparse" array where invalid peaks
 * may be interspersed with valid ones; for consistency checking, n_valid_peaks
 * gives the exact number of peak entries having the 'valid' field set to != 0
 */
struct fsi_channel_measurement {
	unsigned int		channel_id;		/**< channel id of this measurement */
	enum fsi_channel_type	type;			/**< type of channel (distance/Bragg) */
	int			npeaks;			/**< number of requested entries in peaks array */
	int			n_valid_peaks;		/**< number of valid entries in peaks array */
	struct fsi_hw_status	hw_status;		/**< hardware error status */
	struct fsi_peak_result	peaks[FSI_MAX_PEAKS];	/**< peak measured values */
};

/**
 * this collects full/raw data for an individual channel, as per the
 * expert/diagnostic functions
 *
 * NB: all raw data as stored in the data[] array, with one remarkable
 * exception, fit a 16-bit unsigned integer: raw data in scope mode, raw
 * data linearized, gas cell data are the result of 12-bit ADC
 * operations, hence 16 bit of dynamic range are more than enough.
 * However, for spectra (linearized FFT), the values returned must be
 * given in a log scale, as the dyn range of interest in spectra may
 * span more than 100dB. The conversion of data[] values from log to
 * natural scales, when type is FSI_EXPERT_LIN_FFT, can be achieved by
 * means of the convenience functions given following the next struct
 * declaration
 */
struct fsi_full_measurement {
	unsigned int		channel_id;		/**< channel id of this measurement */
	enum fsi_channel_type	type;			/**< type of channel (distance/Bragg) */
	enum fsi_expert_mode	mode;			/**< mode of raw data: linearized FFT/data/oscill-raw/gas-cell */
	uint16_t		data[FSI_SAMPLE_SIZE];	/**< raw data (NB: logarithmic for spectra) */
	enum fsi_unit		xunit;			/**< unit of vector's x axis */
	int			nsamples;		/**< invariant: == FSI_SAMPLE_SIZE */
	double			start;			/**< value of x[0] in xunits */
	double			delta;			/**< value of x[1]-x[0] in xunits */
};

/** convenience functions to pass from/to data[] uint16_t log vector to
 * natural scale, double-valued vectors
 */
void fsi_natural_to_log(uint16_t data[], double natural[], int nsamples);
void fsi_log_to_natural(double natural[], uint16_t data[], int nsamples);


/** opaque library handle/context structure */
typedef struct __FSI_state FSI;

/* core API methods */

/** initialize library */
FSI *fsi_lib_init(const char *config_file);

/** close library */
void fsi_lib_exit(FSI *h);

/** configure a channel type, gain and expected peak params */
int fsi_channel_configure(FSI *h, struct fsi_channel_config *cfg);

/** get a channel last configured params */
int fsi_get_channel_configuration(FSI *h, unsigned int channel_id, struct fsi_channel_config *cfg);

/** bulk configure a channel array type, gain and expected peak params */
int fsi_channel_array_configure(FSI *h, struct fsi_channel_config cfg[], int nchannels);

/** receive the whole set of distance/bragg measurements from all channels */
int fsi_receive_measurements(FSI *h, struct fsi_channel_measurement meas[], int *nchannels);

/** receive the whole set of distance/bragg measurements from all channels with a timeout */
int fsi_receive_measurements_timemout(FSI *h, struct fsi_channel_measurement meas[], int *nchannels,
					unsigned int timeout);

/* set/stop scope mode functions */
int fsi_set_scope_mode(FSI *h, int channel_id, enum fsi_expert_mode mode);
void fsi_stop_scope_mode(FSI *h);

/** receive raw measurements of channel_id */
/* int fsi_get_full_samples(FSI *h, int channel_id, enum fsi_expert_mode mode, struct fsi_full_measurement *meas); */
int fsi_get_full_samples(FSI *h, struct fsi_full_measurement *meas);

/* auxiliary helper methods */
int fsi_get_gas_cell_id(FSI *h);
int fsi_get_interferometer_id(FSI *h);

struct fsi_laser *fsi_get_laser(FSI *h);
struct fsi_edfa *fsi_get_edfa(FSI *h, int id);
int fsi_scan(FSI *h);

/* quality parameters of linearization and gas cell fit */

#define	FSI_REF_CH_MAX	4		/**< maximum number of reference channels in IF */
#define	FSI_GC_CH_MAX	2		/**< maximum number of gac cell  channels in IF */

/**
 * \struct fsi_qa_ref_params
 * \brief
 * 	linearization quality figures of merit for a reference channel
 *
 *  The linearization can be assessed by the spectral shape of the
 *  reference interferometer signal; it should ideally be a single,
 *  very narrow peak around a frequency dependent only on the geometry
 *  of the reference channel.
 *
 * The meaning of each field is the following:
 *
 * \var fsi_qa_ref_params::ref_id
 *     Channel ID of the reference channel affected (up to \ref FSI_NBLOCKS ref
 *     channels can live in an interferometer)
 * \var fsi_qa_ref_params::ref_center
 *	Center of the single peak found in the ref channel spectrum
 *	after linearization
 * \var fsi_qa_ref_params::ref_deviation
 *	Full width at half maximum of said single peak, giving a measure
 *	of its spread
 * \var fsi_qa_ref_params::ref_deviation95
 *	Full width at 95% (i.e., at 5% height of the peak), giving a
 *	measure of its spread at the base, near the noise floor
 */
struct fsi_qa_ref_params {
	int	ref_id;			/**< reference channel id, -1 if unknown */
	double	ref_center;		/**< center of peak in lin. ref. spectrum */
	double	ref_deviation;		/**< FWHM of peak in lin. ref. spectrum */
	double	ref_deviation95;	/**< FW at 95% of peak in lin. ref. spectrum */
};

/**
 * \struct fsi_qa_gc_params
 *
 * \brief
 * 	quality figures of merit for a gas cell shape / fit
 *
 *  As to the gas cell fit, the linear fit of the GC signal peaks to
 *  the NIST SRM2519a data has the RMS deviation as its figure of
 *  merit. For the sake of post-mortem analysis, the peaks actually
 *  found *and* used in the fit are also produced. Note that not all R
 *  and P peaks may intervene in the fit computation; there might be
 *  some undetected/unused peaks at the extremes of the swept range
 *  (i.e., the highest-numbered peak IDs).
 *
 * The meaning of each field is the following:
 *
 * \var	fsi_qa_gc_params::gc_id
 *	Channel ID of the analyzed gas cell
 * \var	fsi_qa_gc_params::gc_alpha
 *	Computed alpha for the gas cell
 * \var	fsi_qa_gc_params::gc_zero_value
 *	Ordinate at the origin of the fit. The lsq linear fit gives
 *	freq = A t + B, where the speed (alpha) is A and B is the
 *	zero-time freq.
 * \var	fsi_qa_gc_params::gc_fitting_err
 *	This is the RMS (quadratic norm) of the deviations from the
 *	ideal NIST peaks computed over the found peaks in the alpha
 *	computation process
 * \var	fsi_qa_gc_params::gc_npeaks_fit_R
 *	Number of peaks used from the R lobe. The fitting algorithm is
 *	fed with a contiguous set of peaks spread from the center gap
 *	between R and P lobes, i.e., it may omit some of the initial or
 *	final peaks of the standard NIST HCN spectrum, which are most
 *	uncertain because of their smaller amplitude
 * \var	fsi_qa_gc_params::gc_npeaks_fit_P
 *	Number of peaks used from the P lobe
 * \var	fsi_qa_gc_params::gc_peaks_R[27]
 *	Actual R lobe peaks used to fit
 * \var	fsi_qa_gc_params::gc_peaks_P[26]
 *	Actual P lobe peaks used to fit
 */
struct fsi_qa_gc_params {
	int	gc_id;			/**< gas cell channel id, -1 if unknown */
	double	gc_alpha;		/**< alpha value computed through GC fit */
	double	gc_zero_value;		/**< value of linear fit at zero freq */
	double	gc_fitting_err;		/**< rms of deviation from NIST HCN ref */
	int	gc_npeaks_fit_R;	/**< number of R peaks used in GC fit */
	int	gc_npeaks_fit_P;	/**< number of P peaks used in GC fit */
	double	gc_peaks_R[27];		/**< wavelengths of R peaks going into fit */
	double	gc_peaks_P[26];		/**< wavelengths of P peaks going into fit */
	double	gc_reserved[4];		/**< reserved for future extensions */
};

/**
 * \struct fsi_qa_params
 * \brief
 *	quality parameters of linearization and gas cell fit
 *
 * \var	fsi_qa_params::nref
 *      Number of entries in the ref array below (usually 1 or 4)
 * \var	fsi_qa_params::ngc
 *      Number of entries in the gc array below (one, up to two)
 * \var	fsi_qa_params::ref[FSI_REF_CH_MAX]
 *      Array of \ref fsi_qa_ref_params results
 * \var	fsi_qa_params::gc[FSI_GC_CH_MAX]
 *       Array of \ref fsi_qa_gc_params results
 */
struct fsi_qa_params {
	int				nref;			/**< number of valid ref entries */
	int				ngc;			/**< number of valid gc  entries */
	struct fsi_qa_ref_params	ref[FSI_REF_CH_MAX];	/**< ref qa values cf. \ref fsi_qa_ref_params */
	struct fsi_qa_gc_params		gc[FSI_GC_CH_MAX];	/**< gc  qa values cf. \ref fsi_qa_gc_params */
	uint32_t			qa_reserved[256];	/**< reserved for future extensions */
};

/**
 * \brief
 *    retrieve quality parameters of linearized and gas cell fit 
 *
 * Quality assurance diagnostics for linearization and gas cell reading
 * are obtained by calling \ref fsi_get_qa_params. Grosso modo, the
 * diagnostics compute how narrow is the reference interferometer
 * spectrum, and how good is the LSQ fit of the GC peak frequencies
 * found with respect to the NIST reference data of the HCN spectrum
 */
int fsi_get_qa_params(FSI *h, struct fsi_qa_params *qa);

/* ----------------------------------------------------------------
 * Diagnostics of conditions appearing at the level of
 *    - DI/OT hardware (essentially, board presence)
 *    - data acquisition (ADC signal conversion and DMA)
 *    - data transmission (server<->DI/OT data link losses)
 *    - photodetector hardware (severe health problems thereof)
 * Most of the contents of these data structures are irrelevant/overkill
 * or unsuitable as FESA properties; only the "status" fields should be
 * reported in a tri-state (OK/ERROR/WARNING) value; all else being
 * furnished (if at all) in a form HW experts might peruse when
 * diagnosing severe errors.
 *
 * In other words: ignore everything here except the OK/WARNING/ERROR
 * statuses, and export the entire fsi_hw_diagnostics structure as a
 * binary uninterpreted buffer if further analysis is required.
 * ----------------------------------------------------------------
 */

/**
 * \brief
 *	hardware diagnostics - main structure retrieved in one shot via
 *	the API call \ref fsi_get_hw_diagnostics. It encompasses DI/OT
 *	hw diagnostics, transmission diagnostics and acquisition
 *	diagnostics; each of the related structures contains a 'status'
 *	field, the only one of interest for the upper layers to report
 *	in tri-state form (OK/WARNING/ERROR). If further analysis is
 *	required, the entire struct should be exported as a binary
 *	buffer to be interpreted by an expert (tool)
 *
 * \note
 *	TODO: a macro/translator function must be given along these lines:
 *      \code	
 *	#define FSI_STATUS_OK	0
 *	#define FSI_STATUS_WARNING	-1
 *	#define FSI_STATUS_ERROR	1
 *	int fsi_status_classify(int status);	/ **< status field translation * /
 *	int fsi_is_ok(int status);		/ **< status field is OK * /
 *	int fsi_is_warning(int status);		/ **< status field is WARNING * /
 *	int fsi_is_error(int status);		/ **< status field is ERROR * /
 *      \endcode	
 *	
 *	or, better, just leave 'status' fields with the FSI_STATUS_* values
 *
 */
struct fsi_hw_diagnostics {
	struct fsi_diag_hw       hw;		/**< see fsi_diag_hw    in fsi_txrx.h */
	struct fsi_diag_trans    trans;		/**< see fsi_diag_trans in fsi_txrx.h */
	struct fsi_diag_acq      acq;		/**< see fsi_diag_acq   in fsi_txrx.h */
	uint32_t custom[FSI_CUSTOM];
	uint32_t reserved[FSI_RESERVED];
};
static const int fsi_hw_diagnostics_size = sizeof(struct fsi_hw_diagnostics);

/**
 * \brief retrieve full hardware diagnostics
 */
int fsi_get_hw_diagnostics(FSI *h, struct fsi_hw_diagnostics *diags);

/* ancillary sensor monitoring */

/**
 * \brief
 *	sensor data for temperatures and powers measured in an optical
 *	distribution chassis
 * \warning
 *	FIXME: documentation refers here simply to temp1/temp2 and
 *	power1/power2. The semantics of temp1/temp2 here is a wild
 *	guess; take the "inner_" and "outer_" prefixes with a grain of
 *	salt.
 */
struct fsi_chassis_sensors {
	double	inner_temp;			/**< inner chassis temperature */
	double	outer_temp;			/**< outer chassis temperature */
	double	non_attn_power_status;		/**< optical power measured at non-attenuated branch */
	double	attn_power_status;		/**< optical power measured at attenuated branch */
};

/**
 * \brief
 *	retrieve temperature and power meter data from optical
 *	distribution chassis
 *
 * \warning
 * 	NOT IMPLEMENTED
 *
 * The data[] parameter must have a size enough to handle up to
 * FSI_NBLOCKS elements; the effective number returned, corresponding to
 * the actual number of optical distribution blocks installed, is
 * returned in the nblocks parameter
 *	
 * \param[in]	h		- opaque lib handle/context
 * \param[out]	nblocks		- number of valid entries in data[] array
 * \param[out]	data		- array data[0..nblocks] of measured sensor data
 *
 */
int fsi_get_sensor_data(FSI *h, int nblocks, struct fsi_chassis_sensors *data);

/* auxiliaries to pretty-print constants */
/* not part of the api, just added here for convenience; it might be
 * useful to API clients, though
 */
const char *fsi_decode_channel_type(enum fsi_channel_type type);
const char *fsi_decode_peak_type(enum fsi_peak_type type);
const char *fsi_decode_expert_mode(enum fsi_expert_mode type);
const char *fsi_decode_error_number(int err);
#ifdef __cplusplus
}
#endif

#endif /* FSI_LIB_H_ */
