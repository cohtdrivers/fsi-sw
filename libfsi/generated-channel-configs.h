#include "fsi.h"

struct fsi_channel_config generated_channel_configs[] = {
        [0] = {
                .channel_id	= 32,
                .type		= FSI_CH_GAS_CELL,
                .npeaks		= 0,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
		},
        },
        [1] = {
                .channel_id	= 33,
                .type		= FSI_CH_INTERF,
                .npeaks		= 0,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
		},
        },
        [2] = {
                .channel_id	= 34,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 0.0423,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
        [3] = {
                .channel_id	= 35,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 0.0494,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
        [4] = {
                .channel_id	= 36,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 0.042,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
        [5] = {
                .channel_id	= 37,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 0.05,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
/*
        [6] = {
                .channel_id	= 51,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 0.131,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
*/
        [7] = {
                .channel_id	= 38,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 0.051,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
        [8] = {
                .channel_id	= 39,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 0.051,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
        [9] = {
                .channel_id	= 41,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 0.056,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
        [10] = {
                .channel_id	= 42,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 0.056,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
        [11] = {
                .channel_id	= 43,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 0.051,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
        [12] = {
                .channel_id	= 44,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 0.051,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
        [13] = {
                .channel_id	= 45,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 0.056,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
        [14] = {
                .channel_id	= 46,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 0.056,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
        [15] = {
                .channel_id	= 47,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 0.132,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
        [16] = {
                .channel_id	= 49,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 1.428,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
        [17] = {
                .channel_id	= 50,
                .type		= FSI_CH_DISTANCE,
                .npeaks		= 1,
                .channel_gain	= -1,
                .disabled	= 0,
                .peaks		= {
                        [0] = {
				.position	= 1.418,
				.peak_id	= 0,
				.range1		= 0.002,
				.range2		= 0.002,
				.type		= FSI_PEAK_GAUSS,
                        },
		},
        },
};
