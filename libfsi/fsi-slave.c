#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>

int trim(char *dst, char *src, size_t max)
{
	char *from = src;
	char *to = from + strlen(src);

	while (isspace(*from))
		from++;
	while ((--to >= from) && isspace(*to))
		;
	++to;

	if (to - from < max)
		max = to - from;

	strncpy(dst, from, max);
	dst[max] = 0;

	return max;
}

int mainloop(int conn)
{
	char buf[256];
	char cmd[256];
	int i, res;

	while ((res = read(conn, buf, sizeof(buf))) > 0) {
		int len;

		buf[res] = 0;
		len = trim(cmd, buf, sizeof(cmd));
		printf("%d/%d: ", len, res);
		printf("[%s] [", cmd);
		for (i = 0; i < res; i++)
			printf("%02x ", buf[i]);
		printf("]\n");
		if (buf[0] == 'q')
			return -1;
	}
	return res;
}

int listen_client(char *ifname, char *port, int (*mainloop)(int conn))
{
	int	fd, conn;
	int	err;
	struct addrinfo hints, *res;
	struct sockaddr_in _peer, *peer = &_peer;
	socklen_t	sz;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	if ((err = getaddrinfo(NULL, "4510", &hints, &res)) != 0) {
		fprintf(stderr, "could not get address info: %s\n", gai_strerror(err));
		return -1;
	}
	if ((fd = socket(res->ai_family, res->ai_socktype, 0)) < 0) {
		perror("socket creation");
		return fd;
	}
	printf("AF_INET = %d, AF_INET6 = %d\n", AF_INET, AF_INET6);
	if ((err = bind(fd, res->ai_addr, res->ai_addrlen)) < 0) {
		perror("binding failed");
		return err;
	}
	if ((err = listen(fd, 10)) < 0) {
		perror("cannot listen");
		return err;
	}
	while ((conn = accept(fd, (struct sockaddr *)peer, &sz)) >= 0) {
		int ret;

		ret = mainloop(conn);
		if (ret == -1) {
			return ret;
		} else if (ret == 0) {
			fprintf(stderr, "connection lost, reconnecting\n");
			continue;
		}
	}
	return conn;
}

int main(int argc, char *argv[])
{
	listen_client(NULL, "4510", mainloop);
}
