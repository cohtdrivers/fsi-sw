/*
 * Copyright CERN 2023
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

/* \mainpage fsi-laser: Laser control functions of libfsi
 *
 * fsi-laser exports the interface to communicate with the New Focus
 * Tunable Laser Module TLM-8700
 *
 * To operate the device, a call to fsi_laser_init returns an opaque
 * handle that will be used in all subsequent calls, until the device is
 * relinquished by calling \ref fsi_laser_free. The initialization routine
 * configures silently a number of settings (e.g. trigger_polarity,
 * power_unit) that can and must be safely ignored by the user of the
 * library, because their values are fixed by design. They may, however,
 * be observed by means of a call to \ref fsi_laser_get_expert_status.
 */

#ifndef FSI_LASER_H_
#define FSI_LASER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <limits.h>
#include <sys/time.h>
#include <stdlib.h>

#define	FSI_LASER_MAX_DIAGS	32	/**< max number of stored laser diagnostics */
#define	FSI_LASER_MAX_LENGTH	180	/**< max size of any property type, esp. strings */
#define	FSI_LASER_LINE_LENGTH	256	/**< max length of CLI cmd or answer -- this
					     is overkill, the max being 32 err codes
					     taking 32 * 5 = 160 bytes */

/* error codes for laser API */
#define FSI_LASER_OK		0			/**< success */
#define FSI_LASER_ERR		-2048			/**< reserved */
#define FSI_LASER_CMD_TOO_LONG	FSI_LASER_ERR-1		/**< raw cmd too long */
#define FSI_LASER_WRITE_ERR	FSI_LASER_ERR-2		/**< laser serial write error */
#define FSI_LASER_WRITE_TRUNC	FSI_LASER_ERR-3		/**< laser serial write truncated */
#define FSI_LASER_READ_ERR	FSI_LASER_ERR-4		/**< laser answer read error */
#define FSI_LASER_ANSWER_ERR	FSI_LASER_ERR-5		/**< unterminated laser cli answer */
#define FSI_LASER_INTERNAL_ERR	FSI_LASER_ERR-6		/**< unterminated laser cli answer */
#define FSI_LASER_BAD_PROPERTY	FSI_LASER_ERR-7		/**< unterminated laser cli answer */
#define FSI_LASER_CMD_ERR	FSI_LASER_ERR-8		/**< laser cli cmd unknown */
#define FSI_LASER_ARG_ERR	FSI_LASER_ERR-9		/**< laser cli invalid argument */
#define FSI_LASER_EXE_ERR	FSI_LASER_ERR-10	/**< laser cli cmd execution error */
#define FSI_LASER_ACK		FSI_LASER_ERR-11	/**< laser cli cmd acknowledged */
#define FSI_LASER_BAD_FLOAT	FSI_LASER_ERR-12	/**< failed strtod on cli out */
#define FSI_LASER_BAD_INT	FSI_LASER_ERR-13	/**< failed strtol on cli out */
#define FSI_LASER_BAD_BOOL	FSI_LASER_ERR-14	/**< failed bool conversion on cli out */
#define FSI_LASER_UNKN_FAIL	FSI_LASER_ERR-15	/**< failed conversion on cli out */
#define FSI_LASER_BAD_CONV	FSI_LASER_ERR-16	/**< failed conversion into cli input */

/** configuration params list for ordinary (cyclic) laser operation
 *
 *  usually, these are set at the start and never changed again during
 *  an entire run of acquisitions
 */
struct fsi_laser_configuration {
	int	power_on;		/**< switch laser ON if !=0, off otherwise */
	double	power_request;		/**< set laser power in mW */
	double	sweep_start;		/**< set sweep range start in nm */
	double	sweep_stop;		/**< set sweep range stop in nm */
	double	sweep_speed;		/**< set sweep speed in nm/s */
	long	reserved[4];
};

/** array of latest laser error conditions
 */
struct fsi_laser_errs {
	int	nerrs;			/**< number of entries in diagnostics array */
	char	*diagnostics[FSI_LASER_MAX_DIAGS];
					/**< nerrs latest error diagnostics, up to FSI_LASER_MAX_DIAGS */
	struct timeval
		timestamp;		/**< timestamp of error request */
};

/** \brief
 *	laser status properties for routine laser operation
 *
 *  we normally want to have all of them retrieved/refreshed at every
 *  cycle/scan event
 */
struct fsi_laser_status {
	int	key_locked;		/**< is physical key locked? */
	int	soft_locked;		/**< is software interlock on? */
	int	power_on;		/**< is laser on? */
	double	power_status;		/**< laser power */
	double	sweep_start;		/**< wavelength in nm for sweep start */
	double	sweep_stop;		/**< wavelength in nm for sweep end */
	double	sweep_speed;		/**< sweep speed in nm/s */
	double	temperature;		/**< diode temperature in C */

	struct fsi_laser_errs
		diagnostic_info;	/**< last error messages */

	struct timeval
		timestamp;		/**< timestamp of status request */
	long	reserved[4];
};

/** \brief
 *	laser expert status properties, never required in normal operation
 *	 and useful only for expert manipulation of laser
 *
 *  it would be advisable to retrieve these only when explicitly
 *  required, for debugging purposes, as they are seldom changed or even
 *  used
 */
struct fsi_laser_expert_status {
	char	serial_device[PATH_MAX];	/**< serial line for comm */
	char	device_id[FSI_LASER_MAX_LENGTH];
						/**< laser identification string */
	double	power_max;			/**< nominal max power */
	double	power_min;			/**< nominal min power */
	double	wavelength_max;			/**< nominal maximum sweep wavelength */
	double	wavelength_min;			/**< nominal minimum sweep wavelength */
	double	operation_hours;		/**< unit total operation time */
	int	mode;				/**< sweep mode (always 2) */
	int	modulation_source;		/**< never used **/
	int	trigger_polarity;		/**< 0 active low, 1 active high */
	int	cycles;				/**< number of sweep iterations per scan */
	int	remaining_cycle_cnt;		/**< remaining number of sweeps in current scan */
	int	dwell_time;			/**< time in ms between sweeps */
	int	operation_complete;		/**< see fsi_operation_complete_codes */

	struct timeval
		timestamp;			/**< timestamp of status request */
	long	reserved[4];
};

/** opaque handle to a device status and controls */
struct fsi_laser;

/* recommended wide API for interaction with the laser device
 *
 * all functions refer to an opaque handler h created/destroyed
 * by \ref * fsi_laser_init and \ref fsi_laser_free.
 *
 * Entry points are furnished to set an \ref fsi_laser_configuration and to
 * retrieve \ref fsi_laser_status and \ref fsi_laser_expert_status.
 *
 * \note
 *     Both laser and EDFA can have their status reported at different
 *     levels of detail.
 *
 *     The *_get_status functions are meant to be called periodically to
 *          report states of permanent interest to the application
 *     The *_expert_status functions are meant to be called very rarely,
 *          to report states of sporadic or one-off interest to the application
 *     The fsi_laser_get_diagnostic_info reports specifically the laser
 *          errors accumulated in the device error buffer. There is a large
 *          number of conditions reported (see table below,
 *          fsi_laser_error_codes), but it is likely that all of them should
 *          be coalesced to at most three categories (OK, error, warning).
 *
 * Those aside, raw commands are accessible to trigger a scan, abort it,
 * and to reset or reboot the laser device.
 *
 * For experts, a raw_command routine that implements a single
 * command/response iteration of the laser serial CLI is given.
 *
 * The latest set of laser error conditions can be retrieved via
 * fsi_laser_get_diagnostic_info, which returns a set of decoded
 * diagnostics strings
 */

struct fsi_laser *fsi_laser_init(const char *serial_port);
int fsi_laser_configure(struct fsi_laser *h, struct fsi_laser_configuration *cfg);
int fsi_laser_get_status(struct fsi_laser *h, struct fsi_laser_status *st);
int fsi_laser_get_expert_status(struct fsi_laser *h, struct fsi_laser_expert_status *st);
int fsi_laser_free(struct fsi_laser *h);

int fsi_laser_scan(struct fsi_laser *h);
int fsi_laser_abort(struct fsi_laser *h);
int fsi_laser_reset(struct fsi_laser *h);
int fsi_laser_reboot(struct fsi_laser *h);

int fsi_laser_exec_raw_command(struct fsi_laser *h, char *cmd, char *answer);
int fsi_laser_get_diagnostic_info(struct fsi_laser *h, struct fsi_laser_errs *errs);

/* type-unchecked generic property manipulation interface
 *
 * A property of the laser can be read or set by means of the narrow
 * interface below. The argument value is a structure that contains
 * the property number to get/set (from enum fsi_laser_property_idx),
 * the type of the property (from enum fsi_laser_property_type), and
 * a type-unchecked area for the actual value of the property, which
 * might be
 *  - a command (e.g. fsi_lp_scan): no actual value, write-only action
 *  - an integer or boolean
 *  - a float
 *  - a string less than FSI_LASER_MAX_LENGTH (global static limit)
 *
 *  While properties are sanity-checked (e.g, you cannot get() a command
 *  property nor set() a read-only property; the type is double-checked
 *  internally for consistency, etc), it is up to the user to
 *  ensure the correctness of the untyped value of the property.
 *  It is probably far more convenient and safer, in general, to
 *  call type-checked convenience functions of the wide API.
 */

struct fsi_laser_property;
int fsi_laser_set(struct fsi_laser *h, struct fsi_laser_property *value);
int fsi_laser_get(struct fsi_laser *h, struct fsi_laser_property *value);

enum fsi_laser_property_idx {
	fsi_lp_serial_device		=  0,
	fsi_lp_device_id		=  1,
	fsi_lp_state			=  2,
	fsi_lp_key_lock			=  3,
	fsi_lp_soft_lock		=  4,
	fsi_lp_pmax			=  5,
	fsi_lp_pmin			=  6,
	fsi_lp_power			=  7,
	fsi_lp_power_unit		=  8,
	fsi_lp_domain			=  9,
	fsi_lp_wmax			= 10,
	fsi_lp_wmin			= 11,
	fsi_lp_modulation_source	= 12,
	fsi_lp_start			= 13,
	fsi_lp_stop			= 14,
	fsi_lp_speed			= 15,
	fsi_lp_mode			= 16,
	fsi_lp_dwell_time		= 17,
	fsi_lp_cycles			= 18,
	fsi_lp_ext_temp			= 19,
	fsi_lp_diode_temp		= 20,
	fsi_lp_ophours			= 21,
	fsi_lp_cnt			= 22,
	fsi_lp_trigger_pol		= 23,
	fsi_lp_errors			= 24,
	fsi_lp_operation_complete	= 25,
	fsi_lp_scan			= 26,
	fsi_lp_abort			= 27,
	fsi_lp_reset			= 28,
	fsi_lp_reboot 			= 29,
};

enum fsi_laser_property_type {
	fsi_lpt_command		= 0,
	fsi_lpt_boolean		= 1,
	fsi_lpt_integer		= 2,
	fsi_lpt_floating_point	= 3,
	fsi_lpt_string		= 4,
};

union fsi_laser_property_val {
	int	integer;
	double	floating_point;
	char	string[FSI_LASER_MAX_LENGTH];
};

struct fsi_laser_property {
	enum fsi_laser_property_idx	id;
	enum fsi_laser_property_type	type;
	long				reserved[4];
	union fsi_laser_property_val	value;
};

#ifdef __cplusplus
}
#endif

#endif /* FSI_LASER_H_ */
