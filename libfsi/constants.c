/* constants that have to be shared between the C and the Python side
 * of libfsi have to be propagated in FIVE different places:
 *	- declaration in fsi.h
 *		libfsi/fsi.h:		FSI_EXPERT_LIN_FFT,
 *	- field declaration in constants.c
 *		libfsi/constants.c:	int	_FSI_EXPERT_LIN_FFT;
 *	- field initialization in constants.c
 *		libfsi/constants.c:	._FSI_EXPERT_LIN_FFT = FSI_EXPERT_LIN_FFT,
 *	- field declaration in fsi.fsitypes
 *		fsi/fsitypes.py:	('_FSI_EXPERT_LIN_FFT',       c_int),
 *	- field initialization in fsi.fsitypes
 *		fsi/fsitypes.py:	FSI_EXPERT_LIN_FFT = constants[0]._FSI_EXPERT_LIN_FFT
 *
 * In all likelihood, the fsitypes symbols might be spared by matching the constants static
 * array to the fsitypes Structure. FIXME.
 */

#include "fsi-private.h"

/** interferometer constants of nature */
int _FSI_NBLOCKS = FSI_NBLOCKS;
static struct fsi_constants {
	int	n;
	double	fs;
	int	noblocks;
	int	zpf;
	char	*port_laser;
	char	*port_edfa[FSI_NBLOCKS];
	char	*port_meteo;
	char	*port_console;

	int	_FSI_MAX_MODEL_PARAMS;
	int	_FSI_PEAK_LENGTH;
	int	_FSI_HW_DIAG_SIZE;
	int	_FSI_MAX_PEAKS;
	int	_FSI_SAMPLE_SIZE;

	int	_FSI_NBLOCKS;
	int	_FSI_NEDFAS;
	int	_FSI_NCHANNELS;
	int	_FSI_NGCS;
	int	_FSI_NREFS;

	int	_FSI_ERR_BASE;
	int	_FSI_SOCKET_ERROR;
	int	_FSI_SOCKET_BUF_ERROR;
	int	_FSI_SOCKET_RECVFROM_TIMEOUT;
	int	_FSI_SOCKET_READV_TIMEOUT;
	int	_FSI_SOCKET_CFGTIMEO_ERROR;
	int	_FSI_SOCKET_LISTEN_ERROR;
	int	_FSI_SOCKET_ACCEPT_ERROR;
	int	_FSI_SOCKET_NOCONN;

	int	_FSI_CH_NONE;
	int	_FSI_CH_DISTANCE;
	int	_FSI_CH_BRAGG;
	int	_FSI_CH_GAS_CELL;
	int	_FSI_CH_INTERF;

	int	_FSI_PEAK_NONE;
	int	_FSI_PEAK_LORENTZ;
	int	_FSI_PEAK_GAUSS;
	int	_FSI_PEAK_SINC;
	int	_FSI_PEAK_MOFFAT;

	int	_FSI_UNIT_NONE;
	int	_FSI_UNIT_M;
	int	_FSI_UNIT_NM;
	int	_FSI_UNIT_HZ;
	int	_FSI_UNIT_SAMPLE;
	int	_FSI_UNIT_DB;
	int	_FSI_UNIT_S;

	int	_FSI_EXPERT_NONE;
	int	_FSI_EXPERT_LIN_DATA;
	int	_FSI_EXPERT_OSCILLOSCOPE;
	int	_FSI_EXPERT_GAS_CELL;
	int	_FSI_EXPERT_INTERF;
	int	_FSI_EXPERT_RAW_GAS_CELL;
	int	_FSI_EXPERT_RAW_INTERF;
	int	_FSI_EXPERT_LIN_FFT_M;
	int	_FSI_EXPERT_LIN_FFT_HZ;

	char	*_FSI_SERVER_DEFAULT_SHM;
	char	*_FSI_SERVER_SHM_ENVIRON;

	char	*_FSI_SERVER_SOCKET;
	char	*_FSI_SERVER_GREET;
	char	*_FSI_SERVER_ANS;

	int	_FSI_REF_CH_MAX;
	int	_FSI_GC_CH_MAX;
} _fsi_constants = {
	.n		= 2490000,
	.fs		= 100000000,
	.noblocks	= 1,
	.zpf		= 1,
	.port_laser	= PORT_LASER,
	.port_edfa      = { PORT_EDFA, },
	.port_meteo     = PORT_METEO,
	.port_console   = PORT_CONSOLE,

	._FSI_MAX_MODEL_PARAMS	= FSI_MAX_MODEL_PARAMS,
	._FSI_PEAK_LENGTH	= FSI_PEAK_LENGTH,
	._FSI_HW_DIAG_SIZE	= FSI_HW_DIAG_SIZE,
	._FSI_MAX_PEAKS		= FSI_MAX_PEAKS,
	._FSI_SAMPLE_SIZE	= FSI_SAMPLE_SIZE,

	._FSI_NBLOCKS		= FSI_NBLOCKS,
	._FSI_NEDFAS		= FSI_NEDFAS,
	._FSI_NCHANNELS		= FSI_NCHANNELS,
	._FSI_NGCS		= FSI_NGCS,
	._FSI_NREFS		= FSI_NREFS,

	._FSI_ERR_BASE			= FSI_ERR_BASE,
	._FSI_SOCKET_ERROR		= FSI_SOCKET_ERROR,
	._FSI_SOCKET_BUF_ERROR		= FSI_SOCKET_BUF_ERROR,
	._FSI_SOCKET_RECVFROM_TIMEOUT	= FSI_SOCKET_RECVFROM_TIMEOUT,
	._FSI_SOCKET_READV_TIMEOUT	= FSI_SOCKET_READV_TIMEOUT,
	._FSI_SOCKET_CFGTIMEO_ERROR	= FSI_SOCKET_CFGTIMEO_ERROR,
	._FSI_SOCKET_LISTEN_ERROR	= FSI_SOCKET_LISTEN_ERROR,
	._FSI_SOCKET_ACCEPT_ERROR	= FSI_SOCKET_ACCEPT_ERROR,
	._FSI_SOCKET_NOCONN		= FSI_SOCKET_NOCONN,

	._FSI_CH_NONE			= FSI_CH_NONE,
	._FSI_CH_DISTANCE		= FSI_CH_DISTANCE,
	._FSI_CH_BRAGG			= FSI_CH_BRAGG,
	._FSI_CH_GAS_CELL		= FSI_CH_GAS_CELL,
	._FSI_CH_INTERF			= FSI_CH_INTERF,

	._FSI_PEAK_NONE			= FSI_PEAK_NONE,
	._FSI_PEAK_LORENTZ		= FSI_PEAK_LORENTZ,
	._FSI_PEAK_GAUSS		= FSI_PEAK_GAUSS,
	._FSI_PEAK_SINC			= FSI_PEAK_SINC,
	._FSI_PEAK_MOFFAT		= FSI_PEAK_MOFFAT,

	._FSI_UNIT_NONE			= FSI_UNIT_NONE,
	._FSI_UNIT_M			= FSI_UNIT_M,
	._FSI_UNIT_NM			= FSI_UNIT_NM,
	._FSI_UNIT_HZ			= FSI_UNIT_HZ,
	._FSI_UNIT_SAMPLE		= FSI_UNIT_SAMPLE,
	._FSI_UNIT_DB			= FSI_UNIT_DB,
	._FSI_UNIT_S			= FSI_UNIT_S,

	._FSI_EXPERT_NONE		= FSI_EXPERT_NONE,
	._FSI_EXPERT_LIN_DATA		= FSI_EXPERT_LIN_DATA,
	._FSI_EXPERT_OSCILLOSCOPE	= FSI_EXPERT_OSCILLOSCOPE,
	._FSI_EXPERT_GAS_CELL		= FSI_EXPERT_GAS_CELL,
	._FSI_EXPERT_INTERF		= FSI_EXPERT_INTERF,
	._FSI_EXPERT_RAW_GAS_CELL	= FSI_EXPERT_RAW_GAS_CELL,
	._FSI_EXPERT_RAW_INTERF		= FSI_EXPERT_RAW_INTERF,
	._FSI_EXPERT_LIN_FFT_M          = FSI_EXPERT_LIN_FFT_M,
	._FSI_EXPERT_LIN_FFT_HZ         = FSI_EXPERT_LIN_FFT_HZ,

	._FSI_SERVER_DEFAULT_SHM	= FSI_SERVER_DEFAULT_SHM,
	._FSI_SERVER_SHM_ENVIRON	= FSI_SERVER_SHM_ENVIRON,

	._FSI_SERVER_SOCKET	= FSI_SERVER_SOCKET,
	._FSI_SERVER_GREET	= FSI_SERVER_GREET,
	._FSI_SERVER_ANS	= FSI_SERVER_ANS,

	._FSI_REF_CH_MAX	= FSI_REF_CH_MAX,
	._FSI_GC_CH_MAX         = FSI_GC_CH_MAX,
};
struct fsi_constants *fsi_constants = &_fsi_constants;

