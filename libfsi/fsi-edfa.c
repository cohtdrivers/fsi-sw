/*
 * Copyright CERN 2021
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <sys/file.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <math.h>

#include "fsi-edfa.h"
#include "fsi-edfa-private.h"

static double mW_to_dBm(double mw)
{
	return 10 * log10(mw);
}

static double dBm_to_mW(double dbm)
{
	return pow(10.0, dbm/10.0);
}

static int decode_error(int ch)
{
	int err;

	switch (ch) {
	case '=':
	case '!':
		err = FSI_EDFA_OK;
		break;
	case '*':
		err = FSI_EDFA_COMMAND_UNKNOWN;
		break;
	case '#':
		err = FSI_EDFA_NOT_AUTHORIZED;
		break;
	case '$':
		err = FSI_EDFA_COMMAND_NOT_VALID;
		break;
	default:
		err = FSI_EDFA_ANSWER_ERR;
		break;
	}
	return err;
}

static int set_var(struct fsi_edfa *h, char *varname, char *value)
{
	char cmd[FSI_EDFA_MAX_LENGTH];
	char ans[FSI_EDFA_MAX_LENGTH];
	int err;
	int varlen = strlen(varname);

	strncpy(cmd, varname, sizeof(cmd));
	strcat(cmd, "=");
	strncat(cmd, value, sizeof(cmd) - varlen - 2);
	err = fsi_edfa_exec_raw_command(h, cmd, ans);
	printf("[%s] [%s]\n", cmd, ans);
	if (err != 0)
		return err;
	if (strncasecmp(ans, cmd, varlen) != 0)
		return FSI_EDFA_ANSWER_ERR;
	err = decode_error(ans[varlen]);
	return err;
}

static int set_double_var(struct fsi_edfa *h, char *varname, double value)
{
	int err;
	char literal[20];

	snprintf(literal, sizeof(literal), h->num_format, value);
	err = set_var(h, varname, literal);
	if (err != 0)
		return err;
	return 0;
}

static int get_var(struct fsi_edfa *h, char *varname, char *value)
{
	char cmd[FSI_EDFA_MAX_LENGTH];
	char ans[FSI_EDFA_MAX_LENGTH];
	int err;
	int varlen = strlen(varname);

	strncpy(cmd, varname, sizeof(cmd));
	strcat(cmd, "?");
	err = fsi_edfa_exec_raw_command(h, cmd, ans);
	printf("[%s] [%s]\n", cmd, ans);
	if (err != 0)
		return err;
	if (strncasecmp(ans, cmd, varlen) != 0)
		return FSI_EDFA_ANSWER_ERR;
	err = decode_error(ans[varlen]);
	if (ans[varlen] == '=')
		strcpy(value, &ans[varlen+1]);
	else
		value[0] = 0;

	return err;
}

static int get_int_var(struct fsi_edfa *h, char *varname, int *value)
{
	int err;
	char ans[FSI_EDFA_MAX_LENGTH];

	err = get_var(h, varname, ans);
	if (err != 0)
		return err;
	*value = strtol(ans, NULL, 0);

	return 0;
}

static int get_double_var(struct fsi_edfa *h, char *varname, double *value)
{
	int err;
	char ans[FSI_EDFA_MAX_LENGTH];

	err = get_var(h, varname, ans);
	if (err != 0)
		return err;
	*value = strtod(ans, NULL);

	return 0;
}

int fsi_edfa_configure(struct fsi_edfa *h, struct fsi_edfa_configuration *cfg)
{
	int err = 0;

	err |= set_var(h, "ASS", cfg->edfa_on ? "2" : "0");
	err |= set_double_var(h, "SOP", h->dbm_scale * mW_to_dBm(cfg->power_request));

	return err;
}

int fsi_edfa_get_status(struct fsi_edfa *h, struct fsi_edfa_status *st)
{
	int err = 0;
	uint32_t alarms;

	err |= get_double_var(h, "IPW",	&st->input_power);	st->input_power    = dBm_to_mW(st->input_power/h->dbm_scale);
	err |= get_double_var(h, "SOP",	&st->power_request);    st->power_request  = dBm_to_mW(st->power_request/h->dbm_scale);
	err |= get_double_var(h, "OPW",	&st->power_status);     st->power_status   = dBm_to_mW(st->power_status/h->dbm_scale);
	err |= get_int_var(h, "CAT",	&st->edfa_on);

	/* actually, 0 is off, 2 is on, and any other option should be
	 * error */
	st->edfa_on = !(st->edfa_on == 0);

	/* the key switch is defined by a GPIO pin exported through an
	 * alarm bit */
	err |= fsi_edfa_get_alarms(h, &alarms);
	st->key_locked = alarms & (1<<FSI_EDFA_ALARM_KEYOFF_BIT);

	return err;
}

int fsi_edfa_get_expert_status(struct fsi_edfa *h, struct fsi_edfa_expert_status *st)
{
	int err = 0;

	err |= get_var(h, "SNU",		     	st->serial_number);						
	err |= get_var(h, "DES",		     	st->description);						
	err |= get_var(h, "VER",		     	st->version);						
	err |= get_var(h, "FWI",		     	st->firmware_information);					
	
	err |= get_int_var(h, "ASS",		&st->control_mode);						
	
	err |= get_int_var(h, "AST",	 	&st->autostart);							
	err |= get_double_var(h, "CAT",		&st->case_temperature);						
	
	err |= get_int_var(h, "IC1",		&st->preamp_diode_current_setpoint);				
	err |= get_int_var(h, "ID1",		&st->measured_preamp_diode_current_setpoint);			
	err |= get_double_var(h, "TD1",		&st->preamp_diode_temperature);					
	
	err |= get_int_var(h, "IC2",		&st->prebooster_diode_current_setpoint);				
	err |= get_int_var(h, "ID2",		&st->measured_prebooster_diode_current_setpoint);			
	err |= get_double_var(h, "PON",		&st->nominal_power);						
	err |= get_double_var(h, "POM",		&st->minimum_power_setpoint);					
	err |= get_double_var(h, "SOP",		&st->setpoint_output_power);					
	
	err |= get_double_var(h, "IPT1",		&st->input_power_alarm_threshold);				
	
	err |= get_double_var(h, "LCM1",		&st->limit_current_preamp);					
	err |= get_double_var(h, "LCM2",		&st->limit_current_preboost);					
	err |= get_double_var(h, "PWM",		&st->output_power_mute);						

	st->nominal_power		= dBm_to_mW(st->nominal_power		/ h->dbm_scale);
	st->minimum_power_setpoint	= dBm_to_mW(st->minimum_power_setpoint	/ h->dbm_scale);
	st->setpoint_output_power	= dBm_to_mW(st->setpoint_output_power	/ h->dbm_scale);
	st->input_power_alarm_threshold	= dBm_to_mW(st->input_power_alarm_threshold / h->dbm_scale);

	return 0;
}

int fsi_edfa_factory_settings(struct fsi_edfa *h)
{
	return set_var(h, "RKP", "1");
}

int fsi_edfa_power_on(struct fsi_edfa *h)
{
	return set_var(h, "ASS", "2");
}

int fsi_edfa_power_off(struct fsi_edfa *h)
{
	return set_var(h, "ASS", "0");
}

int fsi_edfa_reset_alarms(struct fsi_edfa *h)
{
	return set_var(h, "ALA", "");
}

int fsi_edfa_get_alarms(struct fsi_edfa *h, uint32_t *alarms)
{
	int err;
	char alarm[FSI_EDFA_MAX_LENGTH];

	err = get_var(h, "ALA", alarm);
	if (err != 0)
		return err;
	*alarms = strtoul(alarm, NULL, 16);

	return 0;
}

/* \brief
 *	obtain EDFA alarms in a expanded structure \ref fsi_edfa_alarms
 *
 */
int fsi_edfa_get_alarms_struct(struct fsi_edfa *h, struct fsi_edfa_alarms *alarms)
{
	uint32_t	mask;
	int		err;

	if ((err = fsi_edfa_get_alarms(h, &mask)) != 0)
		return err;
	fsi_edfa_decode_alarms_struct(mask, alarms);
	return 0;
}

int fsi_edfa_get_warnings(struct fsi_edfa *h, uint32_t *warnings)
{
	int err;
	char warn[FSI_EDFA_MAX_LENGTH];

	err = get_var(h, "WAR", warn);
	if (err != 0)
		return err;
	*warnings = strtoul(warn, NULL, 16);

	return 0;
}

static int lock(struct fsi_edfa *h)
{
	return flock(h->fd, LOCK_EX);
}

static int unlock(struct fsi_edfa *h)
{
	return flock(h->fd, LOCK_UN);
}

int fsi_edfa_exec_raw_command(struct fsi_edfa *h, char *cmd, char *answer)
{
	
	/* FIXME: calls to this function are not error-checked in the
	 * rest of the code
	 */
	char command[FSI_EDFA_MAX_LENGTH];
	int len = strlen(cmd);
	int err = 0;
	char *ptr;

	if (len + 2 > FSI_EDFA_MAX_LENGTH)
		return FSI_EDFA_COMMAND_NOT_VALID;
	strncpy(command, cmd, sizeof(command));
	strcat(command, "\r\n");

	if (lock(h) < 0)
		err = FSI_EDFA_WRITE_ERR;
	err = write(h->fd, command, len+2);
	if (err < 0) {
		err = FSI_EDFA_WRITE_ERR;
		goto error_exit;
	} else if (err < len+2) {
		err = FSI_EDFA_WRITE_TRUNC;
		goto error_exit;
	}

	ptr = answer;
	while (1) {
		err = read(h->fd, ptr, sizeof(answer));
		if (err < 0) {
			err = FSI_EDFA_READ_ERR;
			goto error_exit;
		} else if (err == 0) {
			err = FSI_EDFA_ANSWER_ERR;
			goto error_exit;
		}
		ptr += err;
		if (ptr[-1] == '\r') {
			ptr[-1] = 0;
			goto success;
		}
	}
success:
	err = 0;
error_exit:
	unlock(h);
	return err;
}

/** open a serial device in raw mode with variable params
 *
 *  A poor man's substitute of pyserial's Serial constructor.
 *  FIXME: this is a very basic, blocking interface to the serial
 *  device. We **do** need to implement at least a timed-out version.
 *  FIXME: lifted verbatim (except for speed) from fsi-laser.c---this
 *  duplication needs a good refactor.
 *
 *  Returns a serial device file descriptor, or -1 in case of failure;
 *  sets errno.
 */
static int fsi_serial_open(const char *serial_port)
{
	int fd;
	struct termios tty;
	int err;

	errno = 0;
	if ((fd = open(serial_port, O_RDWR)) < 0)
		goto err;

	/* set comm parameters of laser: raw, 115200 8N1 */
	err = tcgetattr(fd, &tty);
	if (err < 0) goto err;
	cfmakeraw(&tty);	
	err = cfsetspeed(&tty, B19200);
	if (err < 0) goto err;
	tty.c_cflag &= ~(PARENB | CSIZE | CSTOPB);
	tty.c_cflag |= CS8;
	err = tcsetattr(fd, TCSANOW, &tty);
	if (err < 0) goto err;

	return fd;
err:
	return -1;
}

struct fsi_edfa *fsi_edfa_init(const char *serial_port, int edfa_id)
{
	int fd;
	struct fsi_edfa *edfa;
	char description[FSI_EDFA_MAX_LENGTH];

	if ((fd = fsi_serial_open(serial_port)) < 0)
		goto err;

	if ((edfa = malloc(sizeof(*edfa))) == NULL)
		goto err;

	/* initialize edfa context descriptor */
	strncpy(edfa->serial_port, serial_port, sizeof(edfa->serial_port));
	edfa->fd = fd;
	memset(&edfa->status, 0, sizeof(edfa->status));
	memset(&edfa->expert_status, 0, sizeof(edfa->expert_status));

	/* if EDFA is of platform M102, use dBm/10 power units;
	 * default to dBm for platform M160 */
	if (get_var(edfa, "DES", description) != 0) {
		goto err;
	}
	edfa->dbm_scale = 1.0;
	edfa->num_format = "%.1f";
	if (strstr(description, "M102") != NULL) {
		edfa->dbm_scale = 10.0;
		edfa->num_format = "%.0f";
	}

	return edfa;
err:
	return NULL;
}

int fsi_edfa_free(struct fsi_edfa *h)
{
	close(h->fd);
	free(h);
	return 0;
}

/**
 *  \brief
 *	edfa_alarms - table to decode the alarm mask bits
 */
static struct edfa_alarms {
	int	bit;
	char	*message;
} edfa_alarms[] = {
	{ .bit = 21, .message = "Board temperature error", },
	{ .bit = 20, .message = "Power supply error", },
	{ .bit = 19, .message = "GPIO Key OFF activated", },
	{ .bit = 15, .message = "Low input power 1", },
	{ .bit = 14, .message = "Low output power 1", },
	{ .bit = 13, .message = "Low input power 2", },
	{ .bit = 12, .message = "Low output power 2", },
	{ .bit = 11, .message = "Preamp current error", },
	{ .bit = 10, .message = "Booster current error", },
	{ .bit = 3 , .message = "preamp temp error", },
	{ .bit = 2 , .message = "booster temp error", },
};

/**
 * fsi_decode_alarms - produce a textual representation of the edfa
 * alarma mask
 *
 * \param alarm_mask
 * 	the alarm mask returned by fsi_edfa_get_alarms
 * \param dst
 * 	address of a text buffer to receive the alarm textual panel;
 * 	must be at least 400 bytes size
 */
void fsi_decode_alarms(uint32_t alarm_mask, char *dst)
{
	int i;
	char panel[400];

	memset(panel, 0, sizeof(panel));
	for (i = 0; i < sizeof(edfa_alarms)/sizeof(edfa_alarms[0]); i++) {
		int bit = edfa_alarms[i].bit;
		char *msg = edfa_alarms[i].message;
		if ((1<<bit) & alarm_mask) {
			char tmp[50];
			snprintf(tmp, sizeof(tmp), "%02d: %s\n", bit, msg);
			strcat(panel, tmp);
		}
	}
	strcpy(panel, dst);
}

void fsi_edfa_decode_alarms_struct(uint32_t mask, struct fsi_edfa_alarms *alarms)
{
	uint32_t *p = (void *)alarms;
	*p = mask;
}

int fsi_edfa_main__(int argc, char *argv[])
{
	struct fsi_edfa *handle = fsi_edfa_init("/dev/serial/by-id/edfa", 0);
	int err;
	struct fsi_edfa_configuration usual_config;
	struct fsi_edfa_status st;
	struct fsi_edfa_expert_status est;
	char cmd[FSI_EDFA_MAX_LENGTH];

	usual_config.edfa_on = 0; 		/* off */
	usual_config.power_request = 251; 	/* power setpoint for isr */

	while (fgets(cmd, sizeof(cmd), stdin) != NULL) {
		char word[FSI_EDFA_MAX_LENGTH];
		char ans[FSI_EDFA_MAX_LENGTH];
		char *code = strtok(cmd, " \t\n");
		char *op = strtok(NULL, "\n");

		strncpy(word, code, sizeof(word));
		if (op == NULL) {
			err = get_var(handle, word, ans);
			printf("err:%d [%s] [%s]\n", err, cmd, ans);
		} else {
			err = set_var(handle, cmd, op);
			printf("err:%d [%s] [%s]\n", err, cmd, op);
		}
	}

	err = fsi_edfa_configure(handle, &usual_config);
	printf("error = %d\n", err);
	err = fsi_edfa_get_status(handle, &st);
	printf("error = %d\n", err);
	err = fsi_edfa_get_expert_status(handle, &est);
	printf("error = %d\n", err);

	fsi_edfa_free(handle);

	return 0;
}

void display_status(struct fsi_edfa_status *st)
{
        printf("input_power: %12.2f\n",   st->input_power);            /**< measured power at EDFA input (mW) */
        printf("power_request: %12.2f\n", st->power_request);          /**< power setpoint requested (mW) */
        printf("power_status: %12.2f\n",  st->power_status);           /**< measured power at EDFA output (mW) */
        printf("edfa_on: %d\n",           st->edfa_on);                /**< is power (=amplification) activated? */
        printf("key_locked: %d\n",        st->key_locked);             /**< is EDFA key-locked? */
}

int fsi_edfa_main(int argc, char *argv[])
{
	struct fsi_edfa *handle = fsi_edfa_init("/dev/serial/by-id/edfa", 0);
	int err;
	struct fsi_edfa_configuration usual_config;
	struct fsi_edfa_status st;
	struct fsi_edfa_expert_status est;
	double power;

	for (power = 60; power <= 500; power *= 1.2345) {

		usual_config.edfa_on = 0; 		/* off */
		usual_config.power_request = power; 	/* power setpoint for isr */

		err = fsi_edfa_configure(handle, &usual_config);
		printf("error = %d\n", err);
		usual_config.edfa_on = 1;
		err = fsi_edfa_configure(handle, &usual_config);
		printf("error = %d\n", err);
		err = fsi_edfa_get_status(handle, &st);
		printf("error = %d\n", err);
		display_status(&st);
		err = fsi_edfa_get_expert_status(handle, &est);
		printf("error = %d\n", err);
	}

	fsi_edfa_free(handle);

	return 0;
}
