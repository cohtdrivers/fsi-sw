/*
 * Copyright CERN 2023
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef FSI_LASER_PRIVATE_H_
#define FSI_LASER_PRIVATE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <linux/limits.h>
#include <stdio.h>

struct fsi_operation_complete_codes {
	int	code;
	char	*meaning;
} fsi_operation_complete_codes[] = {
	{-2, "unrecoverable error", },
	{-1, "recoverable error", },
	{ 0, "operation pending", },
	{ 1, "operation complete", },
	{ 2, "module initializing", },
};

struct fsi_laser_sweep_modes {
	int	mode;
	char	*meaning;
} fsi_laser_sweep_modes[] = {
	{ 1, "automatic step mode", },
	{ 2, "uni-directional forward swept mode", },
	{ 3, "bi-directional swept mode", },
	{ 4, "uni- directional reverse swept mode", },
};


struct fsi_laser_modulation_sources {
	int	source;
	char	*meaning;
} fsi_laser_modulation_sources[] = {
	{ 0, "no modulation", },
	{ 1, "coherence control", },
	{ 3, "external analog modulation", },
};

struct fsi_laser {
	char				serial_port[PATH_MAX];
	int				fd;
	struct fsi_laser_status		status;
	struct fsi_laser_expert_status	expert_status;
	/* no need to add a mutex; fd is used for flock(2)-based
	 * advisory locking */
};

extern char* fsi_laser_decode_error(int code);

#ifdef __cplusplus
}
#endif

#endif /* FSI_LASER_PRIVATE_H_ */
