import numpy as np

def unw(p):
    '''unwrap phase vector.'''
    
    period = 2 * np.pi
    discont = period / 2
    interval_high = period / 2
    interval_low = -interval_high

    dd = np.diff(p)
    ddmod = np.mod(dd - interval_low, period) + interval_low
    # fix the unlikely case of dd == +period/2
    ddmod[(ddmod == interval_low) & (dd > 0)] = interval_high
    phase_correct = ddmod - dd
    phase_correct[abs(dd) < discont] = 0
    res = np.array(p, copy=True, dtype=p.dtype)
    res[1:] = res[1:] + phase_correct.cumsum()

    return res

