/*
 * Copyright CERN 2021
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <math.h>
#include <stdio.h>
#include <stdint.h>

#include <cuda.h>
#include <cufft.h>
#include <cublas_v2.h>
#include <cuda_profiler_api.h>

#include "../fsi.h"
#include "../fsi-private.h"
/* FIXME: this needs fsi.h */
// #define FSI_SAMPLE_SIZE 2500000
// #define FSI_NCHANNELS	256

struct fsi_buffers {
	double		lin[FSI_NCHANNELS][FSI_SAMPLE_SIZE];
	double		spec[FSI_NCHANNELS][FSI_SAMPLE_SIZE];
	uint16_t	raw[FSI_NCHANNELS][FSI_SAMPLE_SIZE];
} buffers;

double fsi_ref_buffer[2*FSI_SAMPLE_SIZE + 2];

struct channel {
	int index;
	char name[16];
	uint16_t *data;
} time_domain_data[] = {
	{  0,	"BALL",		buffers.raw[ 0], },
	{  1,	"COL1",		buffers.raw[ 1], },
	{  2,	"COL2",		buffers.raw[ 2], },
	{  3,	"GAS" ,		buffers.raw[ 3], },
	{  4,	"HLS1",		buffers.raw[ 4], },
	{  5,	"HLS2",		buffers.raw[ 5], },
	{  6,	"HLS3",		buffers.raw[ 6], },
	{  7,	"HLS4",		buffers.raw[ 7], },
	{  8,	"HLS5",		buffers.raw[ 8], },
	{  9,	"HLS6",		buffers.raw[ 9], },
	{ 10,	"HLS7",		buffers.raw[10], },
	{ 11,	"HLS8",		buffers.raw[11], },
	{ 12,	"INC1",		buffers.raw[12], },
	{ 13,	"INC2",		buffers.raw[13], },
	{ 14,	"INC3",		buffers.raw[14], },
	{ 15,	"INC4",		buffers.raw[15], },
	{ 16,	"LON1",		buffers.raw[16], },
	{ 17,	"REF",		buffers.raw[17], },
};
int nchannels = sizeof(time_domain_data) / sizeof(time_domain_data[0]);

void fill_time_domain_data(void)
{
	int i;

	for (i = 0; i < nchannels; i++) {
		FILE *f;
		int j;
		f = fopen(time_domain_data[i].name, "rb");
		time_domain_data[i].data = buffers.raw[i];
		fread(time_domain_data[i].data, sizeof(uint16_t), 2490000, f);
		for (j = 0; j < FSI_SAMPLE_SIZE; j++)
			buffers.lin[i][j] = buffers.raw[i][j];
	}
	int j;
	for (j = 0; j < FSI_SAMPLE_SIZE; j++) {
		buffers.lin[0][j] = 1 - (j % 2) ;
		buffers.lin[1][j] = j;
		buffers.lin[2][j] = 1 - 2 * (j % 2) ;
	}
}

void dump_time_domain_data(void)
{
	int i;

	for (i = 0; i < nchannels; i++) {
		FILE *f;
		char fname[32];

		strcpy(fname, "spec-");
		strcat(fname, time_domain_data[i].name);
		f = fopen(fname, "wb");
		fwrite(buffers.spec[i], sizeof(double), 2490002, f);
		fclose(f);
	}
}

__global__ void fft_to_hilbert(double *v, int n)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;

	if (tid >= n)
		return;
	if (tid == 0)
		v[tid] = 0;
	else if (tid == n / 2)
		;
	else if (tid < n / 2) {
		v[2*tid]   = 2 * v[2*tid];
		v[2*tid+1] = 2 * v[2*tid+1];
	} else if (tid > n / 2) {
		v[2*tid]   = 0;
		v[2*tid+1] = 0;
	}
}

/**
 * compute argument phi of a vector cmplx of n complex numbers
 */
__global__ void angle(double *phi, double *cmplx, int n)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;

	/* FIXME: this is not required, but block allocation
	 * needs an optimize here and elsewhere
	 */
	while (tid < n) {
		phi[tid] = atan2(cmplx[2*tid+1], cmplx[2*tid]);
		tid += blockDim.x * gridDim.x;
	}
}

/**
 * compute first difference dd of a vector v of reals. Buffers
 * dd and v must not be the same (this simple-minded algorithm is racy
 * for in-place differencing).
 *
 * NB: note that dd[0] is v[0] to match dimensions --- equivalently,
 * diff assumes null off boundary values for vector v
 */
__global__ void diff(double *dd, double *v, int n)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if (tid < n && tid > 0) {
		dd[tid] = v[tid] - v[tid-1];
	} else if (tid == 0)
		dd[tid] = v[tid];
}

/**
 * compute the vector of phase corrections by +2pi at the points
 * in vector dd where phase falls by more than pi
 */
__global__ void ddmod(double *phi_correct, double *dd, int n)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if (tid < n) {
		phi_correct[tid] = fmod(dd[tid] + M_PI,  2*M_PI) - M_PI;
		if (phi_correct[tid] == -M_PI && dd[tid] > 0)
			phi_correct[tid] = M_PI;
		if (phi_correct[tid] < M_PI)
			phi_correct[tid] = 0;
		else
			phi_correct[tid] -= dd[tid];
	}
}

int hilbert(double *ref, long n)
{
	cudaError ret;

	cufftResult cc;
	cufftHandle plan1, plan1r;
	cufftDoubleReal *dev_ref = NULL;
	cufftDoubleComplex *dev_cref = NULL;
	cufftDoubleReal *phi = NULL;
	cufftDoubleReal *del = NULL;
	cufftDoubleReal *dd = NULL;
	cufftDoubleReal *phi_correct = NULL;

	cublasStatus_t cs;
	cublasHandle_t handle;

	double zero = 0.0, two = 2.0, one_over_n = 1.0 / n;

	cs = cublasCreate(&handle);

	/* compute FFT of raw REF channel */
	cc = cufftPlan1d(&plan1, n, CUFFT_D2Z, 1);	/* FIXME: offline this */
	ret = cudaMalloc(&dev_ref, FSI_SAMPLE_SIZE * sizeof(cufftDoubleReal));
	ret = cudaMalloc(&dev_cref, FSI_SAMPLE_SIZE * sizeof(cufftDoubleComplex));
	ret = cudaMemcpy(dev_ref, ref, sizeof(double) * n, cudaMemcpyHostToDevice);
	cc = cufftExecD2Z(plan1, dev_ref, dev_cref);

	/* convert to FFT of Hilbert transform */
	if (0) {
	    cs = cublasZdscal(handle, 1,     &zero, &dev_cref[0], 1);
	    cs = cublasZdscal(handle, n/2-1, &two,  &dev_cref[1], 1);
	    cs = cublasZdscal(handle, n/2-1, &zero, &dev_cref[n/2+1], 1);
	} else
		fft_to_hilbert<<<n,1>>>((double *)dev_cref, n);

	cc = cufftPlan1d(&plan1r, n, CUFFT_Z2Z, 1);	/* FIXME: offline this */
	cc = cufftExecZ2Z(plan1r, dev_cref, dev_cref, CUFFT_INVERSE);
	cs = cublasZdscal(handle, n, &one_over_n, dev_cref, 1);
	ret = cudaMemcpy(ref, dev_cref, sizeof(double[2]) * n, cudaMemcpyDeviceToHost);

	/* calculate phase angle and unwrap */
	ret = cudaMalloc(&phi, FSI_SAMPLE_SIZE * sizeof(cufftDoubleReal));
	ret = cudaMalloc(&del, FSI_SAMPLE_SIZE * sizeof(cufftDoubleReal));
	ret = cudaMalloc(&dd, FSI_SAMPLE_SIZE * sizeof(cufftDoubleReal));
	ret = cudaMalloc(&phi_correct, FSI_SAMPLE_SIZE * sizeof(cufftDoubleReal));
	angle<<<ceil(n/512.0),512>>>(phi, (double *)dev_cref, n);
	diff<<<n,1>>>(dd, phi, n);
	ddmod<<<n,1>>>(phi_correct, dd, n);

	cs = cublasDestroy(handle);

	return 0;
}

static int test_hilbert(int argc, char *argv[]);

static int test_main(int argc, char *argv[])
{
	cufftHandle	plan;
	cudaStream_t	stream = NULL;
	cudaError	ret;
	cufftResult	cc;
	int		refidx = 17;

	fill_time_domain_data();

	int n = 2400000;
	int ndim = FSI_SAMPLE_SIZE;
	int halfndim = FSI_SAMPLE_SIZE / 2;
	int fft_size = n;
	int batch_size = 17;
	printf(
		"n = %d\n"
		"fft_size = %d\n"
		"batch_size = %d\n", n, fft_size, batch_size);


	cufftDoubleReal *lins = NULL;
	cufftDoubleComplex *clins = NULL;
	ret = cudaMalloc(&lins, sizeof(cufftDoubleReal) * FSI_SAMPLE_SIZE * FSI_NCHANNELS);
	printf(
		"lins = %p\n"
		"cudaError = %d\n"
		"cufftHandle = %p\n"
		"cudaStream_t = %p\n", lins, ret, &plan, stream);

	ret = cudaMemcpy(lins, buffers.lin, sizeof(buffers.lin), cudaMemcpyHostToDevice);
	printf("cudaMemcpy[%zd]-> = %d\n", sizeof(buffers.lin), ret);

	cudaProfilerStart();

	/* compute FFT of raw REF channel */
	memcpy(fsi_ref_buffer, buffers.lin[refidx], FSI_SAMPLE_SIZE * sizeof(double));
	hilbert(fsi_ref_buffer, n);

	/* compute D2Z fft in place */
	cc = cufftPlanMany(&plan, 1, &n, &ndim, 1, ndim,
			   &halfndim, 1, halfndim,
			   CUFFT_D2Z, FSI_NCHANNELS);
	printf("cufftPlanMany = %d\n", cc);
	clins = (cufftDoubleComplex *)lins;
	for (int cnt = 0; cnt < 10; cnt++) {
		cc = cufftExecD2Z(plan, lins, clins);
		printf("cufftExecD2Z = %d\n", cc);
	}
	cudaProfilerStop();

	ret = cudaMemcpy(buffers.spec, clins, sizeof(buffers.spec), cudaMemcpyDeviceToHost);
	printf("cudaMemcpy[%zd]<- = %d\n", sizeof(buffers.spec), ret);

	ret = cudaFree(lins);
	printf(
		"lins = %p\n"
		"cudaError = %d\n"
		"cufftHandle = %p\n"
		"cudaStream_t = %p\n", lins, ret, &plan, stream);
	dump_time_domain_data();

	return 0;
}

int main(int argc, char *argv[])
{
	if (argc == 1 || strcmp(argv[1], "main") == 0)
	    return test_main(argc, argv);
	else
	    return test_hilbert(argc, argv);
}

#define SIZE	50
static double t[2*SIZE], x[2*SIZE];
static double result[] = {
    0.00380740829901749,       -0.973665022798749,
      0.995342878446301,       0.0784588126354958,
     -0.255598831859535,        0.975779146424375,
     -0.918568025026867,       -0.414687344468421,
      0.511767280622435,       -0.855353677142343,
       0.78699627681897,         0.60299925259613,
     -0.721321819178045,        0.692747727194654,
     -0.575085745835647,       -0.828775643119117,
      0.893936963680969,        -0.45271906082056,
      0.324976069883763,        0.937246246858528,
     -0.980257596782626,        0.179998270375852,
    -0.0247446324547444,        -1.00670138257924,
      0.996172759523373,        0.124908302253127,
     -0.270482704161485,        0.956682891923366,
     -0.903460420595145,         -0.4194855130723,
      0.562043915268864,       -0.832803678928927,
      0.733815562917345,        0.684060027507441,
     -0.788551536118411,        0.608607478794201,
     -0.468517451373404,       -0.881216866293352,
      0.951227070736967,       -0.319840028863393,
      0.160676003347428,        0.987423567300844,
     -0.996112144716037,      -0.0109216103479182,
      0.187644006061531,       -0.983565852227181,
      0.940023770827501,        0.354884646464903,
     -0.506287821112709,         0.85906451560618,
     -0.752268948131676,       -0.649135286874989,
      0.783204441414066,       -0.628087880001741,
      0.478742087519286,        0.887592123009115,
     -0.948316627357579,        0.303547185641745,
     -0.120353803888047,       -0.981889913923337,
	1.0017778984863,       0.0607274992454434,
     -0.247397509276008,        0.982148072234882,
     -0.898316883910894,       -0.435540870676913,
      0.601269301289083,       -0.781768575988776,
      0.673845736068263,        0.736470076951765,
     -0.855921968771069,        0.541733198925932,
     -0.325767291648207,       -0.953487455412036,
      0.994889613548623,      -0.0748494268817031,
    -0.0666883454622202,        0.978395879943749,
     -0.958138937099729,      -0.0489798314667337,
};

static int test_hilbert(int argc, char *argv[])
{
	double mean = 0.0;
	int i, n = 40;

	for (i = 0; i < n; i++) {
		t[i] = 0.1*i;
		x[i] = sin((17.0 + 0.1*t[i])*t[i]);
		mean += x[i];
	}
	mean /= n;
	for (i = 0; i < n; i++)
		x[i] -= mean;

	hilbert(x, n);

	double norm_diff = 0.0;
	for (i = 0; i < n; i++) {
		norm_diff += hypot(x[2*i] - result[2*i],
				   x[2*i+1] - result[2*i+1]);
		/* leave this only for debugging purposes
		 * 	printf("%03d: %12.5g %12.5g    %12.5g %12.5g\n", i,
		 *		x[2*i], result[2*i], x[2*i+1], result[2*i+1]);
		 */
	}
	printf("norm diff = %20.15g\n", norm_diff);
	return norm_diff < 1e-13;
}
