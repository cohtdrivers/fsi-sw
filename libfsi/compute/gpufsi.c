#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "fsi-private.h"

#define	N		FSI_SAMPLE_SIZE
#define	FSI_NO_CHID	-1	/* FIXME: invalid chid, maybe must go to
				 * fsi-private.h or fsi.h */
const int n = 2490000;		/* FIXME: dreadful constant of Nature */

/**
 * \brief
 * 	blank a channel configuration with sensible defaults
 */
void _fsi_clear_channel_config(struct fsi_channel_config *cfg, int channel_id)
{
	cfg->channel_id = channel_id;
	cfg->type = FSI_CH_NONE;
	cfg->npeaks = 0;
	cfg->channel_gain = 0;
	cfg->dc_coupling = 0;
	memset(cfg->peaks, 0, sizeof(cfg->peaks));
	cfg->disabled = 1;
}

int _fsi_channel_configure(struct fsi_channel_config *ch, struct fsi_channel_config *cfg)
{
	int i;

	ch->channel_id = cfg->channel_id;
	ch->type = cfg->type;
	ch->npeaks = cfg->npeaks;
	if (!((cfg->npeaks >= 0) && (cfg->npeaks <= FSI_MAX_PEAKS)))
		return FSI_CFG_BAD_NPEAKS;
	ch->channel_gain = !!cfg->channel_gain;
	ch->dc_coupling = !!cfg->dc_coupling;
	for (i = 0; i < ch->npeaks; i++)
		ch->peaks[i] = cfg->peaks[i];
	ch->disabled = cfg->disabled;

	return 0;
}


struct fsi_channel_config channel_configs[FSI_NCHANNELS];

/*
 * test: these channel acquisition data are scattered along
 * the buffer using random indexes for full
 * generality: their (dis)order cannot be worse, and their actual
 * significance can only be inferred from the configuration data
 * structure. This is intently done to prevent reliance on
 * any preconception on the layout of the acq_buffer structure.
 */

struct test_data {
	char	*filename;
	int	channel_id;
	int	type;
	int	pos_in_buffer;		/* completely randomize in buffer */
} test_data[] = {
    { "data/GAS",    32,	FSI_CH_GAS_CELL,    101, },
    { "data/REF",    33,	FSI_CH_INTERF,      213, },
    { "data/INC1",   34,	FSI_CH_DISTANCE,     96, },
    { "data/INC2",   35,	FSI_CH_DISTANCE,    208, },
    { "data/INC3",   36,	FSI_CH_DISTANCE,      7, },
    { "data/INC4",   37,	FSI_CH_DISTANCE,     95, },
    { "data/HLS1",   38,	FSI_CH_DISTANCE,    218, },
    { "data/HLS2",   39,	FSI_CH_DISTANCE,    103, },
    { "data/HLS3",   41,	FSI_CH_DISTANCE,     49, },
    { "data/HLS4",   42,	FSI_CH_DISTANCE,      6, },
    { "data/HLS5",   43,	FSI_CH_DISTANCE,    216, },
    { "data/HLS6",   44,	FSI_CH_DISTANCE,     97, },
    { "data/HLS7",   45,	FSI_CH_DISTANCE,    106, },
    { "data/HLS8",   46,	FSI_CH_DISTANCE,    191, },
    { "data/BALL",   47,	FSI_CH_DISTANCE,    235, },
    { "data/COL1",   49,	FSI_CH_DISTANCE,     41, },
    { "data/COL2",   50,	FSI_CH_DISTANCE,    246, },
};
int ntest_data = sizeof(test_data) / sizeof(test_data[0]);

void fill_fsi_acq_buffer(struct fsi_acq_buffer *acq)
{
	int i;

	acq->n = 2490000;
	acq->nchans = ntest_data;
	acq->ops = &_default_bops;
	for (i = 0; i < FSI_NCHANNELS; i++)
		acq->chid[i] = acq->bidx[i] = FSI_NO_CHID;
	for (i = 0; i < ntest_data; i++) {
		struct test_data *datum = &test_data[i];
		int bidx = datum->pos_in_buffer;
		int chid = datum->channel_id;
		uint16_t *dst = acq->buffer[bidx];

		FILE *f = fopen(datum->filename, "rb");
		fread(dst, sizeof(uint16_t), n, f);
		fclose(f);

		acq->chid[bidx] = chid;
		acq->bidx[chid] = bidx;
	}
}

void display_fsi_acq_buffer(struct fsi_acq_buffer *acq)
{
	int i, nchans;
	int chids[FSI_NCHANNELS];

	acq->ops->channel_list(acq, chids, &nchans);
	for (i = 0; i < nchans; i++) {
		int chid = chids[i];
		uint16_t *v = acq->ops->get_by_chid(acq, chid);
		int j;

		printf("chid:%03d: buf[%03d]  ", chid, acq->bidx[chid]);
		if (v == NULL) {
			printf("null data vector for chid %d at bidx %d\n",
				chid, acq->bidx[chid]);
			continue;
		}
		for (j = 0; j < 6; j++)
			printf("%5d ", v[j]);
		printf(" ... \n");
	}
}

void fill_acq_buffer(void)
{
	int i;

	for (i = 0; i < ntest_data; i++) {
		struct test_data *datum = &test_data[i];
		uint16_t *dst = _acq_buffer[datum->pos_in_buffer];
		FILE *f = fopen(datum->filename, "rb");
		fread(dst, sizeof(uint16_t), n, f);
		fclose(f);
	}
}


int acquire_buffer(uint16_t acq_buffer[FSI_NCHANNELS][N], int *chid_to_buf)
{
	int i;

	fill_acq_buffer();
	memcpy(acq_buffer, _acq_buffer, sizeof(_acq_buffer));
	for (i = 0; i < FSI_NCHANNELS; i++)
		chid_to_buf[i] = FSI_NO_CHID;
	for (i = 0; i < ntest_data; i++) {
		struct test_data *td = &test_data[i];
		chid_to_buf[td->channel_id] = td->pos_in_buffer;
	}
	return 0;
}
void display_acquired_buffer(uint16_t acq_buffer[][N], int *chid_to_buf)
{
	int i;
	for (i = 0; i < FSI_NCHANNELS; i++) {
		int bidx = chid_to_buf[i];
		uint16_t *v;
		int j;

		if (bidx == FSI_NO_CHID)
			continue;
		printf("chid:%03d: buf[%03d]  ", i, bidx);
		v = acq_buffer[bidx];
		for (j = 0; j < 6; j++)
			printf("%5d ", v[j]);
		printf(" ... \n");
	}
}

void init_channel_map(void)
{
	int i;
	int gcs = 0, refs = 0, dists = 0, braggs = 0;

	for (i = 0; i < FSI_NCHANNELS; i++) {
		struct fsi_channel_config *cfg = &_channel_configs[i];
		_fsi_clear_channel_config(cfg, i);
		channel_map[i].cfg = cfg;
	}
	/* initialize with particular test_data data instances */
	for (i = 0; i < ntest_data; i++) {
		struct test_data *td = &test_data[i];
		int chid = td->channel_id;
		struct channel_map *cm = &channel_map[chid];
		struct fsi_channel_config _data, *data = &_data;

		/* this stands in for a genuine call to
		 * fsi_channel_configure with real params */
		_fsi_clear_channel_config(data, chid);
		data->channel_id = chid;
		data->type = td->type;
		data->disabled = 0;
		_fsi_channel_configure(cm->cfg, data);
		cm->bidx = td->pos_in_buffer;

		/* tally channels by type */
		switch (cm->cfg->type) {
		case FSI_CH_GAS_CELL:
			gcs++;
			break;
		case FSI_CH_INTERF:
			refs++;
			break;
		case FSI_CH_DISTANCE:
			dists++;
			break;
		case FSI_CH_BRAGG:
			braggs++;
			break;
		case FSI_CH_NONE:
		default:
			break;
		}
	}
	/* determine as a function of the channel
	 * type (gas/ref), optical blocks, etc
	 * first, gas cells in label order
	 * second, refs in optical block order
	 * third, measurement channels in chid order
	 * fourth, Bragg channels in chid order
	 */
	int gc = 0;
	int ref = gc + gcs;
	int dist = ref + refs;
	int bragg = dist + dists;
	int nchans = bragg + braggs;
	assert(nchans == ntest_data);

	for (i = 0; i < FSI_NCHANNELS; i++) {
		struct channel_map *cm = &channel_map[i];
		switch (cm->cfg->type) {
		case FSI_CH_GAS_CELL:
			cm->gidx = gc++;
			break;
		case FSI_CH_INTERF:
			cm->gidx = ref++;
			break;
		case FSI_CH_DISTANCE:
			cm->gidx = dist++;
			break;
		case FSI_CH_BRAGG:
			cm->gidx = bragg++;
			break;
		case FSI_CH_NONE:
		default:
			break;
		}
	}
	assert(gc == gcs);
	assert(ref-gc == refs);
	assert(dist-ref == dists);
	assert(bragg-dist == braggs);
}

static const char *symtype[] = {
	[FSI_CH_GAS_CELL] = "gas",
	[FSI_CH_INTERF] = "interf",
	[FSI_CH_DISTANCE] = "dist",
	[FSI_CH_BRAGG] = "bragg",
	[FSI_CH_NONE] = "none",
};

static void chid_to_ob_bd_ch(unsigned chid, unsigned *ob, unsigned *bd, unsigned *ch)
{
	unsigned obmask = 0xc0;
	unsigned bdmask = 0x38;
	unsigned chmask = 0x07;
	*ob = (obmask & chid) >> 6;
	*bd = ((bdmask & chid) >> 3) + 1;
	*ch = chmask & chid;
}

void display_channel_map(void)
{
	int i;

	for (i = 0; i < FSI_NCHANNELS; i++) {
		struct channel_map *c = &channel_map[i];
		struct fsi_channel_config *cfg = channel_map[i].cfg;
		unsigned ob, bd, ch;

		assert(i == cfg->channel_id);
		if (cfg->type == FSI_CH_NONE)
			continue;
		chid_to_ob_bd_ch(cfg->channel_id, &ob, &bd, &ch);
		printf("%03d(%d/%d/%d): %-6s buf:%03d  gidx:%03d\n",
			cfg->channel_id, ob, bd, ch,
			symtype[cfg->type],
			c->bidx, c->gidx);
	}
}
/* configuration integrity validation errors */
#define FSI_CFG_OK				0
#define FSI_CFG_ERRBASE				-4097
#define FSI_CFG_TOO_MANY_GAS_CELLS		FSI_CFG_ERRBASE-1
#define FSI_CFG_TOO_MANY_REFERENCE_CHANNELS	FSI_CFG_ERRBASE-2
/*
#define FSI_CFG_		FSI_CFG_ERRBASE-3
#define FSI_CFG_		FSI_CFG_ERRBASE-4
#define FSI_CFG_		FSI_CFG_ERRBASE-5
#define FSI_CFG_		FSI_CFG_ERRBASE-6
#define FSI_CFG_		FSI_CFG_ERRBASE-7
#define FSI_CFG_		FSI_CFG_ERRBASE-8
#define FSI_CFG_		FSI_CFG_ERRBASE-9
#define FSI_CFG_		FSI_CFG_ERRBASE-10
#define FSI_CFG_		FSI_CFG_ERRBASE-11
#define FSI_CFG_		FSI_CFG_ERRBASE-12
*/

int validate_channel_map(struct channel_map *cm)
{
	int	refs[FSI_NBLOCKS];
	int	gcs[FSI_NGCS];
	int	nrefs, ngcs;
	int	i;

	nrefs = ngcs = 0;
	for (i = 0; i < FSI_NCHANNELS; i++) {
		struct channel_map *cm = &channel_map[i];
		struct fsi_channel_config *cfg = cm->cfg;

		switch (cfg->type) {
		case FSI_CH_GAS_CELL:
			if (ngcs >= FSI_NGCS)
				return FSI_CFG_TOO_MANY_GAS_CELLS;
			gcs[ngcs++] = cfg->channel_id;
			break;
		case FSI_CH_INTERF:
			if (nrefs >= FSI_NBLOCKS)
				return FSI_CFG_TOO_MANY_REFERENCE_CHANNELS;
			refs[nrefs++] = cfg->channel_id;
			break;
		case FSI_CH_DISTANCE:
			break;
		case FSI_CH_BRAGG:
			break;
		case FSI_CH_NONE:
		default:
			break;
		}
	}
	if (gcs[0] == refs[0])		/* FIXME: delete this when warning removed */
		return -1;
	return 0;	/* all great */
}

#ifdef GPUFSI_MAIN
int old_main(void)
{
	extern FSI *fsi_comp_lib_init(const char *);
	extern void fsi_comp_lib_exit(FSI *);

	fill_acq_buffer();
	init_channel_map();
	display_channel_map();
	if (validate_channel_map(channel_map) != 0) {
		fprintf(stderr, "condition found in channel_map validation\n");
		exit(1);
	}

	FSI *h = fsi_comp_lib_init(NULL);
	fsi_comp_lib_exit(h);
	return 0;
}

int main(void)
{
	fill_fsi_acq_buffer(&fsi_acq_buffer);
	display_fsi_acq_buffer(&fsi_acq_buffer);

	return 0;
}
#endif /* GPUFSI_MAIN */
