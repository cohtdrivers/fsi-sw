#include <math.h>
#include <fftw3.h>

#define	N	2500000

double sine[N];
double f[N+2];


int main(int argc, char *argv[])
{
	int i;
	double twopi_over_n = 2 * M_PI / N;
	fftw_plan p;

	for (i = 0; i < N; i++) {
		sine[i] = cos(twopi_over_n * 10 * i);
		sine[i] += 0.5*sin(twopi_over_n * 5 * i);
	}
	p = fftw_plan_r2r_1d(N, sine, f, FFTW_FORWARD, FFTW_ESTIMATE);
	for (i = 0; i < 20; i++)
		fftw_execute(p);
	for (i = 0; i < 20; i++) {
		printf("%4d: %12.6f\n", i, f[i]);
	}
	printf("[....]\n");
	for (i = N/2-20; i < N/2+2; i++) {
		printf("%4d: %12.6f\n", i, f[i]);
	}
	printf("[....]\n");
	for (i = N-20; i < N+2; i++) {
		printf("%4d: %12.6f\n", i, f[i]);
	}
	return 0;
}
