#include <cufft.h>
#include <cublas_v2.h>

#include "errcodes.h"

static const char *cufft_error_string[] = {
    [CUFFT_SUCCESS] = "The cuFFT operation was successful",
    [CUFFT_INVALID_PLAN] = "cuFFT was passed an invalid plan handle",
    [CUFFT_ALLOC_FAILED] = "cuFFT failed to allocate GPU or CPU memory",
    [CUFFT_INVALID_TYPE] = "No longer used",
    [CUFFT_INVALID_VALUE] = "User specified an invalid pointer or parameter",
    [CUFFT_INTERNAL_ERROR] = "Driver or internal cuFFT library error",
    [CUFFT_EXEC_FAILED] = "Failed to execute an FFT on the GPU",
    [CUFFT_SETUP_FAILED] = "The cuFFT library failed to initialize",
    [CUFFT_INVALID_SIZE] = "User specified an invalid transform size",
    [CUFFT_UNALIGNED_DATA] = "No longer used",
    [CUFFT_INCOMPLETE_PARAMETER_LIST] = "Missing parameters in call",
    [CUFFT_INVALID_DEVICE] = "Execution of a plan was on different GPU than plan creation",
    [CUFFT_PARSE_ERROR] = "Internal plan database error",
    [CUFFT_NO_WORKSPACE]   = "No workspace has been provided prior to plan execution",
    [CUFFT_NOT_IMPLEMENTED] = "Function does not implement functionality for parameters given",
    [CUFFT_LICENSE_ERROR] = "Used in previous versions",
    [CUFFT_NOT_SUPPORTED]  = "Operation is not supported for parameters given"
};
__attribute__((unused))
static int cufft_nerrs = sizeof(cufft_error_string)/sizeof(cufft_error_string[0]);

const char *cufft_get_error_string(int code)
{
	return cufft_error_string[code];
}

static const char *cublas_error_string[] = {
	[CUBLAS_STATUS_SUCCESS] = "CUBLAS_STATUS_SUCCESS",
	[CUBLAS_STATUS_NOT_INITIALIZED] = "CUBLAS_STATUS_NOT_INITIALIZED",
	[CUBLAS_STATUS_ALLOC_FAILED] = "CUBLAS_STATUS_ALLOC_FAILED",
	[CUBLAS_STATUS_INVALID_VALUE] = "CUBLAS_STATUS_INVALID_VALUE",
	[CUBLAS_STATUS_ARCH_MISMATCH] = "CUBLAS_STATUS_ARCH_MISMATCH",
	[CUBLAS_STATUS_MAPPING_ERROR] = "CUBLAS_STATUS_MAPPING_ERROR",
	[CUBLAS_STATUS_EXECUTION_FAILED] = "CUBLAS_STATUS_EXECUTION_FAILED",
	[CUBLAS_STATUS_INTERNAL_ERROR] = "CUBLAS_STATUS_INTERNAL_ERROR",
	[CUBLAS_STATUS_NOT_SUPPORTED] = "CUBLAS_STATUS_NOT_SUPPORTED",
	[CUBLAS_STATUS_LICENSE_ERROR] = "CUBLAS_STATUS_LICENSE_ERROR",
};
__attribute__((unused))
static int cublas_nerrs = sizeof(cublas_error_string)/sizeof(cublas_error_string[0]);

const char *cublas_get_error_string(int code)
{
	return cublas_error_string[code];
}
