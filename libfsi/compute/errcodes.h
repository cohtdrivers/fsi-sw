#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern const char *cufft_get_error_string(int code);
extern const char *cublas_get_error_string(int code);

#ifdef __cplusplus
}
#endif /* __cplusplus */

