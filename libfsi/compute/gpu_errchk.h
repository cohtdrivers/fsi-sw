/**
 * @ brief
 * 	ancillary macros for CUDA inline error checking/reporting
 */

#include <stdio.h>
#include <stdlib.h>

#include <cufft.h>
#include <cublas_v2.h>

#include "errcodes.h"

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__, 1); }
inline void gpuAssert(cudaError_t code, const char *file, int line, int abort)
{
	if (code != cudaSuccess) {
		fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort)
			exit(code);
	}
}


#define cufftErrchk(ans) { cufftAssert((ans), __FILE__, __LINE__, 1); }
inline void cufftAssert(cufftResult code, const char *file, int line, int abort)
{
	if (code != CUFFT_SUCCESS) {
		fprintf(stderr,"cufftAssert: error code %s %s %d\n", cufft_get_error_string(code), file, line);
		if (abort)
			exit(code);
	}
}

#define cublasErrchk(ans) { cublasAssert((ans), __FILE__, __LINE__, 1); }
inline void cublasAssert(cublasStatus_t code, const char *file, int line, int abort)
{
	if (code != CUBLAS_STATUS_SUCCESS) {
		fprintf(stderr,"cublasAssert: error code %s %s %d\n", cublas_get_error_string(code), file, line);
		if (abort)
			exit(code);
	}
}
