#include <stdint.h>

#define	NBLOCKS		4
#define	FSI_SAMPLE_SIZE	2500000
#define	N		FSI_SAMPLE_SIZE
#define	MAX_GCS		2
#define	CHANNELS_BLOCK	64
#define	MAX_CHANNELS	(NBLOCKS*CHANNELS_BLOCK)

#define	DOUBLE	double

struct channel_buffers {
	uint16_t	samples[MAX_CHANNELS][FSI_SAMPLE_SIZE];
};

struct spectral_buffers {
	DOUBLE		channel[MAX_CHANNELS][FSI_SAMPLE_SIZE];
};

enum fsi_channel_type {
	FSI_CH_NONE = 0,
	FSI_CH_DISTANCE = 1,	/**< configure as distance meas. channel */
	FSI_CH_BRAGG,		/**< configure as Bragg channel */
	FSI_CH_GAS_CELL,	/**< gas cell channel */
	FSI_CH_INTERF,		/**< reference interferometer channel */
};

struct fsi_channel_config {
	unsigned int	channel_id;		/**< channel to be configured */
	enum fsi_channel_type	type;		/**< type of channel (distance/Bragg) */
	int		npeaks;			/**< number of valid peaks to be requested in peaks array */
	int		channel_gain;		/**< bool: *do* make use of channel gain */
	int		dc_coupling;		/**< bool: true only if channel is DC coupled */
	struct fsi_peak_config
			peaks[FSI_MAX_PEAKS];	/**< peak requests (each peak has separate params */
	/** a disabled channel, though fully configured, is ignored by
	 * the measurement logic and does not return measurements.
	 * Useful to reserve a channel that might be swapped with
	 * another during commissioning */
	unsigned int	disabled;
};

struct channel_configurations {
	union {
	struct fsi_channel_config 	fsi_channel_config;
	struct {
		int	channel_id;
		int	channel_type;
		int	npeaks;
		int	gain;
		int	coupling;
		struct fsi_peak_config peaks[FSI_MAX_PEAKS];
		int	disabled;
	};
	};
} channel_configurations[FSI_NCHANNELS];


int init_ref(double *dst, const char *fname, int n)
{
	FILE *f;
	int i;
	uint16_t ref16[N];

	if ((f = fopen(fname, "rb")) == NULL) {
		fprintf(stderr, "cannot open %s file", fname);
		exit(1);
	}
	fread(ref16, sizeof(uint16_t), n, f);
	fclose(f);
	for (i = 0; i < n; i++)
		dst[i] = ref16[i];
	return 0;
}


