#include <assert.h>
#include <stdio.h>
#include "fsi-private.h"
#include "../generated-channel-configs.h"

/**
 * @brief	full information on raw DIOT-acquired data
 *
 * 	The structure is meant to store up to FSI_NCHANNELS channel
 * 	vectors acquired and stored in absolutely arbitrary positions in
 * 	the uint16_t vector buffer; no assumption is made on the order
 * 	at all.
 */
struct fsi_acq_buffer {
	int		n;			/* common length of acquisitions */
	int		nchans;			/* number of actual channels acquired */
	int		chid[FSI_NCHANNELS];	/* chid[i]: channel_id of i-th entry in buffer */
	int		bidx[FSI_NCHANNELS];	/* bidx[chid]: index in buffer of channel_id data */
	uint16_t	buffer[FSI_NCHANNELS][FSI_SAMPLE_SIZE];
						/* actual data buffer */
	struct fsi_acq_buffer_ops *ops;
};

#define FSI_NO_CHID	-1		/* invalid channel_id or buffer index */

/**
 * @brief	methods to navigate an fsi_acq_buffer
 */
struct fsi_acq_buffer_ops {
	int		(*n)(struct fsi_acq_buffer *buf);
	int		(*nchans)(struct fsi_acq_buffer *buf);
	uint16_t	*(*get_by_chid)(struct fsi_acq_buffer *buf, int chid);
	void		(*channel_list)(struct fsi_acq_buffer *buf,int *chids, int *nchans);
};

static int _n(struct fsi_acq_buffer *buf)
{
	return buf->n;
}

static int _nchans(struct fsi_acq_buffer *buf)
{
	return buf->nchans;
}

static uint16_t *_get_by_chid(struct fsi_acq_buffer *buf, int chid)
{
	int bidx;

	if (chid > FSI_NCHANNELS || chid < 0)
		return NULL;
	bidx = buf->bidx[chid];
	if (bidx == FSI_NO_CHID)
		return NULL;
	return &buf->buffer[bidx][0];
}

static void _channel_list(struct fsi_acq_buffer *buf, int *chids, int *nchans)
{
	int chid;
	int cnt = 0;

	for (chid = 0; chid < FSI_NCHANNELS; chid++) {
		if (buf->bidx[chid] == FSI_NO_CHID)
			continue;
		*chids++ = chid;
		cnt++;
	}
	assert(cnt == buf->nchans);
	if (nchans != NULL)
		*nchans = cnt;
}

/**
 * actual set of default methods of an fsi_acq_buffer
 */
static struct fsi_acq_buffer_ops _default_bops = {
	.n = _n,
	.nchans = _nchans,
	.get_by_chid = _get_by_chid,
	.channel_list = _channel_list,
};

struct fsi_acq_buffer fsi_acq_buffer;

/*
 * test: these channel acquisition data are scattered along
 * the buffer using random indexes for full
 * generality: their (dis)order cannot be worse, and their actual
 * significance can only be inferred from the configuration data
 * structure. This is intently done to prevent reliance on
 * any preconception on the layout of the acq_buffer structure.
 */

static struct test_data {
	char	*filename;
	int	channel_id;
	int	type;
	int	pos_in_buffer;		/* completely randomize in buffer */
} test_data[] = {
    { "data/GAS",    32,	FSI_CH_GAS_CELL,    101, },
    { "data/REF",    33,	FSI_CH_INTERF,      213, },
    { "data/INC1",   34,	FSI_CH_DISTANCE,     96, },
    { "data/INC2",   35,	FSI_CH_DISTANCE,    208, },
    { "data/INC3",   36,	FSI_CH_DISTANCE,      7, },
    { "data/INC4",   37,	FSI_CH_DISTANCE,     95, },
    { "data/HLS1",   38,	FSI_CH_DISTANCE,    218, },
    { "data/HLS2",   39,	FSI_CH_DISTANCE,    103, },
    { "data/HLS3",   41,	FSI_CH_DISTANCE,     49, },
    { "data/HLS4",   42,	FSI_CH_DISTANCE,      6, },
    { "data/HLS5",   43,	FSI_CH_DISTANCE,    216, },
    { "data/HLS6",   44,	FSI_CH_DISTANCE,     97, },
    { "data/HLS7",   45,	FSI_CH_DISTANCE,    106, },
    { "data/HLS8",   46,	FSI_CH_DISTANCE,    191, },
    { "data/BALL",   47,	FSI_CH_DISTANCE,    235, },
    { "data/COL1",   49,	FSI_CH_DISTANCE,     41, },
    { "data/COL2",   50,	FSI_CH_DISTANCE,    246, },
};
static int ntest_data = sizeof(test_data) / sizeof(test_data[0]);

/**
 * @brief   fill acq with the above sample data taken from a real,
 *          beautiful acquisition happened in the SCT, called
 *          no_tocar_20240124T114108*.mat
 */
void fill_fsi_acq_buffer(struct fsi_acq_buffer *acq)
{
	int i;

	acq->n = 2490000;
	acq->nchans = ntest_data;
	acq->ops = &_default_bops;
	for (i = 0; i < FSI_NCHANNELS; i++)
		acq->chid[i] = acq->bidx[i] = FSI_NO_CHID;
	for (i = 0; i < ntest_data; i++) {
		struct test_data *datum = &test_data[i];
		int bidx = datum->pos_in_buffer;
		int chid = datum->channel_id;
		uint16_t *dst = acq->buffer[bidx];

		FILE *f = fopen(datum->filename, "rb");
		fread(dst, sizeof(uint16_t), acq->n, f);
		fclose(f);

		acq->chid[bidx] = chid;
		acq->bidx[chid] = bidx;
	}
}

void display_fsi_acq_buffer(struct fsi_acq_buffer *acq)
{
	int i, nchans;
	int chids[FSI_NCHANNELS];
	int n = acq->ops->n(acq);

	acq->ops->channel_list(acq, chids, &nchans);
	for (i = 0; i < nchans; i++) {
		int chid = chids[i];
		uint16_t *v = acq->ops->get_by_chid(acq, chid);
		int j;

		printf("chid:%03d: buf[%03d]  ", chid, acq->bidx[chid]);
		if (v == NULL) {
			printf("null data vector for chid %d at bidx %d\n",
				chid, acq->bidx[chid]);
			continue;
		}
		for (j = 0; j < 4; j++)
			printf("%5d ", v[j]);
		printf(" ... ");
		for (j = 0; j < 4; j++)
			printf("%5d ", v[n+j-4]);
		printf("\n");
	}
}

int test_fsi_acq_buffer_main(int argc, char *argv[])
{
	fill_fsi_acq_buffer(&fsi_acq_buffer);
	display_fsi_acq_buffer(&fsi_acq_buffer);

	return 0;
}

#ifdef GPUFSI_MAIN
int main(int argc, char *argv[])
{
	test_fsi_acq_buffer_main(argc, argv);
}
#endif /* GPUFSI_MAIN */
