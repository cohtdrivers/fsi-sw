#include <cub/cub.cuh>

extern "C"
void cumsum(double *input, double *output, int nelems)
{
	void     *d_temp_storage = NULL;
	size_t   temp_storage_bytes = 0;
	cub::DeviceScan 	scanner;

	scanner.InclusiveSum(d_temp_storage, temp_storage_bytes, input, output, nelems);
	cudaMalloc(&d_temp_storage, temp_storage_bytes);
	scanner.InclusiveSum(d_temp_storage, temp_storage_bytes, input, output, nelems);
}
