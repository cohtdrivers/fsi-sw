#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <cuda.h>
#include <cufft.h>
#include <cublas_v2.h>

#include "gpu_errchk.h"

#define FSI_SAMPLE_SIZE	2500000
#define	N		FSI_SAMPLE_SIZE

double ref_double[N];
double spec_double[2*N];
double phi_double[N];

static struct work_area {
	cufftDoubleReal		(*real)[20][N];
	cufftDoubleComplex	(*complex)[3][N];

	cufftHandle plan1;
	cufftHandle plan1r;

	cublasHandle_t handle;
} _wa, *work_area = &_wa;

/*
 * perform all static GPU initializations for linearizer code
 */
int linearize_init(int n)
{
	if (n > N) {
		fprintf(stderr, "error: GPU linearization: "
				"bad n = %d > FSI_SAMPLE_SIZE = %d\n",
				n, N);
		exit(1);
	}

	gpuErrchk(cudaMalloc(&work_area->real, sizeof(*(work_area->real))));
	gpuErrchk(cudaMalloc(&work_area->complex, sizeof(*(work_area->complex))));
	cufftErrchk(cufftPlan1d(&work_area->plan1, n, CUFFT_D2Z, 1));
	cufftErrchk(cufftPlan1d(&work_area->plan1r, n, CUFFT_Z2Z, 1));
	cublasErrchk(cublasCreate(&work_area->handle));

	return 0;
}

int linearize_exit(void)
{
	cublasErrchk(cublasDestroy(work_area->handle));
	cufftErrchk(cufftDestroy(work_area->plan1));
	gpuErrchk(cudaFree(work_area->complex));
	gpuErrchk(cudaFree(work_area->real));

	return 0;
}


int fft(cufftDoubleReal *ref, int n, cufftDoubleComplex *spec)
{
	/* compute FFT of raw REF channel */
	cufftErrchk(cufftExecD2Z(work_area->plan1, ref, spec));
	
	return 0;	
}

int ifft(cufftDoubleComplex *spec, int n)
{
	double one_over_n = 1.0 / n;

	/* compute in-place inverse FFT of complex spectrum */
	cufftErrchk(cufftExecZ2Z(work_area->plan1r,spec,spec, CUFFT_INVERSE));
	cublasErrchk(cublasZdscal(work_area->handle, n, &one_over_n, spec, 1));

	return 0;	
}

int hilbert_fold(cufftDoubleComplex *spec, int n)
{
	double zero = 0.0, two = 2.0;
	cublasHandle_t handle = work_area->handle;

	cublasErrchk(cublasZdscal(handle, 1,     &zero, &spec[0], 1));
	cublasErrchk(cublasZdscal(handle, n/2-1, &two,  &spec[1], 1));
	cublasErrchk(cublasZdscal(handle, n/2-1, &zero, &spec[n/2+1], 1));

	return 0;
}

/**
 * compute argument phi of a vector cmplx of n complex numbers
 */
__global__ void angle(double *phi, double *cmplx, int n)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;

	/* FIXME: this is not required, but block allocation
	 * needs an optimize here and elsewhere
	 */
	while (tid < n) {
		phi[tid] = atan2(cmplx[2*tid+1], cmplx[2*tid]);
		tid += blockDim.x * gridDim.x;
	}
}

/**
 * compute the vector of phase corrections by +2pi at the points
 * in vector dd where phase falls by more than pi
 *
 * shamelessly lifted from numpy code
 */
__global__ void phase_correct(double *phase_corrections, double *v, int n,
				double *dd, double *ddmod)	/* scratch areas */
{
	double period = 2*M_PI;
	double discont = period / 2;
	double interval_high = period / 2;
	double interval_low = period / 2;

	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if (tid < n && tid > 0) {
		double pc;
		dd[tid] = v[tid] - v[tid-1];
		pc = fmod(dd[tid] - interval_low, period) + interval_low;
		if (pc == interval_low && dd[tid] > 0)
			pc = interval_high;
		if (fabs(dd[tid]) < discont)
			pc = 0;
		else
			pc -= dd[tid];
		phase_corrections[tid] = pc;
	} else if (tid == 0)
		phase_corrections[tid] = 0;
}

/*
 * phi_norm - normalize an instantaneous phase vector

   This kernel computes into phin an affine transformation of phi, so
   that the values phi[0]..phi[n-1], assumed monotonous, grow exactly
   from phin[0] = 0 to phin[n-1] = n - 1.
 */
__global__ void phi_norm(double *phi, int n, double *phin)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	double phi0 = phi[0];
	double phi1 = phi[n-1];

	if (tid < n) {
		phin[tid] = (n-1) * (phi[tid] - phi0) / (phi1 - phi0);
	}
}

/*
 * interpolation correlative intervals are defined by the array of
 * indices idx; the invariant for it is
 *     (phin[idx[i]+1] > i) & (i >= phin[idx[i]])		0 <= i < n-1
 * where phin is the normalized phase (i.e., the phase of the analytic
 * signal normalized to span from phin[0] = 0 to phin[n-1] = n-1). This
 * is done so that idx is the inverse function of phin, or, put another
 * way, the linearized (interpolated) value of f(t), for t in [i, i+1),
 * must be found interpolating the non-linearized values of f taken at
 * [f[idx[i]], f[idx[i] + 1]).
 *
 */
__global__ void interp_indexes(double *phi, int n, int *idxs, double *t)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;

	if (tid < n - 1) {
		int j;
		double phi0 = phi[tid];
		double phi1 = phi[tid+1];
		for (j = ceil(phi0); j < phi1; j++) {
			if (j < phi0)	/* FIXME: this cannot be */
				continue;
			idxs[j] = tid;
			t[j] = (j - phi0) / (phi1 - phi0);
		}
	} else if (tid == n) {
		/* last interval, rightmost end */
		idxs[n-1] = n-2;
		t[n-1] = 1.0;
	}
}

int init_ref(double *dst, const char *fname, int n)
{
	FILE *f;
	int i;
	uint16_t ref16[N];

	if ((f = fopen(fname, "rb")) == NULL) {
		fprintf(stderr, "cannot open %s file", fname);
		exit(1);
	}
	fread(ref16, sizeof(uint16_t), n, f);
	fclose(f);
	for (i = 0; i < n; i++)
		dst[i] = ref16[i];
	return 0;
}

extern "C"
void cumsum(double *input, double *output, int nelems);

void linearize(double *ref, int n, double *phi)
{
	int i = 0;
	const double one = 1.0;

	cufftDoubleComplex *spec		= (*work_area->complex)[0];
	cufftDoubleReal *dd			= (*work_area->real)[i++];
	cufftDoubleReal *ddmod			= (*work_area->real)[i++];
	cufftDoubleReal *phase_corrections	= (*work_area->real)[i++];
	cufftDoubleReal *cum_phc		= (*work_area->real)[i++];

	fft(ref, n, spec);
	hilbert_fold(spec, n);
	ifft(spec, n);
	angle<<<n,1>>>(phi, (double *)spec, n);
	phase_correct<<<n,1>>>(phase_corrections, phi, n, dd, ddmod);
	cumsum(phase_corrections, cum_phc, n);
	cublasErrchk(cublasDaxpy(work_area->handle, n, &one, cum_phc, 1, phi, 1));
}

__global__ void interp(double *ref, int n, int *inv, double *t, double *refl)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;

	if (tid < n) {
		int interv = inv[tid];

		refl[tid] = ref[interv];
		refl[tid] += t[tid] * (ref[interv+1] - ref[interv]);
	}
}

void display_doublev(double *v, int where, int howmany, const char *lbl)
{
	double tmpd[howmany];
	int j;

	gpuErrchk(cudaMemcpy(tmpd, &v[where], howmany * sizeof(v[0]),
				cudaMemcpyDeviceToHost));
	printf("%11s: ", lbl);
	for (j = 0; j < howmany; j++)
		printf("%11.3f ", tmpd[j]);
	printf("\n");
}

void display_intv(int *v, int where, int howmany, const char *lbl)
{
	int tmpi[howmany];
	int j;

	gpuErrchk(cudaMemcpy(tmpi, &v[where], howmany * sizeof(v[0]),
				cudaMemcpyDeviceToHost));
	printf("%11s: ", lbl);
	for (j = 0; j < howmany; j++)
		printf("%11d ", tmpi[j]);
	printf("\n");
}

void display_interpolator(double *phin, int where, int howmany, int *idx, double *t)
{
	int j;
	printf("%11s: ", "j");
	for (j = 0; j < howmany; j++)
		printf("%11d ", where+j);
	printf("\n");
	display_intv(idx, where, howmany, "idx");
	display_doublev(phin, where, howmany, "phin");
	display_doublev(t, where, howmany, "t");
}

/**
 * interpolator - precompute interpolation coefficients from
 * a linear phase data vector
 *
 *   x	is a monotonically increasing vector representing the
 *   	growth of instantaneous phase, i.e., x[j] is the instantaneous
 *      phase of ref[i]
 *   n	is the length of all vectors involved in the computation
 *   interv, t
 *	computed interval correspondences and interpolation coefficients
 *	(see below)
 *
 *	First of all, x values are normalized by an affine shift/scaling
 *	so that x[0] = 0, x[n-1] = n-1. Once this is done, the
 *	interpolation coefficients are computed so that
 *
 *		x[interv[j]] <= j < x[interv[j] + 1]
 *
 *	and t[j] is the relative position of j in the above interval
 *	(x[interv[j]], x[interv[j] + 1]). In this manner, the values of
 *	any magnitude in equispaced points of time, V[j], can be
 *	computed by interpolating the values of v in interv[j] and
 *	interv[j]+1 by the weights (1-t), t.
 */
void interpolator(double *x, int n, int *interv, double *t)
{
	double *xn;
	const int nthreads = 512;
	const int nblocks = (n + (nthreads-1)) / nthreads;

	gpuErrchk(cudaMalloc(&xn, N * sizeof(xn[0])));

	/* normalize x to run from 0 to n-1 */
	phi_norm<<<nblocks,nthreads>>>(x, n, xn);

	/* compute the intervals of interpolation and the linear
	 * interpolation factor corrsponding to equally spaced samples
	 * given the linearized pase data in x
	 */
	interp_indexes<<<nblocks,nthreads>>>(x, n, interv, t);
}

int test_linearize(int argc, char *argv[])
{
	const int n = 2490000;
	const int nthreads = 512;
	const int nblocks = (n + (nthreads-1)) / nthreads;

	int *idx;
	double *t;
	double *refl;

	init_ref(ref_double, "data/REF", n);

	cufftDoubleReal *ref, *phi, *phin;
	gpuErrchk(cudaMalloc(&ref, N * sizeof(ref[0])));
	gpuErrchk(cudaMalloc(&phi, N * sizeof(phi[0])));
	gpuErrchk(cudaMalloc(&phin, N * sizeof(phin[0])));
	gpuErrchk(cudaMalloc(&idx, N * sizeof(idx[0])));
	gpuErrchk(cudaMalloc(&t, N * sizeof(t[0])));
	gpuErrchk(cudaMalloc(&refl, N * sizeof(refl[0])));
	gpuErrchk(cudaMemcpy(ref, ref_double, sizeof(*ref_double) * N,
				cudaMemcpyHostToDevice));

	linearize_init(n);
	linearize(ref, n, phi);
	interpolator(phi, n, idx, t);

	/* interpolation info is ready: show it */
	display_interpolator(phin, 0, 12, idx, t);
	printf("\n");
	display_interpolator(phin, n-10, 10, idx, t);
	printf("\n");

	/* now, interpolate ref */
	interp<<<nblocks,nthreads>>>(ref, n, idx, t, refl);
	display_doublev(refl, 0, 10, "refl");
	display_doublev(refl, n-10, 10, "refl");

	linearize_exit();

	gpuErrchk(cudaMemcpy(phi_double, phi, sizeof(*phi_double) * n,
				cudaMemcpyDeviceToHost));
	gpuErrchk(cudaFree(refl));
	gpuErrchk(cudaFree(t));
	gpuErrchk(cudaFree(idx));
	gpuErrchk(cudaFree(phi));
	gpuErrchk(cudaFree(ref));

	return 0;
}

int main(int argc, char *argv[])
{
	return test_linearize(argc, argv);
}
