/*
 * Copyright CERN 2021
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <sys/file.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <math.h>

#include "fsi-laser.h"
#include "fsi-laser-private.h"

/* laser cli answer error codes */
#define	ACK		"*"	/**< Acknowledge */
#define	CMD_ERR		"!"     /**< Unknown Command */
#define	ARG_ERR		"#"     /**< Illegal Command Argument */
#define	EXE_ERR		"&"     /**< Execution Error */

struct fsi_laser_property_ops {
	int (*setter)(struct fsi_laser *h, struct fsi_laser_property *prop);
	int (*getter)(struct fsi_laser *h, struct fsi_laser_property *prop);
	int (*validate)(struct fsi_laser *h, struct fsi_laser_property *prop);
};

static struct fsi_property_descriptor {
	enum fsi_laser_property_idx	id;
	enum fsi_laser_property_type	type;
	struct fsi_laser_property_ops	*ops;
	char				*cmd;
	char				*query;
	int (*(reserved[5]))(struct fsi_laser *h, struct fsi_laser_property *prop);
} laser_properties[] = {
	[fsi_lp_serial_device]		= { .id = fsi_lp_serial_device,		.type = fsi_lpt_string,		.ops = NULL, .cmd = NULL,	.query = NULL,	},
	[fsi_lp_device_id]		= { .id = fsi_lp_device_id,		.type = fsi_lpt_string,		.ops = NULL, .cmd = NULL,	.query = "id?",	},
	[fsi_lp_state]			= { .id = fsi_lp_state,			.type = fsi_lpt_boolean,	.ops = NULL, .cmd = "laz",	.query = "laz?", },
	[fsi_lp_key_lock]		= { .id = fsi_lp_key_lock,		.type = fsi_lpt_boolean,	.ops = NULL, .cmd = "int",	.query = "int?", },
	[fsi_lp_soft_lock]		= { .id = fsi_lp_soft_lock,		.type = fsi_lpt_boolean,	.ops = NULL, .cmd = "int",	.query = "int?", },
	[fsi_lp_pmax]			= { .id = fsi_lp_pmax,			.type = fsi_lpt_floating_point,	.ops = NULL, .cmd = NULL,	.query = "pmax?", },
	[fsi_lp_pmin]			= { .id = fsi_lp_pmin,			.type = fsi_lpt_floating_point,	.ops = NULL, .cmd = NULL,	.query = "pmin?", },
	[fsi_lp_power]			= { .id = fsi_lp_power,			.type = fsi_lpt_floating_point,	.ops = NULL, .cmd = "pwr",	.query = "pwr?", },
	[fsi_lp_power_unit]		= { .id = fsi_lp_power_unit,		.type = fsi_lpt_integer,	.ops = NULL, .cmd = "pwru",	.query = "pwru?", },
	[fsi_lp_domain]			= { .id = fsi_lp_domain,		.type = fsi_lpt_integer,	.ops = NULL, .cmd = "unit",	.query = "unit?", },
	[fsi_lp_wmax]			= { .id = fsi_lp_wmax,			.type = fsi_lpt_floating_point,	.ops = NULL, .cmd = NULL,	.query = "wmax?", },
	[fsi_lp_wmin]			= { .id = fsi_lp_wmin,			.type = fsi_lpt_floating_point,	.ops = NULL, .cmd = NULL,	.query = "wmin?", },
	[fsi_lp_modulation_source]	= { .id = fsi_lp_modulation_source,	.type = fsi_lpt_integer,	.ops = NULL, .cmd = "sms",	.query = "sms?", },
	[fsi_lp_start]			= { .id = fsi_lp_start,			.type = fsi_lpt_floating_point,	.ops = NULL, .cmd = "str",	.query = "str?", },
	[fsi_lp_stop]			= { .id = fsi_lp_stop,			.type = fsi_lpt_floating_point,	.ops = NULL, .cmd = "stop",	.query = "stop?", },
	[fsi_lp_speed]			= { .id = fsi_lp_speed,			.type = fsi_lpt_floating_point,	.ops = NULL, .cmd = "spd",	.query = "spd?", },
	[fsi_lp_mode]			= { .id = fsi_lp_mode,			.type = fsi_lpt_integer,	.ops = NULL, .cmd = "mode",	.query = "mode?", },
	[fsi_lp_dwell_time]		= { .id = fsi_lp_dwell_time,		.type = fsi_lpt_integer,	.ops = NULL, .cmd = "dwl",	.query = "dwl?", },
	[fsi_lp_cycles]			= { .id = fsi_lp_cycles,		.type = fsi_lpt_integer,	.ops = NULL, .cmd = "num",	.query = "num?", },
	[fsi_lp_ext_temp]		= { .id = fsi_lp_ext_temp,		.type = fsi_lpt_floating_point,	.ops = NULL, .cmd = NULL,	.query = "tmpe?", },
	[fsi_lp_diode_temp]		= { .id = fsi_lp_diode_temp,		.type = fsi_lpt_floating_point,	.ops = NULL, .cmd = NULL,	.query = "tmp?", },
	[fsi_lp_ophours]		= { .id = fsi_lp_ophours,		.type = fsi_lpt_floating_point,	.ops = NULL, .cmd = NULL,	.query = "ophours?", },
	[fsi_lp_cnt]			= { .id = fsi_lp_cnt,			.type = fsi_lpt_integer,	.ops = NULL, .cmd = NULL,	.query = "cnt?", },
	[fsi_lp_trigger_pol]		= { .id = fsi_lp_trigger_pol,		.type = fsi_lpt_integer,	.ops = NULL, .cmd = "trpol",	.query = "trpol?", },
	[fsi_lp_errors]			= { .id = fsi_lp_errors,		.type = fsi_lpt_string, 	.ops = NULL, .cmd = NULL,	.query = "err?", },
	[fsi_lp_operation_complete]	= { .id = fsi_lp_operation_complete,	.type = fsi_lpt_integer,	.ops = NULL, .cmd = NULL,	.query = "opc?", },
	[fsi_lp_scan]			= { .id = fsi_lp_scan,			.type = fsi_lpt_command,	.ops = NULL, .cmd = "scan",	.query = NULL, },
	[fsi_lp_abort]			= { .id = fsi_lp_abort,			.type = fsi_lpt_command,	.ops = NULL, .cmd = "abort",	.query = NULL, },
	[fsi_lp_reset]			= { .id = fsi_lp_reset,			.type = fsi_lpt_command,	.ops = NULL, .cmd = "reset",	.query = NULL, },
	[fsi_lp_reboot]			= { .id = fsi_lp_reboot,		.type = fsi_lpt_command,	.ops = NULL, .cmd = "reboot",	.query = NULL, },
};

/*
 * static int laser_properties_no = sizeof(laser_properties) / sizeof(laser_properties[0]);
 */

/** open a serial device in raw mode with variable params
 *
 *  A poor man's substitute of pyserial's Serial constructor.
 *  FIXME: this is a very basic, blocking interface to the serial
 *  device. We **do** need to implement at least a timed-out version.
 *
 *  Returns a serial device file descriptor, or -1 in case of failure;
 *  sets errno.
 */
static int fsi_serial_open(const char *serial_port)
{
	int fd;
	struct termios tty;
	int err;

	errno = 0;
	if ((fd = open(serial_port, O_RDWR)) < 0)
		goto err;

	/* set comm parameters of laser: raw, 115200 8N1 */
	err = tcgetattr(fd, &tty);
	if (err < 0) goto err;
	cfmakeraw(&tty);	
	err = cfsetspeed(&tty, B115200);
	if (err < 0) goto err;
	tty.c_cflag &= ~(PARENB | CSIZE | CSTOPB);
	tty.c_cflag |= CS8;
	err = tcsetattr(fd, TCSANOW, &tty);
	if (err < 0) goto err;

	return fd;
err:
	return -1;
}

/** initialize communication context with the TLM-8700 laser
 *
 *  In case of success, a valid pointer to an opaque laser context is
 *  returned. In case of error, NULL is returned and errno is set
 *  accordingly.
 */
struct fsi_laser *fsi_laser_init(const char *serial_port)
{
	int fd;
	struct fsi_laser *ctx;
	
	if ((fd = fsi_serial_open(serial_port)) < 0)
		goto err;

	if ((ctx = malloc(sizeof(*ctx))) == NULL)
		goto err;

	/* initialize laser context descriptor */
	strncpy(ctx->serial_port,  serial_port, sizeof(ctx->serial_port));
	ctx->fd = fd;
	memset(&ctx->status, 0, sizeof(ctx->status));
	memset(&ctx->expert_status, 0, sizeof(ctx->expert_status));

	return ctx;
err:
	return NULL;
}

static int get_prop(struct fsi_laser *h, char *query,
	enum fsi_laser_property_type type, void *value);
static int get_prop_by_idx(struct fsi_laser *h,
	enum fsi_laser_property_idx index, void *value);
int fsi_laser_get_status(struct fsi_laser *h, struct fsi_laser_status *st)
{
	int lock;

	gettimeofday(&st->timestamp, NULL);

	get_prop_by_idx(h, fsi_lp_key_lock, &lock);
	st->key_locked = lock & 2;
	st->soft_locked = lock & 1;

	get_prop_by_idx(h, fsi_lp_state, &st->power_on);
	get_prop_by_idx(h, fsi_lp_power, &st->power_status);
	get_prop_by_idx(h, fsi_lp_start, &st->sweep_start);
	get_prop_by_idx(h, fsi_lp_stop, &st->sweep_stop);
	get_prop_by_idx(h, fsi_lp_speed, &st->sweep_speed);
	get_prop_by_idx(h, fsi_lp_diode_temp, &st->temperature);

	fsi_laser_get_diagnostic_info(h, &st->diagnostic_info);

	return 0;
}

int fsi_laser_get_expert_status(struct fsi_laser *h, struct fsi_laser_expert_status *st)
{
	gettimeofday(&st->timestamp, NULL);
	strncpy(st->serial_device, h->serial_port, sizeof(st->serial_device));
	get_prop_by_idx(h, fsi_lp_device_id, &st->device_id);
	get_prop_by_idx(h, fsi_lp_pmax, &st->power_max);
	get_prop_by_idx(h, fsi_lp_pmin, &st->power_min);
	get_prop_by_idx(h, fsi_lp_wmax, &st->wavelength_max);
	get_prop_by_idx(h, fsi_lp_wmin, &st->wavelength_min);
	get_prop_by_idx(h, fsi_lp_ophours, &st->operation_hours);
	get_prop_by_idx(h, fsi_lp_mode, &st->mode);
	get_prop_by_idx(h, fsi_lp_modulation_source, &st->modulation_source);
	get_prop_by_idx(h, fsi_lp_trigger_pol, &st->trigger_polarity);
	get_prop_by_idx(h, fsi_lp_cycles, &st->cycles);
	get_prop_by_idx(h, fsi_lp_cnt, &st->remaining_cycle_cnt);
	get_prop_by_idx(h, fsi_lp_dwell_time, &st->dwell_time);
	get_prop_by_idx(h, fsi_lp_operation_complete, &st->operation_complete);

	return 0;
}

/** detroy current laser communication context
 *
 *  returns 0 on success, -1 on error, setting errno.
 */
int fsi_laser_free(struct fsi_laser *h)
{
	int err;

	errno = 0;
	err = close(h->fd);
	free(h);

	return err;
}

static int do_command(struct fsi_laser *h, char *command);

/** start a sweep scan of the laser
 */
int fsi_laser_scan(struct fsi_laser *h)
{
	int is_laser_on;

	/*
	 * WARNING: It turns out that the TLM-8700, when state is off, performs
	 * the 'scan' command without error at any level. We are then forced to
	 * check the 'on' state before scanning, or signal failure otherwise
	*/
	get_prop_by_idx(h, fsi_lp_state, &is_laser_on);
	if (!is_laser_on)
		return FSI_LASER_CMD_ERR;
	return do_command(h, "scan");
}

int fsi_laser_abort(struct fsi_laser *h)
{
	return do_command(h, "abort");
}

int fsi_laser_reset(struct fsi_laser *h)
{
	return do_command(h, "rst");
}

int fsi_laser_reboot(struct fsi_laser *h)
{
	return do_command(h, "reboot");
}

static int lock(struct fsi_laser *h)
{
	return flock(h->fd, LOCK_EX);
}

static int unlock(struct fsi_laser *h)
{
	return flock(h->fd, LOCK_UN);
}

int fsi_laser_exec_raw_command(struct fsi_laser *h, char *cmd, char *answer)
{
	/* FIXME: calls to this function are not error-checked in the
	 * rest of the code
	 */
	char command[FSI_LASER_LINE_LENGTH];
	int len = strlen(cmd);
	int err = 0;
	char *ptr;

	if (len + 2 > FSI_LASER_LINE_LENGTH)
		return FSI_LASER_CMD_TOO_LONG;
	strncpy(command, cmd, sizeof(command));
	strcat(command, "\n");

	if (lock(h) < 0)
		return FSI_LASER_WRITE_ERR;
	err = write(h->fd, command, len+1);
	if (err < 0) {
		err = FSI_LASER_WRITE_ERR;
		goto error_exit;
	} else if (err < len+1) {
		err = FSI_LASER_WRITE_TRUNC;
		goto error_exit;
	}

	ptr = answer;
	while (1) {
		err = read(h->fd, ptr, sizeof(answer));
		if (err < 0) {
			err = FSI_LASER_READ_ERR;
			goto error_exit;
		} else if (err == 0) {
			err = FSI_LASER_MAX_LENGTH;
			goto error_exit;
		}
		ptr += err;
		if (ptr[-2] == '\n' && ptr[-1] == '\r') {
			ptr[-2] = 0;
			goto success;
		}
	}
success:
	err = 0;
error_exit:
	unlock(h);
	return err;
}

int fsi_laser_get_diagnostic_info(struct fsi_laser *h, struct fsi_laser_errs *errs)
{
	char errcodes[FSI_LASER_MAX_LENGTH];
	char *tok;
	int i;

	gettimeofday(&errs->timestamp, NULL);
	get_prop_by_idx(h, fsi_lp_errors, errcodes);
	tok = strtok(errcodes, ";");
	i = 0;
	while (tok != NULL) {
		int errnum;
		errnum = strtol(tok, NULL, 10);
		errs->diagnostics[i++] = fsi_laser_decode_error(errnum);
		tok = strtok(NULL, ";");
	}
	errs->nerrs = i;

	if ((errs->nerrs == 1) && (strcmp(errs->diagnostics[0], "no error has occurred") == 0))  {
		errs->nerrs = 0;
		errs->diagnostics[0] = "";
	}

	return 0;
}

static int set_prop(struct fsi_laser *h, char *varname,
	enum fsi_laser_property_type type, void *value)
{
	char cmd[FSI_LASER_LINE_LENGTH];
	char ans[FSI_LASER_LINE_LENGTH];
	int written;
	union fsi_laser_property_val *vp = value;

	switch (type) {
	case fsi_lpt_string:
		written = snprintf(cmd, sizeof(cmd), "%s %s", varname, vp->string);
		break;
	case fsi_lpt_floating_point: {
		written = snprintf(cmd, sizeof(cmd), "%s %.2f", varname, vp->floating_point);
		break;
	}
	case fsi_lpt_integer:
		written = snprintf(cmd, sizeof(cmd), "%s %d", varname, vp->integer);
		break;
	case fsi_lpt_boolean:
		written = snprintf(cmd, sizeof(cmd), "%s %d", varname, !!vp->integer);
		break;
	default:
		return FSI_LASER_BAD_PROPERTY;
		break;
	}
	if (written < 0)
		return FSI_LASER_BAD_CONV;
	else if (written >= sizeof(cmd))
		return FSI_LASER_CMD_TOO_LONG;
	fsi_laser_exec_raw_command(h, cmd, ans);
	if (strncmp(ans, ACK, sizeof(ans)) == 0)
		return 0;
	if (strncmp(ans, CMD_ERR, sizeof(ans)) == 0)
		return FSI_LASER_CMD_ERR;
	if (strncmp(ans, EXE_ERR, sizeof(ans)) == 0)
		return FSI_LASER_EXE_ERR;
	else
		return 0;
}

static int set_int(struct fsi_laser *h, char *varname, int value)
{
	int tmp = value;

	return set_prop(h, varname, fsi_lpt_integer, &tmp);
}

static int set_float(struct fsi_laser *h, char *varname, float value)
{
	double tmp = value;

	return set_prop(h, varname, fsi_lpt_floating_point, &tmp);
}

static int get_prop_by_idx(struct fsi_laser *h,
	enum fsi_laser_property_idx index, void *value)
{
	return get_prop(h,
		laser_properties[index].query,
		laser_properties[index].type, value);
}
static int get_prop(struct fsi_laser *h, char *query,
	enum fsi_laser_property_type type, void *value)
{
	char ans[FSI_LASER_LINE_LENGTH];
	char *endptr;
	int err;

	err = fsi_laser_exec_raw_command(h, query, ans);
	if (err < 0)
		return err;
	if (strncmp(ans, CMD_ERR, sizeof(ans)) == 0)
		return FSI_LASER_CMD_ERR;
	if (strncmp(ans, EXE_ERR, sizeof(ans)) == 0)
		return FSI_LASER_EXE_ERR;

	switch (type) {
	case fsi_lpt_string:
		strncpy(value, ans, FSI_LASER_MAX_LENGTH);
		break;
	case fsi_lpt_floating_point: {
		double *res = value;
		*res = strtod(ans, &endptr);
		if (endptr == ans)
			return FSI_LASER_BAD_FLOAT;
		break;
	}
	case fsi_lpt_integer: {
		int *res = value;
		*res = strtol(ans, &endptr, 10);
		if (endptr == ans)
			return FSI_LASER_BAD_INT;
		break;
	}
	case fsi_lpt_boolean: {
		int *res = value;
		*res = strtol(ans, &endptr, 10);
		if (endptr == ans)
			return FSI_LASER_BAD_BOOL;
		if (!!*res != *res)
			return FSI_LASER_BAD_BOOL;
		break;
	}
	default:
		return FSI_LASER_BAD_PROPERTY;
		break;
	}
	return 0;
}

static int do_command(struct fsi_laser *h, char *command)
{
	char ans[FSI_LASER_LINE_LENGTH];

	fsi_laser_exec_raw_command(h, command, ans);
	if (strncmp(ans, ACK, sizeof(ans)) == 0)
		return 0;
	if (strncmp(ans, ARG_ERR, sizeof(ans)) == 0)
		return FSI_LASER_ARG_ERR;
	if (strncmp(ans, CMD_ERR, sizeof(ans)) == 0)
		return FSI_LASER_CMD_ERR;
	if (strncmp(ans, EXE_ERR, sizeof(ans)) == 0)
		return FSI_LASER_EXE_ERR;
	return FSI_LASER_UNKN_FAIL;
}

struct props {
	char	*query;
	char	*name;
	enum fsi_laser_property_type	type;
};

static struct props props[] = {
	{ "pmax?", "max power", 		fsi_lpt_floating_point, },
	{ "pmin?", "min power", 		fsi_lpt_floating_point, },
	{ "wmax?", "max wavelength", 		fsi_lpt_floating_point, },
	{ "wmin?", "min wavelength", 		fsi_lpt_floating_point, },
	{ "pwr?",  "power", 			fsi_lpt_floating_point, },
	{ "str?",  "start", 			fsi_lpt_floating_point, },
	{ "stop?", "stop", 			fsi_lpt_floating_point, },
	{ "spd?",  "speed", 			fsi_lpt_floating_point, },
	{ "tmpe?", "ext_temp", 			fsi_lpt_floating_point, },
	{ "tmp?",  "diode_temp", 		fsi_lpt_floating_point, },
	{ "ophours?", "ophours",   		fsi_lpt_floating_point, },
	{ "int?", 	"interlock state",	fsi_lpt_integer, },
	{ "pwru?", 	"power unit",		fsi_lpt_integer, },
	{ "unit?", 	"domain",		fsi_lpt_integer, },
	{ "sms?", 	"modulation source",	fsi_lpt_integer, },
	{ "mode?", 	"sweep mode",		fsi_lpt_integer, },
	{ "dwl?", 	"dwell time",		fsi_lpt_integer, },
	{ "num?", 	"cycles",		fsi_lpt_integer, },
	{ "cnt?", 	"count",		fsi_lpt_integer, },
	{ "trpol?", 	"trigger polarity",	fsi_lpt_integer, },
	{ "opc?", 	"operation complete",	fsi_lpt_integer, },
	{ "laz?", 	"laser power on",	fsi_lpt_boolean, },
	{ "id?", 	"device id",		fsi_lpt_string, },
	{ "err?", 	"error codes",		fsi_lpt_string, },
	{ "laz?", 	"laser power on",	fsi_lpt_boolean, },
};
static int nprops = sizeof(props) / sizeof(props[0]);

int fsi_laser_configure(struct fsi_laser *h, struct fsi_laser_configuration *cfg)
{
	int err;

	err = set_int(h, "laz", !!cfg->power_on) ||
		set_float(h, "pwr", cfg->power_request) ||
		set_float(h, "str", cfg->sweep_start) ||
		set_float(h, "stop", cfg->sweep_stop) ||
		set_int(h, "spd", floor(cfg->sweep_speed+0.5));

	return err;
}

static int laser_default_config(struct fsi_laser *h)
{
	int err;
	struct fsi_laser_configuration usual_config;

	usual_config.power_on = 1;
	usual_config.power_request = 3.0;
	usual_config.sweep_start = 1520.0;
	usual_config.sweep_stop = 1570.0;
	usual_config.sweep_speed = 2000.0;

	err =	set_int(h, "unit", 0) ||
		set_int(h, "pwru", 1) ||
		set_int(h, "mode", 2) ||
		set_int(h, "sms", 0) ||
		set_int(h, "trpol", 1) ||
		set_int(h, "num", 1) ||
		set_int(h, "dwl", 0) ||
		set_int(h, "int", 0);

	err = err || fsi_laser_configure(h, &usual_config);

	return err;
}

static void print_diags(struct fsi_laser_status *st)
{
	struct fsi_laser_errs *e = &st->diagnostic_info;
	int i;

	printf("laser diags [%ld.%06ld]:\n",
		e->timestamp.tv_sec, e->timestamp.tv_usec);
	for (i = 0; i < e->nerrs; i++)
		printf("\t%s\n", e->diagnostics[i]);
}

static void print_status(struct fsi_laser_status *st)
{
	printf("%s: %d\n", "key_locked", st->key_locked);
	printf("%s: %d\n", "soft_locked", st->soft_locked);
	printf("%s: %d\n", "power_on", st->power_on);
	printf("%s: %.2f\n", "power", st->power_status);
	printf("%s: %.2f\n", "sweep_start", st->sweep_start);
	printf("%s: %.2f\n", "sweep_stop", st->sweep_stop);
	printf("%s: %.2f\n", "sweep_speed", st->sweep_speed);
	printf("%s: %.2f\n", "temperature", st->temperature);
	printf("timestamp: %ld.%06ld\n",
			st->timestamp.tv_sec,
			st->timestamp.tv_usec);
}

static void print_expert_status(struct fsi_laser_expert_status *st)
{
	printf("%s: %s\n", "serial_device", st->serial_device);
	printf("%s: %s\n", "device_id", st->device_id);
	printf("%s: %.2f\n", "power_max", st->power_max);
	printf("%s: %.2f\n", "power_min", st->power_min);
	printf("%s: %.2f\n", "wavelength_max", st->wavelength_max);
	printf("%s: %.2f\n", "wavelength_min", st->wavelength_min);
	printf("%s: %.2f\n", "operation_hours", st->operation_hours);
	printf("%s: %d\n", "mode", st->mode);
	printf("%s: %d\n", "modulation_source", st->modulation_source);
	printf("%s: %d\n", "trigger_polarity", st->trigger_polarity);
	printf("%s: %d\n", "cycles", st->cycles);
	printf("%s: %d\n", "remaining_cycle_cnt", st->remaining_cycle_cnt);
	printf("%s: %d\n", "dwell_time", st->dwell_time);
	printf("%s: %d\n", "operation_complete", st->operation_complete);
	printf("timestamp: %ld.%06ld\n",
			st->timestamp.tv_sec,
			st->timestamp.tv_usec);
}

int fsi_laser_main(int argc, char *argv[])
{
	struct fsi_laser *handle = fsi_laser_init("/dev/serial/by-id/laser");
	const char *const fsi_decode_error_number(int err);

	int err;
	char answer[256];
	int power_on;

	err = laser_default_config(handle);
	fprintf(stderr, "default configuration errcode: %d\n", err);

	err = fsi_laser_exec_raw_command(handle, "laz 0", answer);
	printf("ans[%s], %d (=%s)\n", answer, err, fsi_decode_error_number(err));
	err = get_prop_by_idx(handle, fsi_lp_state, &power_on);
	printf("ans[%s], %d (=%s) power_on = %d\n", answer, err, fsi_decode_error_number(err), power_on);
	err = fsi_laser_scan(handle);
	printf("ans[%s], %d (=%s)\n", answer, err, fsi_decode_error_number(err));

	return 0;
	while (0) {
		int len;
		char command[FSI_LASER_LINE_LENGTH];
		char answer[FSI_LASER_LINE_LENGTH];
		struct fsi_laser_status st, *stp = &st;
		struct fsi_laser_expert_status est, *estp = &est;

		int i;
		for (i = 0; i < nprops; i++) {
			struct props *p = &props[i];
			union fsi_laser_property_val v, *vp = &v;

			err = get_prop(handle, p->query, p->type, vp);

			if (err != 0)
				fprintf(stderr, "err %d: could not get %s\n", err, p->query);

			switch (p->type) {
			case fsi_lpt_string:
				printf("%-10s = %s\n", p->name, vp->string);
				break;
			case fsi_lpt_floating_point:
				printf("%-10s = %.2f\n", p->name, vp->floating_point);
				break;
			case fsi_lpt_integer:
			case fsi_lpt_boolean:
				printf("%-10s = %d\n", p->name, vp->integer);
				break;
			default:
				fprintf(stderr, "bad property type %d\n", p->type);
				break;
			}
		}

		err = fsi_laser_get_status(handle, stp);
		print_status(stp);
		print_diags(stp);
		err = fsi_laser_get_expert_status(handle, estp);
		print_expert_status(estp);
		fgets(command, sizeof(command), stdin);
		len = strlen(command);
		if (command[len-1] == '\n')
			command[len-1] = 0;
		fsi_laser_exec_raw_command(handle, command, answer);
		printf("%s\n", answer);
	}

	return 0;
}
