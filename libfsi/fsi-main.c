/*
 * Copyright CERN 2023
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <unistd.h>
#include <stdio.h>

#include "fsi.h"
#include "fsi-laser.h"
#include "fsi-edfa.h"

/* gas cell has no parameters, only its identity */
struct fsi_channel_config gas_cell = {
	.channel_id	= 0,
	.type		= FSI_CH_GAS_CELL,
	/* don't care */
};

/* reference interferometer has no parameters, only its identity */
struct fsi_channel_config reference = {
	.channel_id	= 1,
	.type		= FSI_CH_INTERF,
	/* don't care */
};

/* HLS sensor typically has a single reflection peak */
struct fsi_channel_config hls1 = {
	.channel_id	= 4,
	.type		= FSI_CH_DISTANCE,
	.npeaks		= 1,
	.channel_gain	= -1,
	.disabled	= 0,
	.peaks		= {
		[0] = {
		.position	= 75900,
		.range1		= 2000,
		.range2		= 2000,
		.type		= FSI_PEAK_GAUSS,
		},
	},
};

/* ball sensor typically has a single reflection peak */
struct fsi_channel_config ball = {
	.channel_id	= 26,
	.type		= FSI_CH_DISTANCE,
	.npeaks		= 1,
	.channel_gain	= -1,
	.disabled	= 0,
	.peaks		= {
		[0] = {
		.position	= 221325,
		.range1		= 5000,
		.range2		= 5000,
		.type		= FSI_PEAK_GAUSS,
		},
	},
};

/* testbed sensors may have a number of reflector peaks; this example
 * exhibits six. Peak ranges can vary, but conservative values have been
 * passed here.
 */
struct fsi_channel_config t4 = {
	.channel_id	= 10,
	.type		= FSI_CH_DISTANCE,
	.npeaks		= 6,
	.channel_gain	= -1,
	.disabled	= 0,
	.peaks		= {
		[0] = {
		.position	= 466040,
		.range1		= 4000,		/* make ranges +- or up down */
		.range2		= 4000,
		.type		= FSI_PEAK_GAUSS,
		},
		[1] = {
		.position	= 478880,
		.range1		= 4000,
		.range2		= 4000,
		.type		= FSI_PEAK_GAUSS,
		},
		[2] = {
		.position	= 518680,
		.range1		= 4000,
		.range2		= 4000,
		.type		= FSI_PEAK_GAUSS,
		},
		[3] = {
		.position	= 781440,
		.range1		= 4000,
		.range2		= 4000,
		.type		= FSI_PEAK_GAUSS,
		},
		[4] = {
		.position	= 926000,
		.range1		= 4000,
		.range2		= 4000,
		.type		= FSI_PEAK_GAUSS,
		},
		[5] = {
		.position	= 983440,
		.range1		= 4000,
		.range2		= 4000,
		.type		= FSI_PEAK_GAUSS,
		},
	},
};

struct fsi_channel_config *channel_configs[] = {
	&gas_cell, &reference, &hls1, &ball, &t4,
};

#include "generated-channel-configs.h"
int nchannel_configs = sizeof(generated_channel_configs) / sizeof(generated_channel_configs[0]);

void display_cc(struct fsi_channel_config *cc)
{
	const char *typ;
	int peak;

	printf("channel_id = %d ", cc->channel_id);
	typ = fsi_decode_channel_type(cc->type);
	printf("type = %d (%s)\n", cc->type, typ ? typ : "NULL");
	if (cc->type != FSI_CH_DISTANCE)
		return;
	for (peak = 0; peak < cc->npeaks; peak++) {
		struct fsi_peak_config *p = &cc->peaks[peak];
		printf("    %s peak#%d at %.4f (-%.4f, +%.4f)\n",
			typ, peak, p->position, p->range1, p->range2);
	}
}

void display_meas(struct fsi_channel_measurement *m)
{
	const char *typ;
	int i;

	printf("----- channel# %03d:", m->channel_id);
	typ = fsi_decode_channel_type(m->type);
	printf("type = %d (%s)\n", m->type, typ ? typ : "NULL");
	for (i = 0; i < m->npeaks; i++) {
		struct fsi_peak_result *peak = &m->peaks[i];
		if (!peak->valid)
			continue;
		printf("    peak# %d: %12.7f m +-%8.2f um Hz, snr = %4.1f dB, stderr = %4.1f um, "
			    "amp: %8.3g, type = %2d (%s)\n",
				peak->peak_id, peak->center, 1.0e6 * peak->width,
				peak->snr, peak->fit_params.stderr * 1.0e6, peak->fit_params.amplitude,
				peak->type, fsi_decode_peak_type(peak->type));
	}
}

void display_full_sample(struct fsi_full_measurement *measp)
{
	int i;
	double mean(uint16_t *p, long n);
	static double dst[2500000];

	printf("ch#%03d t%d m%d [%7d]: [%6.2d %6.2d %6.2d %6.2d %6.2d ... %6.2d] avg %6.2f\n",
		measp->channel_id,
		measp->type,
		measp->mode,
		measp->nsamples,
		measp->data[0],
		measp->data[1],
		measp->data[2],
		measp->data[3],
		measp->data[4],
		measp->data[measp->nsamples-1],
		mean(measp->data, measp->nsamples));
	printf("start: %g; delta: %g\n", measp->start, measp->delta);
	if (0) {		/* only debugging scaffolding */
		FILE *f;
		f = fopen("raw_channel", "w");
		for (i = 0; i < measp->nsamples; i++)
			fprintf(f, "%d\n", measp->data[i]);
		fclose(f);
		fsi_log_to_natural(dst, measp->data, measp->nsamples);
		f = fopen("log_channel", "w");
		for (i = 0; i < measp->nsamples; i++)
			fprintf(f, "%g\n", dst[i]);
	}
}

void display_qa(struct fsi_qa_params *qa)
{
	printf("ch#%03d ref: center %8.4f MHz, fwhm %6.2f Hz, fw95m %6.2f Hz\n",
		qa->ref[0].ref_id,
		qa->ref[0].ref_center / 1.0e6,
		qa->ref[0].ref_deviation,
		qa->ref[0].ref_deviation95);
	printf("ch#%03d gc:  alpha %11.6f THz/s, rms err %8.3f MHz/s\n",
		qa->gc[0].gc_id,
		qa->gc[0].gc_alpha / 1.0e12,
		qa->gc[0].gc_fitting_err / 1.0e6);
}

struct fsi_laser_configuration lc = {
	.power_on	= 1,
	.power_request	= 3.0,
	.sweep_start	= 1520.0,
	.sweep_stop	= 1570.0,
	.sweep_speed	= 2000.0,
};
struct fsi_edfa_configuration ec = {
	.edfa_on	= 1,
	.power_request	= 470,		/* 14 dBm = 25.1mW */
};

/** wait for python code to signal the end of transfer
 *
 *  \param timeout
 *  	timeout in ms
 *  FIXME: for the moment, a no-op
 */
int wait_for_diot(int timeout)
{
	return 0;
}

/** wait for python code to signal the end of computation
 *  FIXME: for the moment, a no-op
 *  \param timeout
 *  	timeout in ms
 */
int wait_for_numericals(int timeout)
{
	return 0;
}

int main(int argc, char *argv[])
{
	FSI *ifm;
	struct fsi_laser *laser;
	struct fsi_edfa *edfa;
	struct fsi_channel_config cc;

	int i, acq, nacq = 1;

	/* take number of acquisitions */
	if (argc > 1)
		nacq = strtol(argv[1], NULL, 0);

	/* initialize interferometer context */
	ifm = fsi_lib_init(NULL);
	if (ifm == NULL) {
		perror("could not init FSI lib, exiting");
		return -1;
	}

	/* configure peripherals; most of the time, this sets default
	 * configuration values that are rarely changed, and are always
	 * passed via structures like the above */
	laser = fsi_get_laser(ifm);		/* laser_init need not be called */
	edfa = fsi_get_edfa(ifm, 0);		/* edfa_init  need not be called */
	fsi_laser_configure(laser, &lc);	/* default values for most time 
						   of apparatus operation */
	fsi_edfa_configure(edfa, &ec);		/* ditto */

	/* channel configuration is taken here from static structs
	 * defined in a separate file, and displayed for verification
	 */
	for (i = 0; i < nchannel_configs; i++)
		fsi_channel_configure(ifm, &generated_channel_configs[i]);

	for (i = 0; i < nchannel_configs; i++) {
		fsi_get_channel_configuration(ifm, generated_channel_configs[i].channel_id, &cc);
		display_cc(&cc);
	}

	/* now perform some regular cycles of acq/measurement */
	for (acq = 0; acq < nacq; acq++) {
		const int FSI_NCHANNELS = 256;	/* FIXME: move into private */
		int err = 0, nchannels = 0, ch;
		struct fsi_channel_measurement meas[FSI_NCHANNELS];
		//const int t5_channel = 54;	/* FIXME: depends on ISR setting */
		//const int hls2_channel = 39;	/* FIXME: depends on SCT setting */
		const int lon1_channel = 51;	/* FIXME: depends on SCT setting */
		//const int ref_channel  = 33;	/* FIXME: depends on SCT setting */
		//const int gas_channel  = 32;	/* FIXME: depends on SCT setting */
		struct fsi_full_measurement full;
		struct fsi_qa_params qa;

		/* e.g. choose a T5 channel to display spectrum */
		// fsi_set_scope_mode(ifm, t5_channel, FSI_EXPERT_LIN_FFT);
		/* e.g. choose the HLS2 channel to display spectrum */
		if (acq % 2 == 0)
			fsi_set_scope_mode(ifm, lon1_channel, FSI_EXPERT_LIN_FFT_M);
		else
			fsi_set_scope_mode(ifm, lon1_channel, FSI_EXPERT_NONE);

		/* now do something insane: scan when laser OFF */
		if (0) {
			char ans[256];
			fsi_laser_exec_raw_command(laser, "laz 0", ans);
		}
		err = fsi_scan(ifm);
		printf("------- scan #%d, err: %d (%s) -------\n", acq, err,
			fsi_decode_error_number(err));
		err = fsi_receive_measurements(ifm, meas, &nchannels);
		printf("------- meas #%d, err: %d (%s) -------\n", acq, err,
			fsi_decode_error_number(err));
		for (ch = 0; ch < nchannels; ch++) {
			display_meas(&meas[ch]);
		}

		err = fsi_get_qa_params(ifm, &qa);
		printf("------- qa #%d, err: %d (%s) -------\n", acq, err,
			fsi_decode_error_number(err));
		display_qa(&qa);
		for (ch = 0; ch < nchannels; ch++) {
			display_meas(&meas[ch]);
		}
		err = fsi_get_full_samples(ifm, &full);
		printf("------- full ch#%d data err: %d\n", lon1_channel, err);
		display_full_sample(&full);
		printf("-------  -------\n");
			
	}
	fsi_lib_exit(ifm);

	return 0;
}
