/*
 * Copyright CERN 2023
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef FSI_PRIVATE_LIB_H_
#define FSI_PRIVATE_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <limits.h>
#include <stdlib.h>
#include "fsi.h"
#include "../solib/memo.h"

#define	FSI_SERVER_DEFAULT_SHM	"fsi"
#define	FSI_SERVER_SHM_ENVIRON	"FSI_SERVER_SHMEM"

#define	FSI_SERVER_SOCKET	"/tmp/.fsi_engine.s"
#define	FSI_SERVER_GREET	"hello fsi server"
#define	FSI_SERVER_ANS		"hello libfsi"
#define	FSI_MAX_MSG_LEN		256

/* "constants of nature" internal to interferometer */
#define FSI_NBLOCKS	4		/**< number of optical blocks per ifm */
#define FSI_NEDFAS	FSI_NBLOCKS	/**< one ifmeter -> 4 EDFAs or less */
#define FSI_NCHANNELS	256		/**< one ifmeter -> 256 channels, not all configured */
#define FSI_NGCS	2		/**< one ifmeter -> two GCs, primary and backup */
#define FSI_NREFS	FSI_NBLOCKS	/**< one ifmeter -> one ref channel per optical block */

/* configurations --- static for the current single interferometer, to
 * be drawn from a config file for flexibility
 */
#define PORT_LASER	"/dev/serial/by-id/laser"
#define PORT_EDFA	"/dev/serial/by-id/edfa"
#define PORT_METEO	"/dev/serial/by-id/meteo"
#define PORT_CONSOLE	"/dev/serial/by-id/console"

/* library-private data structures defining the state of the
 * interferomenter
 */

struct fsi_channel {
	unsigned int		channel_id;
	enum fsi_channel_type	type;
	int			channel_gain;
	int			dc_coupling;
	unsigned int		disabled;
	int			rpeaks;
	struct fsi_peak_config	requested_peaks[FSI_MAX_PEAKS];

	/* measurement results */
	struct timeval		timestamp;
	int			mpeaks;
	struct fsi_peak_result	measured_peaks[FSI_MAX_PEAKS];
	struct fsi_hw_status	hw_status;
	struct fsi_full_measurement
				*full;	/* NULL unless requested */
};

struct fsi_diot {
	/** FIXME: this is just a provisory note, probably
	 * more/less fields need to be here */
	char			hostname[PATH_MAX];
	char			ctrl_port;
	int			sockfd;
	int32_t			reserved[64];
};

struct fsi_interferometer {
	int			nblocks;		/* number of optical blocks */
	int			nedfas;			/* number of EDFAs */
	int			nrefs;			/* number of reference chs */
	int			ngas;			/* number of GCs */
	int			nchannels;		/* number of phys channels */
	struct fsi_laser	*laser;			/* one and only one laser */
	struct fsi_edfa		*edfa[FSI_NBLOCKS];	/* nblocks EDFAs registered here */
	struct fsi_diot		*diot[FSI_NBLOCKS];	/* nblocks diot crates registered here */
	struct fsi_channel	*channel;		/* addr of array of fixed length FSI_NCHANNELS */
	struct fsi_channel	channels[FSI_NCHANNELS];
};

struct __FSI_state {
	/* physical configuration of the interferometer
	 * WARNING: assumed invariant: nblocks == nrefs == nedfas
	 * WARNING: assumed invariant: nblocks, ngas and nchannels are constants of nature
	 */
	int			nblocks;		/* actual number of optical blocks */
	int			nedfas;			/* actual number of EDFAs */
	int			nrefs;			/* actual number of reference chs */
	int			ngas;			/* actual number of GCs */
	int			nchannels;		/* actual number of phys channels */
	struct fsi_laser	*laser;			/* one and only one laser */
	struct fsi_edfa		*edfa[FSI_NBLOCKS];	/* nblocks EDFAs registered here */
	struct fsi_diot		*diot[FSI_NBLOCKS];	/* nblocks diot crates registered here */
	struct fsi_channel	*channel;		/* addr of array of fixed length FSI_NCHANNELS */
	struct fsi_channel	channels[FSI_NCHANNELS];

	/**
	 * these field store the internal state variables of the interferometer
	 * during an acquisition cycle; they would rarely be reported
	 * directly through the library API
	 */
	int			fs;
	int			n;
	int			zpf;
	char			batch[256];
	char			laser_configs[1024];
	int			gas_cell_ids[FSI_NGCS];
	int			ref_ids[FSI_NREFS];

	struct timeval		timestamp;		/**< timestamp of last valid acq cycle */
	double			alpha;			/**< computed alpha */
	double			idx_to_distance;
	double			n_air;			/**< always 1.0! */

	int			full_chid;		/**< channel id of requested full channel */
	enum fsi_expert_mode	full_mode;		/**< mode requested for full channel */

	struct fsi_qa_params	qa_params;

	/* unused/unknown */
	int			proto;
	int			sockfd;
	int			fd;
	struct config		*cfg;
};

/* sundry translation functions that return human-readable descriptions
 * of numeric values defined in the fsi.h API
 * FIXME: make public???
 */
extern char* fsi_laser_decode_error(int code);
extern const char *fsi_decode_channel_type(enum fsi_channel_type type);
extern const char *fsi_decode_peak_type(enum fsi_peak_type type);
extern const char *fsi_decode_expert_mode(enum fsi_expert_mode type);

/* implementation of a peak fitting response */
extern int simulate_peak(struct fsi_peak_config *cfg, struct fsi_peak_result *res);

/* handling of the mempool */
struct fsi_mem_pool {
	int		fd;
	void		*addr;
	unsigned long	size;
};
struct fsi_mem_pool *fsi_get_mem_pool(void);
int fsi_release_mem_pool(struct fsi_mem_pool *pool);

/* manipulation of the fsi python server control socket */
int fsi_ctrl_socket(char *path);
int fsi_wake_server(int fd, char *msg);
int fsi_wait_on_server(int fd, char *msg);
int fsi_free_ctrl_socket(int fd);

/** constants of nature for the interferometer -- needed mostly to
 * initialize configurations and to set never-changing defaults */
struct fsi_constants *fsi_constants;

#ifdef __cplusplus
}
#endif

#endif /* FSI_PRIVATE_LIB_H_ */
