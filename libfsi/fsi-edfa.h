/*
 * Copyright CERN 2023
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

/** \mainpage fsi-edfa: EDFA control functions of libfsi
 *
 * fsi-edfa exports the interface to communicate with the KeopSys
 * Erbium-Doped Fiber Amplifier CEFA M160
 *
 * FIXME:
 * To operate the device, a call to fsi_edfa_init returns an opaque
 * handle that will be used in all subsequent calls, until the device is
 * relinquished by calling \ref fsi_edfa_free. The initialization routine
 * configures silently a number of settings (e.g. control_mode,
 * autostart) that can and must be safely ignored by the user of the
 * library, because their values are fixed by design. They may, however,
 * be observed by means of a call to \ref fsi_edfa_get_expert_status.
 */

#ifndef FSI_EDFA_H_
#define FSI_EDFA_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <limits.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdint.h>

/* EDFA API status codes */
#define FSI_EDFA_OK				0			/* ! answer from serial port */
#define FSI_EDFA_BERR				-3030
#define FSI_EDFA_COMMAND_UNKNOWN		FSI_EDFA_BERR - 3	/* # answer from serial port */
#define FSI_EDFA_NOT_AUTHORIZED			FSI_EDFA_BERR - 4	/* * answer from serial port */
#define FSI_EDFA_COMMAND_NOT_VALID		FSI_EDFA_BERR - 5	/* $ answer from serial port */
#define FSI_EDFA_WRITE_ERR			FSI_EDFA_BERR - 6	/* command too long */
#define FSI_EDFA_WRITE_TRUNC			FSI_EDFA_BERR - 7	/* could not write to serial */
#define FSI_EDFA_READ_ERR			FSI_EDFA_BERR - 8	/* write incomplete */
#define FSI_EDFA_ANSWER_ERR			FSI_EDFA_BERR - 9	/* bad answer */

/** maximum length of a string returned by EDFA API functions
 */
#define	FSI_EDFA_MAX_LENGTH 	128

/**
 * set of parameters that are configured at will in normal operation.
 * Usually, we care only about the on/off status of the EDFA (i.e.,
 * whether it is actually amplifying power or not) and the power
 * setpoint we request (e.g. 25.1 mW to reach 1mW at each end of a 16x
 * splitter).
 */
struct fsi_edfa_configuration {
	int	edfa_on;			/**< activate power (=amplification) */
	double	power_request;			/**< power setpoint requested (mW) */
};

/**
 * set of parameters of interest for us in EDFA monitoring. We 
 */
struct fsi_edfa_status {
	double	input_power;			/**< measured power at EDFA input (mW) */
	double	power_request;		/**< power setpoint requested (mW) */
	double	power_status;		/**< measured power at EDFA output (mW) */
	int	edfa_on;			/**< is power (=amplification) activated? */
	int	key_locked;			/**< is EDFA key-locked? */
	int	reserved[8];
};

/**
 * this set of parameters is only of interest to the expert monitoring
 * the correct EDFA functioning; it should not be retrieved unless
 * explicitly requested to do so by an expert command. Most of them are
 * constants of the appartus and never change (e.g. serial number or
 * operational limits)
 */
struct fsi_edfa_expert_status {
	char	serial_number[20];			/* SNU?		 */
	char	description[128];			/* DES?		 */
	char	version[20];				/* VER?		 */
	char	firmware_information[128];		/* FWI?		 */

	int	control_mode;				/* ASS?/ASS=	 */

	int	autostart;				/* AST?/AST=	 */
	double	case_temperature;			/* CAT?		 */

	int	preamp_diode_current_setpoint;		/* IC1?/IC1=	 */
	int	measured_preamp_diode_current_setpoint;	/* ID1?		 */
	double	preamp_diode_temperature;		/* TD1?		 */

	int	prebooster_diode_current_setpoint;	/* IC2?/IC2=	 */
	int	measured_prebooster_diode_current_setpoint;	/* ID2?  */
	double	nominal_power;				/* PON?		 */
	double	minimum_power_setpoint;			/* POM?		 */
	double	setpoint_output_power;			/* SOP?/SOP=	 */

	double	input_power_alarm_threshold;		/* IPT1?	 */

	double	limit_current_preamp;			/* LCM1?	 */
	double	limit_current_preboost;			/* LCM2?	 */
	double	output_power_mute;			/* PWM?		 */
	int	reserved[16];
};

/** opaque handle to an EDFA status and controls */
struct fsi_edfa;

/**
 * \brief
 *	auxiliary struct to decode EDFA error mask
 *
 * \warning
 *	this is machine-dependent and extremely questionable as a way of
 *	unmasking a bit mask, but, if constrained to gcc in an x86_64
 *	architecture, it might even scale when we move to a different
 *	EDFA model, with an entirely different error mask layout.
 *	Thanks, Keopsys! */

struct fsi_edfa_alarms {
	unsigned	pad1				:2;
	unsigned	booster_temp_error		:1;	/**< bit = 2 , booster temp error" */
	unsigned	preamp_temp_error		:1;     /**< bit = 3 , preamp temp error */

	unsigned	pad2				:6;
	unsigned	booster_current_error		:1;     /**< bit = 10, Booster current error */
	unsigned	preamp_current_error		:1;     /**< bit = 11, Preamp current error */
	unsigned	low_output_power_2		:1;     /**< bit = 12, Low output power 2 */
	unsigned	low_input_power_2		:1;     /**< bit = 13, Low input power 2 */
	unsigned	low_output_power_1		:1;     /**< bit = 14, Low output power 1 */
	unsigned	low_input_power_1		:1;     /**< bit = 15, Low input power 1 */

	unsigned	pad3				:3;
	unsigned	gpio_key_off_activated		:1;     /**< bit = 19, GPIO Key OFF activated */
	unsigned	power_supply_error		:1;     /**< bit = 20, Power supply error */
	unsigned	board_temperature_error		:1;	/**< bit = 21, Board temperature error */
};


/* recommended wide API for interaction with the EDFA device
 *
 * all functions refer to an opaque handler h created/destroyed
 * by \ref * fsi_edfa_init and \ref fsi_edfa_free. Given that up to 4
 * EDFAs can live in one interferometer, the utility function
 * fsi_edfa_find_by_serial_number allows identification of EDFAs by
 * their serial number.
 *
 * Entry points are furnished to set an \ref fsi_edfa_configuration and to
 * retrieve \ref fsi_edfa_status and \ref fsi_edfa_expert_status.
 *
 * \note
 *    Both laser and EDFA can have their status reported at different
 *    levels of detail.
 *
 *    The *_get_status functions are meant to be called periodically to
 *         report states of permanent interest to the application
 *    The *_expert_status functions are meant to be called very rarely,
 *         to report states of sporadic or one-off interest to the application
 *    The fsi_laser_get_diagnostic_info reports specifically the laser
 *         errors accumulated in the device error buffer. There is a large
 *         number of conditions reported (see table below,
 *         fsi_laser_error_codes), but it is likely that all of them should
 *         be coalesced to at most three categories (OK, error, warning).
 *
 * Those aside, raw commands are accessible to switch amplification
 * on/off and to restore all configurations of the EDFA to factory
 * settings
 *
 * For experts, a raw_command routine that implements a single
 * command/response iteration of the laser serial CLI is given.
 *
 * The function \ref fsi_edfa_get_alarms allows to retrieve the latest
 * error/alarm conditions of the device. These conditions are reset by
 * the command \refe fsi_edfa_reset_alarms
 *
 */

struct fsi_edfa *fsi_edfa_init(const char *serial_port, int edfa_id);
int fsi_edfa_find_by_serial_number(char *edfa_serial_number,
					char *serial_port, int *edfa_id);
int fsi_edfa_configure(struct fsi_edfa *h, struct fsi_edfa_configuration *cfg);
int fsi_edfa_get_status(struct fsi_edfa *h, struct fsi_edfa_status *st);
int fsi_edfa_get_expert_status(struct fsi_edfa *h, struct fsi_edfa_expert_status *st);
int fsi_edfa_get_alarms(struct fsi_edfa *h, uint32_t *alarms);
int fsi_edfa_get_alarms_struct(struct fsi_edfa *h, struct fsi_edfa_alarms *alarms);
int fsi_edfa_free(struct fsi_edfa *h);

/* convenience command functions */
int fsi_edfa_factory_settings(struct fsi_edfa *h);
int fsi_edfa_power_on(struct fsi_edfa *h);
int fsi_edfa_power_off(struct fsi_edfa *h);
int fsi_edfa_reset_alarms(struct fsi_edfa *h);
int fsi_edfa_get_warnings(struct fsi_edfa *h, uint32_t *warnings);
int fsi_edfa_exec_raw_command(struct fsi_edfa *h, char *cmd, char *answer);
void fsi_decode_alarms(uint32_t alarm_mask, char *dst);
void fsi_edfa_decode_alarms_struct(uint32_t mask, struct fsi_edfa_alarms *alarms);

#ifdef __cplusplus
}
#endif

#endif /* FSI_EDFA_H_ */
