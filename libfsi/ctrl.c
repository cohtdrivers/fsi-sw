#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "fsi-private.h"

/*
 * fsi_ctrl_socket - connect to fsi server control socket
 * 
 * \param path - path to UNIX domain socket. Use /tmp/.fsi_engine.s if NULL
 *
 * \returns
 *	a fd for the control socket, or a negative value in case of error. Sets errno,
 *      with EHOSTDOWN value when server is not identified as such
 */
int fsi_ctrl_socket(char *path)
{
	int fd, err;
	struct sockaddr_un addr, *addrp = &addr;
	char buf[FSI_MAX_MSG_LEN];

	if (path == NULL)
		path = FSI_SERVER_SOCKET;
	
	fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd < 0)
		return fd;
	memset(addrp, 0, sizeof(*addrp));
	addrp->sun_family = AF_UNIX;
	strncpy(addrp->sun_path, FSI_SERVER_SOCKET, sizeof(addrp->sun_path)-1);
	err = connect(fd, (struct sockaddr *)addrp, sizeof(*addrp));
	if (err < 0)
		return err;
	send(fd, FSI_SERVER_GREET, strlen(FSI_SERVER_GREET), 0);
	err = recv(fd, buf, sizeof(buf), 0);
	if (err < 0)
		return err;
	buf[err] = 0;
	if (strcmp(buf, FSI_SERVER_ANS) != 0) {
		errno = EHOSTDOWN;
		return -1;
	}
	return fd;
}

/*
 * fsi_free_ctrl_socket - free control socket from fsi lib
 *
 * \param
 *	fd - socket fd to free
 *
 * \returns
 *	zero on sucess, -1 on error and errno is set
 */
int fsi_free_ctrl_socket(int fd)
{
	if (fd >= 0)
		return close(fd);
	return 0;
}

/*
 * fsi_wake_server - send the fsi server a sync ctrl message
 *
 * \param
 *	fd	- socket fd of the server unix domain ctrl socket
 *	msg	- message to send
 *
 * \returns
 *	zero on sucess, -1 on error and errno is set
 */
int fsi_wake_server(int fd, char *msg)
{
	return send(fd, msg, strlen(msg), 0);
}

/*
 * fsi_wait_on_server - wait until server signals end of operation
 *
 * \param
 *	fd	- socket fd of the server unix domain ctrl socket
 *	msg	- address to return message received from server - must have
 *			more than FSI_MAX_MSG_LEN bytes length
 *
 * \returns
 *	zero on sucess, -1 on error and errno is set
 */
int fsi_wait_on_server(int fd, char *msg)
{
	char ans[FSI_MAX_MSG_LEN];
	int err;

	err = recv(fd, msg, sizeof(ans)-1, 0);
	if (err > 0)
		msg[err] = 0;
	else	/* FIXME: server is dead, exit gracefully */
		;
	return err;
}

#ifdef CTRL_MAIN
int main(int argc, char *argv[])
{
	char cmd[FSI_MAX_MSG_LEN];
	int fd = -1;
	printf("cmd> ");
	while (fgets(cmd, sizeof(cmd), stdin) != NULL) {
		int len = strlen(cmd);
		cmd[len] = 0;
		if (strcmp(cmd, "c\n") == 0) {
			if ((fd = fsi_ctrl_socket(NULL)) < 0) {
				fprintf(stderr, "could not open socket\n");
				exit(1);
			}
		} else if (strcmp(cmd, "d\n") == 0) {
			fsi_free_ctrl_socket(fd);
			fd = -1;
		} else if (strcmp(cmd, "q\n") == 0) {
			goto exit;
		} else {
			if (fd < 0) {
				printf("please connect first (command 'c')\n");
			} else
				send(fd, cmd, len, 0);
		}
		printf("cmd> ");
	}
exit:
	fsi_free_ctrl_socket(fd);
}
#endif /* CTRL_MAIN */
