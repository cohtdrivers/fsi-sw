/*
 * Copyright CERN 2023
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdlib.h>
#include "fsi-private.h"

static struct fsi_laser_error_codes {
    int     code;
    char    *message;
} fsi_laser_error_codes[] = {

	{ -104,	"data received different than expected", },
	{ -108,	"more parameters were received than expected", },
	{ -109,	"fewer parameters were received than required", },
	{ -110,	"unknown command received", },
	{ -113,	"header syntactically correct, but undefined for device", },
	{ -124,	"argument length exceeds the limit", },
	{ -140,	"parser could not find any known data type", },
	{ -141,	"data contains invalid characters", },
	{ -148,	"character found in argument not permitted", },
	{ -151,	"string consists too many chars or missing 2nd double quote", },

	{ -200,	"command can not be executed due to an illegal device condition", },
	{ -221,	"command can not be executed as settings conflict", },
	{ -222,	"command can not be executed as the argument was outside the range", },
	{ -310,	"error occurred while performing system preset", },
	{ -320,	"fault detected while writing to EEPROM", },

  	{    0, "no error has occurred", },
  	{    1, "laser over power occurred", },
  	{    2, "laser over current occurred", },
 	{   20, "temperature regulation not achieved before time-out", },
 	{   21, "temperature regulation not achieved after dropping out of regulation", },
	{ 200,	"FPGA initialization failure", },
	{ 201,	"power on self test (POST) failure", },
	{ 204,	"RTOS task initialization failure", },
	{ 220,	"POST: module operating temperature outside of limits", },
	{ 222,	"POST: voice coil motor (VCM) power test failure", },
	{ 223,	"POST: VCM offset calibration failure", },
	{ 224,	"POST: encoder initialization failure", },
	{ 225,	"POST: VCM polarity fault", },
	{ 226,	"POST: VCM travel limits not successfully found", },
	{ 227,	"POST: thermo-electric cooler (TEC) driver initialization failure", },
	{ 228,	"POST: analog circuit initialization failure", },
	{ 229,	"POST: over power test failure", },
	{ 230,	"POST: I2C communication failure", },
	{ 231,	"POST: Fan tachometer outside limits", },

	{ 232,	"POST: VCM Power, enabling -12V FPGA detects overvoltage", },
	{ 233,	"POST: VCM Power, enabling -12V FPGA detects duty limit", },
	{ 234,	"POST: VCM Power, VCMP greater than 50mV off ground", },
	{ 235,	"POST: VCM Power, VCMI offset exceeds limits", },
	{ 236,	"POST: VCM Power, VCM boost converter out-of-range", },
	{ 237,	"POST: High Speed configuration with only one operating fan", },
	{ 240,	"POST: VCM Encoder Misalignment Fault", },
	{ 241,	"POST: encoder index reset failure", },
	{ 242,	"POST: encoder index search failure", },
	{ 245,	"POST: measured TEC current suggests High Speed Assembly on Standard electronics configuration", },
	{ 247,	"POST: measured limits imply 4096x interpolation for High Speed Laser Assembly", },
	{ 248,	"POST: measured limits imply 416x interpolation for Standard Laser Assembly", },
	{ 250,	"POST: incorrect TEC drive polarity", },
	{ 251,	"POST: low TEC current reading while heating", },
	{ 252,	"POST: low TEC current reading while cooling", },
	{ 253,	"POST: external +5V excess deltaV under load", },
	{ 254,	"POST: +VLASER outside expected limits", },
	{ 255,	"POST: FPGA OVERI protection reset error", },
	{ 256,	"POST: laser current regulation exceeds limits", },
	{ 257,	"POST: laser diode short circuit protect error", },
	{ 258,	"POST: OVERI comparator tripped unexpectedly", },
	{ 259,	"POST: OVERI comparator did not trip as expected", },
	{ 260,	"VCM torque bias first move failure", },
	{ 261,	"VCM torque bias forward move failure", },
	{ 262,	"VCM torque bias reverse move failure", },
	{ 263,	"VCM homing failure", },
	{ 264,	"POST: VCM driver diagnostic failed during positive excursion", },
	{ 265,	"POST: VCM driver diagnostic failed during negative excursion", },
	{ 266,	"POST: VCM driver diagnostic, external +12V excess deltaV under load", },
	{ 267,	"POST: VCM driver diagnostic, VCM current lower than expected for installed laser configuration", },
	{ 268,	"POST: VCM driver diagnostic, -12V supply FPGA shutdown duty cycle limit timeout", },
	{ 269,	"POST: VCM driver diagnostic, -12V supply FPGA shutdown over voltage detected", },
	{ 270,	"laser tuning initialization failure", },
	{ 271,	"laser scanning data structure initialization failure", },
	{ 272,	"laser power initialization failure", },
	{ 273,	"laser current initialization failure", },
	{ 300,	"sweep motion failure", },
	{ 301,	"internal motion failure", },
	{ 302,	"flash memory erase or write failure", },
	{ 305,	"laser temperature thermal runaway error", },

	{ 306,	"move requested when motor controller is not initialized", },
	{ 307,	"move requested when motor controller is already busy", },
	{ 312,	"Calibration Data Save Failure", },
	{ 313,	"FPGA Update Failure", },
	{ 314,	"VCM Encoder Misalignment Run-Time Fault", },
	{ 320,	"power fault: external +5V supply less than 20 percent low", },
	{ 321,	"power fault: external +12V supply less than 20 percent low", },
	{ 322,	"power fault: internal -12V supply shutdown due to output over-voltage", },
	{ 323,	"power fault: internal -12V supply shutdown due to duty cycle limit", },
	{ 324,	"power fault: internal -12V supply less than 20 percent low", },
	{ 325,	"Fan1 tachometer reading outside expected limits", },
	{ 326,	"Fan2 tachometer reading outside expected limits", },
};

static int fsi_laser_error_codes_max = sizeof(fsi_laser_error_codes)/sizeof(fsi_laser_error_codes[0]);

char* fsi_laser_decode_error(int code)
{
	int i;

	for (i = 0; i < fsi_laser_error_codes_max; i++) {
		if (fsi_laser_error_codes[i].code == code)	
			return fsi_laser_error_codes[i].message;
	}
	return NULL;
}
