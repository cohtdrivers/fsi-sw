#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

#define FSI_NSAMPLES	2500000		/* magic number */
#define FSI_NCHANNELS	256		/* magic number, oversized---one bank is 32 */

struct sample_block {
	union {
		uint8_t		byte[12*4];
		uint32_t	word[12];
		struct {
			uint8_t		bcl[4][8];
			uint8_t		bch[4][4];
		};
	};
} sample_buffer[FSI_NSAMPLES];
uint16_t acq_buffer[FSI_NCHANNELS][FSI_NSAMPLES];

const char *default_test_file = "dump_new_pd_b1.bin";
/**
 * @brief
 * 	do a diot fsi memory bank descrambling
 * 
 * The src buffer contains nblocks blocks, the smp-th block packing the
 * data corresponding to the smp-th sample in the time domain. The
 * packed structure consists of 12 32-bit words, laid out in the
 * following shape: (b = board 0-3, c = channel 0-7)
 *
 * byte    |                      3|                      2|                      1|                      0|  
 * bit     |31...................24|23...................24|15...................24|7.....................0|     
 * word:00 |b0c3[ 7..4] b0c3[ 3..0]|b0c2[ 7..4] b0c2[ 3..0]|b0c1[ 7..4] b0c1[ 3..0]|b0c0[ 7..4] b0c0[ 3..0]|	low byte b0, c=0-3
 * word:01 |b0c7[ 7..4] b0c7[ 3..0]|b0c6[ 7..4] b0c6[ 3..0]|b0c5[ 7..4] b0c5[ 3..0]|b0c4[ 7..4] b0c4[ 3..0]| 	low byte b0, c=4-7
 * word:02 |b1c3[ 7..4] b1c3[ 3..0]|b1c2[ 7..4] b1c2[ 3..0]|b1c1[ 7..4] b1c1[ 3..0]|b1c0[ 7..4] b1c0[ 3..0]|	low byte b1, c=0-3
 * word:03 |b1c7[ 7..4] b1c7[ 3..0]|b1c6[ 7..4] b1c6[ 3..0]|b1c5[ 7..4] b1c5[ 3..0]|b1c4[ 7..4] b1c4[ 3..0]| 	low byte b1, c=4-7
 * word:04 |b2c3[ 7..4] b2c3[ 3..0]|b2c2[ 7..4] b2c2[ 3..0]|b2c1[ 7..4] b2c1[ 3..0]|b2c0[ 7..4] b2c0[ 3..0]|	low byte b2, c=0-3
 * word:05 |b2c7[ 7..4] b2c7[ 3..0]|b2c6[ 7..4] b2c6[ 3..0]|b2c5[ 7..4] b2c5[ 3..0]|b2c4[ 7..4] b2c4[ 3..0]| 	low byte b2, c=4-7
 * word:06 |b3c3[ 7..4] b3c3[ 3..0]|b3c2[ 7..4] b3c2[ 3..0]|b3c1[ 7..4] b3c1[ 3..0]|b3c0[ 7..4] b3c0[ 3..0]|	low byte b3, c=0-3
 * word:07 |b3c7[ 7..4] b3c7[ 3..0]|b3c6[ 7..4] b3c6[ 3..0]|b3c5[ 7..4] b3c5[ 3..0]|b3c4[ 7..4] b3c4[ 3..0]|    low byte b3, c=4-7
 * word:08 |b0c7[11..8] b0c6[11..8]|b0c5[11..8] b0c4[11..8]|b0c3[11..8] b0c2[11..8]|b0c1[11..8] b0c0[11..8]|	high nibble, b0c0-7
 * word:09 |b1c7[11..8] b1c6[11..8]|b1c5[11..8] b1c4[11..8]|b1c3[11..8] b1c2[11..8]|b1c1[11..8] b1c0[11..8]|	high nibble, b1c0-7
 * word:10 |b2c7[11..8] b2c6[11..8]|b2c5[11..8] b2c4[11..8]|b2c3[11..8] b2c2[11..8]|b2c1[11..8] b2c0[11..8]|	high nibble, b2c0-7
 * word:11 |b3c7[11..8] b3c6[11..8]|b3c5[11..8] b3c4[11..8]|b3c3[11..8] b3c2[11..8]|b3c1[11..8] b3c0[11..8]|	high nibble, b3c0-7
 */
void descramble(uint8_t *src, uint16_t *dst, size_t nblocks)
{
	int smp;
	struct sample_block *sample_buffer = (void *)src;

	for (smp = 0; smp < nblocks; smp++) {
		struct sample_block *blk = &sample_buffer[smp];
		uint16_t (*acq_buffer)[FSI_NSAMPLES] = (void *)dst;
		unsigned b, c;

		for (b = 0; b < 4; b++) {
			for (c = 0; c < 8; c++) {
				unsigned chid = (b << 3) | c;
				uint16_t *chbuf = &acq_buffer[chid][smp];
				uint16_t lo = blk->bcl[b][c];
				uint16_t hi = (blk->bch[b][c/2] >> (4*(c % 2))) & 0xf;
				*chbuf = (hi<<8) | lo;
			}
		}
	}
}

int main(int argc, char *argv[])
{
	FILE *f;
	size_t nblocks;
	unsigned b, c;
	char *test_file;

	if (argc > 1)
		test_file = argv[1];
	if ((f = fopen(test_file, "rb")) == NULL) {
		fprintf(stderr, "cannot open .bin file %s, exiting\n", test_file);
		exit(1);
	};
	nblocks = fread(sample_buffer, sizeof(struct sample_block), FSI_NSAMPLES, f);
	assert(nblocks == FSI_NSAMPLES);
	fclose(f);

	descramble((uint8_t *)sample_buffer, (uint16_t *)acq_buffer, nblocks);

	for (b = 0; b < 4; b++) {
		for (c = 0; c < 8; c++) {
			char fname[64];
			unsigned chid = (b << 3) | c;
			size_t nw;
			sprintf(fname, "_ch%03d:b%d:c%d.u16", chid, b+1, c);
			FILE *f;
			f = fopen(fname, "wb");
			nw = fwrite(acq_buffer[chid], sizeof(uint16_t), nblocks, f);
			assert(nw == FSI_NSAMPLES);
			fclose(f);
		}
	}

}
