#!/usr/bin/env python3
# vim: ts=8 sw=4 sts=4 et ai
# coding: utf-8

import socket
import os
import time
import logging
logging.basicConfig(level=logging.DEBUG)

class ConnectionLost(Exception):
    '''signals a lost connection in comm socket.'''
    pass
class OutOfSync(Exception):
    '''signals a loss of synchronism between libfsi and fsi_server.'''
    pass

class SyncSocket:

    def __init__(self, sockfile):
        '''initialize synchronization channel with library.'''

        self.ctrlsocket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.socket_path = sockfile
        if os.path.exists(sockfile):
            # this 'leave' trick is necessary to cleanly exit from IPython,
            # see issue #13796 at https://github.com/ipython/ipython/issues/13796
            leave = False
            try:
                os.unlink(sockfile)
            except PermissionError as e:
                logging.error(f'cannot delete existing control socket {sockfile}, aborting')
                logging.error(f'please delete {sockfile} and restart!')
                leave = True
            if leave: sys.exit(1)
        self.ctrlsocket.bind(sockfile)
        self.ctrlsocket.listen()
        logging.info('server control socket {sockfile} created')
        self.wait_for_connect()

    def wait_for_connect(self):
        self.ctrlconn = None
        logging.info('waiting for ctrl socket... ')
        self.ctrlconn, _ = self.ctrlsocket.accept()
        logging.info(f'connected with {self.ctrlconn}')
        greet = self.ctrlconn.recv(256)
        logging.debug(f'received greet = {greet}')
        if greet == b'hello fsi server':
            self.ctrlconn.send(b'hello libfsi')

    def wait_on_lib(self, label=''):
        '''wait until library signals change of state.'''

        msg = self.ctrlconn.recv(256)
        if msg == b'':
            logging.error('connection lost, exiting')
            raise ConnectionLost(f'waiting on {label}')
            # this exception should lead to a re-calling
            # of wait_for_connect(), client is dead
        msg = str(msg, 'ascii')
        logging.debug(f'**** {msg} ****')
        # FIXME: this block is probably superfluous, just as the optional
	# 'label' param
        if False:
            words = msg.split()
            if words[0] != label:
                logging.error(f'out of sync! [{msg}] while waiting on [{label}]')
        return msg

    def wake_lib(self, msg):
        '''wake library waiting on server finished op.'''
        try:
            self.ctrlconn.send(bytes(msg, 'ascii'))
        except BrokenPipeError as e:
            raise ConnectionLost(f'lost connection sending {msg}')
            # this exception should lead to a re-calling
            # of wait_for_connect(), client is dead

    def mainloop(self, iterations=None):

        while True:
            ts = time.asctime()
            try:
                # lib<->server sync point A
                # wait for scan done
                msg = self.wait_on_lib('scan')
                if msg == 'scan ok':
                    self.wake_lib('ack')
                elif msg == 'scan err':
                    self.wake_lib('ack')
                    continue
                else:
                    self.wake_lib('''nack at 'scan' wait''')
                    raise OutOfSync(f'{ts}:scan')

                logging.info('acquiring...')
                logging.info('linearizing...')
                logging.info('alpha...')

                # lib<->server sync point B
                # wait for lib measurement request
                # FIXME: there is little reason to sync at this
                # point, when it might be more efficient and logical
                # to do it at the next FIXME comment
                cmd = self.wait_on_lib()
                if cmd != 'meas':
                    self.wake_lib('''nack at 'meas' wait''')
                    logging.error(f'{ts}: {cmd} received waiting for "meas"')
                    logging.error(f'resyncing')
                    raise OutOfSync(f'{ts}:meas')
                logging.info('computing')

                # lib<->server sync point C
                # signal results ready
                self.wake_lib('calc')   # or 'calc err' if failed calc
                # FIXME: should we wait for an ack here? we'll see...
                logging.info('end of loop')

            except (ConnectionLost, ConnectionResetError) as e:
                logging.error(f'{ts}: Ctrl Socket connection lost, restarting server')
                self.wait_for_connect()
                continue
            except OutOfSync as e:
                logging.error(f'resyncing: {e}')
                continue

def main():
    s = SyncSocket('/tmp/sync')
    s.mainloop()

if __name__ == '__main__':
    main()

