#include <stdbool.h>

#define FSI_EDFA_ALARM		2
#define FSI_EDFA_WARNING	1

struct fsi_edfa_alarm_data {
	int		bit;
	int		type;
	char		*description;
	char		*conditions;
};

struct fsi_edfa_alarm_data fsi_edfa_um70_alarms[] = {
	{ 15, 	FSI_EDFA_ALARM,	 	"Laser off (pumps are off)",				"", },
	{ 14, 	FSI_EDFA_WARNING,	"Loss off output power",				"Power < (CPU-2dB)", },
	{ 13, 	FSI_EDFA_ALARM,  	"Loss of input power",					"Power < ASI", },
	{ 12, 	FSI_EDFA_WARNING,	"Case temperature too high",				"Case Temperature > 63°C", },
	{ 11, 	FSI_EDFA_ALARM,  	"Second stage diode temperature too high",		"Temperature > 41°C", },
	{ 10, 	FSI_EDFA_ALARM,  	"Second stage diode temperature too low",		"Temperature < 16°C", },
	{  9, 	FSI_EDFA_ALARM,  	"First stage diode temperature too high",		"Temperature > 41°C", },
	{  8, 	FSI_EDFA_ALARM,  	"First stage diode temperature too low",		"Temperature < 16°C", },
	{  7, 	FSI_EDFA_WARNING,	"Second stage diode current is high",			"Current > hard setting value", },
	{  6, 	FSI_EDFA_ALARM,  	"Diode current is too low",				"Current < 25mA", },
	{  5, 	FSI_EDFA_WARNING,	"First stage diode current is high",			"Current > hard setting value", },
	{  4, 	FSI_EDFA_ALARM,  	"Temperature control is off",				"", },
	{  3, 	FSI_EDFA_ALARM,  	"Key off",						"High level on pin #21", },
	{  2, 	FSI_EDFA_WARNING,	"Electrical board reset happen",			"", },
	{  1, 	FSI_EDFA_WARNING,	"Second stage diode temperature is far from set point",	"Temperature < (TC2 - 3°C) / Temperature > (TC2 + 3°C)", },
	{  0, 	FSI_EDFA_WARNING,	"First stage diode temperature is far from set point",	"Temperature < (TC1 - 3°C) / Temperature > (TC1 + 3°C)", },
};

struct fsi_edfa_alarm_data fsi_edfa_um243_alarms[] = {
	{ 21,	FSI_EDFA_ALARM,		"Board temperature error",	"CAT<TCLor CAT>TCH", },
	{ 20,	FSI_EDFA_ALARM,		"Power supply error",           "power supply too low or too high", },
	{ 19,	FSI_EDFA_ALARM,		"GPIO Key OFF activated",       "Key pin on the interface is low", },
	{ 15,	FSI_EDFA_ALARM,		"Low input power 1",            "IPW < IPT1", },
	{ 14,	FSI_EDFA_ALARM,		"Low output power 1",           "OPW < OPT1 for ID1 > I1T (depend of the config)", },
	{ 13,	FSI_EDFA_ALARM,		"Low input power 2",            "IPW2 < IPT2", },
	{ 12,	FSI_EDFA_ALARM,		"Low output power 2",           "OPW2 < OPT2 for ID2 > I2T (depend of the config)", },
	{ 11,	FSI_EDFA_ALARM,		"Preamp current error",         "Alarm if IDx>LCMx Persistant alarm (reset only if KeyOFF or ALA=0)", },
	{ 10,	FSI_EDFA_ALARM,		"Booster current error",        "Alarm if IDx>LCMx Persistant alarm (reset only if KeyOFF or ALA=0)", },
	{  3,	FSI_EDFA_ALARM,		"preamp temp",			"", },
	{  2,	FSI_EDFA_ALARM,		"error booster temp error",	"", },
};

struct fsi_edfa_um243_alarms {
	bool	board_temperature_error;
	bool	power_supply_error;
	bool	gpio_key_off_activated;
	bool	low_input_power_1;
	bool	low_output_power_1;
	bool	low_input_power_2;
	bool	low_output_power_2;
	bool	preamp_current_error;
	bool	booster_current_error;
	bool	preamp_temp;
	bool	error_booster_temp_error;
};
struct fsi_edfa_um70_alarms {
	bool	laser_off;
	bool	loss_of_input_power;
	bool	second_stage_diode_temperature_too_high;
	bool	second_stage_diode_temperature_too_low;
	bool	first_stage_diode_temperature_too_high;
	bool	first_stage_diode_temperature_too_low;
	bool	diode_current_too_low;
	bool	temperature_control_off;
	bool	key_off;
};

/*

	{ 15, 	FSI_EDFA_ALARM,	 	"laser off (pumps are off)",				"", },
	{ 13, 	FSI_EDFA_ALARM,  	"loss of input power",					"Power < ASI", },
	{ 11, 	FSI_EDFA_ALARM,  	"second stage diode temperature too high",		"Temperature > 41°C", },
	{ 10, 	FSI_EDFA_ALARM,  	"second stage diode temperature too low",		"Temperature < 16°C", },
	{  9, 	FSI_EDFA_ALARM,  	"first stage diode temperature too high",		"Temperature > 41°C", },
	{  8, 	FSI_EDFA_ALARM,  	"first stage diode temperature too low",		"Temperature < 16°C", },
	{  6, 	FSI_EDFA_ALARM,  	"diode current is too low",				"Current < 25mA", },
	{  4, 	FSI_EDFA_ALARM,  	"temperature control is off",				"", },
	{  3, 	FSI_EDFA_ALARM,  	"key off",						"High level on pin #21", },
*/
