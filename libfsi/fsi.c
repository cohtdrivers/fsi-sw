/*
 * Copyright CERN 2021-2023
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <string.h>
#include <stdio.h>
#include <math.h>
#include <sys/time.h>

#include "fsi.h"
#include "fsi-private.h"
#include "fsi-laser.h"
#include "fsi-edfa.h"

__attribute__((unused))
static char fsi_lib_version[] = "libfsi_version: " GIT_VERSION;

struct fsi_channel {
	unsigned int		channel_id;
	enum fsi_channel_type	type;
	int			channel_gain;
	int			dc_coupling;
	unsigned int		disabled;
	int			rpeaks;
	struct fsi_peak_config	requested_peaks[FSI_MAX_PEAKS];

	/* measurement results */
	struct timeval		timestamp;
	int			mpeaks;
	struct fsi_peak_result	measured_peaks[FSI_MAX_PEAKS];
	struct fsi_hw_status	hw_status;
	struct fsi_full_measurement
				*full;	/* NULL unless requested */
};

struct fsi_diot {
	/** FIXME: this is just a provisory note, probably
	 * more/less fields need to be here */
	char			hostname[PATH_MAX];
	char			ctrl_port;
	int			sockfd;
	int32_t			reserved[64];
};

struct fsi_interferometer {
	int			nblocks;		/* number of optical blocks */
	int			nedfas;			/* number of EDFAs */
	int			nrefs;			/* number of reference chs */
	int			ngas;			/* number of GCs */
	int			nchannels;		/* number of phys channels */
	struct fsi_laser	*laser;			/* one and only one laser */
	struct fsi_edfa		*edfa[FSI_NBLOCKS];	/* nblocks EDFAs registered here */
	struct fsi_diot		*diot[FSI_NBLOCKS];	/* nblocks diot crates registered here */
	struct fsi_channel	*channel;		/* addr of array of fixed length FSI_NCHANNELS */
	struct fsi_channel	channels[FSI_NCHANNELS];
};

struct __FSI_state {
	/* physical configuration of the interferometer
	 * WARNING: assumed invariant: nblocks == nrefs == nedfas
	 * WARNING: assumed invariant: nblocks, ngas and nchannels are constants of nature
	 */
	int			nblocks;		/* actual number of optical blocks */
	int			nedfas;			/* actual number of EDFAs */
	int			nrefs;			/* actual number of reference chs */
	int			ngas;			/* actual number of GCs */
	int			nchannels;		/* actual number of phys channels */
	struct fsi_laser	*laser;			/* one and only one laser */
	struct fsi_edfa		*edfa[FSI_NBLOCKS];	/* nblocks EDFAs registered here */
	struct fsi_diot		*diot[FSI_NBLOCKS];	/* nblocks diot crates registered here */
	struct fsi_channel	*channel;		/* addr of array of fixed length FSI_NCHANNELS */
	struct fsi_channel	channels[FSI_NCHANNELS];

	/**
	 * these field store the internal state variables of the interferometer
	 * during an acquisition cycle; they would rarely be reported
	 * directly through the library API
	 */
	int			fs;
	int			n;
	int			zpf;
	char			batch[256];
	char			laser_configs[1024];
	int			gas_cell_ids[FSI_NGCS];
	int			ref_ids[FSI_NREFS];

	struct timeval		timestamp;		/**< timestamp of last valid acq cycle */
	double			alpha;			/**< computed alpha */
	double			idx_to_distance;
	double			n_air;			/**< always 1.0! */

	int			full_chid;		/**< channel id of requested full channel */
	enum fsi_expert_mode	full_mode;		/**< mode requested for full channel */

	struct fsi_qa_params	qa_params;

	/* unused/unknown */
	int			proto;
	int			sockfd;
	int			fd;
	struct config		*cfg;
};

/* double-check sizes of structures */
void print_size_data(void)
{
	printf("%-30s: %8zd bytes\n", "struct fsi_channel", sizeof(struct fsi_channel));
	printf("%-30s: %8zd bytes\n", "struct fsi_diot", sizeof(struct fsi_diot));
	printf("%-30s: %8zd bytes\n", "struct fsi_interferometer", sizeof(struct fsi_interferometer));
	printf("%-30s: %8zd bytes\n", "struct __FSI_state", sizeof(struct __FSI_state));
}

static struct fsi_mem_pool *_static_mem_pool;
static int _ctrl_fd;
static struct fsi_expert_vectors {
	double	raw[FSI_SAMPLE_SIZE];
	double	lin[FSI_SAMPLE_SIZE];
	double	fft[FSI_SAMPLE_SIZE];
} *_raw_data;

int sync_server(char *msg)
{
	return fsi_wake_server(_ctrl_fd, msg);
}

int wait_on_server(char *ans)
{
	return fsi_wait_on_server(_ctrl_fd, ans);
}


double mean(uint16_t *v, long n)
{
	double s = 0.0;
	long i;

	for (i = 0; i < n; i++)
		s += v[i];
	return s/n;
}

/** @brief      interferometer initialization function
 *  @return	a pointer to an opaque data structure representing the
 *              apparatus
 *  @todo       FIXME: it might be convenient to pass a configuration
 *		file/stream as a parameter, so as not to hardcode configuration
 *		path(s)
 */
FSI *fsi_lib_init(const char *config_file)
{
	struct __FSI_state	*h = NULL;
	int i;

	/* FIXME: read configuration here */
	/* As of SCT, we have a basically hardcoded config
	 * of 1 optical block */
	struct fsi_mem_pool *mp = fsi_get_mem_pool();
	if (mp == NULL)
		goto fsi;
	_ctrl_fd = fsi_ctrl_socket(NULL);
	if (_ctrl_fd < 0)
		goto socket;

	/* FIXME: this is now static; switch configs with a flag?
	   if ((h = malloc(sizeof(*h))) == NULL)
		goto fsi;
	 */
	_static_mem_pool = mp;
	h = mp->addr;
	_raw_data = (struct fsi_expert_vectors *)&h[1];

	h->nblocks = 1;
	h->nchannels = FSI_NCHANNELS;

	if ((h->laser = fsi_laser_init(PORT_LASER)) == NULL)
		goto laser;
	for (i = 0; i < h->nblocks; i++) {
		h->edfa[i] = fsi_edfa_init(PORT_EDFA, i);
		if (h->edfa[i] == NULL)
			goto edfa;
	}
	h->channel = h->channels;
	memset(h->channels, 0, sizeof(h->channels));
	for (i = 0; i < h->nchannels; i++) {
		h->channel[i].type = FSI_CH_NONE;
		h->channel[i].disabled = 1;
	}
	h->full_chid  = -1;
	h->full_mode  = FSI_EXPERT_NONE;
	print_size_data();
	return h;

edfa:
	while (--i >= 0)
		fsi_edfa_free(h->edfa[i]);
	fsi_laser_free(h->laser);
laser:
	fsi_free_ctrl_socket(_ctrl_fd);
socket:
	/* FIXME: now this is in the mempool
	free(h); */
	fsi_release_mem_pool(mp);
fsi:
	return NULL;
}

/** close library */
void fsi_lib_exit(FSI *h)
{
	int i;

	for (i = 0; i < h->nblocks; i++)
		fsi_edfa_free(h->edfa[i]);
	fsi_laser_free(h->laser);
	/* FIXME: now this is in the mempool
	free(h);
	 */
	fsi_release_mem_pool(_static_mem_pool);

	return;
}

static struct fsi_channel *id_to_channel(FSI *h, int channel_id)
{
	/* FIXME: validate id and type against physical channel */
	return &h->channel[channel_id];
}

/**
 * @brief	set up a single channel configuration
 * @param[in]	h		- opaque lib handle/context
 * @param[in]	cfg		- a fsi_channel_config instance
 */
int fsi_channel_configure(FSI *h, struct fsi_channel_config *cfg)
{
	int i;
	struct fsi_channel *ch = id_to_channel(h, cfg->channel_id);

	ch->channel_id = cfg->channel_id;
	ch->type = cfg->type;
	/* FIXME: validate id and type against physical channel */
	ch->rpeaks = cfg->npeaks;
	if (!((cfg->npeaks >= 0) && (cfg->npeaks <= FSI_MAX_PEAKS)))
		return FSI_CFG_BAD_NPEAKS;
	ch->channel_gain = cfg->channel_gain;
	ch->dc_coupling = cfg->dc_coupling;
	for (i = 0; i < ch->rpeaks; i++)
		ch->requested_peaks[i] = cfg->peaks[i];
	ch->disabled = cfg->disabled;
	ch->full = NULL;	

	return 0;
}

/**
 * @brief	set up in bulk a number of channel configurations
 * @param[in]	h		- opaque lib handle/context
 * @param[in]	cfg		- a fsi_channel_config array of innstances
 * @param[in]	nchannels	- number of elements of the cfg[] array
 *
 * @return 0 on success, -i < 0 on failure, where i is the position
 * within the cfg[] array of the first config failure
 */
int fsi_channel_array_configure(FSI *h, struct fsi_channel_config cfg[], int nchannels)
{
	int i, ret;
	
	for (i = 0; i < nchannels; i++)
		if ((ret = fsi_channel_configure(h, &cfg[i])) != 0)
			return -i;

	return 0;
}

/**
 * @brief	obtain a channel configuration
 * @param[in]	h		- opaque lib handle/context
 * @param[in]	channel_id	- channel_id to request config from
 * @param[out]	cfg		- a fsi_channel_config instance
 */
int fsi_get_channel_configuration(FSI *h,
	unsigned int channel_id, struct fsi_channel_config *cfg)
{
	struct fsi_channel *ch = id_to_channel(h, channel_id);
	int i;

	cfg->channel_id = ch->channel_id;
	cfg->type = ch->type;
	cfg->npeaks = ch->rpeaks;
	cfg->channel_gain = ch->channel_gain;
	cfg->dc_coupling = ch->dc_coupling;
	cfg->disabled = ch->disabled;
	for (i = 0; i < ch->rpeaks; i++)
		cfg->peaks[i] = ch->requested_peaks[i];

	return 0;
}

/**
 * @brief	recover the result of measurement processing
 * @param[in]	h		- opaque lib handle/context
 * @param[out]	meas		- array of measurements
 * @param[out]	nchannels	- number of valid entries returned in meas
 */
int fsi_receive_measurements(FSI *h, struct fsi_channel_measurement meas[], int *nchannels)
{
	int i, j, nch;
	struct fsi_channel_measurement *m = &meas[0];
	char msg[FSI_MAX_MSG_LEN];

	/* lib<->server sync point B */
	sync_server("meas");
	/* lib<->server sync point C */
	wait_on_server(msg);
	if (strncmp(msg, "calc err", strlen("calc err")) == 0) {
		/* FIXME: be specific about the case where acq did not come
		 * because DIOT was offline. This needs to be tackled somewhat
		 * differently, somewhere else.
		 *
		 * All possible calc errors to date:
		 *
		 * 'calc err: diot down'
		 * 'calc err: acq not ready'		# meas when scan expected
		 * 'calc err: bad fit stderr'		# bad fit std error (never happens)
		 * 'calc err: failed acq'		# inconsisten channel metadata
		 * 'calc err: packets lost'		# timeout or noconn error in memo
		 * 'calc err: malformed gas cell'
		 * 'calc err: unstable output power at EDFA'
		 * 
		 */
		if (strncmp(msg, "calc err: diot down", strlen("calc err: diot down")) == 0)
			return FSI_DIOT_DOWN;
		else
			return FSI_ABORTED_COMPUTATION;
	}
	else if (strcmp(msg, "calc") != 0) {
		fprintf(stderr, "out of sync: bad ack[%s] from server\n", msg);
		return FSI_SERVER_OUT_OF_SYNC;
	} else	/* all ok, calc done, proceed */
		;

	nch =  0;
	for (i = 0; i < FSI_NCHANNELS; i++) {
		struct fsi_channel *ch = &h->channel[i];
		if (ch->disabled)
			continue;
		if (ch->type == FSI_CH_DISTANCE || ch->type == FSI_CH_BRAGG) {
			nch++;
			m->channel_id	= ch->channel_id;
			m->type		= ch->type;
			m->npeaks	= ch->mpeaks;
			for (j = 0; j < ch->mpeaks; j++) {
				m->peaks[j] = ch->measured_peaks[j];
			}
			m++;
		}
	}
	*nchannels = nch;
	return 0;
}

int fsi_receive_measurements_timemout(FSI *h, struct fsi_channel_measurement meas[], int *nchannels, unsigned int timeout)
{
	return 0;
}

/**
 * @brief	announce the interferometer that channel_id is now in scope mode 'mode'
 * @param[in]	h		- opaque lib handle/context
 * @param[in]	channel_id	- id of channel to monitor in raw/scope mode
 * @param[in]	mode		- type of raw data vector to monitor
 *
 * @return	0 on success, -1 on invalid parameters, sets errno
 *
 * @warning setting the mode to FSI_EXPERT_NONE terminates scope mode
 * for the specified channel_id. A simpler way to stop scope mode is to
 * call \ref fsi_stop_scope_mode()
 */
int fsi_set_scope_mode(FSI *h, int channel_id, enum fsi_expert_mode mode)
{
	/* FIXME: do param validation here */
	h->full_chid = channel_id;
	h->full_mode = mode;

	return 0;
}

/**
 * @brief	disable any raw mode operation in the interferometer
 * @param[in]	h		- opaque lib handle/context
 */
void fsi_stop_scope_mode(FSI *h)
{
	h->full_chid = -1;
	h->full_mode = FSI_EXPERT_NONE;
}

/**
 * @brief	obtain current channel and mode monitored in scope mode
 * @param[in]	h		- opaque lib handle/context
 * @param[out]	channel_id	- id of channel undeer monitoring in raw/scope mode
 * @param[out]	mode		- type of raw data vector to monitor
 */
int fsi_get_scope_mode(FSI *h, int *channel_id, enum fsi_expert_mode *mode)
{
	*channel_id = h->full_chid;
	*mode = h->full_mode;

	return *mode != FSI_EXPERT_NONE;
}

/**
 * @brief	is scope mode on?
 * @param[in]	h		- opaque lib handle/context
 *
 * @return	1 if some channel is being monitored in scope mode, 0
 *		otherwise
 */
int fsi_scope_mode_on(FSI *h)
{
	int channel_id;
	enum fsi_expert_mode mode;

	return fsi_get_scope_mode(h, &channel_id, &mode);
}

/**
 * @brief	recover the samples of the channel designated for scope mode, if any
 * @param[in]	h	- opaque lib handle/context
 * @param[out]	meas	- returned full measurement structure
 *
 * @warning	this function MUST be called, if called at all,
 *		after \ref fsi_receive_measurements; at return it will provide
 *		the raw measurement requested in the current status set
 *		by \ref fsi_set_scope_mode, or silently return doing
 *		nothing when the scope mode is FSI_EXPERT_NONE
 */
int fsi_get_full_samples(FSI *h, struct fsi_full_measurement *meas)
{
	double *ptr;
	int n = 2490000, i;
	int channel_id = h->full_chid;
	int mode = h->full_mode;

	if (mode == FSI_EXPERT_NONE)
		return 0;

	meas->channel_id = channel_id;
	meas->type = h->channels[channel_id].type;
	meas->mode = mode;

	/* sanity-check modes, weed out nonsensical combinations */
	/*  FIXME: better describde this all through a struct */
	if ((mode == FSI_EXPERT_OSCILLOSCOPE || mode == FSI_EXPERT_LIN_DATA) &&
		! (meas->type == FSI_CH_DISTANCE))
		goto bad_mode;
	if ((mode == FSI_EXPERT_RAW_GAS_CELL || mode == FSI_EXPERT_GAS_CELL) &&
		! (meas->type == FSI_CH_GAS_CELL))
		goto bad_mode;
	if ((mode == FSI_EXPERT_RAW_INTERF || mode == FSI_EXPERT_INTERF) &&
		! (meas->type == FSI_CH_INTERF))
		goto bad_mode;
	if ((mode == FSI_EXPERT_LIN_FFT_HZ || mode == FSI_EXPERT_LIN_FFT_M) &&
		! (meas->type == FSI_CH_INTERF || meas->type == FSI_CH_DISTANCE))
		goto bad_mode;
	
	if (mode == FSI_EXPERT_OSCILLOSCOPE ||
	    mode == FSI_EXPERT_RAW_GAS_CELL ||
	    mode == FSI_EXPERT_RAW_INTERF ) {
		ptr = _raw_data[channel_id].raw;
	} else if (mode == FSI_EXPERT_LIN_DATA ||
		   mode == FSI_EXPERT_GAS_CELL ||
		   mode == FSI_EXPERT_INTERF) {
		ptr = _raw_data[channel_id].lin;
	} else if (mode == FSI_EXPERT_LIN_FFT_HZ ||
		   mode == FSI_EXPERT_LIN_FFT_M)  {
		ptr = _raw_data[channel_id].fft;
		n = n / 2;
	} else
		goto bad_mode;
	meas->nsamples = n;
	if (mode == FSI_EXPERT_LIN_FFT_HZ) {
		fsi_natural_to_log(meas->data, ptr, n);
		meas->xunit = FSI_UNIT_HZ;
		meas->start = 0;			/* FIXME: fill: refactor expert_vectors */
		meas->delta = h->fs / h->n;
	} else if (mode == FSI_EXPERT_LIN_FFT_M) {
		fsi_natural_to_log(meas->data, ptr, n);
		meas->xunit = FSI_UNIT_M;
		meas->start = 0;			/* FIXME: fill: refactor expert_vectors */
		meas->delta = h->idx_to_distance;
	} else {
		for (i = 0; i < n; i++)
			meas->data[i] = (uint16_t)ptr[i];
		meas->xunit = FSI_UNIT_S;
		meas->start = 0;			/* FIXME: fill: refactor expert_vectors */
		meas->delta = h->n / h->fs;
	}
	return 0;
bad_mode:
	return FSI_BAD_FULL_MODE;
}

/**
 * \brief
 *    retrieve quality parameters of linearized and gas cell fit
 *
 * Quality assurance diagnostics for linearization and gas cell reading
 * are obtained by calling \ref fsi_get_qa_params. Grosso modo, the
 * diagnostics compute how narrow is the reference interferometer
 * spectrum, and how good is the LSQ fit of the GC peak frequencies
 * found with respect to the NIST reference data of the HCN spectrum
 */
int fsi_get_qa_params(FSI *h, struct fsi_qa_params *qa)
{
	struct fsi_qa_params *computed_qa = &h->qa_params;
	int nP, nR;

	qa->ref[0].ref_id           = computed_qa->ref[0].ref_id;
	qa->ref[0].ref_center       = computed_qa->ref[0].ref_center;
	qa->ref[0].ref_deviation    = computed_qa->ref[0].ref_deviation;
	qa->ref[0].ref_deviation95  = computed_qa->ref[0].ref_deviation95;
	qa->gc[0].gc_id             = computed_qa->gc[0].gc_id;
	qa->gc[0].gc_alpha          = computed_qa->gc[0].gc_alpha;
	qa->gc[0].gc_fitting_err    = computed_qa->gc[0].gc_fitting_err;

	qa->gc[0].gc_npeaks_fit_R   = nR = computed_qa->gc[0].gc_npeaks_fit_R;
	qa->gc[0].gc_npeaks_fit_P   = nP = computed_qa->gc[0].gc_npeaks_fit_P;
	memcpy(qa->gc[0].gc_peaks_R, computed_qa->gc[0].gc_peaks_R, sizeof(double) * nR);
	memcpy(qa->gc[0].gc_peaks_P, computed_qa->gc[0].gc_peaks_P, sizeof(double) * nP);

	/* in other words, except for zero-filling unused values,
	 * memcpy(qa, h->qa); will do */
	return 0;
}

/**
 * @brief	find channel id of first gas cell in interferometer
 * @param[in]	h		- opaque lib handle/context
 * @return	the channel id of the first gas cell, or -1 if none was
 *			 found
 * @todo	this entry point must be extended to find a secondary
 *                   	gas cell when present
 */
int fsi_get_gas_cell_id(FSI *h)
{
	int i = 0;

	for (i = 0; i < h->nchannels; i++)
		if (h->channel[i].type == FSI_CH_GAS_CELL)
			return h->channel[i].channel_id;
	return -1;
}

/**
 * @brief	find channel id of first reference channel in interferometer
 * @param[in]	h		- opaque lib handle/context
 * @return	the channel id of the first reference channel, or -1 if none was
 *			found
 * @todo	this entry point must be extended to find by bank, or
 * 			the four refs
 */
int fsi_get_interferometer_id(FSI *h)
{
	int i = 0;

	for (i = 0; i < h->nchannels; i++)
		if (h->channel[i].type == FSI_CH_INTERF)
			return h->channel[i].channel_id;
	return -1;
}

struct fsi_laser *fsi_get_laser(FSI *h)
{
	return h->laser;
}

struct fsi_edfa *fsi_get_edfa(FSI *h, int id)
{
	return h->edfa[id];
}

int fsi_scan(FSI *h)
{
	struct fsi_laser *laser = fsi_get_laser(h);
	int err;
	char msg[FSI_MAX_MSG_LEN], *report;

	/* FIXME: I see no reason why we do this, instead of syncing
	 * **only** when scan suceeds. Give it a try later
	 */
	err = fsi_laser_scan(laser);
	report = (err == 0) ? "scan ok" : "scan err";

	/* lib<->server sync point A */
	sync_server(report);
	wait_on_server(msg);

	if (strcmp(msg, "ack") == 0) {
		return err;
	} else {
		fprintf(stderr, "out of sync: bad ack[%s] from server\n", msg);
		return FSI_SCAN_SYNC_ERR;
	}
}

int fsi_get_sensor_data(FSI *h, int nblocks, struct fsi_chassis_sensors *data)
{
	return FSI_NOT_IMPLEMENTED;
}

/**
 * conversions to/from logarithmic scale to keep dynamic range in
 * spectral raw data
 */

/* theoretical max value of spectral bin < 1e10, thence
 * log10 values of spectral bins must be scaled by this
 */
static const uint16_t log_scale = 6000;

void fsi_natural_to_log(uint16_t data[], double natural[], int nsamples)
{
	int i;

	for (i = 0; i < nsamples; i++) {
		/* clip underflown bin values */
		data[i] = (uint16_t) log_scale * fmax(log10(natural[i]), 1);
	}
}
void fsi_log_to_natural(double natural[], uint16_t data[], int nsamples)
{
	int i;

	for (i = 0; i < nsamples; i++) {
		natural[i] = pow(10.0, (double)data[i] / log_scale);
	}
}

/**
 * These functions implement the correspondence between channel_id
 * (chid for short) and triplets (oblock, board, ch), where
 *
 * 	oblock == optical block# (0..3) == diot# == edfa#
 * 	board  == board number in diot crate (1..8)
 * 	ch     == channel number in board (0..7)
 */

static unsigned int geo_to_chid(int oblock, int board, int ch)
{
	return (oblock & 0x3) << 6 |  ((board-1) & 0x7) << 3  | (ch & 0x7);
}


static int id_to_ch(unsigned int channel_id)
{
	return channel_id & 0x7;
}

static int id_to_oblock(unsigned int channel_id)
{
	return channel_id >> 6 & 0x3;
}

static int id_to_board(unsigned int channel_id)
{
	return (channel_id >> 3 & 0x7) + 1;
}
static void chid_to_geo(unsigned int channel_id, int *oblock, int *board, int *ch)
{
	*oblock = id_to_oblock(channel_id);
	*board  = id_to_board(channel_id);
	*ch     = id_to_ch(channel_id);
}

#include <assert.h>
#include <stdio.h>

__attribute__((unused))
static  void show_geo_to_chid_mapping(void)
{
	int oblock, board, ch;

	for (oblock = 0; oblock < 4; oblock++)
	for (board = 1; board <= 8; board++)
	for (ch = 0; ch < 8; ch++) {
		int a, b, c;
		unsigned int pp = geo_to_chid(oblock, board, ch);
		printf("%d:%d:%d  %d (%2x)\n", oblock, board, ch, pp, pp);
		chid_to_geo(pp, &a, &b, &c);
		assert(a == oblock);
		assert(b == board);
		assert(c ==ch);
	}
}

#ifdef TEST_GEO
int main(int argc, char * argv[])
{
	show_geo_to_chid_mapping();
}
#endif /* TEST_GEO */
