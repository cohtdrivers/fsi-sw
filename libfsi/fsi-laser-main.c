/*
 * Copyright CERN 2023
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

int main(int argc, char *argv[])
{
	extern int fsi_laser_main(int argc, char *argv[]);

	return fsi_laser_main(argc, argv);
}
