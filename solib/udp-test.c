// Client side implementation of UDP client-server model
#define _GNU_SOURCE /* See feature_test_macros(7) */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <stdint.h>
#include <pthread.h>

#include <sched.h>

uint64_t get_tics()
{
    struct timezone tz = {0, 0};
    struct timeval tv;

    gettimeofday(&tv, &tz);
    return tv.tv_sec * 1000000ULL + tv.tv_usec;
}

#define PORT 8080
#define MAXLINE 1024

int psize = 7500;
int attempts = 100000;
int n_threads = 4;
int n_cpus = 2;

struct thread_state
{
    int id;
    uint64_t bytes_transferred;
    double duration;
    pthread_t thread;
};

void *worker_thread(void *p)
{
    struct thread_state *st = (struct thread_state *)p;
    int sockfd;
    char buffer[65536];
    char *hello = "Hello from client";
    struct sockaddr_in servaddr;

    int num_cores = n_cpus; //sysconf(_SC_NPROCESSORS_ONLN);

    int core_id = st->id % num_cores;

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(core_id, &cpuset);

    pthread_t current_thread = pthread_self();
    pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
    printf("Thread affinity %d->%d\n", st->id, core_id);

    // Creating socket file descriptor
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }

    st->bytes_transferred = 0;

    memset(&servaddr, 0, sizeof(servaddr));

    // Filling server information
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT + st->id);
    servaddr.sin_addr.s_addr = 0x0200000a;

    //printf("sock: %d\n", sockfd);

    int n, len;

    //    for(;;)
    {
        int i = 0;
        double total = 0;
        uint64_t t_start = get_tics();
        for (i = 0; i < attempts; i++)
        {
            int rv = sendto(sockfd, (const char *)buffer, psize,
                            MSG_CONFIRM, (const struct sockaddr *)&servaddr,
                            sizeof(servaddr));

            total += rv;
        }
        uint64_t t_end = get_tics();
        st->duration = (double)(t_end - t_start) / 1e6;
        st->bytes_transferred = total;

        fprintf(stderr, "[Thread %d] sent %lld bytes in %.3f s!\n", st->id, st->bytes_transferred, st->duration);
    }

    close(sockfd);
    return NULL;
}

// Driver code
int main(int argc, char *argv[])
{
    int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
    if (argc < 3)
    {
        printf("usage: %s threads attempts\n", argv[0]);
        return -1;
    }

    n_threads = atoi(argv[1]);
    attempts = atoi(argv[2]);

    if (argc > 3)
        psize = atoi(argv[3]);

    if (argc == 5)
    {
        n_cpus = atoi(argv[4]);
        printf("Requested CPUs number %d\n",n_cpus);
        if (n_cpus > num_cores)
        {
          n_cpus = num_cores;
          printf("Trying to use too many CPUs, seeting n_cpus to max=%d",n_cpus);
        }
    } 
    else
        n_cpus  = num_cores;

    struct thread_state thr[16];
    int i;

    printf("Running with %d threads (%d CPUs), %d packets of %d bytes\n", n_threads, n_cpus, attempts, psize);

    uint64_t t_start = get_tics();

    for (i = 0; i < n_threads; i++)
    {
        thr[i].id = i;
        pthread_create(&thr[i].thread, NULL, worker_thread, &thr[i]);
    }

    //sleep(100);

    for (i = 0; i < n_threads; i++)
    {
        pthread_join(thr[i].thread, NULL);
    }

    uint64_t t_end = get_tics();

    uint64_t total_bytes = 0;
    double total_time = 0.0;

    for (i = 0; i < n_threads; i++)
    {
        total_bytes += thr[i].bytes_transferred;
        total_time += thr[i].duration;
    }

    double dt = (double)(t_end - t_start) / 1e6;

    printf("Total (real-time): %lld bytes in %.3f s = %.0f Mbytes/second\n", total_bytes, dt, (double)total_bytes / dt / 1048576.0);
    printf("Total (thread-average-time): %lld bytes in %.3f s = %.0f Mbytes/second\n", total_bytes, total_time, (double)total_bytes / total_time * (double) n_threads / 1048576.0);

    return 0;
}