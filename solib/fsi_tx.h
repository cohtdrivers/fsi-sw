#ifndef __FSI_TX_LIB_H__
#define __FSI_TX_LIB_H__

#define PACKED __attribute__((packed))

#define MB_factor 1048576.0

// "enum"
#define C_UDP 0
#define C_TCP 1

#define IP_ADDR_LEN 16 /* in theory max 15*/
#define AP_NAME_LEN 20 /* max name of application*/

/*
 * default startup values
 */
#define DEFAULT_SERVER_IP          "192.168.1.2"
#define DEFAULT_CONNECTION_TYPE    C_TCP
#define DEFAULT_PAYLOAD_SIZE       62800 //optimize for 9010 bytes of IP frag pkt
#define DEFAULT_VERBOSITY_LEVEL    1
#define DEFAULT_NO_DMA_CONFIG      0
#define DEFAULT_NO_PHOTODET_CONFIG 0
#define DEFAULT_NO_EXT_TRIG_CONFIG 0
#define DEFAULT_ACQ_BANK           1 
#define DEFAULT_SAMPLE_NUMBER      2490000
#define DEFAULT_MMAPTYPE           1

/*
 * other config
 */
#define TX_BUFFER_SIZE_SAMPLES (32768*4)
#define PORT 8080
#define MIN_ALLOWED_DEADTIME_IN_SEC 4 // deadtime between acquisitions

/*
 * Return value of fsi_check_rx_locked() function
 */
#define LOCKED_OK  1
#define LOCKED_ERR 0

struct packet_header{
    
    int thread_id;
    int packet_id;
    int payload_size;
    int expected_pkt_n;
    int chan;
    int board;
    int first_sample_id;
    int sample_number;
};

typedef struct {
    int c_type;               //connection type (UPD/TCP)
    int s_payload;            //FSI payload size
    int n_packets;            //Number of packets per bank (TODO: clean)
    int verbose;              // verbosity level
    int no_dma;               //if TRUE, do not DMA from PS's RAM where the data is
    int no_photodet;          //if TRUE, skipp all access to photodet HW
    int no_ext_trig;          //if TRUE, do not enable external trigger
    char ip_add[IP_ADDR_LEN]; // IP address of the server
    int n_bank;               // Bank to transmit (0: boards 1-4, 1: boards 5-8)
    int n_sample;             // Numbers of samples to be read
    int mmaptype;             // way of mapping DDR into user space: 0: direct mmap(), 1: u-dma-buf driver
    char ap_name[AP_NAME_LEN];// store name of the application
} RunTimeOpts;

struct packet_fsi{
    struct packet_header header;
    uint16_t payload[TX_BUFFER_SIZE_SAMPLES];
};

#endif
