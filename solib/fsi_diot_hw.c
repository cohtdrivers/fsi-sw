#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/mman.h>

#include "diot_fsi_regs.h"
#include "fsi_diot_hw.h"


static void *m_axi_base;
static unsigned page_size;

static void m_axi_writel(uint32_t addr, uint32_t data)
{
	if(addr > page_size)
        {
              printf("%s: address=%d goes beyond page boundry \n",__func__, addr);
              return;
        }
        *(volatile uint32_t *)(m_axi_base + addr) = data;
        if(DEBUG_HW)printf("%s: Write: addr=0x%x | data=0x%x \n",__func__, addr, data);
}

uint32_t m_axi_readl(uint32_t addr)
{
	uint32_t val;
	if(addr > page_size)
        {
              printf("%s: address=%d goes beyond page boundry \n",__func__, addr);
              return 0;
        }
        val = *(volatile uint32_t *)(m_axi_base + addr);
        if(DEBUG_HW) printf("%s: Read: addr=0x%x | data=0x%x \n",__func__, addr, val);
        return val;
}

uint32_t m_axi_readl_bank(int bank, uint32_t _addr)
{
	unsigned base = 0;
	uint32_t addr = 0;
        uint32_t val  = 0;
	if (bank < 0 || bank > 1)
        {
               printf("%s: bank=%d out of range \n",__func__, bank);
               return 0;
        }
	addr = SYS_TOP_SUB_REG_CONFIGS + (bank * SYS_TOP_SUB_REG_CONFIGS_SIZE) + _addr;
	if(addr > page_size)
        {
              printf("%s: address=%d goes beyond page boundry \n",__func__, addr);
              return 0;
        }
        val  = m_axi_readl(addr);
        if(DEBUG_HW) printf("%s: Read: addr=0x%x | data=0x%x \n",__func__, addr, val);
        return val;
}

int m_axi_map(int addr)
{
        int fd;
        int value = 0;


        unsigned page_addr, page_offset;
        page_size=sysconf(_SC_PAGESIZE);

        /* Open /dev/mem file */
        fd = open ("/dev/mem",  O_RDWR );
        if (fd < 1) {
                printf("%s: /dev/mem: %s\n", __func__, strerror(errno));
                exit(1);
        }
        if(DEBUG_HW) printf("%s: opened /dev/mem \n",__func__);
        /* mmap the device into memory */
        page_addr = (addr & (~(page_size-1)));
        page_offset = addr - page_addr;
        m_axi_base = mmap(NULL, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, page_addr);

        close(fd);
        if (m_axi_base == MAP_FAILED) {
                fprintf(stderr, "%s: mmap(/dev/mem): %s\n",__func__, strerror(errno));
                exit(1);
        }
        if(DEBUG_HW) printf("%s: mmap() doen \n",__func__);

        /* Read value from the device register */

        value = (int)m_axi_readl(page_offset);
        printf("%s: Address = 0x%08x | Value = 0x%08x \n",__func__, page_addr, value);

        return value;
}

int m_axi_unmap(void)
{

        unsigned page_size=sysconf(_SC_PAGESIZE);
        munmap(m_axi_base, page_size);
        return 0;
}

// static unsigned get_dma_length(void)
// {
//   return m_axi_readl(SYS_TOP_SUB_REG_LENGTH);
// }

void dma_init(unsigned nsamp, uint32_t ddr_phys_addr_B0, uint32_t ddr_phys_addr_B1)
{
  // Set address of DDR bank 0
  m_axi_writel(SYS_TOP_SUB_REG_CONFIGS |
               SYS_TOP_SUB_REG_CONFIGS_ADDRESS, ddr_phys_addr_B0);

  // Set address of DDR bank 1
  // Note a bit weired addressing which is correct: CONFIGS is a table so
  // by adding SIZE of a single entry, the second item in the table is reached.
  m_axi_writel(SYS_TOP_SUB_REG_CONFIGS |
               SYS_TOP_SUB_REG_CONFIGS_ADDRESS |
               SYS_TOP_SUB_REG_CONFIGS_SIZE, ddr_phys_addr_B1);

  m_axi_writel(SYS_TOP_SUB_REG_LENGTH, nsamp);
}

int udmabuf_set_param(char *dev, char *param, unsigned long value)
{
    unsigned char  path[1024];
    unsigned char  attr[1024];
    int fd = 0;
    sprintf(path, "/sys/class/u-dma-buf/%s/%s", dev, param);
    if(DEBUG_HW) printf("%s: write %ld [0x%lx] to %s",__func__, value, value, path);
    if ((fd  = open(path, O_WRONLY)) != -1) {
        sprintf(attr, "%ld", value);
        write(fd, attr, strlen(attr));
        close(fd);
        if(DEBUG_HW) printf(" - success\n");
        return 1;
    }
    if(DEBUG_HW) printf(" - error\n");
    return -1;
}

unsigned long udmabuf_get_param(char *dev, char *param, int format)
{
    unsigned char  path[1024];
    unsigned char  attr[1024];
    unsigned long  value;
    int fd=0;
    sprintf(path, "/sys/class/u-dma-buf/%s/%s", dev, param);
    if(DEBUG_HW) printf("%s: read %s",__func__, path);
    if ((fd  = open(path, O_RDONLY)) != -1) {
        read(fd, attr, 1024);
        switch (format)
        {
          case PARAM_FORMAT_DEC:
            sscanf(attr, "%ld", &value);
            break;
          case PARAM_FORMAT_HEX:
            sscanf(attr, "%lx", &value);
            break;
          default:
            sscanf(attr, "%ld", &value);
            break;
        }
        close(fd);
        if(DEBUG_HW) printf(" - success: %ld [0x%lx]\n", value, value);
    }
    return value;
}

void * mmap_udmabuf(char *dev, unsigned buf_size) {

    int fd=0;
    void *mem;

    printf("%s: open() device %s,\n", __func__, dev);

    if ((fd  = open(dev, O_RDWR)) == -1) 
    {
      perror("open() filed\n");
      return MAP_FAILED;
    }

    printf("%s: open() OK,\n", __func__);


//     unsigned buf_size = 251658240; // TODO: read size,see: https://github.com/ikwzm/udmabuf#size-1
    mem = mmap(NULL, buf_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    if (mem == MAP_FAILED) 
    {
      perror("Can't map memory");
      return MAP_FAILED;
    }

//     close(fd);.// ??
    printf("%s: mmap OK\n", __func__);
    return mem;

}

void * mmap_ram(unsigned addr) {

    off_t offset = addr;

    size_t pagesize = sysconf(_SC_PAGE_SIZE);
    off_t page_base = (offset / pagesize) * pagesize;

    int fd = open("/dev/mem", O_RDWR);
    printf("%s: open() OK, 0x%x,\n", __func__, (unsigned int)page_base);

    void *mem = mmap(NULL, 0x20000000, PROT_READ | PROT_WRITE, MAP_SHARED, fd, page_base);
//     void *mem = mmap(NULL, 0x20000000, PROT_READ | PROT_WRITE, MAP_SHARED, fd, addr);
    if (mem == MAP_FAILED) {
        perror("Can't map memory");
    }
    close(fd);
    printf("%s: mmap OK\n", __func__);
    return mem;

}


int cmd_fsi_regs(void)
{
  unsigned i;

  if(!m_axi_base)
     m_axi_map(M_AXI_BASE);

  printf("ident:   %08x\n",
              m_axi_readl(SYS_TOP_SUB_REG_IDENT));
  printf("status:  %08x\n",
              m_axi_readl(SYS_TOP_SUB_REG_STATUS));
  printf("control: %08x\n",
              m_axi_readl(SYS_TOP_SUB_REG_CONTROL));
  printf("config:  %08x\n",
              m_axi_readl(SYS_TOP_SUB_REG_CONFIG));
  printf("length:  %08x\n",
              m_axi_readl(SYS_TOP_SUB_REG_LENGTH));
  for (i = 0; i < 2; i++)
    {
      unsigned addr = SYS_TOP_SUB_REG_CONFIGS
        + (i * SYS_TOP_SUB_REG_CONFIGS_SIZE);
      printf("address  %u: %08x\n", i,
                 m_axi_readl(addr + SYS_TOP_SUB_REG_CONFIGS_ADDRESS));
      printf("cur addr %u: %08x\n", i,
                 m_axi_readl(addr + SYS_TOP_SUB_REG_CONFIGS_CUR_ADDRESS));
      printf("status   %u: %08x\n", i,
                 m_axi_readl(addr + SYS_TOP_SUB_REG_CONFIGS_STATUS));
    }
  printf("stat1:  %08x\n",
              m_axi_readl(SYS_TOP_SUB_REG_PERIPH_STAT1));
  printf("stat2:  %08x\n",
              m_axi_readl(SYS_TOP_SUB_REG_PERIPH_STAT2));
  return 0;
}

/*
  @brief: returns the DMA status
  @return: 0 if DMA is not done, 1 if DMA is done
*/
int is_dma_done(void)
{
  uint32_t status = m_axi_readl(SYS_TOP_SUB_REG_STATUS);
  if ((status & SYS_TOP_SUB_REG_STATUS_DMA_DONE) != 0){
    return 1;
  }
  else {
    return 0;
  }
}

/*
 * @brief: Clear the DMA_DONE bit in the status register
*/
void clear_dma_done(void)
{
  // Safety:
  // this register is indicated as RW in cheby, but only DMA_DONE is W1C, the rest is RO
  m_axi_writel(SYS_TOP_SUB_REG_STATUS, SYS_TOP_SUB_REG_STATUS_DMA_DONE);
}

/*
  @brief: Trigger from software the Sampling
*/
void fsi_trig_from_sw()
{
  m_axi_writel(SYS_TOP_SUB_REG_CONTROL, SYS_TOP_SUB_REG_CONTROL_SW_TRIG);
}

void fsi_trig_enable(unsigned en)
{
  unsigned val;

  val = m_axi_readl(SYS_TOP_SUB_REG_CONFIG);
  if (en)
  {
    val |= SYS_TOP_SUB_REG_CONFIG_TRIG_EN;
   fprintf(stderr,"[Enable trigger]");
  }
  else
  {
    val &= ~SYS_TOP_SUB_REG_CONFIG_TRIG_EN;
    fprintf(stderr,"[Disable trigger]");
  }
  m_axi_writel(SYS_TOP_SUB_REG_CONFIG, val);
}

int fsi_dma_status_get(int bank, struct fsi_dma_status *s)
{
  if (bank < 0 || bank > 1)
    return -1;

  uint32_t ret = m_axi_readl_bank(bank, SYS_TOP_SUB_REG_CONFIGS_STATUS);
  s->axi_err  = (uint8_t)((ret & SYS_TOP_SUB_REG_CONFIGS_STATUS_FIFO_MAX_MASK ) >> SYS_TOP_SUB_REG_CONFIGS_STATUS_AXI_SHIFT);
  s->fifo_max = (uint8_t)((ret & SYS_TOP_SUB_REG_CONFIGS_STATUS_FIFO_MAX_MASK ) >> SYS_TOP_SUB_REG_CONFIGS_STATUS_FIFO_MAX_SHIFT);
  s->fifo_err = (uint8_t)((ret & SYS_TOP_SUB_REG_CONFIGS_STATUS_FIFO_ERRS_MASK) >> SYS_TOP_SUB_REG_CONFIGS_STATUS_FIFO_ERRS_SHIFT);
  return 0;
}

void read_dma_fifo_status (void)
{
  int d;
  int dma_banks=2;
  struct fsi_dma_status s;
  fprintf(stderr, "[ ");
  for (d = 0; d < dma_banks; d++)
  {
    fsi_dma_status_get(d, &s);
    fprintf(stderr, "DMA %d status: axi_err=%d, max=%2d, err=%2d",
            d, s.axi_err, s.fifo_max, s.fifo_err);
    if(d==dma_banks-1)
      fprintf(stderr, "]");
    else
      fprintf(stderr, " | ");
  }
}

uint32_t fsi_get_rx_locked_mask(void)
{
  uint32_t s2 = m_axi_readl(SYS_TOP_SUB_REG_PERIPH_STAT2);
  return  0xff & (( s2 & SYS_TOP_SUB_REG_PERIPH_STAT2_RX_LOCKED_MASK )  >> SYS_TOP_SUB_REG_PERIPH_STAT2_RX_LOCKED_SHIFT);
}

int fsi_do_resync(void)
{
  uint32_t val;
  static int resync_cnt = 0;
  val = m_axi_readl(SYS_TOP_SUB_REG_CONFIG);
  val |= SYS_TOP_SUB_REG_CONFIG_RESYNC;
  m_axi_writel(SYS_TOP_SUB_REG_CONFIG, val);
  return ++resync_cnt;
}

/////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// new stuff for diags: 
void fsi_wait_cmd(void)
{
  unsigned mask;
  unsigned i;

  mask = m_axi_readl(SYS_TOP_SUB_REG_CMD_BOARDS);

  for (i = 0; i < 4; i++) {
    unsigned status = m_axi_readl(SYS_TOP_SUB_REG_CMD_STATUS);
    unsigned done = (status >> SYS_TOP_SUB_REG_CMD_STATUS_DONE_SHIFT);
    if ((done & mask) == mask)
      return;
  }

  /* Could happen if a board is sending samples.  */
  printf("cmd timeout\n");
}

void fsi_exec_cmd(unsigned cmd)
{
  m_axi_writel(SYS_TOP_SUB_REG_CMD_VALUE, cmd);
  m_axi_writel(SYS_TOP_SUB_REG_CMD_CTRL,
            SYS_TOP_SUB_REG_CMD_CTRL_EXEC);
  fsi_wait_cmd();
}

void fsi_send_value(unsigned val)
{
  m_axi_writel(SYS_TOP_SUB_REG_CMD_VALUE, val);
  m_axi_writel(SYS_TOP_SUB_REG_CMD_CTRL,
            SYS_TOP_SUB_REG_CMD_CTRL_SEND);
  fsi_wait_cmd();
}

void fsi_sel_boards(unsigned boards)
{
  m_axi_writel(SYS_TOP_SUB_REG_CMD_BOARDS, boards);
}

unsigned fsi_get_reply(unsigned slot)
{
  return m_axi_readl(SYS_TOP_SUB_REG_CMD_REPLY | (slot * 4));
}


unsigned fsi_get_boards_present_mask(void)
{
  unsigned s1 = m_axi_readl(SYS_TOP_SUB_REG_PERIPH_STAT1);
  return~s1 & SYS_TOP_SUB_REG_PERIPH_STAT1_INP_MASK;
}
unsigned fsi_get_boards_off_mask(void)
{
  unsigned c1 = m_axi_readl(SYS_TOP_SUB_REG_CONFIG);
  return c1 & SYS_TOP_SUB_REG_CONFIG_PERIPH_OFF_MASK;
}


int fsi_acq_trig_type_get(uint32_t *t)
{
  //TODO: local config that must be kept somewhere on SB
  *t=1;
}

int fsi_periph_config_get(struct fsi_periph_config *c)
{
  int i = 0;
  unsigned boards_present = fsi_get_boards_present_mask();
  unsigned boards_enabled = ~ fsi_get_boards_off_mask();

  for (i = 0; i < 8; i++)
  {
    c[i].periph_enabled = 0x1 & (boards_enabled >> i);
  }
  //////////////////////////////////////////////////////////////////////////////
  // get afe gain
  //////////////////////////////////////////////////////////////////////////////
  fsi_sel_boards(0xFF);
  fsi_exec_cmd(0x59);
  fsi_exec_cmd(0x54);
  for (i = 0; i < 8; i++) 
  {
    if (!(boards_present & (1 << i))) 
    {
      /* Board not present.  */
      c[i].afe_gain = 0xFF;
      continue;
    }
    c[i].afe_gain = fsi_get_reply(i);
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // get adc pattern
  // diot_util> fsi_do_cmd boards 0xFF
  // diot_util> fsi_do_cmd value 0x25000000
  // diot_util> fsi_do_cmd exec 0x5c
  //////////////////////////////////////////////////////////////////////////////
  // NEED GW modifications
  for (i = 0; i < 8; i++) 
  {
    c[i].adc_config = 0xFF;// not available
  }
  //////////////////////////////////////////////////////////////////////////////
  // get acdc config
  //////////////////////////////////////////////////////////////////////////////
  fsi_sel_boards(0xFF); // all boards
  fsi_send_value(0x0);  // value   = 0x0
  fsi_exec_cmd(0x5d);   // command = acdc
  for (i = 0; i < 8; i++) 
  {
    if (!(boards_present & (1 << i))) 
    {
      /* Board not present.  */
      c[i].ch0_acdc_config = 0xFF;
      continue;
    }
    c[i].ch0_acdc_config = fsi_get_reply(i);
  }
  return 0;
}

int fsi_periph_status_get(struct fsi_periph_status *s)
{
  uint8_t spi_busy, triggers, servmod_in, fsi_ready, sys_locked, adc_locked , rx_locked;
  uint32_t stat1 = m_axi_readl(SYS_TOP_SUB_REG_PERIPH_STAT1);
  uint32_t stat2 = m_axi_readl(SYS_TOP_SUB_REG_PERIPH_STAT2);

  spi_busy   = (uint8_t)(( stat1 & SYS_TOP_SUB_REG_PERIPH_STAT1_SPI_BUSY_MASK )   >> SYS_TOP_SUB_REG_PERIPH_STAT1_SPI_BUSY_SHIFT);
  triggers   = (uint8_t)(( stat1 & SYS_TOP_SUB_REG_PERIPH_STAT1_TRIGGERS_MASK )   >> SYS_TOP_SUB_REG_PERIPH_STAT1_TRIGGERS_SHIFT);
  servmod_in = (uint8_t)((~stat1 & SYS_TOP_SUB_REG_PERIPH_STAT1_INP_MASK )        >> SYS_TOP_SUB_REG_PERIPH_STAT1_INP_SHIFT);
  fsi_ready  = (uint8_t)(( stat2 & SYS_TOP_SUB_REG_PERIPH_STAT2_FSI_READY_MASK )  >> SYS_TOP_SUB_REG_PERIPH_STAT2_FSI_READY_SHIFT);
  sys_locked = (uint8_t)(( stat2 & SYS_TOP_SUB_REG_PERIPH_STAT2_SYS_LOCKED_MASK ) >> SYS_TOP_SUB_REG_PERIPH_STAT2_SYS_LOCKED_SHIFT);
  adc_locked = (uint8_t)(( stat2 & SYS_TOP_SUB_REG_PERIPH_STAT2_ADC_LOCKED_MASK ) >> SYS_TOP_SUB_REG_PERIPH_STAT2_ADC_LOCKED_SHIFT);
  rx_locked  = (uint8_t)(( stat2 & SYS_TOP_SUB_REG_PERIPH_STAT2_RX_LOCKED_MASK )  >> SYS_TOP_SUB_REG_PERIPH_STAT2_RX_LOCKED_SHIFT);

  for (int i = 0; i < 8; i++)
  {
    s[i].bitmask = 0x0;
    s[i].bitmask = (0x1 & (spi_busy   >> i)) << 0 |
                   (0x1 & (triggers   >> i)) << 1 |
                   (0x1 & (servmod_in >> i)) << 2 |
                   (0x1 & (fsi_ready  >> i)) << 3 |
                   (0x1 & (sys_locked >> i)) << 4 |
                   (0x1 & (adc_locked >> i)) << 5 |
                   (0x1 & (rx_locked  >> i)) << 6 ;
  }
    return 0;
}

int fsi_dma_config_get(int bank, struct fsi_dma_config *c)
{
  if (bank < 0 || bank > 1)
    return -1;
  // address set per bank
  c->dma_addr   = m_axi_readl_bank(bank, SYS_TOP_SUB_REG_CONFIGS_ADDRESS);
  // bank length common to both banks
  c->dma_length = m_axi_readl(SYS_TOP_SUB_REG_LENGTH);

  return 0;
}

