#ifndef _RECEIVER_H
#define _RECEIVER_H

#define BIG	3000000
#define NCHANS	32

struct channel_acq {
	int		done;
	int		board;
	int		channel;
	int		nsamples;
	uint16_t	samples[BIG];
};

#endif /* _RECEIVER_H */
