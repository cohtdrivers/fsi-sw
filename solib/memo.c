/* vim: set ts=8 sw=8: */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/uio.h>

#include "fsi_tx.h"
#include "memo.h"

uint16_t fbb_16bw[FBB_SIZE_16BW];
uint32_t fbb_32bw[FBB_SIZE_32BW];
int verbose = 0;
int saw_check = 0; //enable check of received data in case the signal is known to be saw,
                   //i.e. if the board is configured in debug mode... and generates saw
                   //
void dump_start_packet(struct packet_fsi *pkt, char *timestamp)
{
	struct packet_header *p = &pkt->header;

	printf("timestamp: %s\n", timestamp);
	printf("thread_id:       %10d %08x\n", p->thread_id, p->thread_id);
	printf("packet_id:       %10d %08x\n", p->packet_id, p->packet_id);
	printf("payload_size:    %10d %08x\n", p->payload_size, p->payload_size);
	printf("expected_pkt_n:  %10d %08x\n", p->expected_pkt_n, p->expected_pkt_n);
	printf("chan:            %10d %08x\n", p->chan, p->chan);
	printf("board:           %10d %08x\n", p->board, p->board);
	printf("first_sample_id: %10d %08x\n", p->first_sample_id, p->first_sample_id);
	printf("sample_number:   %10d %08x\n", p->sample_number, p->sample_number);
}

void dump_header(struct packet_header *p)
{
	printf("id:%6d exp:%6d sz:%7d b:%03d ch%03d fsid: %6d sn: %8d\n",
		p->packet_id, p->expected_pkt_n, p->payload_size, p->board,
		p->chan, p->first_sample_id, p->sample_number);
}

/*
 * These functions take care of the wretched memory layout that the DIOT
 * acquisition board gives for the ADC samples.
 */
uint16_t unscramble_sample(unsigned board, unsigned channel, uint32_t *samp)
{
	/* not anymore b = 3 - ((board - 1) & 3); */
	unsigned b = 3 - ((board) & 3);		/* board = 01234567 -> b = 32103210 */
	unsigned ch = channel;

	uint64_t hi = samp[2 * b] | ((uint64_t)samp[2 * b + 1] << 32);
	uint32_t lo = samp[8 + b];
	uint16_t result = (((hi >> (8 * ch)) & 0xff) << 4) | ((lo >> (4 * ch)) & 0x0f);

	return result;
}

void unscramble_buffer(uint16_t *dst_16bw, uint32_t *src_32bw,
	int nsamples, unsigned board_msk, unsigned chan_msk)
{
	int bank_cur = 0;	// bank
	for (bank_cur = 0; bank_cur < 2; bank_cur++) {
		if (bank_cur == 0 && (board_msk & 0x0F) == 0)
			continue;
		if (bank_cur == 1 && (board_msk & 0xF0) == 0)
			continue;
		int bank_start = bank_cur * nsamples * 12;
		int s_cur;
		for (s_cur = 0; s_cur < nsamples; s_cur++) {
			int b_cur, chan;
			for (b_cur = 0; b_cur < 4; b_cur++) {	// for all boards in one bank
				int board = b_cur + bank_cur * 4;	// calculate board id (id starts at 1)
				if ((board_msk & (1 << board)) == 0)		/* skip unwanted boards */
					continue;
				for (chan = 0; chan < 8; chan++) {
					if ((chan_msk & (1 << chan)) == 0)	/* skip unwanted channels */
						continue;
					int src = bank_start + s_cur * 12;	// current offset
					int dst = (board * N_CHAN + chan) * N_MAX_SAMPLES + s_cur;
					fbb_16bw[dst] = unscramble_sample(b_cur, chan, &fbb_32bw[src]);
				}
			}
		}
	}	/* XXXX end of unscrambling: why
		 * the HELL is this not in a separate func? */
}

FILE *create_file_per_chan(int board, int chan, int sample_number, char *base_name)
{
    FILE *fptr;
    char filename[50];

    sprintf(filename, "%s-b%d-ch%d.dat",base_name, board, chan);
    fptr = fopen(filename,"w");
    fprintf(fptr,"#Acquisition for:\n");
    fprintf(fptr,"#Board   number: %d\n", board);
    fprintf(fptr,"#Channel number: %d (per board)\n", chan);
    fprintf(fptr,"#Sample  number: %d (per channel)\n", sample_number);
    fprintf(fptr,"#----------------------------------\n");
    return fptr;
}

void write_to_file_entire_chan(FILE *fptr, uint16_t *buf, int board, int chan, int n_samples)
{
    int i;
    
    for (i=0;i<n_samples; i++)
        fprintf(fptr,"%d\n",buf[i]);
}

void copy_to_cruncher(int b, int ch, int samples, void *buf, void *acq_area_addr)
{
	return;
}

uint64_t get_tics()
{
	struct timeval tv;

	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000000ULL + tv.tv_usec;
}

void check_saw_signal(int nchans, struct fsi_chan_acq_header *result)
{
	int i, j;
	uint16_t last=0;
        uint16_t expected=0;

	for (i = 0; i < nchans; i++) {
		struct fsi_chan_acq_header *ch = &result[i];
                last = ch->data[0];
		for (j=1;j < ch->n; j++) {
			if(last == 4095)
				expected = 0;
                        else
				expected = last + 1;
			if (ch->data[j] != expected) {
				printf("[Board = %d, chan=%d, sample = %d] "
					"value = %d | expected value =%d\n",
					ch->board, ch->channel, j, ch->data[j],
					expected);
                        }
                        last = ch->data[j];
                }
	}
	printf("Data OK\n");
}

void save_files(int nchans, struct fsi_chan_acq_header *result);

/*
 * Function that manages establishing TCP or UDP server that waits for clients
 * to connect.
 *
 * Arguments:
 * *sfd  - handle to return the socket descriptor
 * proto - defines which type of protocol to use: UDP or TCP
 *
 * Return:
 * int   - descriptor to be passed to the fsi_receive_acquisition() function
 *         - for UDP, it is socket descriptor
 *         - for TCP, it is the connection description
 */
int fsi_connect(int *sfd, int proto)
{
	int sockfd, connfd;
	int ret = 0;
	struct sockaddr_in servaddr, cli;
	unsigned addr_size = sizeof(cli);
	*sfd = 0;

	/* socket creation and verification */
	if (proto == C_UDP){
		if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
			ret = FSI_SOCKET_ERROR;
			goto out;
                }
		ret = sockfd;// For UDP, socket descriptor is returned
	}
	if (proto == C_TCP) {
		if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
			ret = FSI_SOCKET_ERROR;
			goto out;
                }
		// For TCP, socket descriptor is not retunred here but 
		// connection descriptor is returned later
	}
	*sfd = sockfd;

	/* specify the total per-socket buffer space reserved for receives  */
	int recv_buff = (1<<25);	/* 2^25 = 32MB */
	int yes = 1;
	int res = setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &recv_buff,
			     sizeof(recv_buff));
	if (res < 0) {
		ret = FSI_SOCKET_BUF_ERROR;
		goto out2;
	}
#ifndef MEMO_MAIN
	//use onlyl when this function is called from the data-cranching application.
	//if running with main, we want to receive data continously
	/* 10 sec timeout for stalled communication */
	struct timeval timeout = { 10, 0, };
#else
        struct timeval timeout = {  0, 0, };
#endif
	res = setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &timeout,
			     sizeof(timeout));
	if (res < 0) {
		ret = FSI_SOCKET_CFGTIMEO_ERROR;
		goto out2;
	}

	res = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

	/* assign IP, PORT */
	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(PORT);
	/* bind newly created socket to given IP and verify */
	if ((bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr))) != 0) {
		ret = FSI_CANNOT_BIND;
		goto out2;
	}
	if (proto == C_TCP) {
		if ((listen(sockfd, 1)) != 0) {
			ret = FSI_SOCKET_LISTEN_ERROR;
			goto out2;
		}
		if(verbose)
			printf ("Waiting for connections on port %d ... \n",PORT);

		connfd = accept(sockfd, (struct sockaddr *)&cli, &addr_size);
		if (connfd < 0) {
			ret = FSI_SOCKET_ACCEPT_ERROR;
			goto out2;
		}
		ret = connfd; // For TCP, socket descriptor is returned
		if(verbose)
			printf("Server accept the client...\n");
	}
	goto out;
	out2:
		close(sockfd);
	out:
		return ret;
	
}
/*
 * Function that closes socket openned by fsi_connect.
 *
 * Argument:
 * sockfd - socket descriptor
 *
 * Return:
 * int    - currently always zero
 */
int fsi_disconnect(int sockfd)
{
	close(sockfd);
	return 0;
}
	
int fsi_receive_acquisition(unsigned board_msk, unsigned chan_msk,
	struct fsi_chan_acq_header *hdr, int proto, int fd, uint64_t t_arr[])
{
	int ret = 0;
	int sockfd = (proto == C_UDP) ? fd : 0; // for UDP it is socket fd
	int connfd = (proto == C_TCP) ? fd : 0; // for TCP it is connecitn fd
	struct sockaddr_in cli;
	unsigned addr_size = sizeof(cli);
	static char buf[RX_BUFFER_SIZE_BYTES];
	int pkt_n = 0;
	uint64_t rx_bytes = 0;
	int n = 0;
	int lost_packets_total = 0;
	int s_per_ch = 0;	// samples per channel
	int expected_payload = 0;
	int start_offset = 0;	//start offset dependign on the bank
	int which_boards_mask = 0;
	int expected_data_size = 0;
	char filename[30];
	char timestamp[30];

	/* zero-in static rx buffers */
	memset(fbb_16bw, 0, sizeof(fbb_16bw));
	memset(fbb_32bw, 0, sizeof(fbb_32bw));

	pkt_n = 0;
	rx_bytes = 0;

	struct packet_fsi *pkt = (struct packet_fsi *) buf;

	t_arr[T_SERVER_RDY] = get_tics();
	/* do *one* scan UDP retrieval */
	/* recv  "start transmission packet" */
	if (proto == C_UDP) {
		n = recvfrom(sockfd, buf, sizeof(buf), 0,
				(struct sockaddr *)&cli, &addr_size);
		if (verbose)
			printf("UDP: n=%d [bytes]\n", n);
	}
	if (proto == C_TCP) {
		n = recv(connfd, buf, sizeof(struct packet_header) ,0);
		if (verbose)
			printf("TCP: n=%d [bytes]\n", n);
	}
	t_arr[T_INIT_PKT_RXED] = get_tics();
	if (n == -1) {
		ret = FSI_SOCKET_RECVFROM_TIMEOUT;
		fprintf(stderr,
			"memo.so: FSI_SOCKET_RECVFROM_TIMEOUT errno %d = %s\n",
			errno, strerror(errno));
		goto out2;
	} else if (n == 0) {
		ret = FSI_SOCKET_NOCONN;
		fprintf(stderr,
			"memo.so: FSI_SOCKET_NOCONN errno %d = %s\n",
			errno, strerror(errno));
		goto out2;
	}

	s_per_ch = pkt->header.sample_number;
	which_boards_mask = pkt->header.board;
	expected_payload = pkt->header.payload_size;
	expected_data_size = pkt->header.sample_number * 12 * 4;//[bytes]

	/* create timestamp and filename */
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	strftime(timestamp, sizeof(timestamp), "%Y%m%dT%H%M%S", &tm);
	strftime(filename, sizeof(filename), "fsi_acq_%Y%m%dT%H%M%S", &tm);

	/* indicate start of acq parameters */
	if (verbose)
		dump_start_packet(pkt, timestamp);

	/* FIXME: this is bound not to work if we deal with
	 * boards that live in BOTH banks, unless the override
	 * of which_boards_mask in the second case is sooo
	 * clever as to do the right thing. I doubt it.
	 *
	 * Investigate!
	 */
	if (which_boards_mask & 0xF0)
		start_offset = s_per_ch * 12;	// if bank 1 starts at this address
	if (which_boards_mask & 0x0F)
		start_offset = 0;	// if bank 0 starts at this address

	if(proto == C_TCP)
	do {
		n =recv(connfd, &fbb_32bw[start_offset], sizeof(fbb_32bw),0);
		/* prepare next acquisition? */
		start_offset += n/sizeof(uint32_t);
		rx_bytes += n;
		if (verbose)
				printf("TCP: n=%d [bytes], total %ld out of %d [bytes]\n",
				n, rx_bytes, expected_data_size);
		if ( rx_bytes == expected_data_size)
			break;
	} while (n > 0);

	struct packet_header h_cur;
	if (proto == C_UDP)
        do {
		struct iovec iov[2];
		int iovcnt = sizeof(iov) / sizeof(struct iovec);

		iov[0].iov_base = &h_cur;
		iov[0].iov_len = sizeof(h_cur);
		iov[1].iov_base = &fbb_32bw[start_offset];
		iov[1].iov_len = expected_payload;
		ssize_t iovlen = iov[0].iov_len + iov[1].iov_len;

		int lost_packets = 0;

		if ((n = readv(sockfd, iov, iovcnt)) == -1) {
			perror("readv timed out:");
			lost_packets_total += 1;
			ret = FSI_SOCKET_READV_TIMEOUT;
			goto out3;
		} else if (n != iovlen) {
			/* this is not a problem: this must
			 * nearly always
			 * happen at the last readv(), meaning end of the block!
			   printf("readv() problem: read %d bytes of %zu\n", n, iovlen);
			   return FSI_READV_PROBLEM; */
		}

		/* handle packet loss */
		lost_packets = h_cur.packet_id - pkt_n;
		lost_packets_total += lost_packets;
		while (lost_packets-- > 0) {
			memset(&fbb_32bw[start_offset], 0, sizeof(uint16_t) * h_cur.sample_number);
			start_offset += h_cur.sample_number * 12;
		}

		/* prepare next acquisition? */
		start_offset += h_cur.sample_number * 12;
		rx_bytes += n;
		pkt_n = h_cur.packet_id;
		pkt_n++;

	} while (pkt_n < h_cur.expected_pkt_n);

	t_arr[T_DATA_RXED] = get_tics();

	/* do the unscrambling of the horrendous sample layout
	 * coming from the ADC
	 */
	unscramble_buffer(fbb_16bw, fbb_32bw, s_per_ch, board_msk, chan_msk);

	int nchans, board, chan;
	nchans = 0;
	for (board = 0; board < 8; board++) {
		if (!(board_msk & (1 << board)))
			continue;
		for (chan = 0; chan < 8; chan++) {
			if (!(chan_msk & (1 << chan)))
				continue;

			/* FIXME: it is HERE where actual samples are saved */
			int offset = (board * N_CHAN + chan) * N_MAX_SAMPLES;
			struct fsi_chan_acq_header *hdrp = &hdr[nchans++];

			memset(hdrp, 0, sizeof(*hdrp));
			hdrp->bom = 0xaabbccdd;
			hdrp->board = board + 1;
			hdrp->channel = chan;
			strncpy(hdrp->timestamp, timestamp, sizeof(hdrp->timestamp));
			hdrp->n = s_per_ch;
			hdrp->data = &fbb_16bw[offset];
			if (verbose)
				printf("report: b%d:c%d, samples %d at offset %d (%p)\n",
					board + 1, chan, s_per_ch,
					offset, &fbb_16bw[offset]);
		}
	}
	t_arr[T_DATA_UNSCRB] = get_tics();
	t_arr[D_PKTS_RXED]   = pkt_n-lost_packets_total;
	t_arr[D_BYTES_RXED]  = rx_bytes;

	//if the board is configured in debug mode... and generates saw 
	if(saw_check)
		check_saw_signal(nchans, hdr);
	out3:
		if (lost_packets_total > 0)
			ret = -lost_packets_total;
		else
			ret = nchans;
	out2:
		return ret;
}
int fsi_print_stats(double stats[])
{
	//provide some useful info when free running app
	fprintf(stderr, "--> rx-ed: ");
        fprintf(stderr, "data=%.0f [bytes] + ",stats[ST_DATA]);
        fprintf(stderr, "overhd=%.0f [bytes] ", stats[ST_OVERHD]);
	fprintf(stderr, "| rx at %.3f Gbps ", stats[ST_GBPS]);
	fprintf(stderr, "| dur: %.3f s = ",   stats[ST_ACQ]);
	fprintf(stderr, "[trans] %.3f + ",    stats[ST_TRANS]);
	fprintf(stderr, "[unscr] %.3f + ",    stats[ST_UNSCR]);
	fprintf(stderr, "[procs] %.3f + ",    stats[ST_PROC] );
	fprintf(stderr, "| cycle: %.3f",      stats[ST_CYCLE]);
	fprintf(stderr, "\n");
	return 0;
}

int fsi_performance_stats(uint64_t t_arr[], double stats[])
{
	t_arr[T_ACQ_DONE] = get_tics();
	int pkt_h_size = sizeof(struct packet_header);
	// packet header size to be subtracted from received data (zero for TCP)
	stats[ST_OVERHD]   = (double)(pkt_h_size * t_arr[D_PKTS_RXED]);
	
	stats[ST_DATA]     = (double)(t_arr[D_BYTES_RXED] - stats[ST_OVERHD] );

	// Duratino: entire cycle - only makes sense if periodic acquisition
	stats[ST_CYCLE]    = (double)(t_arr[T_ACQ_DONE]     -t_arr[T_SERVER_RDY])/1e6;

	// Acquisition: from first frame to saved/processed data
	stats[ST_ACQ]      = (double)(t_arr[T_ACQ_DONE]     -t_arr[T_INIT_PKT_RXED])/1e6;

	// Transmission only
	stats[ST_TRANS]    = (double)(t_arr[T_DATA_RXED]    -t_arr[T_INIT_PKT_RXED])/1e6;

	// Unscrambling only
	stats[ST_UNSCR]    = (double)(t_arr[T_DATA_UNSCRB]  -t_arr[T_DATA_RXED])/1e6;

	// data processing: either saving or crunching (assuming this happens before this function is called)
	stats[ST_PROC]    = (double)(t_arr[T_ACQ_DONE]     -t_arr[T_DATA_UNSCRB])/1e6;

	// speed of transmission
	double total_MBps  = (double)t_arr[D_BYTES_RXED] / stats[ST_TRANS] / MB_factor;
	stats[ST_GBPS]     = (total_MBps*8.0)/1024.0;

	return 0;
}

static struct fsi_chan_acq_header hdr[N_BOARD*N_CHAN];

void save_files(int nchans, struct fsi_chan_acq_header *result)
{
	int i;
	char fname[30];

	snprintf(fname, sizeof(fname), "fsi_acq_%s", result->timestamp);
	
	for (i = 0; i < nchans; i++) {
		struct fsi_chan_acq_header *ch = &result[i];
		FILE *f = create_file_per_chan(ch->board, ch->channel, ch->n, fname);
		write_to_file_entire_chan(f, ch->data, ch->board, ch->channel, ch->n);
		fclose(f);
	}
}

int memo_main(int argc, char *argv[])
{
	int nchans = 0;
	unsigned board_mask;
        int proto = DEFAULT_CONNECTION_TYPE;
        uint64_t perf[T_SIZE]; //array to capture performance
        double stats[DEFAULT_ACQ_NUM][ST_SIZE]; // array to capture stats
        int i=0, fd=0;
        int sockfd = 0;
	struct fsi_chan_acq_header *result = hdr;

	if (argc == 1)
		board_mask = 0xf0;		/* default: pick all boards in bank 1 */
	if (argc == 2 || argc == 3) {
		board_mask = strtoul(argv[1], NULL, 16);
		if (board_mask == 0) {
			fprintf(stderr, "usage: %s [board_mask] [proto: tcp/upd]\n", argv[0]);
			exit(1);
		}
	}
	if (argc == 3) {
		if(strcmp(argv[2], "udp") == 0)
			proto = C_UDP;
		else if(strcmp(argv[2], "tcp") == 0)
			proto = C_TCP;
		else {
			fprintf(stderr, "usage: %s [board_mask] [proto: tcp/upd]\n", argv[0]);
			exit(1);
		}
	}
	fd=fsi_connect(&sockfd, proto);
	if(fd < 0)
	{
		fprintf(stderr, "error %d in fsi_connect()\n", fd);
		exit(1);
	}
	for (i=0; i< DEFAULT_ACQ_NUM; i++){
		nchans = fsi_receive_acquisition(board_mask, 0xff, result, proto, fd, perf);
		if (nchans <= 0)
			fprintf(stderr, "error %d in fsi_receive_acquisition()\n", nchans);
		else
			save_files(nchans, result);
                fsi_performance_stats(perf, stats[i]);
                fsi_print_stats(stats[i]);
	}
	fsi_disconnect(sockfd);
	return nchans;
}
