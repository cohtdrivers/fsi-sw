/* vim: set ts=8 sw=8: */

int main(int argc, char *argv[])
{
	extern int memo_main(int argc, char *argv[]);

	return memo_main(argc, argv);
}
