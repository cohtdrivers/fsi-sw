CC ?= gcc
CROSS_COMPILE ?= aarch64-none-linux-gnu-
CFLAGS = -std=c99 -g
LDLIBS = -lpthread
PETALINUX_SDK ?= /opt/petalinux-2019.2-diot-sdk
DIOT_ARCH = aarch64-xilinx-linux
X86_64_SYSROOT = $(PETALINUX_SDK)/sysroots/x86_64-petalinux-linux
ARM64_SYSROOT = $(PETALINUX_SDK)/sysroots/aarch64-xilinx-linux

# build diot binaries only when Petalinux SDK is present
ifeq ($(HAVE_DIOT_SDK),y)
    DIOT_BINS = target_all
else
    DIOT_BINS =
endif

.PHONY: all solib clean host_all target_all

all: host_all $(DIOT_BINS)
host_all: fsi_bandwidth_test memo memo.so
target_all: fsi_tx
target_all: CROSS_COMPILE = $(X86_64_SYSROOT)/usr/bin/$(DIOT_ARCH)/$(DIOT_ARCH)-
target_all: CC = $(CROSS_COMPILE)gcc --sysroot $(ARM64_SYSROOT)

memo.o: memo.h
memo.o: CFLAGS+=-Wall -fPIC
memo.so: memo.o
	$(CC) $(CFLAGS) $^ -shared -o $@ $(LDFLAGS)
memo: memo-main.o memo.o
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)


fsi_bandwidth_test: CFLAGS+=-DFSI_MAIN
fsi_bandwidth_test.o: diot_fsi_regs.h fsi_bandwidth_test.h
fsi_bandwidth_test: fsi_bandwidth_test.o
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS) $(LDLIBS)
	
fsi_tx: CFLAGS+=-DFSI_MAIN
fsi_tx: fsi_diot_hw.o fsi_diags.o fsi_tx.o
	$(CC) $(CFLAGS) $^ -o $@ 

fsi_tx.o: fsi_tx.c 
	$(CC) $(CFLAGS) -c $< -o $@

fsi_diot_hw.o: fsi_diot_hw.c
	$(CC) $(CFLAGS) -c $< -o $@

fsi_diags.o: fsi_diags.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f *.[oa] *.so
	rm -f fsi_bandwidth_test memo fsi_tx fsi-util
clean-data:
	rm -f fsi_acq_*.dat
