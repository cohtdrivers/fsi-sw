// Client side implementation of UDP client-server model
#define _GNU_SOURCE /* See feature_test_macros(7) */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <stdint.h>
#include <pthread.h>

uint64_t get_tics()
{
    struct timezone tz = {0, 0};
    struct timeval tv;

    gettimeofday(&tv, &tz);
    return tv.tv_sec * 1000000ULL + tv.tv_usec;
}

#define PORT 28080

int n_threads = 4;
int n_cpus = 2;

struct thread_state
{
    int id;
    uint64_t bytes_transferred;
    double duration;
    pthread_t thread;
};

void *worker_thread(void *p)
{
    struct thread_state *st = (struct thread_state *)p;

    st->bytes_transferred = 0;
    int sockfd, connfd, len;
    struct sockaddr_in servaddr, cli;
    char buf[1048576];
    int num_cores = n_cpus; //sysconf(_SC_NPROCESSORS_ONLN);

    int core_id = st->id % num_cores;

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(core_id, &cpuset);

    pthread_t current_thread = pthread_self();
    pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
    printf("Thread affinity %d->%d\n", st->id, core_id);
    do
    {
        // socket create and verification
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd == -1)
        {
            printf("socket creation failed...\n");
            exit(0);
        }

    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int)) < 0)
       printf("setsockopt(SO_REUSEADDR) failed");

     int recv_buff = 1048576;

    int res = setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &recv_buff, sizeof(recv_buff));

    printf("RV %d\n", res );


        bzero(&servaddr, sizeof(servaddr));

        // assign IP, PORT
        servaddr.sin_family = AF_INET;
        servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
        servaddr.sin_port = htons(PORT + st->id);

        // Binding newly created socket to given IP and verification
        if ((bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr))) != 0)
        {
            printf("socket bind failed...\n");
            exit(0);
        }

        // Now server is ready to listen and verification
        if ((listen(sockfd, 5)) != 0)
        {
            printf("Listen failed...\n");
            exit(0);
        }
        else
            printf("[thread %d] Server listening @ port %d..\n", st->id, PORT + st->id);
        for (;;)
        {

            len = sizeof(cli);

            // Accept the data packet from client and verification
            connfd = accept(sockfd, (struct sockaddr *)&cli, &len);
            if (connfd < 0)
            {
                printf("server accept failed...\n");
                exit(0);
            }
            else
                printf("[thread %d] server accept the client...\n", st->id);

            // Function for chatting between client and server
            for (;;)
            {
                int n = recv(connfd, buf, sizeof(buf), 0);

                //printf("rx %d\n", n );
                if (n <= 0)
                    break;
            }
            printf("[thread %d] connection closed...\n", st->id);

            // After chatting close the socket
            close(connfd);
        }
    } while (1);

    return 0;
}

// Driver code
int main(int argc, char *argv[])
{
    int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
    if (argc < 2)
    {
        printf("usage: %s threads\n", argv[0]);
        return -1;
    }

    n_threads = atoi(argv[1]);
    
    if (argc == 3)
    {
        n_cpus = atoi(argv[2]);
        if (n_cpus > num_cores)
        {
          n_cpus = num_cores;
          printf("Trying to use too many CPUs, seeting n_cpus to max=%d",n_cpus);
        }
    } 
    else
        n_cpus  = num_cores;

    struct thread_state thr[16];
    int i;

    printf("Running with %d threads distributed over %d CPUs\n", n_threads, n_cpus);

    for (i = 0; i < n_threads; i++)
    {
        thr[i].id = i;
        pthread_create(&thr[i].thread, NULL, worker_thread, &thr[i]);
    }

    for (i = 0; i < n_threads; i++)
    {
        pthread_join(thr[i].thread, NULL);
    }

    return 0;
}