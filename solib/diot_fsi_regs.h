#ifndef __CHEBY__SYS_TOP_SUB_REG__H__
#define __CHEBY__SYS_TOP_SUB_REG__H__
#define SYS_TOP_SUB_REG_SIZE 164 /* 0xa4 */

/* Identification and version */
#define SYS_TOP_SUB_REG_IDENT 0x0UL
#define SYS_TOP_SUB_REG_IDENT_PRESET 0x46534932UL

/* None */
#define SYS_TOP_SUB_REG_STATUS 0x4UL
#define SYS_TOP_SUB_REG_STATUS_DMA_DONE 0x8UL  // Set at the end of DMA transfer, write 1 to clear
#define SYS_TOP_SUB_REG_STATUS_SAMPLING 0x4UL  // Peripheral boards are sending sample
#define SYS_TOP_SUB_REG_STATUS_READY 0x2UL     // All peripheral boards connected are ready
#define SYS_TOP_SUB_REG_STATUS_TRANSFER 0x1UL  // Data trasnfer (DMA) is in progress

/* None */
#define SYS_TOP_SUB_REG_CONTROL 0x8UL
#define SYS_TOP_SUB_REG_CONTROL_SW_TRIG 0x1UL
#define SYS_TOP_SUB_REG_CONTROL_PRBS 0x2UL

/* None */
#define SYS_TOP_SUB_REG_CONFIG 0xcUL
#define SYS_TOP_SUB_REG_CONFIG_PERIPH_OFF_MASK 0xff00UL
#define SYS_TOP_SUB_REG_CONFIG_PERIPH_OFF_SHIFT 8
#define SYS_TOP_SUB_REG_CONFIG_CLK_SEL_MASK 0x60UL
#define SYS_TOP_SUB_REG_CONFIG_CLK_SEL_SHIFT 5
#define SYS_TOP_SUB_REG_CONFIG_RST_N 0x10UL
#define SYS_TOP_SUB_REG_CONFIG_RESYNC 0x4UL
#define SYS_TOP_SUB_REG_CONFIG_PATTERNS 0x2UL
#define SYS_TOP_SUB_REG_CONFIG_TRIG_EN 0x1UL

/* Number of samples for a data transfer */
#define SYS_TOP_SUB_REG_LENGTH 0x10UL

/* None */
#define SYS_TOP_SUB_REG_CONFIGS 0x20UL
#define SYS_TOP_SUB_REG_CONFIGS_SIZE 12 /* 0xc */

/* None */
#define SYS_TOP_SUB_REG_CONFIGS_ADDRESS 0x0UL

/* None */
#define SYS_TOP_SUB_REG_CONFIGS_CUR_ADDRESS 0x4UL

/* None */
#define SYS_TOP_SUB_REG_CONFIGS_STATUS 0x8UL
#define SYS_TOP_SUB_REG_CONFIGS_STATUS_AXI_MASK 0xfUL
#define SYS_TOP_SUB_REG_CONFIGS_STATUS_AXI_SHIFT 0
#define SYS_TOP_SUB_REG_CONFIGS_STATUS_FIFO_ERRS_MASK 0xff0000UL
#define SYS_TOP_SUB_REG_CONFIGS_STATUS_FIFO_ERRS_SHIFT 16
#define SYS_TOP_SUB_REG_CONFIGS_STATUS_FIFO_MAX_MASK 0xff00UL
#define SYS_TOP_SUB_REG_CONFIGS_STATUS_FIFO_MAX_SHIFT 8

/* None */
#define SYS_TOP_SUB_REG_PERIPH_STAT1 0x40UL
#define SYS_TOP_SUB_REG_PERIPH_STAT1_SPI_BUSY_MASK 0xff0000UL
#define SYS_TOP_SUB_REG_PERIPH_STAT1_SPI_BUSY_SHIFT 16
#define SYS_TOP_SUB_REG_PERIPH_STAT1_TRIGGERS_MASK 0xff00UL
#define SYS_TOP_SUB_REG_PERIPH_STAT1_TRIGGERS_SHIFT 8
#define SYS_TOP_SUB_REG_PERIPH_STAT1_INP_MASK 0xffUL
#define SYS_TOP_SUB_REG_PERIPH_STAT1_INP_SHIFT 0

/* None */
#define SYS_TOP_SUB_REG_PERIPH_STAT2 0x44UL
#define SYS_TOP_SUB_REG_PERIPH_STAT2_FSI_READY_MASK 0xff000000UL
#define SYS_TOP_SUB_REG_PERIPH_STAT2_FSI_READY_SHIFT 24
#define SYS_TOP_SUB_REG_PERIPH_STAT2_SYS_LOCKED_MASK 0xff0000UL
#define SYS_TOP_SUB_REG_PERIPH_STAT2_SYS_LOCKED_SHIFT 16
#define SYS_TOP_SUB_REG_PERIPH_STAT2_ADC_LOCKED_MASK 0xff00UL
#define SYS_TOP_SUB_REG_PERIPH_STAT2_ADC_LOCKED_SHIFT 8
#define SYS_TOP_SUB_REG_PERIPH_STAT2_RX_LOCKED_MASK 0xffUL
#define SYS_TOP_SUB_REG_PERIPH_STAT2_RX_LOCKED_SHIFT 0

/* None */
#define SYS_TOP_SUB_REG_CMD_BOARDS 0x48UL
#define SYS_TOP_SUB_REG_CMD_BOARDS_SEL_MASK 0xffUL
#define SYS_TOP_SUB_REG_CMD_BOARDS_SEL_SHIFT 0

/* Value to be sent to the boards */
#define SYS_TOP_SUB_REG_CMD_VALUE 0x4cUL

/* Control for commands and values */
#define SYS_TOP_SUB_REG_CMD_CTRL 0x50UL
#define SYS_TOP_SUB_REG_CMD_CTRL_SEND 0x2UL
#define SYS_TOP_SUB_REG_CMD_CTRL_EXEC 0x1UL

/* Status for commands and values */
#define SYS_TOP_SUB_REG_CMD_STATUS 0x54UL
#define SYS_TOP_SUB_REG_CMD_STATUS_DONE_MASK 0xff00UL
#define SYS_TOP_SUB_REG_CMD_STATUS_DONE_SHIFT 8
#define SYS_TOP_SUB_REG_CMD_STATUS_ERR 0x1UL

/* None */
#define SYS_TOP_SUB_REG_CMD_REPLY 0x60UL
#define SYS_TOP_SUB_REG_CMD_REPLY_SIZE 4 /* 0x4 */

/* None */
#define SYS_TOP_SUB_REG_CMD_REPLY_VALUE 0x0UL

/* Statistic selection */
#define SYS_TOP_SUB_REG_STAT_SEL 0x80UL
#define SYS_TOP_SUB_REG_STAT_SEL_CH_MASK 0xfUL
#define SYS_TOP_SUB_REG_STAT_SEL_CH_SHIFT 0
#define SYS_TOP_SUB_REG_STAT_SEL_BOARD_MASK 0x70UL
#define SYS_TOP_SUB_REG_STAT_SEL_BOARD_SHIFT 4
#define SYS_TOP_SUB_REG_STAT_SEL_DELAY 0x100UL
#define SYS_TOP_SUB_REG_STAT_SEL_BRANCH 0x200UL
#define SYS_TOP_SUB_REG_STAT_SEL_CLEAR 0x80000000UL

/* Number of ticks since last stat reset */
#define SYS_TOP_SUB_REG_STAT_CLOCK 0x88UL

/* Per lane bit lock failure counter */
#define SYS_TOP_SUB_REG_STAT_BIT_LOCK 0x90UL

/* Per lane byte lock failure counter */
#define SYS_TOP_SUB_REG_STAT_BYTE_LOCK 0x94UL

/* Per lane PRBS errors */
#define SYS_TOP_SUB_REG_STAT_PRBS 0x98UL

/* delay control */
#define SYS_TOP_SUB_REG_DELAY_CTRL 0x9cUL
#define SYS_TOP_SUB_REG_DELAY_CTRL_VTC_EN 0x1UL
#define SYS_TOP_SUB_REG_DELAY_CTRL_FREEZE 0x2UL
#define SYS_TOP_SUB_REG_DELAY_CTRL_INC 0x10UL
#define SYS_TOP_SUB_REG_DELAY_CTRL_DEC 0x20UL

/* Per delay cell current value */
#define SYS_TOP_SUB_REG_DELAY_CNTVALUE 0xa0UL

struct sys_top_sub_reg {
  /* [0x0]: REG (ro) Identification and version */
  uint32_t ident;

  /* [0x4]: REG (ro) (no description) */
  uint32_t status;

  /* [0x8]: REG (rw) (no description) */
  uint32_t control;

  /* [0xc]: REG (rw) (no description) */
  uint32_t config;

  /* [0x10]: REG (rw) Number of samples for a data transfer */
  uint32_t length;

  /* padding to: 8 words */
  uint32_t __padding_0[3];

  /* [0x20]: REPEAT (no description) */
  struct configs {
    /* [0x0]: REG (rw) (no description) */
    uint32_t address;

    /* [0x4]: REG (ro) (no description) */
    uint32_t cur_address;

    /* [0x8]: REG (ro) (no description) */
    uint32_t status;
  } configs[2];

  /* bug? manual addition to cheby's output - padding to 16 words */
  uint32_t __padding_manual_0[2];
  /* [0x40]: REG (ro) (no description) */
  uint32_t periph_stat1;

  /* [0x44]: REG (ro) (no description) */
  uint32_t periph_stat2;

  /* [0x48]: REG (rw) (no description) */
  uint32_t cmd_boards;

  /* [0x4c]: REG (rw) Value to be sent to the boards */
  uint32_t cmd_value;

  /* [0x50]: REG (wo) Control for commands and values */
  uint32_t cmd_ctrl;

  /* [0x54]: REG (rw) Status for commands and values */
  uint32_t cmd_status;

  /* padding to: 24 words */
  uint32_t __padding_1[2];

  /* [0x60]: REPEAT (no description) */
  struct cmd_reply {
    /* [0x0]: REG (ro) (no description) */
    uint32_t value;
  } cmd_reply[8];

  /* [0x80]: REG (rw) Statistic selection */
  uint32_t stat_sel;

  /* padding to: 34 words */
  uint32_t __padding_2[1];

  /* [0x88]: REG (ro) Number of ticks since last stat reset */
  uint64_t stat_clock;

  /* [0x90]: REG (ro) Per lane bit lock failure counter */
  uint32_t stat_bit_lock;

  /* [0x94]: REG (ro) Per lane byte lock failure counter */
  uint32_t stat_byte_lock;

  /* [0x98]: REG (ro) Per lane PRBS errors */
  uint32_t stat_prbs;

  /* [0x9c]: REG (rw) delay control */
  uint32_t delay_ctrl;

  /* [0xa0]: REG (ro) Per delay cell current value */
  uint32_t delay_cntvalue;
};

#endif /* __CHEBY__SYS_TOP_SUB_REG__H__ */
