/*
 * Copyright CERN 2023
 * Author: Maciej Marek Lipinski <mlipinsk _at_ cern.ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef __FSI_TXRX_LIB_H__
#define __FSI_TXRX_LIB_H__

#include <stdint.h>

#define FSI_PKT_MAGIC_NUM    0xDEADCAFE
#define FSI_PKT_VER          1

#define FSI_PKT_TYPE_CONFIG  1
#define FSI_PKT_TYPE_DIAG    2
#define FSI_PKT_TYPE_DATA    3	/* only UPD */

#define FSI_DTYPE_MEAS       1	/* acquired data */
#define FSI_DTYPE_TEST_SAW   2	/* sending saw packet */
#define FSI_DTYPE_MEAS_NEWP  3	/* new type of packing the bits/bytes.. */


/** common header used in all fsi packets except when transmitting data using TCP */
struct fsi_common_header {
	uint32_t	magic_number;	/**< e.g.: 0xDEADCAFE - to ensure we read FSI packet */
	uint32_t	message_type;	/**< types:
					 *	1) params_packet
					 *	2) data_packet
					 *	3) diag_packet
					 *	4) ver_packet
					 *	5) ctrl_packet
					 */
	uint32_t	message_ver;	/**< version of the layout of the message type that follows */
	uint32_t	trans_id;	/**< transmission ID (could be based on timestamp) */
};

/* ****************************************************************************
 * 1) params_packet
 * ****************************************************************************
 * packet sent to provide to the server the config/parameters of DIOT/transmission
 * (info useful for reception and data crunching)
 */

struct fsi_params_packet {
	struct fsi_common_header
			hdr;				/**< see above */
	uint32_t	samples_per_chan;		/**< configured number of samples to be acquired */
	uint32_t	boards_mask;			/**< tell which boards were used for acquisition
								(*AND* of enabled and present boards) */
	uint8_t		dtype[8];			/**< data sent, could be different per board (not chan) */
	uint32_t	data_size_bytes;		/**< size [Bytes] of FSI data sent at once
								1) UDP: in payload on a singel packet
								2) TCP: in one go (full bank) */
	uint32_t	pkt_expected_num;		/**< expected number of UPD packets or 1 for TCP */
};

/* ****************************************************************************
 * 2) data_packet
 * ****************************************************************************
 *
 * packet used to transmit data when using UDP
 */
struct fsi_data_packet_udp {
	struct fsi_common_header
			hdr;				/**< see above */
	uint32_t	packet_id;			/**< id of first sample */
	uint32_t	pkt_expected_num;		/**< expected number of UPD pkts (useful redundancy?) */
	uint32_t	first_sample_id;		/**< number of samples in the packet */
	uint32_t	fsi_data[];			/**< samples */
};

/*  just for reference, not really to be used. When TCP is used, the raw
 *  data is directly pushed on one side and pops up on the other.
 */
#if 0
struct fsi_data_packet_tcp {
  uint32_t fsi_data[];			/* samples */
};
#endif


/*
 * ****************************************************************************
 * 3) diag_packet
 * ****************************************************************************
 * struct fsi_diag_packet {
 *   struct fsi_common_header hdr;         // see above
 *   struct fsi_diag_hw       hw;          // see below
 *   struct fsi_diag_trans    trans;       // see below
 *   struct fsi_diag_acq      acq;         // see below
 *   uint32_t custom[FSI_CUSTOM];
 *   uint32_t reserved[FSI_RESERVED];
 * }
 * NOTE: the structure is put here commented out for readability, this header
 * first defines the structures used inside the fsi_diag_packet, then the
 * structure is defined.
*/

/* ********* diag structures that are used in the diag packet *****************/
#define FSI_CUSTOM    10			/**< some bytes for dev data */
#define FSI_RESERVED  10			/**< reserved for future use */

/* diags relevant to HW */
#define FSI_HSTATUS_OK       0			/**< no HW errors,  */
#define FSI_HSTATUS_ERR      1			/**< HW error -> go read DIOT-sepecific diags */

/**
 * \brief
	DI/OT hw diagnostics
 * \warning
 *	only tri-state 'status' field of interest here for FESA
 */
struct fsi_diag_hw {
	uint32_t status;			/**< generic OK or not OK status */
	uint32_t board_pres_mask;		/**< mask of present boards as detected by DIOT-SB */
	uint32_t reserved[FSI_RESERVED];
};

/* diags relevant for transmission */
#define FSI_TSTATUS_OK       0			/**< no transmssion errors,  */
#define FSI_TSTATUS_ERR      1			/**< an unidentified transmission error */
#define FSI_TSTATUS_LOST_PKT 2			/**< an unidentified transmission error */

/**
 * \brief
 *	transmission diagnostics
 * \warning
 *	only tri-state 'status' field of interest here for FESA
 */
struct fsi_diag_trans {
	uint32_t status;				/**< TCP/UDP: status of transmission */
	uint32_t pkts_txed;				/**< TCP=1, UDP: number of successfully txed pkts */
	uint32_t pkts_lost;				/**< TCP=0, UDP: number of lost pkts */
	uint32_t bytes_txed;				/**< TCP/UPD: sccessfully transmitted bytes */
	uint32_t reserved[FSI_RESERVED];
};

/*
 * struct fsi_diag_acq {
 *   uint32_t             status;		// acquisition status (general OK or not)
 *   struct dma_status    dma_s[2];		// DMA status for the two banks
 *   struct dma_config    dma_c[2];		// DMA config for the two banks
 *   struct periph_status per_s[8];		// Photodet peripheral boards status
 *   struct periph_config per_c[8];		// Photodet peripheral boards config
 *   uint32_t reserved[FSI_RESERVED];
 * }
 * NOTE: the structure is put here commented out for readability, this header
 * first defines the structures used inside the fsi_diag_acq, then the structure
 * is defined.
 */

/**
 * \brief
 *	DMA status (aligned with sys_top_sub_regs.cheby)
 * \warning
 *	summarized as OK/ERR/WARNING in the 'status' field of
 *	fsi_diag_acq
 * \warning
 *	If at all, exported by FESA within the fsi_hw_diagnostics
 *	uninterpreted binary blob; feel free to ignore
 */
struct fsi_dma_status{
	uint8_t	axi_err;				/**< AXI bus errors */
	uint8_t	fifo_max;				/**< Max the FIFO was filled */
	uint8_t	fifo_err;				/**< FIFO errors */
	uint8_t	reserved[FSI_RESERVED];
};

/**
 * \brief
 *	DMA configuration of last acquisition
 * \warning
 *	of interest only in post-mortem debugging of a failed
 *	acquisition.
 * \warning
 *	If at all, exported by FESA within the fsi_hw_diagnostics
 *	uninterpreted binary blob; feel free to ignore
 */
struct fsi_dma_config {
	uint32_t dma_length;				/**< depth of dma buffer */
	uint32_t dma_addr;				/**< start address of memory */
	uint32_t reserved[FSI_RESERVED];
};

/**
 * \brief
 *	photodetector board status (aligned with sys_top_sub_regs.cheby)
 * \note
 *	bool type used is just to make the description more palatable;
 *	likely it should be simply uint32_t with some bits reserved
 *
 * \warning
 *	summarized as OK/ERR/WARNING in the 'status' field of
 *	fsi_diag_acq
 * \warning
 *	these conditions have several levels of severity. In general:
 *	- spi_busy should NEVER occur (it is a transient condition only
 *	  during configuration of the ADC, not in acquisition)
 *	- the *_locked bits MUST be true; it is a serious problem if not
 *	- in short, a boolean function of these bits must be translated
 *	  into an OK/ERROR value of the fsi_diag_acq status field
 */
struct fsi_periph_status {
	union {
	uint32_t bitmask;	/**< status bitmask, see below struct */
	struct {
		unsigned spi_busy    :1;	/**< 0: SPI busy bit */
		unsigned triggers    :1;    	/**< 1: Trigger bit */
		unsigned servmod_in  :1;  	/**< 2: Servmod inputs (1: not present, 0: present) */
		unsigned fsi_ready   :1;   	/**< 3: Ready status */
		unsigned sys_locked  :1;  	/**< 4: sys->periph synchronized */
		unsigned adc_locked  :1;  	/**< 5: periph adc synchronized */
		unsigned rx_locked   :1;   	/**< 6: peryph->sys rx synchronized */
		unsigned reserved    :25;	/**< 7-31: reserved bits, 25 to make the mask 32 bits.. */
	};
	/**< WARNING: check layout of these bitfields: NONPORTABLE!! */
	};
};

/*  Photodet board config relevant for the acquisition
 *  this might as well go to the "struct fsi_config_packet" packet, but for
 *  FESA API it belongs here
 */
/* some values for the configuration record of a photodetector */
# define FSI_ADC_CONFIG_MEAS   0		/**< acquire data */
# define FSI_ADC_CONFIG_RAMP   1		/**< generate ramp */

/**
 * \brief
 *	photodetector board config relevant for the acquisition
 * \warning
 *	of interest only in post-mortem debugging of a failed
 *	ADC reading operation
 * \warning
 *	If at all, exported by FESA within the fsi_hw_diagnostics
 *	uninterpreted binary blob; feel free to ignore
 */
struct fsi_periph_config {
	uint32_t periph_enabled;			/**< Enable/disable periph board
								0: disabled (even if present)
								1: enabled (it might not be present) */
	uint32_t afe_gain;				/**< Configured gain */
	uint32_t adc_config;				/**< Measure or generate ramp */
	uint32_t ch0_acdc_config;			/**< Ch0 configuration of acdc relay */
	uint32_t reserved[FSI_RESERVED];
};
 ;
/* diags relevant for acquisition */
#define FSI_ASTATUS_OK       0			/**< no acquisition errors */
#define FSI_ASTATUS_ERR      1			/**< an unidentified acquisition error */
/* [...]					     specific acquisition errors to be defined */

/**
 * \brief
 *	acquisition diagnostics
 * \warning
 *	only tri-state 'status' field of interest here for FESA
 */
struct fsi_diag_acq {
	uint32_t             status;		/**< acquisition status (general OK or not) */
	struct fsi_dma_status    dma_s[2];	/**< DMA status for the two banks */
	struct fsi_dma_config    dma_c[2];	/**< DMA config for the two banks */
	struct fsi_periph_status per_s[8];	/**< Photodet peripheral boards status */
	struct fsi_periph_config per_c[8];	/**< Photodet peripheral boards config */
	uint32_t acq_trig_type;			/**< Enum: 0: disabled; 1: external; 2: software */
	uint32_t reserved[FSI_RESERVED];
};

struct fsi_diag_packet {
	struct fsi_common_header hdr;		/**< see \ref fsi_common_header above */
	struct fsi_diag_hw       hw;		/**< see \ref fsi_diag_hw       above */
	struct fsi_diag_trans    trans;		/**< see \ref fsi_diag_trans    above */
	struct fsi_diag_acq      acq;		/**< see \ref fsi_diag_acq      above */
	uint32_t custom[FSI_CUSTOM];
	uint32_t reserved[FSI_RESERVED];
};

/*  ****************************************************************************
 *  4) ver_packet
 *  ****************************************************************************
 *  Information about version of hw/gw/sw running on the DIOT chassis
 *  (system board / photodet module)
 */
struct fsi_ver_packet {
	struct fsi_common_header hdr;		/**< see \ref fsi_common_header */
	uint32_t hw_sb_ver;			/**< DIOT-SB hardware version */
	uint32_t hw_pd_ver[8];			/**< DIOT-PD hardware version for each board */
	uint32_t gw_sb_ver;			/**< DIOT-SB gateware version used (git hash) */
	uint32_t gw_pd_ver[8];			/**< DIOT-PD gateware version used (git hash) */
	uint32_t sw_sb_ver;			/**< DIOT-SB generic software version (git hash) */
	uint32_t sw_fsi_ver;			/**< DIOT-SB FSI-specific SW version (git hash) */
};

/*  ****************************************************************************
 *  5) config_packet
 *  ****************************************************************************
 *  two ideas
 *  1) IDEA: have a structure to indicate the most common config params
 */
struct fsi_trans_config {
	uint32_t samples_per_chan;		/**< number of samples to be acquired */
	uint8_t  dtype[8];			/**< data sent, could be different per board (not chan) */
	uint8_t  proto;				/**< Enum: 0: TCP, 1: UDP */
	uint32_t reserved[FSI_RESERVED];
};

struct fsi_trig_config {
	uint32_t acq_trig_type;			/**< Enum: 0: disabled; 1: external; 2: software */
	uint32_t reserved[FSI_RESERVED];
};

struct fsi_ctrl_packet {
	struct fsi_common_header hdr;			/**< see above */
	uint32_t                 cmd_mask;		/**< bitmask which tells which of the below is valid
							     it can happen that only part of the information
							     is to be used, e.g.
							     1) trigger from sw     [cmd_mask=0x200]
							     2) config first periph [cmd_mask=0x002]
							     3) config all periphs  [cmd_mask=0x1FE] */
	struct fsi_trans_config      trans;		/**< cmd=b0, see above */
	struct fsi_periph_config     per_c[8]	;	/**< cmd=b1-9, ... see above */
	struct fsi_trig_config       trig;		/**< cmd=b10 */
	uint32_t sw_trigger;				/**< cmd=b11 */
	uint32_t custom[10];				/**< cmd=b12-21 */
	uint32_t reserved[12];				/**< cmd=b21-31 */
};
/*  -----------------------------------------------------------------------------
 *  2) IDEA: generic param=value commands
 */
#if 0
struct fsi_ctrl_cmd {
  uint32_t flags;			/**< flags[0] is always a valid bit, other bits reserved. */
  uint32_t param;			/**< param ID */
  uint32_t value;			/**< param value */
};

struct fsi_ctrl_packet {
  struct fsi_common_header	hdr;		/**< see above */
  uint32_t			cmd_num;	/**< number of valid commands in the table below */
  struct fsi_ctrl_cmd		cmds[20];	/**< table of commands */
};
#endif

/*
 * Proposed transmission schema
 *
 * 1) UDP/UDP
 *   - open socket on a single port
 *   - DIOT SB sends only when client (re-)starts
 *     a) ver_packet
 *   - DIOT SB sends  for each acquisition:
 *     a) fsi_params_packet
 *     b) fsi_data_packet_udp
 *     c) fsi_diag_packet
 *   - server can send:
 *     a) fsi_config_packet
 *
 * 2) The memo.c has 3 functions:
 *    a) fsi_init_acquisition()
 *       - open sockets and transmission network as needed
 *       - executed only once
 *
 *    b) fsi_receive_acquisition()
 *       - start:
 *         * UDP: waits for fsi_config_packet
 *         * TCP: waits for connection on both ports and fsi_config_packet
 *       - loop
 *         * receive fsi_data_packet_udp/fsi_data_packet_tcp
 *         * receive fsi_diag_packet
 *         * crunch/return data
 */
#endif