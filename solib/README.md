

To compile fsi-bandwidth-tests with Cuda enviornment, run

```shell
make
```

This will generate following in `build` folder:
-shared library : libfsibandwidth.so
-object file: fsi_bandwidth_test.o
-cuda obj file:copy_to_gpu.cu


Environment required to build

Compiler: gcc, nvcc 
Cuda enviornent-/usr/local/cuda

If any complaints for cuda:

```shell
export PATH=/usr/local/cuda/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda/lib64 ${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
```

-------------------------------------------------------------------------

Run and Test from Python script:

```shell
python3 fsi_bandwidth_test.py

```
With Parameters

```shell
python3 fsi_bandwidth_test.py  -s -z 1 -i 10.11.34.99 -h 0 -j 1 -e 0xF

```
