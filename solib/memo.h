/* vim: set ts=8 sw=8: */

#include <stdint.h>

#define FSI_ERR_BASE			-9090
#define FSI_SOCKET_ERROR		(FSI_ERR_BASE-1)
#define FSI_CANNOT_BIND			(FSI_ERR_BASE-2)
#define FSI_BAD_FBB_ALLOC		(FSI_ERR_BASE-3)
#define FSI_READV_PROBLEM		(FSI_ERR_BASE-4)
#define FSI_NO_TCP_TRANSFER		(FSI_ERR_BASE-5)
#define	FSI_SOCKET_BUF_ERROR		(FSI_ERR_BASE-6)
#define	FSI_SOCKET_RECVFROM_TIMEOUT	(FSI_ERR_BASE-7)
#define	FSI_SOCKET_READV_TIMEOUT	(FSI_ERR_BASE-8)
#define FSI_SOCKET_CFGTIMEO_ERROR	(FSI_ERR_BASE-9)
#define FSI_SOCKET_LISTEN_ERROR		(FSI_ERR_BASE-10)
#define FSI_SOCKET_ACCEPT_ERROR		(FSI_ERR_BASE-11)
#define FSI_SOCKET_NOCONN		(FSI_ERR_BASE-12)

#define N_BOARD       8
#define N_CHAN        8
#define N_MAX_SAMPLES 2500000
#define FBB_SIZE_16BW (N_BOARD*N_CHAN*N_MAX_SAMPLES)
#define FBB_SIZE_32BW (FBB_SIZE_16BW/2)

#define RX_BUFFER_SIZE_BYTES 1048576
#define PORT 8080

// timestamps
#define T_SERVER_RDY      0
#define T_INIT_PKT_RXED   1
#define T_DATA_RXED       2
#define T_DATA_UNSCRB     3
#define T_ACQ_DONE        4
#define D_PKTS_RXED       5
#define D_BYTES_RXED      6
#define T_SIZE            7

//stats
#define ST_DATA           0
#define ST_OVERHD         1
#define ST_CYCLE          2
#define ST_ACQ            3
#define ST_TRANS          4
#define ST_UNSCR          5
#define ST_PROC           6
#define ST_GBPS           7
#define ST_SIZE           8

#define DEFAULT_ACQ_NUM   1    // memo-only

struct fsi_chan_acq_header {
	uint32_t	bom;
	uint32_t	board;
	uint32_t	channel;
	char		timestamp[16];
	uint32_t	n;
	uint16_t	*data;
	uint32_t	reserved[8];
};
