# requires gcc-aarch64-linux-gnu package in ubuntu/debian or path to your 
# crosscompiler

# current development destinations:
server="cs-774-fsidev:~/FSI-tmp/"
board="root@diot-sb-fsi-softlab:/usr/bin/"

# call make with cross-compilation for DIOT SB
make CROSS_COMPILE=aarch64-linux-gnu-gcc

# send to the dev locations/machine
scp fsi_tx $board
scp memo $server
