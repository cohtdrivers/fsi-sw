#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/uio.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <stdint.h>
#include "diot_fsi_regs.h"
#include <sched.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <time.h>
#include <netdb.h>
#include "fsi_diot_hw.h"
#include "fsi_tx.h"
#include "fsi_txrx.h"
#include "fsi_diags.h"


char pkt_types[][10]    = {"unknow", "config", "diag", "data","NA"};
char hw_status[][10]    = {"OK", "ERR","NA"};
char trans_status[][10] = {"OK", "ERR", "LOST PKTS", "NA"};
char periph_state[][10] = {"Dis", "Ena","NA"};
char periph_adc[][10]   = {"Meas Data", "Ramp", "NA"};
char periph_acdc[][10]  = {"Unknown", "AC", "DC", "NA"};

int fsi_diags_print(struct fsi_diag_packet *dp)
{
  fsi_diags_fprint(stdout, dp);
}

int fsi_diags_fprint(FILE *fptr, struct fsi_diag_packet *dp)
{
  int i = 0;
  fprintf(fptr, "Header  : magic_num=0x%x | pkt_type=%s | pkt_ver=%d | trans_id=%d \n",
    dp->hdr.magic_number,
    pkt_types[INX(dp->hdr.message_type,4)],
    dp->hdr.message_ver,
    dp->hdr.trans_id
  );

  fprintf(fptr,     "HW diags: boards present:");
  for (i=0; i < 8; i++)
  {
    if(dp->hw.board_pres_mask & (1<<i))
      fprintf(fptr, "%d", i+1);
  }
  fprintf(fptr, " | status=%s\n", hw_status[INX(dp->hw.status,2)]);

  fprintf(fptr,     "Trans   : status=%s, pkts txed/lost = %d/%d | bytes txed = %d | tx dur = %.3f s\n",
    trans_status[INX(dp->trans.status,3)],
    dp->trans.pkts_txed,
    dp->trans.pkts_lost,
    dp->trans.bytes_txed,
    (((double)dp->trans.reserved[0])/1000000)
  );

  fprintf(fptr,     "Dma status: \n");
  for (i = 0; i < 2; i++)
  {
    fprintf(fptr,   "   bank_%d: axi_err = %3d | fifo_max = %3d | fifo_err = %3d\n",
      i,
      dp->acq.dma_s[i].axi_err,
      dp->acq.dma_s[i].fifo_max,
      dp->acq.dma_s[i].fifo_err);
  }

  fprintf(fptr,     "Dma config: \n");
  for (i = 0; i < 2; i++)
  {
    fprintf(fptr,   "   bank_%d: dma_addr = 0x%x | dma_length = %d\n",
    i,
    dp->acq.dma_c[i].dma_addr,
    dp->acq.dma_c[i].dma_length);
  }

  fprintf(fptr,     "Periph status: \n");
  for (i = 0; i < 8; i++)
  {
    if(dp->hw.board_pres_mask & (1 << i))
    {
      fprintf(fptr, "   board_%d: spi_busy = %d | triggers = %d | servmod_in = %d | "
             "fsi_ready = %d | sys_locked =%d | adc_locked = %d | rx_locked = %d \n",
        i+1,
        0x1 & (dp->acq.per_s[i].bitmask >> 0),
        0x1 & (dp->acq.per_s[i].bitmask >> 1),
        0x1 & (dp->acq.per_s[i].bitmask >> 2),
        0x1 & (dp->acq.per_s[i].bitmask >> 3),
        0x1 & (dp->acq.per_s[i].bitmask >> 4),
        0x1 & (dp->acq.per_s[i].bitmask >> 5),
        0x1 & (dp->acq.per_s[i].bitmask >> 6)
      );
    }
  }
  fprintf(fptr,     "Periph config: \n");

  for (i = 0; i < 8; i++)
  {
    if(dp->hw.board_pres_mask & (1 << i))
    {
      fprintf(fptr, "   board_%d: periph_enabled = %s | afe_gain = %s |"
             " adc_config = %s | ch0_acdc_config = %s \n",
        i+1,
        periph_state[INX(dp->acq.per_c[i].periph_enabled, 2)],
        periph_state[INX(dp->acq.per_c[i].afe_gain,       2)],
        periph_adc  [INX(dp->acq.per_c[i].adc_config,     2)],
        periph_acdc [INX(dp->acq.per_c[i].ch0_acdc_config, 3)]
      );
    }
  }
}

int fsi_diags_status_msg(char *when, char *msg, struct fsi_diag_packet *dp)
{
  int i = 0;
  sprintf(msg, "%s- b_pres:", when);
  for (i=0; i < 8; i++)
  {
    if(dp->hw.board_pres_mask & (1<<i))
      sprintf(msg, "%s %d", msg, i+1);
  }

  sprintf(msg,"%s | DMA (axi_err/fifo_max/fifo_err):", msg);
  for (i = 0; i < 2; i++)
  {
    sprintf(msg,   "%s Bnk%d:%d/%d/%d ", msg,
      i,
      dp->acq.dma_s[i].axi_err,
      dp->acq.dma_s[i].fifo_max,
      dp->acq.dma_s[i].fifo_err);
  }

  sprintf(msg, "%s Periph_s (spi/trig/servm/f_rdy/sys_l/adc_l/rx_l):", msg);
  for (i = 0; i < 8; i++)
  {
    if(dp->hw.board_pres_mask & (1 << i))
    {
      sprintf(msg, "%s b%d:%d/%d/%d/%d/%d/%d/%d",
        msg, i+1,
        0x1 & (dp->acq.per_s[i].bitmask >> 0),
        0x1 & (dp->acq.per_s[i].bitmask >> 1),
        0x1 & (dp->acq.per_s[i].bitmask >> 2),
        0x1 & (dp->acq.per_s[i].bitmask >> 3),
        0x1 & (dp->acq.per_s[i].bitmask >> 4),
        0x1 & (dp->acq.per_s[i].bitmask >> 5),
        0x1 & (dp->acq.per_s[i].bitmask >> 6)
      );
    }
  }
  sprintf(msg, "%s | Periph_c (ena/afe_g/adc_c/acdc_c):", msg);

  for (i = 0; i < 8; i++)
  {
    if(dp->hw.board_pres_mask & (1 << i))
    {
      sprintf(msg, "%s b%d:%s/%s/%s/%s",
        msg, i+1,
        periph_state[INX(dp->acq.per_c[i].periph_enabled, 2)],
        periph_state[INX(dp->acq.per_c[i].afe_gain,       2)],
        periph_adc  [INX(dp->acq.per_c[i].adc_config,     2)],
        periph_acdc [INX(dp->acq.per_c[i].ch0_acdc_config, 3)]
      );
    }
  }
}