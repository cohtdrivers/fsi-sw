#ifndef __FSI_DIAGS_LIB_H__
#define __FSI_DIAGS_LIB_H__

//this is a function that takes a value (usually read from HW) and
//outputs an index of a table that has textual description of the value.
//The last item in the table is "N/A", so if the input value is out of range
//defined by max (last item in the table), the "N/A" will appear
#define INX(a, max) (((a) > (max)) ? (max) : (a))

int fsi_diags_print(struct fsi_diag_packet *dp);
int fsi_diags_fprint(FILE *fptr, struct fsi_diag_packet *dp);
int fsi_diags_status_msg(char *when, char *msg, struct fsi_diag_packet *dp);

#endif