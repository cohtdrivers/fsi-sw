#ifndef __FSI_DIOT_HW_LIB_H__
#define __FSI_DIOT_HW_LIB_H__

#include "fsi_txrx.h"

#define M_AXI_BASE 0xb0000000
#define M_AXI_SIZE 0x10000

#define DMA0_BUF_ADDR 0x00000000 //0x1000 0000
#define DMA1_BUF_ADDR 0x10000000
#define PHYSICAL_ADDR 0x10000000 //0x80000000

#define OFFSET 0
#define DEBUG_HW 0

#define DDR_B0 0
#define DDR_B1 1

#define PARAM_FORMAT_DEC 0
#define PARAM_FORMAT_HEX 1

/* these are the numeric codes for commands to control the photodetector * ADCs.
 * WARNING: there must be a way to keep this in sync with their stated values in
 *
 * https://ohwr.org/project/fsi-daq/blob/ML-fsi-det-8ch-v2/hdl/rtl/fsi/fsi_adc_pkg.vhd#L36
 * and
 * https://ohwr.org/project/fsi-daq/blob/ML-fsi-det-8ch-v2/hdl/rtl/fsi/fsi_control.vhd#L254
 */

#define	FSI_ADC_EXEC_GETDATA	0x54
#define	FSI_ADC_EXEC_SET_AFE	0x56
#define	FSI_ADC_EXEC_GET_AFE	0x59	/* FIXME: not implemented in HDL!  Mar 9 2023 */

#define	FSI_ADC_EXEC_TX_ADC	0x5c
#define	FSI_ADC_EXEC_RX_ADC	0x55
#define	FSI_ADC_EXEC_LEDS	0x51
#define	FSI_ADC_EXEC_PRBS	0x53

/* control AC/DC relay command and params */
#define	FSI_ADC_EXEC_ACDC	0x5d
#define FSI_ADC_ACDC_NONE	0x0
#define FSI_ADC_ACDC_AC		0x1
#define FSI_ADC_ACDC_DC		0x2
#define FSI_ADC_ACDC_PULSE_WIDTH	0x0016E360 /* x[10ns]=15ms */


uint32_t m_axi_readl(uint32_t addr);
int m_axi_map(int addr);
int m_axi_unmap(void);
void dma_init(unsigned nsamp, uint32_t ddr_phys_addr_B0, uint32_t ddr_phys_addr_B1);
void * mmap_ram(unsigned addr);
void * mmap_udmabuf(char *dev, unsigned buf_size);
int cmd_fsi_regs(void);
int is_dma_done(void);
void clear_dma_done(void);
void fsi_trig_from_sw();
void fsi_trig_enable(unsigned en);
void read_dma_fifo_status (void);
int udmabuf_set_param(char *dev, char *param, unsigned long value);
unsigned long udmabuf_get_param(char *dev, char *param, int format);
int fsi_dma_status_get(int bank, struct fsi_dma_status *s);
int fsi_dma_config_get(int bank, struct fsi_dma_config *c);
int fsi_periph_config_get(struct fsi_periph_config *c);
int fsi_periph_status_get(struct fsi_periph_status *s);
int fsi_acq_trig_type_get(uint32_t *t);
unsigned fsi_get_boards_present_mask(void);

uint32_t fsi_get_boards_present_mask(void);
int fsi_do_resync(void);
uint32_t fsi_get_rx_locked_mask(void);

#endif
