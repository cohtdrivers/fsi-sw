// Client side implementation of UDP client-server model
#define _GNU_SOURCE /* See feature_test_macros(7) */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/uio.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <stdint.h>
#include "diot_fsi_regs.h"
#include <sched.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <time.h>
#include <netdb.h>
#include <syslog.h>
#include "fsi_diot_hw.h"
#include "fsi_tx.h"
#include "fsi_txrx.h"
#include "fsi_diags.h"


/*
 * Data to be transmitted:
 * ~25ms of 12-bit data sample acquired at 100 MHz, then converted to uint16_t
 * 1 sample every 10ns
 * 
 * NOTE: here M=10e6 (rather than 2^20)
 * 
 * Per channel:
 *   Number of samples in 25ms :  2 500 000
 *   Number of bits of raw data: 30 000 000 = 12*2500000 = 3.75 MBytes
 *   Number of bits of uint16_t: 40 000 000 = 16*2500000 = 4.00 MBytes
 * 
 * Per DIOT chassis with 8*8=64 channels of packed data
 *   Number of samples in 25ms :   160 000 000 = 64*2500000
 *   Number of bits of raw data: 1 920 000 000 = 64*30000000 = 240 MBytes
 *   Number of bits of uint16_t: 2 560 000 000 = 64*40000000 = 320 MBytes
 * 
 */

RunTimeOpts rtOpts;

static void *ddr_mem;
static void *ddr_virt_addr[2];

/*
 * Get timestamp
 */
uint64_t get_tics()
{
    struct timezone tz = {0, 0};
    struct timeval tv;

    gettimeofday(&tv, &tz);
    return tv.tv_sec * 1000000ULL + tv.tv_usec;
}
/*
 * Print info about input params
 */
void info(void)
{
         printf(
         "\nUsage:  fsi_tx_b [OPTION]\n\n"
         "\n"
         "-i [server IP]    Provide ip of server to which data is tx-ed\n"
         "-u                Transmit using UDP/IP (default)\n"
         "-t                Transmit using TCP/IP\n"
         "-b [bank]         Bank of photodet boards to read: 0: boards 1-4 (default), 1: boards 5-8\n"
         "-d                DBG_MODE: No DMA from particular location (just empty buffer)\n"
         "-p                DBG_MODE: No Photodet module (skip any HW access, self-trigger)\n"
         "-e                DBG_MODE: No external trigger enabled (signals from laser ignored) \n"
         "-v [level]        Verbosity level (default: level=0)\n"
         "-m [method]       mmap() method to DMA data from RAM (requires proper linux sup/config/driver): \n"
         "                  0: to physical address using reserved region (legacy) \n"
         "                  1: using u-dma-buf driver \n"
         "-s [size Bytes]   Requested payload size of packet (adjusted to multiple of sample bundle)  \n"
         "-?                show this page\n"
         "\n");
}

int fsi_pkt_header_prepare(struct fsi_common_header *h, uint32_t msg_type,
                           uint32_t trans_id)
{
  h->magic_number = FSI_PKT_MAGIC_NUM;
  h->message_type = FSI_PKT_TYPE_DIAG;
  h->message_ver  = FSI_PKT_VER;
  h->trans_id     = trans_id;
}

/*
 * Get startup config (from defaults and/or from input params
 */
int  startup(int argc, char **argv, RunTimeOpts *_rtOpts)
{
   int c;
   // Default config values
   _rtOpts->c_type      = DEFAULT_CONNECTION_TYPE;
   _rtOpts->s_payload   = DEFAULT_PAYLOAD_SIZE;
   _rtOpts->verbose     = DEFAULT_VERBOSITY_LEVEL;
   _rtOpts->no_dma      = DEFAULT_NO_DMA_CONFIG;
   _rtOpts->no_photodet = DEFAULT_NO_PHOTODET_CONFIG;
   _rtOpts->no_ext_trig = DEFAULT_NO_EXT_TRIG_CONFIG;
   _rtOpts->n_bank      = DEFAULT_ACQ_BANK;
   _rtOpts->n_sample    = DEFAULT_SAMPLE_NUMBER;
   _rtOpts->mmaptype    = DEFAULT_MMAPTYPE;
   strncpy(_rtOpts->ip_add,DEFAULT_SERVER_IP, IP_ADDR_LEN);
   strncpy(_rtOpts->ap_name,argv[0], AP_NAME_LEN);

   while( (c = getopt(argc, argv, "i:utdpb:v:m:s:e")) != -1 ) {
     switch(c) {
     case 'i':
       printf("optarg: %s \n",optarg);
       strncpy(_rtOpts->ip_add, optarg, IP_ADDR_LEN);
       break;
     case 'u':
       _rtOpts->c_type = C_UDP ;
       break;
     case 't':
       _rtOpts->c_type = C_TCP ;
       break;
     case 'd':
       _rtOpts->no_dma = 1;
       _rtOpts->no_photodet = 1;
       break;
     case 'p':
       _rtOpts->no_photodet = 1;
       break;
     case 'e':
       _rtOpts->no_ext_trig = 1;
       break;
     case 'b':
       _rtOpts->n_bank = strtol(optarg, &optarg, 0);
       if(_rtOpts->n_bank != 0 &&  _rtOpts->n_bank != 1)
       {
         printf("bank values allowed: 0 or 1, input value: %d\n", _rtOpts->n_bank);
         return -1;
       }
       break;
     case 'v':
       _rtOpts->verbose = strtol(optarg, &optarg, 0);
       break;
     case 'm':
       _rtOpts->mmaptype = strtol(optarg, &optarg, 0);
       break;
     case 's':
       _rtOpts->s_payload = strtol(optarg, &optarg, 0);
       break;
     case '?':
     default:
       info();
       return -1;
       break;
     }
   }

   //adjust payload size to to sample bundel boundary (32 chan * 12 bits = 48 bytes)
   _rtOpts->s_payload =  ((int)(_rtOpts->s_payload/48))*48;
   // number of packets needed to transmit samples in a single bank, i.e. 
   // 4 chan x 12 bits x number of samples
    _rtOpts->n_packets =  (_rtOpts->n_sample*48 ) / _rtOpts->s_payload;
   // "reminder packet"
   if( _rtOpts->n_packets*_rtOpts->s_payload/48 < _rtOpts->n_sample)
      _rtOpts->n_packets++;

   if(_rtOpts->verbose)  printf("------ Settings ------------\n");
   if(_rtOpts->verbose)  printf("Dst IP addr    : %s\n", _rtOpts->ip_add);
   if(_rtOpts->verbose)  printf("Protocol       : %s \n", _rtOpts->c_type==C_UDP?"UDP":"TCP");
   if(_rtOpts->verbose)  printf("Payload        : %d bytes\n", _rtOpts->s_payload);
   if(_rtOpts->verbose)  printf("Acq bank       : %d (0: boards 1-4, 1: boards 5-8)\n", _rtOpts->n_bank);
   if(_rtOpts->verbose)  printf("Samples number : %d (-1 will read from hw)\n", _rtOpts->n_sample);
   if(_rtOpts->verbose)  printf("Packets        : %d\n", _rtOpts->n_packets);
   if(_rtOpts->verbose)  printf("Data           : %s\n", _rtOpts->no_dma==1?"Zeros":"Read from RAM memory");
   if(_rtOpts->verbose)  printf("Photodet HW    : %s\n", _rtOpts->no_photodet==1?"Run without":"Run with");
   if(_rtOpts->verbose)  printf("Ext trig       : %s\n", _rtOpts->no_ext_trig==1?"disabled":"enabled");
   if(_rtOpts->verbose)  printf("mmap() type    : %s\n", _rtOpts->mmaptype==0?"Old (direct mmap()":"New (u-dma-buf)");
   if(_rtOpts->verbose)  printf("---------------------------\n\n");
   return 0;
}

/*
 * A generic log function that writes input message (msg) to syslog
 */
int log_to_syslog(int priority, char *msg)
{
  /*
   * make priority lookup by hand (for some reasons it is not written to syslog
   * (TODO: see whether configuration can be changed to syslog priority/severity)
   */
  char prio[8][5]= {
    "EMEG", // 0: LOG_EMERG
    "ALRT", // 1: LOG_ALERT
    "CRIT", // 2: LOG_CRIT
    "ERRO", // 3: LOG_ERR
    "WARN", // 4: LOG_WARNING
    "NOTE", // 5: LOG_NOTICE
    "INFO", // 6: LOG_INFO
    "DEBG"  // 7: LOG_DEBUG
  };

  char timestamp[30];
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  strftime(timestamp, sizeof(timestamp), "%Y%m%dT%H%M%S", &tm);

  openlog(rtOpts.ap_name, LOG_PID, LOG_USER);
  syslog(priority, "[%s:%s] %s", prio[priority], timestamp, msg);
  closelog();

  return 1;
}

/*
 * Function that checks whether rx_lock is OK. It writes message to syslog if it
 * is not. The functions accepts as input a textual description of a context
 * in which it is called, e.g. function name.
 * This functions assumes there are boards present.
 */
int fsi_check_rx_locked_and_log(char *when)
{
  uint32_t boards_present = fsi_get_boards_present_mask();
  uint32_t rx_locked      = fsi_get_rx_locked_mask();

  if ((boards_present & rx_locked) == boards_present) // all locked
    return LOCKED_OK;
  else //board not locked
  {
    char msg[100];
    sprintf(msg,"%s - Boards not rx_locked (counting from 1/left):", when);

    for (int i = 0; i<8; i++)
      if (((boards_present >> i) & 0x1) == 0x1 && ((rx_locked >> i) & 0x1) == 0x0 )
        sprintf(msg,"%s %d",msg, i+1);

    sprintf(msg,"%s [masks: pres=0x%x, rx_l=0x%x]:", msg, boards_present, rx_locked);
    log_to_syslog(LOG_WARNING, msg);
    return LOCKED_ERR;
  }
}
/*
 * Function triggers resync and logs that it did. The functions accepts as
 * input a textual description of a context in which it is called, e.g.
 * function name.
 */
void fsi_resync_and_log(char *when)
{
  char msg[100];
  int ret = fsi_do_resync();
  sprintf(msg,"%s - Resync (count since start of app = %d)", when, ret);
  log_to_syslog(LOG_INFO, msg);
}
/*
 * This function checkes whether rx_locked is ok, and if it is not it
 * makes a number of attempts (determined by input param) using resync
 * to get rx_locked OK. The functions accepts as input a textual description
 * of a context in which it is called, e.g. function name.
 */
int fsi_check_rx_locked_and_resync(char *when, int attempts)
{
   if (fsi_check_rx_locked_and_log(when) == LOCKED_OK)
     return LOCKED_OK;

   char msg[100];
   int i;

   for (i=0; i < attempts; ++i)
   {
     fsi_resync_and_log(when);
     usleep(100000);

     if (fsi_check_rx_locked_and_log(when) == LOCKED_OK)
     {
       sprintf(msg,"%s - rx_locked OK after %d resync attempts", when, i);
       log_to_syslog(LOG_INFO, msg);
       return LOCKED_OK;
     }
   }
   sprintf(msg,"%s - rx_locked not OK after %d resync attempts, stop trying",
           when, i);
   log_to_syslog(LOG_ERR, msg);
   return LOCKED_ERR;
}

int fsi_diags_acq_get(struct fsi_diag_acq *acq)
{
// 	uint32_t             status;		/**< acquisition status (general OK or not) */
// 	struct fsi_dma_status    dma_s[2];	/**< DMA status for the two banks */
// 	struct fsi_dma_config    dma_c[2];	/**< DMA config for the two banks */
// 	struct fsi_periph_status per_s[8];	/**< Photodet peripheral boards status */
// 	struct fsi_periph_config per_c[8];	/**< Photodet peripheral boards config */
// 	uint32_t acq_trig_type;			/**< Enum: 0: disabled; 1: external; 2: software */
// 	uint32_t reserved[FSI_RESERVED];
  int ret=0, err=0;
  ret=+fsi_dma_status_get(0, &acq->dma_s[0]);
  ret=+fsi_dma_status_get(1, &acq->dma_s[1]);
  if(ret > 0)
    fprintf(stderr, "Error getting fsi_dma_status \n");

  err =+ ret;
  ret = 0;
  ret =+ fsi_dma_config_get(0, &acq->dma_c[0]);
  ret =+ fsi_dma_config_get(1, &acq->dma_c[1]);
  if(ret > 0)
    fprintf(stderr, "Error getting fsi_dma_config_get \n");

  err =+ ret;
  ret = 0;
  ret =+ fsi_periph_status_get(acq->per_s);
  ret =+ fsi_periph_config_get(acq->per_c);
  if(ret > 0)
    fprintf(stderr, "Error getting fsi_periph status/config \n");

  err =+ ret;
  ret = 0;
  ret =+ fsi_acq_trig_type_get(&acq->acq_trig_type);
  if(ret > 0)
    printf("Error getting fsi_acq_trig_type \n");
  err =+ ret;
  return err;
}

int fsi_diag_hw_get(struct fsi_diag_hw *hw)
{
  hw->board_pres_mask =  fsi_get_boards_present_mask();
  hw->status = FSI_HSTATUS_OK; // TODO
}
void fsi_get_status_and_log(char *when)
{
  char msg[100000];

  struct fsi_diag_packet dp;

  fsi_diag_hw_get(&dp.hw);
  fsi_diags_acq_get(&dp.acq);
  fsi_diags_status_msg(when, msg, &dp);
  log_to_syslog(LOG_INFO, msg);
}
/*
 * Init hardware: map DMA and config photodet module
 */
int fsi_hw_init()
{
   int ret = 0;
   unsigned long ddr_phys_addr[2];
   unsigned int  ddr_bufr_size[2];
   //size: rtOpts.n_sample*12*32/8 = rtOpts.n_sample*12*4
   unsigned int  sample_size_per_ddr_bank = rtOpts.n_sample*12*4;//Bytes
   printf("%s: enter,\n", __func__);
   log_to_syslog(LOG_INFO, "fsi_hw_init() - starting fsi_tx");
   // skip dma mapping
   if(!rtOpts.no_dma)
   {
     if(rtOpts.mmaptype == 0) // to be deleted...
     {
       // direct mapping of physical memory - old way
       ddr_mem               = mmap_ram(PHYSICAL_ADDR);
       // set by hand
       ddr_phys_addr[DDR_B0] = PHYSICAL_ADDR+DMA0_BUF_ADDR;
       ddr_phys_addr[DDR_B1] = PHYSICAL_ADDR+DMA1_BUF_ADDR;
       ddr_bufr_size[DDR_B0] = ddr_phys_addr[DDR_B1]-ddr_phys_addr[DDR_B0];
       ddr_bufr_size[DDR_B1] = ddr_phys_addr[DDR_B1]-ddr_phys_addr[DDR_B0];
     }
     else if(rtOpts.mmaptype == 1) // mapping using u-dma-buf driver
     {
       //ready physical addresses (set using device tree, dynamically in future?)
       ddr_phys_addr[DDR_B0] = udmabuf_get_param("udmabuf0", "phys_addr", PARAM_FORMAT_HEX);
       ddr_phys_addr[DDR_B1] = udmabuf_get_param("udmabuf1", "phys_addr", PARAM_FORMAT_HEX);

       // read size of each buffer (set using device tree)
       ddr_bufr_size[DDR_B0] = udmabuf_get_param("udmabuf0", "size", PARAM_FORMAT_DEC);
       ddr_bufr_size[DDR_B1] = udmabuf_get_param("udmabuf1", "size", PARAM_FORMAT_DEC);

       // get virtual address
       ddr_virt_addr[DDR_B0] = mmap_udmabuf("/dev/udmabuf0", ddr_bufr_size[DDR_B0]);
       ddr_virt_addr[DDR_B1] = mmap_udmabuf("/dev/udmabuf1", ddr_bufr_size[DDR_B1]);

       // check if successful
       if (ddr_virt_addr[DDR_B0] == MAP_FAILED ||
           ddr_virt_addr[DDR_B1] == MAP_FAILED)
         return -1;

       /* manual cache management - initial config
        * (https://github.com/ikwzm/udmabuf#2-manual-cache-management-with-the-cpu-cache-still-being-enabled)
        */
       // 1)  Specify memory area shared between CPU and accelerator:
       // 1a) offset from the start address of the allocated buffer in units of byte
       ret += udmabuf_set_param("udmabuf0", "sync_offset", 0x0);
       ret += udmabuf_set_param("udmabuf1", "sync_offset", 0x0);

       // 1b) size of the shared memory area in units of bytes.
       ret += udmabuf_set_param("udmabuf0", "sync_size", sample_size_per_ddr_bank);
       ret += udmabuf_set_param("udmabuf1", "sync_size", sample_size_per_ddr_bank);

       // 2) Specify data transfer direction
       // If the accelerator performs only write accesses to the memory area, 
       // sync_direction should be set to 2(=DMA_FROM_DEVICE)
       ret += udmabuf_set_param("udmabuf0", "sync_direction", 2);
       ret += udmabuf_set_param("udmabuf1", "sync_direction", 2);

       if(ret != 6)
       {
         printf("%s: udmabuf_set_param() failed for one of the params "
                "(ret=%d while should be 6)\n", __func__, ret);
         return -1;
       }
     }
     else 
     {
       printf("%s: unrecognized mmaptype: %d ,\n", __func__, rtOpts.mmaptype);
       return -1;
     }
   }

   printf("%s: DDR Bank 0: ddr_phys_addr=x%lx, size=%d Bytes (samples=%d Bytes)\n",
         __func__, ddr_phys_addr[DDR_B0], ddr_bufr_size[DDR_B0], sample_size_per_ddr_bank);
   printf("%s: DDR Bank 1: ddr_phys_addr=x%lx, size=%d Bytes (samples=%d Bytes)\n",
         __func__, ddr_phys_addr[DDR_B1], ddr_bufr_size[DDR_B1], sample_size_per_ddr_bank);


   // We only support the same size for both DDR banks
   if (ddr_bufr_size[DDR_B0] != ddr_bufr_size[DDR_B1])
   {
       printf("%s: Different buffer size for the two DDR banks\n", __func__);
       return -1;
   }
   
   if (sample_size_per_ddr_bank> ddr_bufr_size[DDR_B0])
   {
       printf("%s: The number of samples exceeds buffer size: \n "
              "%d Bytes is required for %d samples of 12 bits from 32 chan to "
              "fit into a single DDR Bank\n"
              "%d Bytes is available for each bank\n",
               __func__, sample_size_per_ddr_bank, rtOpts.n_sample,ddr_bufr_size[DDR_B0]);
       return -1;
   }

   // skip HW configuration
   if(rtOpts.no_photodet) 
     return 0;

   // get connection to config registers
   if(rtOpts.verbose==3) printf("Enter m_axi_map()\n");
   m_axi_map(M_AXI_BASE);

   //set DMA transfer lenght
   if(rtOpts.verbose==3) printf("Set number of samples (DMA transfer lenght): %d\n", rtOpts.n_sample);

   dma_init((uint32_t)rtOpts.n_sample, ddr_phys_addr[DDR_B0], ddr_phys_addr[DDR_B1]);

   fsi_get_status_and_log("fsi_hw_init()->   ");

   //make sure that there are boards...
   if ( fsi_get_boards_present_mask() == 0x0)
   {
     log_to_syslog(LOG_ERR, "fsi_hw_init() - no boards present, exiting...  ");
     printf("ERROR: no boards present, exiting...\n");
     exit(EXIT_FAILURE);
   }

   //make sure all boards are locked
   if( fsi_check_rx_locked_and_resync("fsi_hw_init()", 2) == LOCKED_ERR)
   {
     log_to_syslog(LOG_ERR, "fsi_hw_init() - rx_locked not OK, cannot start, quiting...  ");
     printf("ERROR: boards not locked...\n");
     exit(EXIT_FAILURE);
   }
//    else
//      log_to_syslog(LOG_INFO, "fsi_hw_init() - rx_locked OK");

   return 0;

}


/*
 * Connection setup and data transmission
 *
 * TODO: separate into
 * 1. network/connection initialization
 * 2. transmission
 */
int fsi_tx(void)
{
    int sockfd;
    int acq_num=1;
    struct sockaddr_in servaddr;

    /*
     * Network/connection config at startup
     */

    memset(&servaddr, 0, sizeof(servaddr));

    // Filling server information
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT);
    servaddr.sin_addr.s_addr = inet_addr(rtOpts.ip_add);

    // Creating socket file descriptor for UDP or TCP
    if(rtOpts.c_type == C_UDP)
    {
        struct addrinfo hints, *dstinfo = NULL, *p = NULL;
        int rv = -1;
        memset(&hints, 0, sizeof hints);
        hints.ai_family = AF_INET;
        hints.ai_socktype = SOCK_DGRAM;
        hints.ai_protocol = IPPROTO_UDP;
        hints.ai_flags = AI_PASSIVE;

        /*For destination address*/
        if ((rv = getaddrinfo(rtOpts.ip_add, "8080", &hints, &dstinfo)) != 0) {
            fprintf(stderr, "getaddrinfo for dest address: %s\n", gai_strerror(rv));
            exit(EXIT_FAILURE);
        }

        // loop through all the results and make a socket
        for(p = dstinfo; p != NULL; p = p->ai_next) {

            if ((sockfd = socket(p->ai_family, p->ai_socktype,
                        p->ai_protocol)) == -1) {
              perror("socket");
              continue;
            }
            /*Taking first entry from getaddrinfo*/
            printf("Socket OK..\n");
            break;
        }
        int send_buff = 33554432;   //=20^25
        int res = setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &send_buff, sizeof(send_buff));
        printf("RV %d\n",res );
        /*Failed to get socket to all entries*/
        if (p == NULL) {
            fprintf(stderr, "Failed to get socket\n");
            exit(EXIT_FAILURE);
        }
        /*Connect this datagram socket to destination address info */
        if((rv= connect(sockfd, p->ai_addr, p->ai_addrlen)) != 0) {
            fprintf(stderr, "connect: %s\n", gai_strerror(rv));
            exit(EXIT_FAILURE);
        }
        printf("Connected..\n");

    }
    else if(rtOpts.c_type == C_TCP)
    {
        printf("Do the TCP/IP magic...\n");

        // socket create and varification
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd == -1) {
            printf("socket creation failed...\n");
            exit(0);
        }
        /*Specifies the total per-socket buffer space reserved for sends.*/
        int send_buff = 1048576;
        int res = setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &send_buff, sizeof(send_buff));

        printf("RV %d\n",res );

        // connect the client socket to server socket
        if (connect(sockfd, (struct sockaddr_in *)&servaddr, sizeof(servaddr)) != 0) {
            printf("Connection with the server failed...\n");
            exit(0);
        }
        printf("Connected to the server..\n");
    }
    else
    {
        printf("ERROR: Wrong connection type chosen: %d\n", rtOpts.c_type);
        return -1;
    }

    /*
     * Data transmission
     */
    while(1)
    {
          uint64_t trigger_time=0;
          // Wait for DMA, provided there is the photodet HW.
          if(!rtOpts.no_photodet)
          {

              /* 
               * ============> not sure this is needed, anyway <================
               * 
               * If '1' is written to "sync_for_device", if sync_direction is 
               * 1(=DMA_TO_DEVICE) or 0(=DMA_BIDIRECTIONAL), the write to the
               * device file flushes a cache specified by sync_offset and 
               * sync_size (i.e. the cached data, if any, will be updated with
               * data on DDR memory).
               */
              udmabuf_set_param("udmabuf0", "sync_for_device", 1);
              udmabuf_set_param("udmabuf1", "sync_for_device", 1);

              /* ============> not sure this is needed, anyway <================ */

              if(rtOpts.no_ext_trig){ // used if for debugging, trigger from SW
                fsi_trig_enable(0);
                fsi_trig_from_sw();
              }
              else{
                fsi_trig_enable(1);
              }

              fprintf(stderr,"Wait DMA ");
              while(is_dma_done() == 0)
                 usleep(100000);

              fsi_get_status_and_log("fsi_tx()->trig-ed ");
          }

          trigger_time = get_tics();

          if(!rtOpts.no_photodet)
          {
              clear_dma_done();
              fsi_trig_enable(0); //disable trigger during acquisition
          }
          if(rtOpts.verbose >= 2) fprintf(stderr, " --> start acq %3d for ", acq_num);

          /* **********************************************************************
          * transmit data
          */

          int i = 0;
          double total = 0;
          uint64_t t_start = 0;// get_tics();
          uint64_t t_end = 0;
          int pkt_h_size = sizeof(struct packet_header);
          int pkt_h_size_all_pkts = 0;
          int pkt_p_size = rtOpts.s_payload + pkt_h_size;
          struct packet_fsi pkt;
          //send a "start transmission" packate to better measure tmie
          int s_cur     = 0;       // current sample offset
          int s_stop    = 0;       // total number of samples per channel
          int s_per_pkt = 0;       // samples that fit into a single frame
          int p_id      = 0;       // packet ID (total for entire transmissin)
          int n_boards_per_acq=0;  // how many boards are read
          int n_chan_per_board=0;  // how many channels per board
          int n_tx_err        =0;  // number of transmission errors
          int n_tx_ok         =0;  // number of correctly transmitted frames
          int which_boards_mask=rtOpts.n_bank==1?0xF0:0x0F;

          n_chan_per_board = 8;                     // always all chans
          n_boards_per_acq = 4;                     // always full bank (4 boards)

          s_stop           = rtOpts.n_sample;     // number of samples (per chan)
          s_per_pkt        = rtOpts.s_payload/48; // samples that fit into a single frame


          //for UDP, send an "info" packet for the receiver/server to now what
          //to expect.
          if(rtOpts.c_type == C_UDP)
          {
              pkt.header.payload_size    = rtOpts.s_payload;
              pkt.header.chan            = n_chan_per_board;  // numb of acq chans per board
              pkt.header.board           = which_boards_mask; 
              pkt.header.sample_number   = rtOpts.n_sample;   // how many samples per chan
              sendto(sockfd, (const char *)&pkt, pkt_h_size,
                                    MSG_CONFIRM, (const struct sockaddr *)&servaddr,
                                    sizeof(servaddr));
          }
          if(rtOpts.c_type == C_TCP)
          {
              printf("Send start packet ...\n");
              pkt.header.payload_size    = rtOpts.s_payload;
              pkt.header.chan            = n_chan_per_board;  // numb of acq chans per board
              pkt.header.board           = which_boards_mask; 
              pkt.header.sample_number   = rtOpts.n_sample;   // how many samples per chan
              int rv=send(sockfd, (const char *)&pkt, pkt_h_size, 0);
          }
          // measure start
          t_start = get_tics();
          if(rtOpts.no_dma) ///////////////////  no DMA  ///////////////////////
          {
              for (i = 0; i < rtOpts.n_packets; i++)
              {
                  pkt.header.thread_id       = 0; //TODO: remove
                  pkt.header.packet_id       = i;
                  pkt.header.payload_size    = rtOpts.s_payload;
                  pkt.header.expected_pkt_n  = rtOpts.n_packets;
                  pkt.header.chan            = 0; //TODO: remove
                  pkt.header.board           = 0; //TODO: remove
                  pkt.header.first_sample_id = i*rtOpts.s_payload/2;
                  pkt.header.sample_number   = rtOpts.s_payload*2;

                  int rv = sendto(sockfd, (const char *)&pkt, pkt_p_size,
                                  MSG_CONFIRM, (const struct sockaddr *)&servaddr,
                                  sizeof(servaddr));
                  total += rv;
              }
          }
          ///////////////////////////// DMA  //////////////////////////////////
          else if(rtOpts.c_type == C_UDP) 
          {
              uint32_t *samp =0;
              s_cur = 0;
              p_id = 0;
              int iovcnt;
              struct iovec iov[2];
              struct packet_header h_cur;

              //assign header part of the payload
              iov[0].iov_base = &h_cur;
              iov[0].iov_len  = sizeof(h_cur);

              if(rtOpts.verbose >= 2)
                 printf("Sending %d samples for bank %d, expected packet number: %d \n",
                        s_stop, rtOpts.n_bank, rtOpts.n_packets);
              if(rtOpts.mmaptype == 0)
              {
                if(rtOpts.n_bank == 0)
                  samp = (uint32_t *)(ddr_mem + DMA0_BUF_ADDR);
                else
                  samp = (uint32_t *)(ddr_mem + DMA1_BUF_ADDR);
              }
              else if(rtOpts.mmaptype == 1)
              {
                if(rtOpts.n_bank == 0)
                  samp = (uint32_t *)(ddr_virt_addr[DDR_B0]);
                else
                  samp = (uint32_t *)(ddr_virt_addr[DDR_B1]);

                /*
                 * If '1' is written to "sync_for_cpu", if sync_direction is
                 * 2(=DMA_FROM_DEVICE) or 0(=DMA_BIDIRECTIONAL), the write to
                 * the device file invalidates a cache specified by sync_offset
                 * and sync_size.
                 */
                if(rtOpts.n_bank == 0)
                  udmabuf_set_param("udmabuf0", "sync_for_cpu", 1);
                else
                  udmabuf_set_param("udmabuf1", "sync_for_cpu", 1);

              }

              for(s_cur = 0; s_cur < s_stop; s_cur+=s_per_pkt )
              {
                 if(s_cur+s_per_pkt<s_stop)
                    pkt_p_size = s_per_pkt*48;
                 else
                    pkt_p_size = (s_stop-s_cur)*48;

                 //fill in FSI header
                 h_cur.thread_id       = 0;
                 h_cur.packet_id       = p_id;
                 h_cur.expected_pkt_n  = rtOpts.n_packets;
                 h_cur.payload_size    = pkt_p_size;
                 h_cur.chan            = 0xff;
                 h_cur.board           = rtOpts.n_bank;
                 h_cur.first_sample_id = s_cur;
                 h_cur.sample_number   = pkt_p_size/48;

                 //fill in (point to) FSI data
                 iov[1].iov_base = &samp[s_cur*12];
                 iov[1].iov_len  = pkt_p_size;
                 iovcnt = sizeof(iov) / sizeof(struct iovec);

                 // send !
                 int rv=writev(sockfd, iov, iovcnt);
                 if(rv<0)
                 {
                   n_tx_err++;
                   rv=0;
                   //TODO: retransmit???
                 }
                 else
                   n_tx_ok++;

                 //count total transmitted size
                 total += rv;
                 if(rtOpts.verbose==3) 
                   printf("TX packet - %s - %3d of %d Bytes (h:%d, p:%d) for bank %d"
                          "(samples %7d to %7d, total tx %.3f MBytes), \n",
                           (rv==0?"ERR":"OK "),
                           p_id, rv, (int)iov[0].iov_len , (int)iov[1].iov_len,
                           rtOpts.n_bank, s_cur, s_cur+s_per_pkt,
                           (double)total/MB_factor);
                 p_id++;
             }
             //calculate header overhead in bytes:
             pkt_h_size_all_pkts = n_tx_ok*pkt_h_size;
          }
          ///////////////////////////// DMA  //////////////////////////////////
          else if(rtOpts.c_type == C_TCP) 
          {
              uint32_t *samp =0;
              // size [in uint32_t]= (rtOpts.n_sample * bits * boards * chan_per_boards) / 32
              // size [in uint32_t]= (rtOpts.n_sample * 12   *  4     *        8       ) / 32
              // size [in uint32_t]=  rtOpts.n_sample * 12
              int bank_size_words = rtOpts.n_sample * 12;

              if(rtOpts.verbose >= 2)
                 printf("Sending %d samples for bank %d using TCP \n",
                        s_stop, rtOpts.n_bank);
              if(rtOpts.mmaptype == 0)
              {
                if(rtOpts.n_bank == 0)
                  samp = (uint32_t *)(ddr_mem + DMA0_BUF_ADDR);
                else
                  samp = (uint32_t *)(ddr_mem + DMA1_BUF_ADDR);
              }
              else if(rtOpts.mmaptype == 1)
              {
                if(rtOpts.n_bank == 0)
                  samp = (uint32_t *)(ddr_virt_addr[DDR_B0]);
                else
                  samp = (uint32_t *)(ddr_virt_addr[DDR_B1]);

                /*
                 * If '1' is written to "sync_for_cpu", if sync_direction is
                 * 2(=DMA_FROM_DEVICE) or 0(=DMA_BIDIRECTIONAL), the write to
                 * the device file invalidates a cache specified by sync_offset
                 * and sync_size.
                 */
                if(rtOpts.n_bank == 0)
                  udmabuf_set_param("udmabuf0", "sync_for_cpu", 1);
                else
                  udmabuf_set_param("udmabuf1", "sync_for_cpu", 1);
              }
                 // send !
              int rv=send(sockfd, samp, bank_size_words*sizeof(uint32_t), 0);
              if(rv<0)
              {
                printf("TX error: %d\n", rv);
                n_tx_err = 1;
              }
              else
                n_tx_ok = 1;

              total += rv;
              //count total transmitted size
              if(rtOpts.verbose==3) 
                 printf("TX packet - %s - %d Bytes for bank %d"
                        "(samples %d, total tx %.3f MBytes), \n",
                         (rv==0?"ERR":"OK "), rv,
                         rtOpts.n_bank, rtOpts.n_sample, 
                         (double)total/MB_factor);
              // no header overhead:
              pkt_h_size_all_pkts = 0;
          }
          t_end = get_tics();
          // measure stop
          ///////////////////////////////////////////////////////////////////////
          double   duration          = (double)(t_end - t_start) / 1e6;
          double total_MBps          = (double)total / duration / MB_factor;
          double total_Gbps          = (total_MBps*8.0)/1024.0;
          double total_chan          =n_chan_per_board*n_boards_per_acq;
          double fsi_data            = total - pkt_h_size_all_pkts;
          fprintf(stderr,
             "--> tx-ed: data=%lld bytes (%.1f MBytes) + overhd=%d [bytes] | %d pkts "
             "[%d err pkts] for %d chan in %.3f s (%.3f Gbps, %.3f chan/s)",
              (long long int)fsi_data, 
              (double)fsi_data/MB_factor,
              pkt_h_size_all_pkts,
              n_tx_ok,
              n_tx_err,
              (int)total_chan,
               duration,
              total_Gbps,
              (double)duration/total_chan
                 );

          fsi_get_status_and_log("fsi_tx()->acqu-ed ");
          //read status of DMA fifos, if HW available
          if(!rtOpts.no_photodet)
            read_dma_fifo_status();

          //check rx_locked status and resync if needed
          fsi_check_rx_locked_and_resync("fsi_tx()", 1);


/* *****************************************************************************
 *
 *         ML the code below sends diag TCP message to the server
 *         To be decided whether this will be used.
 * ****************************************************************************/
/*
          struct fsi_diag_packet dp;
          // header
          fsi_pkt_header_prepare(&dp.hdr, FSI_PKT_TYPE_DIAG,acq_num);

          //get HW diags
          fsi_diag_hw_get(&dp.hw);

          // get transmission diags -> TODO: 
          dp.trans.status     = FSI_TSTATUS_OK; //TODO
          dp.trans.pkts_txed  = n_tx_ok;
          dp.trans.pkts_lost  = n_tx_err;
          dp.trans.bytes_txed = fsi_data;
          dp.trans.reserved[0]= (uint32_t)(duration*1000000);

          //get acquiistion diags
          fsi_diags_acq_get(&dp.acq);

          print the info
          if(rtOpts.verbose==4)
            fsi_diags_print(&dp);

          if(rtOpts.c_type == C_TCP)
          {
            //FIXME: later the diags data will be sent to a different PORT, so no need
            //for this delay. Now, we need to have diags sent as a separate blob of data.
            usleep(500000); 
            int rv=send(sockfd, (const char *)&dp, sizeof(struct fsi_diag_packet), 0);
          }
*/
/* ****************************************************************************/

          uint64_t dead_time_s = MIN_ALLOWED_DEADTIME_IN_SEC;
          while (get_tics() - trigger_time < dead_time_s* 1000000ULL)
          {
            fprintf(stderr, ".");
            usleep(500000);
          }

          if(rtOpts.no_dma || rtOpts.no_photodet)
          {
            fprintf(stderr, "[auto-trig: wait 10s]");
            sleep(10);
          }
          fprintf(stderr, "\n");
          acq_num++;

    }
    close(sockfd);
    return 0;
}

/* *****************************************************************************
 *               Main code
 * *****************************************************************************
 */
int fsi_main(int argc, char *argv[])
{
    //configuration (default and from input args)
    if(startup(argc,argv,&rtOpts)<0) 
        return -1;

    // initialize hw
    fsi_hw_init();

    //transmit data
    fsi_tx();

    return 0;
}

#ifdef FSI_MAIN
int main(int argc, char *argv[])
{
	return fsi_main(argc, argv);
}
#endif /* FSI_MAIN */
