#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/time.h>
#include "receiver.h"

int gimme_data(struct channel_acq acq[], int n)
{
	int ns = 3000000;
	int i, cnt;
	struct timeval tv1, tv2;

	gettimeofday(&tv1, NULL);
	for (i = 0; i < n; i++) {
		uint16_t *p = acq[i].samples;
		acq[i].board = 7;
		acq[i].channel = 32 - i;
		acq[i].nsamples = ns;
		if (i == 0)
			/* fill with visually recognizable pattern */
			for (cnt = 0; cnt < ns; cnt++)
				p[cnt] = (cnt + 1968) % 10000;
		else
			/* copy channel 0 */
			memcpy(p, acq[0].samples, sizeof(acq[0].samples));
		acq[i].done = 1;
	}
	gettimeofday(&tv2, NULL);
	printf("time at gimme_data: %ld usec\n",
		(tv2.tv_sec * 1000000 + tv2.tv_usec) -
		(tv1.tv_sec * 1000000 + tv1.tv_usec));
	return n;
}
