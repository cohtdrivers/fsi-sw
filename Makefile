SOLIBDIR = ./solib
LIBFSIDIR = ./libfsi
FSIUTIL = ./fsi-util

GIT_VER=$(shell git describe --tags --long --always --dirty=.dirty)
PYTHON ?= python3
DATA_FILES = fsi_acq*.dat
DATA_FILES += 202?????T??????.mat 202?????T??????_fft.mat
DATA_FILES += fsi-data-202?????T??????.txt

.PHONY: all clean wheel version clean-wheels

all: version

ifm: CFLAGS += -g -Wall
ifm: LDFLAGS=-lrt
ifm: ifm.o

clean all:
	$(MAKE) -C $(SOLIBDIR) $@
	$(MAKE) -C $(LIBFSIDIR) $@
	# $(MAKE) -C $(FSIUTIL) $@
all: solibs fsi-solibs fsi/Changelog
solibs: $(SOLIBDIR)/memo.so $(LIBFSIDIR)/libfsi.so
fsi-solibs: $(SOLIBDIR)/memo.so $(LIBFSIDIR)/libfsi.so
	cp $^ fsi
fsi/Changelog: Changelog
	cp $^ $@
$(SOLIBDIR)/memo.so:
	$(MAKE) -C $(SOLIBDIR) $(@F)
$(LIBFSIDIR)/libfsi.so:
	$(MAKE) -C $(LIBFSIDIR) $(@F)

clean: clean-here clean-wheels clean-data
wheel: version all
	$(PYTHON) -m build
clean-wheels:
	$(RM) dist/*.whl
	$(RM) -rf *.egg-info
clean-data:
	$(RM) -f $(DATA_FILES)
clean-here:
	$(RM) -f *.o
	$(RM) -f ifm peak.bin
	$(RM) -f fsi-[0-9]*[0-9].log
install: clean-wheels wheel
	$(PYTHON) -m pip uninstall -y fsi
	$(PYTHON) -m pip install -q dist/fsi*.whl
version:
	echo $(GIT_VER)
	echo "$(GIT_VER)" > fsi/.version
	cp fsi/.version $(LIBFSIDIR)
