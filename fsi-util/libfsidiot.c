/*
 * libfsidiot - library to control a DIOT FSI application 
 *
 * Author: Juan David Gonzalez Cobas (dcobas at cern.ch)
 * Based on code by Tristan Gingold and Maciej Marek Lipinski
 * Copyright CERN 2023
 */

#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>

#include "diot_fsi_regs.h"
#include "fsi_diot_hw.h"
#include "libfsidiot.h"

static void *m_axi_base = NULL;
static unsigned page_size;

void m_axi_writel(uint32_t addr, uint32_t data)
{
	if (addr > page_size) {
              printf("%s: address=%d goes beyond page boundry \n",__func__, addr);
              return;
        }
        *(volatile uint32_t *)(m_axi_base + addr) = data;
        if (DEBUG_HW)
		printf("%s: Write: addr=0x%x | data=0x%x \n",__func__, addr, data);
}

uint32_t m_axi_readl(uint32_t addr)
{
	uint32_t val;

	if(addr > page_size) {
              printf("%s: address=%d goes beyond page boundry \n",__func__, addr);
              return 0;
        }
        val = *(volatile uint32_t *)(m_axi_base + addr);
        if (DEBUG_HW)
		printf("%s: Read: addr=0x%x | data=0x%x \n",__func__, addr, val);

        return val;
}

int m_axi_map(int addr)
{
        int fd;
        int value = 0;

        unsigned page_addr, page_offset;
        page_size = sysconf(_SC_PAGESIZE);

        /* Open /dev/mem file */
        fd = open ("/dev/mem",  O_RDWR );
        if (fd < 1) {
                printf("%s: /dev/mem: %s\n", __func__, strerror(errno));
                exit(1);
        }
        if (DEBUG_HW)
		printf("%s: opened /dev/mem \n",__func__);
        /* mmap the device into memory */
        page_addr = (addr & (~(page_size-1)));
        page_offset = addr - page_addr;
        m_axi_base = mmap(NULL, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, page_addr);

        close(fd);
        if (m_axi_base == MAP_FAILED) {
                fprintf(stderr, "%s: mmap(/dev/mem): %s\n",__func__, strerror(errno));
                exit(1);
        }
        if (DEBUG_HW)
		printf("%s: mmap() doen \n",__func__);

        /* Read value from the device register */
        value = (int)m_axi_readl(page_offset);
        printf("%s: Address = 0x%08x | Value = 0x%08x \n",__func__, page_addr, value);

        return value;
}

int m_axi_unmap(void)
{
        unsigned page_size=sysconf(_SC_PAGESIZE);
        munmap(m_axi_base, page_size);
        return 0;
}

void *axi_map(unsigned addr)
{
	m_axi_map(addr);
	return m_axi_base;
}

void axi_unmap(void *addr)
{
	m_axi_unmap();
}

/* return the boards present in the DIOT crate: bit i indicates
 * board (i+1) present, as boards are numbered 1->8 left to right
 */
uint32_t fsi_get_present_boards_mask(void)
{
        uint32_t s1;
	s1 = m_axi_readl(SYS_TOP_SUB_REG_PERIPH_STAT1);
	return (~s1 & SYS_TOP_SUB_REG_PERIPH_STAT1_INP_MASK) >>
		      SYS_TOP_SUB_REG_PERIPH_STAT1_INP_SHIFT;
}

/*
 *  select boards which will be affected by next stateful ADC command.
 *
 *  \param mask
 *  	mask of boards affected
 *  \warning
 *  	as usual, board i in 1..8 maps to bit (i-1) in 0..7
 */
void fsi_set_boards(uint8_t mask)
{
	m_axi_writel(SYS_TOP_SUB_REG_CMD_BOARDS, mask);
}

/* pick one board to exec an ADC command.
 * board must be 1..8. Other values are ignored
 */
void fsi_sel_board(int board)
{
	uint8_t mask = 1<<(board-1);
	fsi_set_boards(mask);
}

/* Get last reply from board (1-8) at fsi SLOT (0-7).  */
uint32_t fsi_get_reply(unsigned board)
{
	int slot = board - 1;
	return m_axi_readl(SYS_TOP_SUB_REG_CMD_REPLY | (slot * 4));
}

/* Get execution status of last sent ADC cmd  */
uint32_t fsi_get_status(void)
{
	return m_axi_readl(SYS_TOP_SUB_REG_CMD_STATUS);
}

void fsi_wait_cmd(void)
{
	uint32_t mask;
	unsigned i;

	for (i = 0; i < 4; i++) {
		mask = m_axi_readl(SYS_TOP_SUB_REG_CMD_BOARDS);
		uint32_t status = m_axi_readl(SYS_TOP_SUB_REG_CMD_STATUS);
		uint32_t done = (status >> SYS_TOP_SUB_REG_CMD_STATUS_DONE_SHIFT);
		if ((done & mask) == mask)
			return;
	}

	/* FIXME: Could happen if a board is sending samples.  */
	printf("cmd timeout\n");
}

void fsi_send_value(uint32_t val)
{
	m_axi_writel(SYS_TOP_SUB_REG_CMD_VALUE, val);
	m_axi_writel(SYS_TOP_SUB_REG_CMD_CTRL, SYS_TOP_SUB_REG_CMD_CTRL_SEND);
	fsi_wait_cmd();
}

void fsi_exec_cmd(uint32_t cmd)
{
	m_axi_writel(SYS_TOP_SUB_REG_CMD_VALUE, cmd);
	m_axi_writel(SYS_TOP_SUB_REG_CMD_CTRL, SYS_TOP_SUB_REG_CMD_CTRL_EXEC);
	fsi_wait_cmd();
}

uint32_t fsi_get_afe(int board)
{
	/* NB (Mar 9 2023): the FSI_ADC_EXEC_GET_AFE command is not
	 * implemented in HDL: it will fail with a 'cmd timeout' trace.
	 * In other words, until HDL is fixed, this function is useless
	 * and we have to trust the success of our fsi_set_afe
	 * operations, blindly */
	fsi_sel_board(board);
	fsi_exec_cmd(FSI_ADC_EXEC_GET_AFE);
	fsi_exec_cmd(FSI_ADC_EXEC_GETDATA);
	return fsi_get_reply(board);
}

/*
 * set TIA gain for the 8 channels of board at once.
 *
 * \param board
 * 	a board number in the 1..8 range
 * \param amp_mask
 * 	bit n == 1 to set TIA gain == 3,
 * 	bit n == 0 to set TIA gain == 1 (no gain),
 *	bits 0..7 correspond to channels 0..7
 */
int fsi_set_afe(int board, uint8_t amp_mask)
{
	fsi_sel_board(board);
	fsi_send_value(amp_mask);
	fsi_exec_cmd(FSI_ADC_EXEC_SET_AFE);
	return fsi_get_reply(board);
}

/*
 * get AC/DC coupling state of a particular board's channel 0
 *
 * \param board
 * 	a board number in the 1..8 range
 * \return
 * 	one of
 * 	FSI_ADC_ACDC_NONE	undefined (initial state)
 * 	FSI_ADC_ACDC_AC		AC coupled
 * 	FSI_ADC_ACDC_DC		DC coupled
 */
uint8_t fsi_get_acdc(int board)
{
	uint32_t coupling = FSI_ADC_ACDC_NONE;
	
	fsi_sel_board(board);
	fsi_send_value (0x0);
	fsi_exec_cmd(FSI_ADC_EXEC_ACDC);
	coupling = 0xF & fsi_get_reply(board);	/* presence? */
	return coupling;
}

/*
 * set AC/DC coupling state of a particular board's channel 0
 *
 * \param board
 * 	a board number in the 1..8 range
 * \param coupling
 * 	one of
 * 	FSI_ADC_ACDC_NONE	undefined (do not use!)
 * 	FSI_ADC_ACDC_AC		AC coupled
 * 	FSI_ADC_ACDC_DC		DC coupled
 * \warning
 * 	action may take some time (?) to complete; do not read back
 * 	status with \ref fsi_get_acdc until one second (?) has elapsed
 */
void fsi_set_acdc(int board, uint8_t coupling)
{
	fsi_sel_board(board);
	fsi_send_value(coupling);
	fsi_exec_cmd(FSI_ADC_EXEC_ACDC);
}

/*
 * enable/disable acquisition trigger
 *
 * \param en
 *      enable trigger if en != 0; disable it otherwise
 */
void fsi_trig_enable(unsigned en)
{
	unsigned config;

	config = m_axi_readl(SYS_TOP_SUB_REG_CONFIG);
	config &= ~SYS_TOP_SUB_REG_CONFIG_TRIG_EN;
	if (en)
		config |= SYS_TOP_SUB_REG_CONFIG_TRIG_EN;
	m_axi_writel(SYS_TOP_SUB_REG_CONFIG, config);
}

uint32_t fsi_get_config(void)
{
	uint32_t config = m_axi_readl(SYS_TOP_SUB_REG_CONFIG);
	return config;
}

/*
 * obtain length of DMA buffer
 */
uint32_t get_dma_length(void)
{
	return m_axi_readl(SYS_TOP_SUB_REG_LENGTH);
}



/*
 * set length of DMA buffer
 *
 * \param length
 * 	new DMA buffer length value 
 */
void set_dma_length(uint32_t length)
{
	m_axi_writel(SYS_TOP_SUB_REG_LENGTH, length);
}


void peripheral_rst_ctrl(char *p)
{
	/* GPIO based - betterdo via diot-util -- otherwise, pull the
	 * entire gpio definition stuff :(
	 */
}
/*
 * perform a reset of the FSI gateware
 *
 * \warning
 * 	Dependency of fsi_reset on peripheral_rst_ctrl needs to be
 * 	elucidated (and eliminated, if possible). To shut up the
 * 	compiler, the diot_gpio-dependent logic is created as a noop: it
 * 	won't reset anything
 */
void fsi_reset(void)
{
	uint32_t val;
	int diot_gpio = 0;		/* do not do via diot_gpio */

	val = m_axi_readl(SYS_TOP_SUB_REG_CONFIG);

	/* Enable clock, put 0 on rst_n  */
	val |= SYS_TOP_SUB_REG_CONFIG_CLK_SEL_MASK;
	if (!diot_gpio)
		val &= ~SYS_TOP_SUB_REG_CONFIG_RST_N;
	else
		peripheral_rst_ctrl("start");
	m_axi_writel(SYS_TOP_SUB_REG_CONFIG, val);
	m_axi_writel(SYS_TOP_SUB_REG_CONFIG, val);

	/* Release rst_n  */
	if (!diot_gpio) {
		val |= SYS_TOP_SUB_REG_CONFIG_RST_N;
		m_axi_writel(SYS_TOP_SUB_REG_CONFIG, val);
	} else
		peripheral_rst_ctrl("stop");
}

uint64_t fsi_read_clock(void)
{
	uint32_t hi, hi1, lo;

	hi = m_axi_readl(SYS_TOP_SUB_REG_STAT_CLOCK | 4);
	while (1) {
		lo = m_axi_readl(SYS_TOP_SUB_REG_STAT_CLOCK);
		hi1 = m_axi_readl(SYS_TOP_SUB_REG_STAT_CLOCK | 4);
		if (hi1 == hi)
			break;
		hi = hi1;
	}
	return ((uint64_t)hi << 32) | (uint64_t)lo;
}

void fsi_resync(void)
{
	unsigned val;
	val = m_axi_readl(SYS_TOP_SUB_REG_CONFIG);
	val |= SYS_TOP_SUB_REG_CONFIG_RESYNC;
	m_axi_writel(SYS_TOP_SUB_REG_CONFIG, val);
}

/*
 * program a delay
 *
 * \param board
 * 	board to program, in the range 1..8
 * \param lane
 * 	lane for the delay, in the range 0..12
 * \return
 * 	0 on success
 * 	-1 on invalid params
 */
int fsi_delay_control(int board, int lane, int incflag)
{
	uint32_t sel, val;

	if (board < 1 || board > 8)
		return -1;
	if (lane < 0 || lane > 12)
		return -1;

	sel = ((board - 1) << SYS_TOP_SUB_REG_STAT_SEL_BOARD_SHIFT)
		| (lane << SYS_TOP_SUB_REG_STAT_SEL_CH_SHIFT)
		| SYS_TOP_SUB_REG_STAT_SEL_BRANCH;
	m_axi_writel(SYS_TOP_SUB_REG_STAT_SEL, sel);
	val = m_axi_readl(SYS_TOP_SUB_REG_DELAY_CTRL);
	if (incflag)
		val |= SYS_TOP_SUB_REG_DELAY_CTRL_INC;
	else
		val |= SYS_TOP_SUB_REG_DELAY_CTRL_DEC;
	m_axi_writel(SYS_TOP_SUB_REG_DELAY_CTRL, val);
	return 0;
}

char *fsi_regs_display(void)
{
	volatile struct sys_top_sub_reg *base_addr = m_axi_base;
	static const char fmt[] =
		"ident:			0x%08x\n"
		"status:			0x%08x\n"
		"control			0x%08x\n"
		"config:			0x%08x\n"
		"length:			0x%08x\n"

		"cfg1 address:		0x%08x\n"
		"cfg1 cur_address:	0x%08x\n"
		"cfg1 status:		0x%08x\n"
		"cfg2 address:		0x%08x\n"
		"cfg2 cur_address:	0x%08x\n"
		"cfg2 status:		0x%08x\n"

		"periph_stat1:		0x%08x\n"
		"periph_stat2:		0x%08x\n"

		"	cmd_boards:			0x%08x\n"
		"	cmd_value:			0x%08x\n"
		"	cmd_ctrl:			0x%08x\n"
		"	cmd_status:			0x%08x\n"
		"	cmd_reply[0]:			0x%08x\n"
		"	cmd_reply[1]:			0x%08x\n"
		"	cmd_reply[2]:			0x%08x\n"
		"	cmd_reply[3]:			0x%08x\n"
		"	cmd_reply[4]:			0x%08x\n"
		"	cmd_reply[5]:			0x%08x\n"
		"	cmd_reply[6]:			0x%08x\n"
		"	cmd_reply[7]:			0x%08x\n";
	static char printable[sizeof(*base_addr) * 2 +sizeof(fmt)];
	snprintf(printable, sizeof(printable),
		fmt,
		base_addr->ident,
		base_addr->status,
		base_addr->control,
		base_addr->config,
		base_addr->length,

		base_addr->configs[0].address,
		base_addr->configs[0].cur_address,
		base_addr->configs[0].status,
		base_addr->configs[1].address,
		base_addr->configs[1].cur_address,
		base_addr->configs[1].status,

		base_addr->periph_stat1,
		base_addr->periph_stat2,

			base_addr->cmd_boards,
			base_addr->cmd_value,
			base_addr->cmd_ctrl,
			base_addr->cmd_status,
			base_addr->cmd_reply[0].value,
			base_addr->cmd_reply[1].value,
			base_addr->cmd_reply[2].value,
			base_addr->cmd_reply[3].value,
			base_addr->cmd_reply[4].value,
			base_addr->cmd_reply[5].value,
			base_addr->cmd_reply[6].value,
			base_addr->cmd_reply[7].value);
	return printable;
}

void fsi_dma_init(uint32_t nsamp)
{
	m_axi_writel(SYS_TOP_SUB_REG_CONFIGS |
			SYS_TOP_SUB_REG_CONFIGS_ADDRESS, PHYSICAL_ADDR+DMA0_BUF_ADDR);
	m_axi_writel(SYS_TOP_SUB_REG_CONFIGS |
			SYS_TOP_SUB_REG_CONFIGS_ADDRESS |
			SYS_TOP_SUB_REG_CONFIGS_SIZE, PHYSICAL_ADDR+DMA1_BUF_ADDR);
	m_axi_writel(SYS_TOP_SUB_REG_LENGTH, nsamp);
}

void fsi_dma_cache_invalid_with_nsamp(unsigned nsamp)
{
	printf("Invalidating cache not implemented, unexpected behavior possible !\n");
	//   Xil_DCacheInvalidateRange(DMA0_BUF_ADDR, nsamp * 16 * 3);
	//   Xil_DCacheInvalidateRange(DMA1_BUF_ADDR, nsamp * 16 * 3);
}
