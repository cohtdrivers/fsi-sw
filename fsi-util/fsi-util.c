#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/mman.h>

#include <readline/readline.h>
#include <readline/history.h>

#include "diot_fsi_regs.h"
#include "fsi_diot_hw.h"
#include "libfsidiot.h"

static void *global_base = 0;
/*
int cmd_fsi_dump(char *args)
{
	int fd;
	unsigned i, j;
	unsigned page_addr;
	static void *dma_base;

	if (arg_memory("dump", args) != 0)
		printf("default params: adr=0x%x, len=%d\n",arg_addr,arg_len);
	//     return -1;

	page_addr = (arg_addr & (~(page_size-1)));
	dma_base = mmap(NULL, 0x10000000, PROT_READ|PROT_WRITE, MAP_SHARED, fd, page_addr);
	printf("Ignoring arg_len=%d, seting len to %d (page size)\n", arg_len, page_size);
	close(fd);
	if (dma_base == MAP_FAILED) {
		fprintf(stderr, "%s: mmap(/dev/mem): %s\n",__func__, strerror(errno));
		return -1;
	}
	printf("%s: mmap() done \n",__func__);


	for (i = 0; i < arg_len; i += 16)
	{
		printf("%08x:", page_addr + i);
		for (j = 0; j < 16; j += 4)
		{
			//           if(i+j > page_size)
			//           {
			//              printf("\n");
			//              printf("end of page\n");
			//              break;
			//           }

			unsigned val = *(volatile uint32_t *)(dma_base + i + j);
			printf(" %08x", val);
		}
		printf("\n");
	}

	arg_addr += arg_len;
	munmap(dma_base, page_size);
	return 0;
}
*/

int emio_init(void)
{
	/*TODO: reset PL?
	printf("Blink LED2\n");
	cmd_led2("on");
	sleep(1);
	cmd_led2("off");
	*/
	return 0;
}

static const char *decode_coupling(int coupling)
{
	switch (coupling) {
	case FSI_ADC_ACDC_NONE:
		return "NONE";
	case FSI_ADC_ACDC_AC:
		return "AC";
	case FSI_ADC_ACDC_DC:
		return "DC";
	default:
		return "**error**";
	}
}

void test_set_acdc(void)
{
	unsigned board;

	fsi_set_acdc(1, FSI_ADC_ACDC_AC);
	fsi_set_acdc(2, FSI_ADC_ACDC_DC);
	fsi_set_acdc(3, FSI_ADC_ACDC_AC);
	fsi_set_acdc(4, FSI_ADC_ACDC_DC);
	fsi_set_acdc(5, FSI_ADC_ACDC_AC);
	fsi_set_acdc(6, FSI_ADC_ACDC_DC);
	fsi_set_acdc(7, FSI_ADC_ACDC_AC);
	fsi_set_acdc(8, FSI_ADC_ACDC_DC);
	sleep(1);

	printf("all AC/DC...\n");
	for (board = 1; board <= 8; board++) {
		printf("board# %d: %s\n", board, decode_coupling(fsi_get_acdc(board)));
	}

	
	fsi_set_acdc(1, FSI_ADC_ACDC_DC);
	fsi_set_acdc(2, FSI_ADC_ACDC_AC);
	fsi_set_acdc(3, FSI_ADC_ACDC_DC);
	fsi_set_acdc(4, FSI_ADC_ACDC_AC);
	fsi_set_acdc(5, FSI_ADC_ACDC_DC);
	fsi_set_acdc(6, FSI_ADC_ACDC_AC);
	fsi_set_acdc(7, FSI_ADC_ACDC_DC);
	fsi_set_acdc(8, FSI_ADC_ACDC_AC);
	sleep(1);

	printf("all DC/AC...\n");
	for (board = 1; board <= 8; board++) {
		const char *symbol = decode_coupling(fsi_get_acdc(board));
		printf("board# %d: %s\n", board, symbol);
	}

	fsi_set_acdc(1, FSI_ADC_ACDC_DC);
	fsi_set_acdc(2, FSI_ADC_ACDC_DC);
	fsi_set_acdc(3, FSI_ADC_ACDC_DC);
	fsi_set_acdc(4, FSI_ADC_ACDC_DC);
	fsi_set_acdc(5, FSI_ADC_ACDC_DC);
	fsi_set_acdc(6, FSI_ADC_ACDC_DC);
	fsi_set_acdc(7, FSI_ADC_ACDC_DC);
	fsi_set_acdc(8, FSI_ADC_ACDC_DC);
	sleep(1);

	printf("all DC...\n");
	for (board = 1; board <= 8; board++) {
		printf("board# %d: %s\n", board, decode_coupling(fsi_get_acdc(board)));
	}
}

/*
 *	init				Inits FSI acquisition
 *	dma n samps			Play with FSI DMA
 *	dump adr len			Dump data from memory
 *	regs				Dump FSI regs
 *	config [+-[patterns|trig|rst|clk]|resync]
 *					Set FSI config regs
 *	clear adr len			Clear samples in  memory
 *	samp samp [-0|-1] [NBR]		Read samples from mem
 *	read				Read data
 *	do_cmd boards mask		ADC command - select boards
 *	do_cmd value value		ADC command - send value
 *	do_cmd exec cmd   		ADC command - send command
 *	do_cmd status 		ADC command - request cmd status
 *	do_cmd reply  		ADC command - show replies
 *
 *	rst     	  	  	reset periph boards
 *	status  	  	  	disp board status
 *	ramp    	  	  	ADC send ramp pattern [unsupported]
 *	normal  	  	  	ADC send data [unsupported]
 *	on/off  	  	  	enable/disable trigger
 *	length [NUM]	  	  	disp/set number of samples per trigger
 *	afe [BOARD VAL]   	  	disp/set analog front-end gain
 *	acdc [BOARD VAL]  	  	disp/set coupling for chan1: AC or DC
 *	stat [clear]	  	  	disp (or clear) statistics
 *	resync  			resync iserdes bit slip
 *	vtc [on|off]			enable/disable iodelay vtc
 *	freeze [on|off]			enable/disable iodelay adjustments
 *	ddelay BRD LANE inc|dec		adjust data iserdes odelay
 *	sdelay BRD LANE inc|dec		adjust sampling iserdes odelay
 *	prbs [on|off]			[undocumented]
 */
int cmd_fsi_init(char *args);

#define CHECK_ARGS(required)			\
do { 								\
	if (argc != (required + 1)) {				\
		printf("%s, bad number of args\n", argv[0]);	\
		return -1;					\
	}							\
} while (0)

int do_init(int argc, char *argv[])
{
	CHECK_ARGS(0);
	cmd_fsi_init(NULL);

	return 0;
}

int do_status(int argc, char *argv[])
{
	unsigned s1 = m_axi_readl(SYS_TOP_SUB_REG_PERIPH_STAT1);
	unsigned s2 = m_axi_readl(SYS_TOP_SUB_REG_PERIPH_STAT2);
	unsigned mask = ~s1 & SYS_TOP_SUB_REG_PERIPH_STAT1_INP_MASK;
	unsigned i;

	CHECK_ARGS(0);
	printf("stat1: %08x, stat2: %08x\n", s1, s2);
	printf("spi-busy: %02x, trigger: %02x, presence: %02x\n",
		(s1 >> SYS_TOP_SUB_REG_PERIPH_STAT1_SPI_BUSY_SHIFT) & mask,
		(s1 >> SYS_TOP_SUB_REG_PERIPH_STAT1_TRIGGERS_SHIFT) & mask,
		mask);

	for (i = 0; i < 8; i++) {
		if (!(mask & (1 << i))) {
			/* Board not present.  */
			continue;
		}
		printf ("slot %u: ", i + 1);

		if (!(s2 & (1 << i)))
			printf ("!");
		printf("rx-locked ");
		if (s2 & (1 << i)) {
			if (!(s2 & (1 << (8 + i))))
				printf ("!");
			printf("adc-locked ");

			if (!(s2 & (1 << (16 + i))))
				printf ("!");
			printf("sys-locked ");

			if (!(s2 & (1 << (24 + i))))
				printf ("!");
			printf("fsi-ready");
		}
		printf("\n");
	}
	return 0;
}


int do_regs(int argc, char *argv[])
{
	CHECK_ARGS(0);
	printf(fsi_regs_display());
	return 0;
}

int do_trigger(int argc, char *argv[])
{
	int enable = 0;

	CHECK_ARGS(1);

	if (strcmp(argv[1], "on"))
		enable = 1;
	else if (strcmp(argv[1], "off"))
		enable = 0;
	else {
		printf("bad argument %s\n", argv[1]);
		return -1;
	}
	fsi_trig_enable(enable);
	return 0;
}

int do_length(int argc, char *argv[])
{
	if (argc == 1) {
		printf("current length: %u\n", get_dma_length());
	} else if (argc == 2) {
		unsigned long val = strtol(argv[1], NULL, 0);

		if (val <= 0) {
			printf("bad length number\n");
			return -1;
		} else
			set_dma_length(val);
	}
	return 0;
}

void display_stats(void)
{
	unsigned long clk_ms = fsi_read_clock() / 100000;

	printf ("clock: %lu sec + %lu msec\n",
			clk_ms / 1000, clk_ms % 1000);

	uint32_t mask = fsi_get_present_boards_mask();

	for (unsigned i = 0; i < 8; i++) {
		if (!(mask & (1 << i))) {
			/* Board not present.  */
			continue;
		}
		for (unsigned j = 0; j < 13; j++) {
			unsigned v;
			unsigned sel;

			sel = (i << SYS_TOP_SUB_REG_STAT_SEL_BOARD_SHIFT)
				| (j << SYS_TOP_SUB_REG_STAT_SEL_CH_SHIFT);
			m_axi_writel(SYS_TOP_SUB_REG_STAT_SEL, sel);
			v = m_axi_readl(SYS_TOP_SUB_REG_STAT_BYTE_LOCK);
			printf("slot %u, la %02u: byte:%08x ", i + 1, j, v);
			v = m_axi_readl(SYS_TOP_SUB_REG_STAT_BIT_LOCK);
			printf("bit:%08x ", v);
			v = m_axi_readl(SYS_TOP_SUB_REG_STAT_PRBS);
			printf("prbs:%08x -", v);
			for (unsigned k = 0; k < 4; k++) {
				m_axi_writel(SYS_TOP_SUB_REG_STAT_SEL, sel | (k << 8));
				v = m_axi_readl(SYS_TOP_SUB_REG_DELAY_CNTVALUE);
				printf (" %03u", v);
			}
			printf("\n");
		}
	}
}

int do_stat(int argc, char *argv[])
{
	if (argc == 1)
		display_stats();
	else if (argc == 2 && strcmp(argv[2], "clear") == 0) {
		m_axi_writel(SYS_TOP_SUB_REG_STAT_SEL, SYS_TOP_SUB_REG_STAT_SEL_CLEAR);
	} else {
		printf("bad stat command\n");
		return -1;
	}
	return 0;
}

int do_resync(int argc, char *argv[])
{
	CHECK_ARGS(0);
	fsi_resync();
	return 0;
}

int do_vtc(int argc, char *argv[])
{
	uint32_t val;

	val = m_axi_readl(SYS_TOP_SUB_REG_DELAY_CTRL);
	if (argc == 1) {
		printf ("delay ctrl: %04x ", val);
		if (!(val & SYS_TOP_SUB_REG_DELAY_CTRL_VTC_EN))
			printf("!");
		printf("vtc");
		printf("\n");
		return 0;
	} else if (argc == 2 && strcmp(argv[1], "on") == 0) 
		val |= SYS_TOP_SUB_REG_DELAY_CTRL_VTC_EN;
	else if (strcmp(argv[1], "off") == 0)
		val &= ~SYS_TOP_SUB_REG_DELAY_CTRL_VTC_EN;
	else {
		printf("bad vtc command\n");
		return -1;
	}
	m_axi_writel(SYS_TOP_SUB_REG_DELAY_CTRL, val);
	return 0;
}


int do_afe(int argc, char *argv[])
{
	if (argc == 1) {
		unsigned boards = fsi_get_present_boards_mask();
		for (unsigned i = 0; i < 8; i++) {
			if (!(boards & (1 << i))) {
				/* Board not present.  */
				continue;
			}
			printf("slot %u: afe=%02x\n", i + 1, fsi_get_afe(i+1));
		}
	} else if (argc == 3) {
		char *endptr;
		unsigned brd = strtol(argv[1], NULL, 0);
		uint8_t msk = strtoul(argv[2], &endptr, 0);

		if (brd < 1 || brd > 8) {
			printf ("incorrect board number\n");
			return -1;
		}
		if (*endptr != 0) {
			printf ("amplification mask must be a number\n");
			return -1;
		}
			fsi_set_afe(brd, msk);
	} else {
		printf("incorrect number of arguments\n");
		return -1;
	}
	return 0;
}

int do_acdc(int argc, char *argv[])
{
	int board;
	if (argc == 1) {
		uint8_t present = fsi_get_present_boards_mask();
		printf("present boards: %x\n", present);
		for (board = 1; board <= 8; board++) {
			uint8_t coupling;
			char *coupling_text;
			if (!(present & (1<<(board-1))))
				continue;
			coupling = fsi_get_acdc(board);
			switch (coupling) {
				case FSI_ADC_ACDC_AC:
					coupling_text = "AC";
					break;
				case FSI_ADC_ACDC_DC:
					coupling_text = "DC";
					break;
				case FSI_ADC_ACDC_NONE:
					coupling_text = "undefined";
					break;
				default:
					printf("bad coupling value %x\n", coupling);
					return -1;
			}
			printf("slot %d: %s coupled (%x)\n", board, coupling_text, coupling);
		}
	} else if (argc == 3) {
		unsigned board = strtoul(argv[1], NULL, 0);
		char *coupling_str = argv[2];
		unsigned coupling;

		if (board < 1 || board > 8) {
			printf ("incorrect board number\n");
			return 0;
		}
		if (strcmp(coupling_str, "ac") == 0 ||
			(strtoul(coupling_str, NULL, 0) == FSI_ADC_ACDC_AC))
				coupling = FSI_ADC_ACDC_AC;
		else if (strcmp(coupling_str, "dc") == 0 ||
			(strtoul(coupling_str, NULL, 0) == FSI_ADC_ACDC_DC))
				coupling = FSI_ADC_ACDC_DC;
		else {
			printf("coupling (%s) must be: {ac|dc|0x1|0x2}\n", coupling_str);
			return -1;
		}
		fsi_set_acdc(board, coupling);
	} else {
		printf("bad number of arguments\n");
		return -1;
	}
	return 0;
}

int do_dma(int argc, char *argv[])
{
	unsigned status;
	unsigned nsamp = 200;
	unsigned val;

	if (argc == 2) {
		nsamp = strtol(argv[1], NULL, 0);
		fsi_dma_init(nsamp);
	} else if (argc == 1) {
		nsamp = get_dma_length();
	} else {
		printf("incorrect number of arguments\n");
		return -1;
	}

	fsi_dma_cache_invalid_with_nsamp(nsamp);
	printf("Starting (%u samps)...\n", nsamp);

	// check if all peripheral boards are ready
	status = m_axi_readl(SYS_TOP_SUB_REG_STATUS);
	if ((status & SYS_TOP_SUB_REG_STATUS_READY) != SYS_TOP_SUB_REG_STATUS_READY) {
		printf("Peripherals not ready\n");
		return -1;
	}
	val = m_axi_readl(SYS_TOP_SUB_REG_CONTROL);
	m_axi_writel(SYS_TOP_SUB_REG_CONTROL,
			val | SYS_TOP_SUB_REG_CONTROL_SW_TRIG);
	do {
		// usleep(10);
		status = m_axi_readl(SYS_TOP_SUB_REG_STATUS);
	} while ((status & SYS_TOP_SUB_REG_STATUS_DMA_DONE) == 0);	// Wait for the DMA transfer to complete
	// clear the done flag
	m_axi_writel(SYS_TOP_SUB_REG_STATUS, SYS_TOP_SUB_REG_STATUS_DMA_DONE);

	printf("Done 0x%08x - 0x%08x\n",
			DMA0_BUF_ADDR, DMA0_BUF_ADDR + nsamp * 16 * 3 - 1);
	fsi_dma_cache_invalid_with_nsamp(nsamp);

	return 0;
}

int do_cmd_boards(int argc, char *argv[])
{
	uint8_t	mask;

	CHECK_ARGS(1);
	mask = strtoul(argv[1], NULL, 0);
	fsi_set_boards(mask);

	return 0;
}

int do_cmd_value(int argc, char *argv[])
{
	uint32_t value;

	CHECK_ARGS(1);
	value = strtoul(argv[1], NULL, 0);
	fsi_send_value(value);

	return 0;
}

int do_cmd_exec(int argc, char *argv[])
{
	uint32_t cmd;

	CHECK_ARGS(1);
	cmd = strtoul(argv[1], NULL, 0);
	fsi_exec_cmd(cmd);

	return 0;
}

int do_cmd_status(int argc, char *argv[])
{
	CHECK_ARGS(0);
	printf("cmd status: %08x\n", fsi_get_status());

	return 0;
}

int do_cmd_reply(int argc, char *argv[])
{
	unsigned board;

	CHECK_ARGS(1);
	board = strtol(argv[1], NULL, 0);
	if (board < 1 || board > 8) {
		printf("incorrect board number\n");
		return -1;
	}
	printf("board %d reply: %08x\n", board, fsi_get_reply(board));

	return 0;
}

int do_cmd_wait(int argc, char *argv[])
{
	CHECK_ARGS(0);

	fsi_wait_cmd();

	return 0;
}

/*
int do_(int argc, char *argv[])
{
	return 0;
}

int do_(int argc, char *argv[])
{
	return 0;
}

*/

int do_help(int argc, char *argv[]);

int cmd_fsi_init(char *args)
{
	printf("Enter emio_init()\n");
	//emio_init();

	printf("Enter m_axi_map()\n");
	//m_axi_map(M_AXI_BASE);

	printf("Enter fsi_reset()\n");
	//fsi_reset();

	printf("Enter dma_init()\n");
	//dma_init(250);

	printf("Enter fsi_trig_enable()\n");

	//fsi_trig_enable(1);
	//fsi_initialized = 1;
	//
	return 0;
}

struct fsi_cmds {
	char *cmd;
	char *help;
	int (*function)(int argc, char *argv[]);
};
static struct fsi_cmds cmds[] = {
	{ "help",	"help				Show this help",				do_help},
	{ "init",	"init				Inits FSI acquisition",				do_init},
	{ "dma",	"dma nsamps			Play with FSI DMA",				do_dma},
	{ "dump",	"dump adr len			Dump data from memory",				NULL},
	{ "regs",	"regs				Dump FSI regs",					do_regs},
	{ "config",	"config [+-[patterns|trig|rst|clk]|resync]	Set FSI config regs",		NULL},
	{ "clear",	"clear adr len			Clear samples in  memory",			NULL},
	{ "samp",	"samp samp [-0|-1] [NBR]		Read samples from mem",				NULL},
	{ "read",	"read				Read data",					NULL},
	{ "cmd_boards",	"cmd_boards mask			ADC command - select boards",			do_cmd_boards},
	{ "cmd_value",	"cmd_value value			ADC command - send value",			do_cmd_value},
	{ "cmd_exec",	"cmd_exec cmd   			ADC command - send command",			do_cmd_exec},
	{ "cmd_status",	"cmd_status 			ADC command - request cmd status",		do_cmd_status},
	{ "cmd_reply",	"cmd_reply  			ADC command - show replies",			do_cmd_reply},
	{ "rst",	"rst     	  	  	reset periph boards",				NULL},
	{ "status",	"status  	  	  	disp board status",				do_status},
	{ "ramp",	"ramp    	  	  	ADC send ramp pattern [unsupported]",		NULL},
	{ "normal",	"normal  	  	  	ADC send data [unsupported]",			NULL},
	{ "trigger",	"trigger on/off	  	  	enable/disable trigger",			do_trigger},
	{ "length",	"length [NUM]	  	  	disp/set number of samples per trigger",	do_length},
	{ "afe",	"afe [BOARD VAL]   	  	disp/set analog front-end gain",		do_afe},
	{ "acdc",	"acdc [BOARD VAL]  	  	disp/set coupling for chan1: AC or DC",		do_acdc},
	{ "stat",	"stat [clear]	  	  	disp (or clear) statistics",			do_stat},
	{ "resync",	"resync  			resync iserdes bit slip",			do_resync},
	{ "vtc",	"vtc [on|off]			enable/disable iodelay vtc",			do_vtc},
	{ "freeze",	"freeze [on|off]			enable/disable iodelay adjustments",		NULL},
	{ "ddelay",	"ddelay BRD LANE inc|dec		adjust data iserdes odelay",			NULL},
	{ "sdelay",	"sdelay BRD LANE inc|dec		adjust sampling iserdes odelay",		NULL},
	{ "prbs",	"prbs [on|off]			[undocumented]",				NULL},
	{ NULL, },
};

int do_help(int argc, char *argv[])
{
	struct fsi_cmds *ptr;

	for (ptr = cmds; ptr->cmd != NULL; ptr++) {
		printf("%s\n", ptr->help);
	}
	return 0;
}

/**
 * parse - tokenize a cmdline into an argv structure; the argc, argv
 * pair follows the semantics of main(int argc, char *argv[]) args
 */ 
void parse(char *cmdline, int *argc, char *argv[])
{
	const char whitespace[] = " \n\t";
	char *word;
	*argc = 0;

	for (word = strtok(cmdline, whitespace); word; word = strtok(NULL, whitespace)) {
		*argv++ = word;
		*argc += 1;
	}
}

const int argnum = 10;
const int argsize = 64;
const int cmdlin_max_len = 256;

int dispatch_command(char *cmdline)
{
	char arg[argnum][argsize];
	int argc;
	char *argv[argnum+1];		/* +1 entry for NULL at the end */
	int i;
	struct fsi_cmds *cmd;
	const int debug_args = 0;

	for (i = 0; i < argnum; i++)
	    argv[i] = arg[i];
	argv[argnum] = 0;

	parse(cmdline, &argc, argv);
	if (argc == 0)			/* ignore blank lines */
		return 0;
	if (debug_args) {
		for (i = 0; i < argc; i++)
		    printf("%d: [%s]\n", i, argv[i]);
	}

	for (cmd = cmds; cmd->cmd != NULL; cmd++) {
		if (strcmp(cmd->cmd, argv[0]) != 0)
			continue;
		if (cmd->function != NULL) {
			if (cmd->function(argc, argv) != 0)
				printf("usage: %s\n", cmd->help);
		} else
			printf("cmd [%s]: not implemented\n\t%s\n", cmd->cmd, cmd->help);
		return -1;
	}
	printf("unknown command %s\n", argv[0]);
	return -1;
}

int tst_main(int argc, char *argv[])
{
	/* TODO: is this necessary? */
	/* emio_init(); */
	int enable_flag, board;
	int32_t mask = 0;
	void *base;

	if (argv[1] == NULL)
		goto usage;

	if (strcmp(argv[1], "on") == 0)
		enable_flag = 1;
	else if (strcmp(argv[1], "off") == 0)
		enable_flag = 0;
	else
		goto usage;

	printf("Enter m_axi_map()\n");
	base = axi_map(M_AXI_BASE);
	// printf("mapped at 0x%p\n", m_axi_base);

	printf("Enter fsi_trig_enable(%d)\n", enable_flag);
	fsi_trig_enable(enable_flag);
	enable_flag = fsi_get_config();
	printf("config register: 0x%08x\n", enable_flag);
	fsi_regs_display();
	mask = fsi_get_present_boards_mask();
	printf("boards mask: present 0x%02x", mask);
	printf(" [");
	for (board = 1; board <= 8; board++, mask >>= 1)
		if (mask & 1)
			printf("%1d", board);
	printf("]\n");
	mask = fsi_get_present_boards_mask();
	for (board = 1; board <= 8; board++) {
		if ((1<<(board-1)) & mask) {
			uint32_t afe = fsi_get_afe(board);
			printf("slot %1d: afe = %02x\n", board, afe);
		}
	}

	test_set_acdc();
	axi_unmap(base);


	return 0;
usage:
	fprintf(stderr, "usage: %s {on|off}\n", argv[0]);
	return 1;
}

int is_blank_line(char *str)
{
	while (isspace(*str))
		str++;
	return *str == 0;
}

int main(int argc, char *argv[])
{
	char cmdline[cmdlin_max_len];
	void *base;
	const char *prompt = "fsi-control> ";
	char *line;

	global_base = base = axi_map(M_AXI_BASE);
	read_history(NULL);
	while ((line = readline(prompt)) != NULL) {
		strncpy(cmdline, line, cmdlin_max_len-1);
		free(line);
		if (!is_blank_line(cmdline))
			add_history(cmdline);
		dispatch_command(cmdline);
	}
	printf("\n");
	write_history(NULL);
	axi_unmap(base);
}
