To build apps for the target, we need to have the correct SDK setup, built from petalinux (ie. with docker)

Once petalinux builds, we need to generate the SDK

```sh
# this requires the petalinux/settings.sh to be sourced as well.
# From inside the petalinux project folder, generate the sdk
> petalinux-config --sdk
# this will take quite a while

# then, generate the sysroot
> petalinux-package --sysroot
# that should allow you to source 
> source <petalinux project folder>/images/linux/sdk/environment-setup-aarch64-xilinx-linux
# or where ever this was generated (this example for my setup)

# now we can test if the settup worked by checking the $CC env var:
> echo $CC
aarch64-xilinx-linux-gcc --sysroot=/home/marqueta/Documents/diot-sb-zu-fsi-sw/sw/petalinux/images/linux/sdk/sysroots/aarch64-xilinx-linux

# build (from this directory)
> make
```

