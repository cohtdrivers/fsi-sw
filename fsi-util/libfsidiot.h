/*
 * Copyright CERN 2023
 * Author: Juan David Gonzalez Cobas <dcobas _at_ cern _dot_ ch>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef _LIBFSIDIOT_H
#define _LIBFSIDIOT_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>

int m_axi_map(int addr);
int m_axi_unmap(void);
void *axi_map(unsigned addr);
void axi_unmap(void *addr);

uint32_t m_axi_readl(uint32_t addr);
void m_axi_writel(uint32_t addr, uint32_t data);

int emio_init(void);
int fsi_init(char *params);
void fsi_reset(void);
void fsi_resync(void);

uint32_t fsi_get_config(void);
uint32_t fsi_get_present_boards_mask(void);
void fsi_trig_enable(unsigned en);
uint64_t fsi_read_clock(void);

uint32_t fsi_get_afe(int board);
int fsi_set_afe(int board, uint8_t amp_mask);
uint8_t fsi_get_acdc(int board);
void fsi_set_acdc(int board, uint8_t coupling);

void fsi_set_boards(uint8_t mask);
void fsi_sel_board(int board);
uint32_t fsi_get_status(void);
void fsi_send_value(uint32_t val);
void fsi_exec_cmd(unsigned cmd);
void fsi_wait_cmd(void);
uint32_t fsi_get_reply(unsigned board);

/*
 * delay control reg
 *	#define SYS_TOP_SUB_REG_DELAY_CTRL 0x9cUL
 *	#define SYS_TOP_SUB_REG_DELAY_CTRL_VTC_EN 0x1UL
 *	#define SYS_TOP_SUB_REG_DELAY_CTRL_FREEZE 0x2UL
 *	#define SYS_TOP_SUB_REG_DELAY_CTRL_INC 0x10UL
 *	#define SYS_TOP_SUB_REG_DELAY_CTRL_DEC 0x20UL
 *
 * Per delay cell current value
 *	#define SYS_TOP_SUB_REG_DELAY_CNTVALUE 0xa0UL
 */
int fsi_delay_control(int board, int lane, int incflag);
void fsi_vtc_enable(int enable_flag);
void fsi_vtc_freeze(int freeze_flag);
/* add getters for these */
int fsi_get_prbs(void);
int fsi_set_prbs(int enable_flag);


void fsi_dma_init(uint32_t nsamp);
uint32_t get_dma_length(void);
void set_dma_length(uint32_t);
void fsi_dma_cache_invalid_with_nsamp(unsigned nsamp);

char *fsi_regs_display(void);
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _LIBFSIDIOT_H */
