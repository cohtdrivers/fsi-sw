select 
    acq.batch, acq.timestamp,
    acq.n, acq.fs, ch.board, ch.channel, ch.label,
    h.index_, h.hint, h.width, 
    pk.mu, pk.sigma, pk.max_
from acquisition acq join channel ch
	on ch.timestamp = acq.timestamp
     join hints h
        on h.channel_id = ch.id
     join peaks pk
        on pk.hint_id = h.id
where ch.timestamp in :timestamp;
