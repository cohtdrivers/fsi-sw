
drop table if exists acquisition;
drop table if exists channel;
drop table if exists hints;
drop table if exists peaks;

create table acquisition (
    id			integer primary key,
    timestamp		varchar(16) unique,
    batch		varchar(20),
    n			integer,
    fs			real,
    temperature		real,
    humidity		real,
    pressure		real,
    n_air           real,
    alpha		real,
    zpf			integer default 1
);

create table channel (
    id			integer primary key,
    board		integer,
    channel		integer,
    diot		integer default 0,
    timestamp		varchar(16)
    			references acquisition(timestamp),
    label		varchar(20),

    -- data, v, l, spec stored elsewhere
    
    unique (timestamp, diot, board, channel)
);

create table hints (
    id			integer primary key,
    hint		real,
    width		real default 0.002,
    channel_id		integer references channel(id),
    index_		integer
);

create table peaks (
    id			integer primary key,
    hint_id		integer references hints(id),
    -- x, y stored elsewhere
    mu			real,
    sigma		real,
    max_		real
);

