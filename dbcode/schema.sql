CREATE TABLE acquisition (
    id			integer primary key,
    timestamp		varchar(16) unique,
    batch		varchar(20),
    n			integer,
    fs			real,
    temperature		real,
    humidity		real,
    pressure		real,
    alpha		real,
    zpf			integer default 1,
    n_air		real);

CREATE TABLE channel (
    id			integer primary key,
    board		integer,
    channel		integer,
    diot		integer default 0,
    timestamp		varchar(16)
    			references acquisition(timestamp)
				on delete cascade
				on update cascade,
    label		varchar(20),

    -- data, v, l, spec stored elsewhere
    
    unique (timestamp, diot, board, channel)
);

CREATE TABLE hints (
    id			integer primary key,
    hint		real,
    width		real default 0.002,
    channel_id		integer references channel(id)
				on delete cascade
				on update cascade,
    index_		integer
);

CREATE TABLE peaks (
    id			integer primary key,
    hint_id		integer references hints(id)
				on delete cascade
				on update cascade,
    -- x, y stored elsewhere
    mu			real,
    sigma		real,
    max_		real
);
