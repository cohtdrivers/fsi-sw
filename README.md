
Installing Requirements:

1] If internet/proxy is available on server:
```
pip install -r requirements.txt

```

2] If no internet is available

On a machine with internet,download all packages in a repository eg:`dependencies`

```
mkdir dependencies

pip download -r requirements.txt -d "./dependencies"

```


Create a tar of all packages and copy to server


```
tar cvfz dependencies.tar.gz dependencies
```


In server:

```
tar zxvf dependencies.tar.gz

cd dependencies

pip install * -f ./ --no-index

```


---------------------------------------------------------------------------------
1] Linearization.py
   - Implementation of Butter Filter, Hilbert Transform, ArcTan Wrap
   - Linear Filter to  be applied to Measured and Gas Cell


2] GasCellfit.py
   - Algorithm devised by David to fit Gas Cell plot with standard SRMCSV plot to find the sweeping speed

3] MeasuredCellFFT.py
   - FFT for Linearized Measured Cell Data
   - Fitting the plot using Lorenztian FIT 
   - Returns final Distance 

4]fsiEnums.py, fsiExceptions.py, fsiInitConfiguartions.py
   - FSI instrument related functions,class, and variables provided by Jarek.

--------------------------------------------------------------------------------
Test script: lib_usage.py 
   - script for using the library for one set of data

--------------------------------------------------------------------------------

Test using data acquired as .dat file:
Run shell script:

```shell
./process.sh -h

usage: ./process.sh -p {File Path} -t {timestamp} -r {Reference Channel} -g {Gas Channel} -m {Meas Channel} -s {show plots}
```
to use example:
```
./process.sh -p /home/mshukla/fsi_data  -t '08:57' -r 3 -g 0 -m 2 -s 'True' 
```
