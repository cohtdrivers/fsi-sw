/**
 * @brief
 * 	This file collects the complete set of error codes returned by
 * 	the different parts of libfsi, with the aim of (one day) emit a
 * 	list of codes, errnos, messages and pertinent error records they
 * 	should be associated with
 */

/* library error codes */
#define	__FSI_ERR_START	-1024
#define	FSI_CFG_BAD_NPEAKS	__FSI_ERR_START-1
#define	FSI_BAD_PEAK_TYPE	__FSI_ERR_START-2
#define	FSI_NOT_IMPLEMENTED	__FSI_ERR_START-3
#define	FSI_ABORTED_COMPUTATION	__FSI_ERR_START-4
#define	FSI_SCAN_SYNC_ERR	__FSI_ERR_START-5
#define	FSI_SERVER_OUT_OF_SYNC	__FSI_ERR_START-6
#define	FSI_BAD_FULL_MODE	__FSI_ERR_START-7
#define	FSI_DIOT_DOWN		__FSI_ERR_START-8

/* error codes for laser API */
#define FSI_LASER_OK		0			/**< success */
#define FSI_LASER_ERR		-2048			/**< reserved */
#define FSI_LASER_CMD_TOO_LONG	FSI_LASER_ERR-1		/**< raw cmd too long */
#define FSI_LASER_WRITE_ERR	FSI_LASER_ERR-2		/**< laser serial write error */
#define FSI_LASER_WRITE_TRUNC	FSI_LASER_ERR-3		/**< laser serial write truncated */
#define FSI_LASER_READ_ERR	FSI_LASER_ERR-4		/**< laser answer read error */
#define FSI_LASER_ANSWER_ERR	FSI_LASER_ERR-5		/**< unterminated laser cli answer */
#define FSI_LASER_INTERNAL_ERR	FSI_LASER_ERR-6		/**< unterminated laser cli answer */
#define FSI_LASER_BAD_PROPERTY	FSI_LASER_ERR-7		/**< unterminated laser cli answer */
#define FSI_LASER_CMD_ERR	FSI_LASER_ERR-8		/**< laser cli cmd unknown */
#define FSI_LASER_ARG_ERR	FSI_LASER_ERR-9		/**< laser cli invalid argument */
#define FSI_LASER_EXE_ERR	FSI_LASER_ERR-10	/**< laser cli cmd execution error */
#define FSI_LASER_ACK		FSI_LASER_ERR-11	/**< laser cli cmd acknowledged */
#define FSI_LASER_BAD_FLOAT	FSI_LASER_ERR-12	/**< failed strtod on cli out */
#define FSI_LASER_BAD_INT	FSI_LASER_ERR-13	/**< failed strtol on cli out */
#define FSI_LASER_BAD_BOOL	FSI_LASER_ERR-14	/**< failed bool conversion on cli out */
#define FSI_LASER_UNKN_FAIL	FSI_LASER_ERR-15	/**< failed conversion on cli out */
#define FSI_LASER_BAD_CONV	FSI_LASER_ERR-16	/**< failed conversion into cli input */

/* EDFA API status codes */
#define FSI_EDFA_OK				0			/* ! answer from serial port */
#define FSI_EDFA_BERR				-3030
#define FSI_EDFA_COMMAND_UNKNOWN		FSI_EDFA_BERR - 3	/* # answer from serial port */
#define FSI_EDFA_NOT_AUTHORIZED			FSI_EDFA_BERR - 4	/* * answer from serial port */
#define FSI_EDFA_COMMAND_NOT_VALID		FSI_EDFA_BERR - 5	/* $ answer from serial port */
#define FSI_EDFA_WRITE_ERR			FSI_EDFA_BERR - 6	/* command too long */
#define FSI_EDFA_WRITE_TRUNC			FSI_EDFA_BERR - 7	/* could not write to serial */
#define FSI_EDFA_READ_ERR			FSI_EDFA_BERR - 8	/* write incomplete */
#define FSI_EDFA_ANSWER_ERR			FSI_EDFA_BERR - 9	/* bad answer */


/* configuration integrity validation errors (gpufsi?) */
#define FSI_CFG_OK				0
#define FSI_CFG_ERRBASE				-4097
#define FSI_CFG_TOO_MANY_GAS_CELLS		FSI_CFG_ERRBASE-1
#define FSI_CFG_TOO_MANY_REFERENCE_CHANNELS	FSI_CFG_ERRBASE-2
